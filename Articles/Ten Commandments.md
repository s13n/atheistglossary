# The ten commandments

Christians often believe that the ten commandments are the origin and objective
foundation of human morals. That's nonsense, of course. I find it rather strange
that thinking people can honestly believe this, particularly when learning about
other, much older legal and moral codices, such as the code of Hammurabi.

For starters, the story in the book of Exodus has the Israelites flee from
Egypt, before they get presented with God's law in the form of the ten
commandments. I find it peculiar how people need to be reminded of the most
basic social rules that they invariably must have encountered while living in
the most advanced civilization of the time. Or do you seriously believe that the
Egyptians didn't know that murder, betrayal, lying etc. were bad? That they
could have erected a whole civilization without knowing? That would strike me as
totally absurd.

So what are the ten commandments trying to achieve?

Note that the commandments come in two different types. The first few are long
and contain some amount of explanation. They concern loyalty to God. The
remaining commandments are short and don't bother with explanations. Indeed, as
I have indicated above, they don't need an explanation because they tell nothing
new. They summarize what everybody already knows.

The first few commandments, however, are new. They are specific to the religion
of the Israelites, what we now call the old covenant. Putting both types of
commandments into the same context is a cunning ruse of the authors of the story
in the bible. They apparently wanted to convey the notion that both types of
commandment belong together, and are equally fundamental and authoritative. In
fact, the order indicates that the first few commandments are more fundamental
even than the ban on murder, theft and betrayal.

And this trick works up to the present day! Most people apparently don't see
through it!

But it is just that: A trick. There is absolutely no reason why loyalty to God
would have to be listed amongst the basic social rules. If the Egyptians could
come up with their rules of law without knowing the Jewish God, as could many
other societies before and after the alleged story in Exodus, then basic morals
can't depend on the Jewish God.

It really is quite simple. When people live together in societies, in greater
numbers, there need to be rules. So they evolve. Societies with good rules are
more successful than ones with bad rules. Killing each other is not normally
very conducive to a successful society, so it becomes a rule not to murder. But
this isn't to be taken as an absolute. There are always some cases where killing
people is being deemed acceptable or even required. And this is for reasons
rooted in society, in the way it is governed.

Witness Moses allegedly having 3000 of his own people killed for worshiping the
golden calf. This act of terror is a means for Moses to secure his power. It
isn't really about justice, or about following the commandments. This becomes
even clearer when you note that Aaron, who made the calf, is spared from death,
and subsequently made a high priest. This is easily explained with power
politics, but not easily with justice or morality.

Or witness the Aztec society in what today is Mexico. Their religious belief was
that the sun only rises each morning when a human gets sacrificed every day. Not
following this ritual would risk everything, because when the sun doesn't rise,
nothing will grow, and everything will remain in darkness. So the high priests
ritually killed people every day, a practice that was regarded as necessary and
totally ethical. Killing someone every day for the sun to rise basically was one
of their divine commandments.

This should make clear how the commandments, and with them the ethical
foundations, are dependent on society, even in the case of the most basic rules
that many people would see as universal, and objectively true. In short: There
is no objective morality. It is all a result of humans cooperating in a society.
