# Violence

Despite the widespread insistence of believers that religion is about peace and
love, it has dawned on a lot of people that religious practice and scripture
both fly in the face of that. They are full of violence. This obvious
discrepancy sparks the age-old question of the relationship between religion and
violence. Is religion a source of violence or an antidote? Is violence endemic
in religion or is religion trying to overcome it?

You may be a Christian apologist and argue that Jesus sacrificed himself in an
act of utter violence in order to overcome it. So of course Christianity is
about overcoming violence, you will say!

But has it worked? Do we observe less violence after his crucifixion than
before? I don't think you can demonstrate that at all! A mere 40 years after his
death the Jewish war ended in a terrible bloodbath right there in the area where
he preached. And some 60 years later it happened again. Indeed, if you look at
Christianity throughout its entire violent history of nearly 2000 years, there
is no sign of a positive effect at all. And the picture is no better with most
other religions.

Right now we have a war in Ukraine where one orthodox Christian country has
invaded another orthodox Christian country, and the patriarch of the attacker's
church openly supports it, justifying its atrocities as a war for religious
traditionalism, against western liberalism. If anything, this shows for the
umpteenth time in history, that religious conviction serves as a justification
for war, and not as an incentive for peace. And invariably, when arguing in
favor of violence, the old testament proves more useful than the new.

But it would be naive to believe that abolishing religion would result in less
violence. Violence is inherent in human psychology, and religion merely deals
with that. It uses it, criticizes it, promotes it, tries to explain it, tries to
moderate it, tries to control and direct it, and tries to console the victims.
The relationship is ambivalent. By ignoring one side you can just as easily make
a case against religion as you can make one for it when you ignore the other.

For most of religion's history people lived in an utterly violent environment.
Nature itself was already violent in the pain and agony it regularly inflicted
on people through illness, hunger, childbirth and accidents. Other humans added
to it with war, rape, theft and oppression. If you live in such an environment,
I guess you need some mental framework to make sense of it. Even an inherently
violent and strictly hierarchical world view, that puts a temperamental and
envious deity at the top, ends up providing some security and peace of mind,
compared to a totally chaotic and incomprehensible struggle for survival.

In fact, IMHO a deity or deities that are so capricious can serve as a better
explanation for a somewhat chaotic and unjust world, than a perfectly good and
benevolent God does. After all, how can you expect things to be better on earth
than in heaven? If even Zeus betrays his wife Hera, then adultery amongst humans
looks more normal. It opens space for an amount of tolerance that you may not
otherwise have. If even Gods prank each other, you might more easily endure a
trick that's being played on you mere mortal. Ideally, this sort of world view
might make you more relaxed about hardship and mishaps. I believe that's what
happened to the Greek, due to their mythology.

The bible tries something like this in the book of Job. In it, Job is submitted
to all sorts of unjust treatment by an asshole God (or his stooge, Satan), in
order to test him. The pedagogic intent seems to be to reassure people that even
though it might not look like it, God will eventually reward loyalty. But - more
importantly in my opinion - it shows a God that doesn't even try to be fair. It
tells us to not expect fair treatment by God. It shows a God that you are not
supposed to criticize, no matter what he does to you. He requires total and
unquestioning submission. It reminds you that you are a nobody who depends 100%
on God, and he doesn't owe you anything at all.

While this may serve a similar purpose for you as the Greek mythology, in that
it explains and motivates the chaos and violence in your life, I like the Greek
version much better, because it leaves human autonomy intact, instead of
establishing such a strict hierarchy that leaves no worth for you anymore, and
leaves you totally at the whim of an asshole.

Violence is best combated with security. People who feel safe tend to be more
tolerant and accepting of others, more peaceful even in the face of unfairness,
and are less needy of a big brother in the sky. This is why we have societies
with its rules and institutions. You don't need a gun when you have a working
system of justice, and as a result, fewer people get killed. If you feel unsafe,
don't buy a gun, build a better society.
