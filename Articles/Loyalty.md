# Loyalty

How does loyalty ring to you as an ethical concept? Is it a predominantly
positive thing, or a negative one? Or is it an ambiguous term that invokes both
kinds of associations in you? What's your attitude towards loyalty?

In particular, how do you see loyalty against truth or honesty? Would you lie
for loyalty, or rather be illoyal to tell the truth? In a pinch, which is more
important?

My own stance tends to give truth priority. That doesn't preclude the occasional
lie, and I'm certainly not going to suggest that I'm perfect. But although I
consider myself a loyal person overall, I do associate some amount of unease
with it. This is because loyalty can very readily be abused, and frequently is
in practice. And I simply don't want to be abused, or made a tool for some
activity I'm not OK with. So I would hesitate calling loyalty a virtue.

The function of loyalty is to give security to one's social environment. A good
life happens in a social environment that doesn't just make yourself
comfortable, but those around you as well. For that you need to be able to trust
each other, that people aren't going to take unfair advantage of you.

You can regard that as a form of mutual loyalty, and as such loyalty is a way of
mutual reassurance and comfort. But if you think about it, you are not actually
loyal to each other, you are loyal to a shared principle, namely the principle
to be fair to each other. Your behavior is just a manifestation of this loyalty
principle.

In fact, when individuals command loyalty, they very often demand something
they're not prepared to reciprocate. Such a personal loyalty quickly becomes
one-sided. This extends to the big picture, i.e. kings and rulers expect and
command the loyalty of their underlings, but what do they pay back with? Are you
obliged to be loyal to them? Or do you enter into a deal here, and if so is it a
fair deal?

The old testament codifies this deal idea in the form of the "old covenant". The
covenant is described as a deal between God and his chosen people, which compels
the people to loyalty towards God, and it compels God to provide for and protect
his chosen people against its enemies. A lot of kingdoms operated on the same
underlying assumption, namely that the people owe loyalty to the king in exchage
for the king providing them with protection. In a way this is a hybrid between
personal loyalty to the king as a person, and institutional loyalty to the
nation you are part of, and which is represented by the king as a ruler.

History has shown that personal loyalty to a ruler is problematic, and the
representation of a nation by a single king is fraught with perils of misrule or
the death of the king. In particular, what happens when the king can't, or
doesn't, fulfill his part of the deal? Are you still obliged to be loyal despite
not having any advantage from it anymore? Do you have to endure hardship because
of a deal that the other side has abandoned? The Romans had this problem when
being ruled by irresponsible emperors such as Caligula or Nero, which undermined
loyalty to them, and thereby destabilized the nation. Fixing the problem
frequently involved killing the ruler, or forcefully deposing him in some other
way. But whose job is that? Would you want to leave that to a makeshift cabal of
disgruntled members of the second rank of power? Isn't this prone to result in
the next irresponsible king?

This is the inherent danger of having power concentrated in a single person. You
have very little defense against that person violating the loyalty deal for
personal gain, or even due to personality defects. The problem led to the
invention of democracy, but at the basis of it is the loyalty problem. If you
are loyal to the nation you are a part of, i.e to a community, albeit a very
large one, then the person of the ruler shouldn't come into it. Losing or
deposing the ruler then doesn't touch the loyalty deal. The nation persists, and
you continue to owe loyalty to it.

The ruler then effectively and explicitly becomes a servant of the nation, and
not its owner. The ruler himself now owes loyalty to the nation, just like you
do. The next realization is that if the ruler violates his obligations, he
should be subject to personal repercussions in the same way you are. At the very
least you ought to be able to get rid of him without having to kill him or wait
for his natural demise. The nation elects the ruler, and can depose him again by
electing another one. And the ruler is subject to the nation's laws like
everyone else. This is in marked difference to a king who can neither be
deposed, nor is subject to the same law as his tributaries.

As a democratic citizen you owe loyalty to the nation, more specifically to its
basic principles laid down in the constitution. You don't owe loyalty to the
ruler, on the contrary the ruler owes loyalty to you, as a member of the nation.
When you owe loyalty to a principle, the principle can't betray you. You can
only be betrayed by others who abuse or disregard the principle. And as long as
the fabric of your democracy is intact, you can retaliate by replacing the
offending ruler with another one.
