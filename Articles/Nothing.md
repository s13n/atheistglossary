# Nothing

Something can't come from nothing, this is what many Christians know for sure.
Except, of course, God. There's always a special rule for God! After all there
must be some benefit in the supernatural, otherwise it would be superfluous.

They don't, however, bother with explaining what they mean with "nothing". It
turns out that nothing is one of those things that seem easy until you think
about them. Note how a contradiction has sneaked into my last sentence: I called
nothing a thing! See how quickly this topic turns into nonsense?

So let me tell you this: Nothing beats nothing, when it comes to difficulty of
explaining it properly. Even time, another notoriously difficult thing to
explain, is easier. Maybe that is because at least, time is a thing. Or is it?

It must be a thing, right? Otherwise it would be nothing, and time is certainly
not nothing! If it is a thing, then presumably something can come from it. This
raises the question if time itself can be regarded as a cause. Comes time, comes
wheel, so the saying goes. So time causes wheel, right?

OK, perhaps I should be a bit more serious.

If you think about it for a while, you realize that there are different kinds of
nothing. If you say that there's nothing in your stomach, you don't mean a
vacuum. There's still something in it, but it doesn't make you feel full. When
you say that there's nothing in your bank account, you're not talking about
anything material, you are talking about a mere number. But the number means
something; it has a value, and it can certainly serve as a cause, or at least as
a prerequisite.

That's two different kinds of nothing already. Some thinkers have tried to find
a catalog of different kinds of nothing. Sabine Hossenfelder, a German
physicist, made a Youtube video about it, in which she presents 9 different
kinds of nothing, or "levels", as she calls it. Nine!

Among those nine levels, there are some that can't plausibly serve as a cause
for anything. For those, the saying applies that I opened this topic with. But
for the other levels, it doesn't apply! Which means that something can indeed
come from nothing! If you choose the right kind of nothing, you can even make
entire universes! See Lawrence Krauss's book "A Universe from Nothing".

All of a sudden, nothing and everything seem rather close to each other!

My hope would be, if you permit, that next time you apply such common sense
notions like "nothing", or what can and can't come from it, you pause for a
moment and think about what you actually mean, and how you can be so sure about
it. Things are more complicated than most people think, and it seems that things
like universes can come into existence in wholly unexpected ways.

I stop here, lest I get accused of much ado about nothing.
