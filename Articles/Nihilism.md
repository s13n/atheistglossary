# Nihilism

Nihilism is often being used as a kind of slur in debates with atheists, as if
it was something inherently bad, and no justification needs to be given for
that. The accusation of nihilism is treated as an argument by itself. But what's
so bad about it? Is there any evidence that nihilism makes bad people? That it
corrupts morals? Are nihilists criminals?

Interestingly, there was a time when atheism was used in the same way, as a slur
that explained itself and didn't need any justification. And some people still
treat the two isms as basically the same. But of course, once you look closer
the picture is a lot more diverse. Not only are there different variants of
nihilism, there are also quite a few tenets of nihilism that aren't that easy to
dismiss. I am a nihilist of sorts, but not in as stupid and ignorant a way you
might like to think. Let me explain.

A nihilist typically asserts that there is no inherent morality or meaning or
purpose in nature, and I would concur with that. But for me, that doesn't mean
that we humans can't create those. For me, morality, purpose or meaning are
concepts rooted in human culture, and not in nature as such. They are not there,
they are made. If humans wouldn't exist, they wouldn't exist, either.

Now, people who use nihilism as a slur seem to imply that people like me do not
only dismiss the pre-existence of those concepts, they also deny the
desirability of them. That they advocate a world without those concepts. That's
not true. It is one instance of the very common conflation of "is" vs. "ought".
For me, the realization that those concepts are not just there, but must be
actively erected, is not a call for, or permission of, depravity, but a call for
responsibility. The realization is a kind of liberation, in that I'm not
condemned to fulfill a pre-existing and rigid ethical framework, but that I am
empowered to participate in its construction. Nihilism, in this sense, is a call
to action and an assignment of responsibility. It enables progress.

I can't quite understand why such a stance would be regarded as depressing,
hopeless and without perspective. A state of mind that you need to overcome to
be able to lead a satisfying life. Isn't it the opposite? Life is an open field
that waits for you to cultivate it!

It seems to me that there are essentially two different expectations of life.
One is that life is preordained and your job is to follow the rails that were
laid for you. If the rails are missing you are lost and don't know what to do.
Your life has no purpose and you're depressed. The other is that life is an open
field and your job is to explore and to cultivate it. Preordained stuff like
rails would only serve to restrict and shackle you. It is the restrictions that
would make you depressed, not the lack of rails. The former's thinking is more
concerned with risks, the latter's with possibilities.

I think that those who are more concerned with risks are influenced by their own
wish for security, in that they subconsciously project it onto the world around
them. They would much rather have a world that features sense already, instead
of a blank sheet that requires them to make sense by (and for) themselves. I
also observe that those people seem to have a very low confidence in their
ability to come up with good sense. Their fear seems to be that everybody who is
given the opportunity to create his own sense, would invariably act out his
egoism, and shape it to put himself in the best possible position at the expense
of everybody else. I guess that casts a very unfavorable light on their own
character. It looks like that's what they would do, given half a chance.

My experience is different. I see people acting morally, and strive for the
common good rather than their own advantage, when educated properly, and being
trusted, rather than being suspected a priori of being incapable of acting in
the interest of the common good. Not everybody, but many people are like that.
It deserves to be fostered. For that, you don't tell people what some deity
allegedly wants from them, but you challenge them to come up with ideas how they
would want things to be arranged. And they will come up with ideas of fairness,
benevolence, proportionality, freedom of speech and such concepts, all by
themselves, without needing any holy scriptures to direct them. We evolved to be
social animals, and in things like this, it shows.

You see that reducing my nihilism to pure self-interest would be missing the
point. Neither is my nihilism an absolute, radical nihilism of everything. I am
nihilistic regarding the universe, the nature around us. I am not nihilistic
regarding humanity and society. This extends to teleology. The universe isn't
teleological. Teleology is a genuinely human concept, an anthropocentric idea.
Whenever we see something that looks like an inherent purpose, or an inherent
will, we are projecting. Our own thought is teleologic, the universe or the
nature around us isn't.
