# Marriage

In a patriarchal society such as ancient Israel or Judah, it is the fathers who
arrange a marriage. It was a deal in which those most affected had comparatively
little say. Indeed, the idea of marriage out of love is comparatively young, and
most certainly not originally Christian. It emerged in Europe with romanticism,
and that is less than three centuries ago. Arguably, we still grapple with the
consequences of this shift, as religious values are still very much in play. In
my view, we haven't yet reached a coherent notion of what marriage is for, and
what we can reasonably expect from it. The romantic notion often prevents a
sober assessment of one's motives and expectations until it is too late.

The first point of contention for me has long been the tension between a
personal, emotional motive, and a social, formal and regulated relationship. It
looks like a trap: You are being lured by emotion, and before you regain your
senses, you are trapped in a web of rules and roles which you can only unweave
at great cost and effort, if at all. The Christian (in my case Catholic) rules
are the worst: You can't properly check what you get yourself into beforehand,
and are not allowed to get out, at least when you take the rules seriously. It
looks like specifically crafted to put you at a disadvantage. No wonder many
catholics I know "cheat". They cheat before marriage and in marriage, and they
get divorced even though their religion officially disallows it. And I support
that. Cheating a rigged system is OK.

Cheating people, however, isn't. But what does that mean? It means to be fair to
people. And if you are married, you arguably should be particularly fair to your
spouse, and the rest of the family. Note that I don't necessarily mean sexual
faithfulness here. In my opinion, it is a matter of mutual agreement what
fairness here means. It may mean to not lie about it. It is nobody else's
business to lay down the rules here, it is between the couple. This is both a
liberty and a responsibility. The hard part is to be conscious, explicit and
honest about one's expectations. And for that very reason I think that no church
and no state has any business interfering here.

That raises the question what marriage, as an official act with legal
consequences, should be for, and what should the rules be? Or should it be
abolished altogether? Let me offer my own opinion for debate:

First of all, forget about love. Marriage should have nothing to do with it. If
you think marriage is a promise of eternal love, or is there to give you a
security to be loved, you delude yourself. Examples of long marriages that seem
to preserve love successfully are no reason to expect that from all marriages.
It is much more reasonable to allow for love to dissipate over time, because it
most likely will anyway. If your expectations aren't unreasonable, you may well
be able to carry on living together without hating each other. Falling in love
with someone else may not even interfere with that. Also read the article on
love, for an exposition of different types of love.

With that out of the way, marriage is useful when the rules are reasonable.
Unfortunately, the current state leaves something to be desired, but that of
course depends on the society you're in. The rules laid down by the state should
be dictated by the interest the state has, and can legitimately have, in
marriage. For example, the state should have an interest in people committing to
care for each other, for children and their education, for frail parents, and
perhaps other members of the family that need care. This regards marriage as the
smallest cell of cooperation in society that fulfills a role that the state
would have a lot more difficulty fulfilling. But this has nothing to do with sex
or gender. For the role of marriage the state ought to have an interest in, the
sexual orientation and preferences should not have any bearing at all.

Seen from this angle, marriage is a contract, a mutual promise. If the state
wants a stake in that, and I think it should, then it would be its duty to come
up with a contract text that is fair to all sides, and flexible enough to cover
as many real life situations and preferences as practical. Here are a few
criteria that I would find important:

Reasonable rules for canceling the contract if the situation has changed
sufficiently to make it pointless or insufferable. And canceling should be
possible without costing an arm and a leg for legal fees. The nature of marriage
includes its being dependent on circumstances. I'm not advocating divorce on a
whim, but my own experience with people divorcing point to the opposite being
the bigger problem: People stay together way too long, and divorce when they
genuinely hate each other and are unable to agree on anything. Nobody can
convince me that children are better off with warring parents than with divorced
parents.

Sexual promiscuity or fidelity or practice shouldn't be relevant for the state.
It is the exclusive matter of the couple to lay down the rules, and the state
should stay out of it. If those rules are violated, that can be a reason for
divorce, but it isn't the role of the state to prescribe the rules here.

The function of such a contract is to protect the rights and interests of all
those involved (what we today would call "stakeholders") in a balanced way. The
stakeholders are not the church or any religious entity. It is not primarily the
state, either. It is those who care, and those who are to be cared for.
Balancing those requires dismissing all ideological restrictions and focus on
the people. I couldn't care less about what sacred scriptures have to say here.

This is very far from what ancient Jewish or Christian marriage is like. Maybe
it shouldn't even be called marriage to make that totally clear.
