# Meeting Jesus

Many people crave for Jesus' second coming. But would they recognize him when it
happens? How would they react?

To be clear, I don't believe one second that it will happen. Jesus is dead for
nearly 2000 years now, and that's as final as it is for everybody else. Even for
people who take seriously that Jesus promised his second coming, it looks
somewhat embarrassing that they still expect something that was supposed to
happen during the lifetime of Jesus' contemporaries. The excuses people have
invented to explain this apparent delay are legend. I think it is high time
people acknowledged reality: It hasn't happened and isn't going to happen!

People regularly believe that the second coming must be imminent because the
times are so bad. Apparently, the times are bad enough for the second coming
every few decades, only to improve again without the second coming having
happened. But people don't learn from that, and perpetually fall for the same
failed predictions. Apocalyptic thinking is as ridiculous as it is unassailable.
The most worrying aspect of it is that some people seem to feel compelled to
make matters worse deliberately, in order to enforce the second coming. We all
need to be on the lookout for such ideologies, because they are very dangerous.

But lets imagine it would really happen, and Jesus reappears on the scene. What
would it be like? Naive songs have been written about that, but the main
reference for people who crave for the second coming is the last book of the
bible, John's revelation.

Now, normal people who read this book invariably wonder what John was smoking
when he wrote it. The thought is absurd that what it describes is going to
happen literally. It describes a bombastic and complicated ceremonial process
with a strong bent towards numerology, in which heaven and earth are completely
replaced by new exemplars. One wonders what this mad and overblown gush has to
do with the Jesus portrayed in the gospels. Apparently, a frustrated old man had
hoped in vain for Jesus' reappearance, and before his death converts his
disappointment into a violent fantasy of a global revolution and a new kingdom
under Jesus. Nowadays, this sort of performance would be a case for
psychotherapy.

If Jesus would stick to his own prior performance, he would appear as an
ordinary human being, one would expect. And this raises the question of how to
recognize him, should he appear in front of us. I imagine a dude of around 30
years appearing in front of me, saying to me: "I am the way and the truth and
the life." I would of course immediately recognize the quote, but would I think
Jesus is standing in front of me, quoting himself? Would you?

I think I know how I would react. I'd be unimpressed. A young dude half my age
talking to me in this pompous manner would evoke a reaction similar to an
indian-sounding phone caller pretending to call from Microsoft, in order to
alert me to a problem with my computer. Depending on my mood, I would either
tell him to fuck right off, or I would tell him politely to live and learn for a
while longer, and perhaps call back in a couple of decades, if he still thinks
by then that he's got something important to say.

But maybe he performs a miracle or two to convince me of his divinity? The
alleged caller from Microsoft is also prepared to perform such a miracle, namely
he pretends to know the license number of my computer, and goes on to direct me
to find it in my computer. Surely, that proves he's from Microsoft, right? Well,
the miracle seems compelling for a sufficient number of people to make this scam
successful.

We have a simple way for people to prove their identity, and that's
an ID card or passport, but of course in matters of divinity or matters of
telephone support/crime this doesn't work, and another reliable method of
identification must be found. The problem is, I have difficulties imagining such
a dependable identification method for deities masquerading as humans.

So before the second coming, it might be best to come up with a generally
accepted identification method for deities. Maybe an ISO standard would be best.
Otherwise I might miss it, or I might mistake Jesus for one of the many
religious lunatics who roam our streets.

It seems I have strayed into sarcasm a bit. Oh well!
