# "You just want to sin"

And you just want to insult me, right?

Seriously, how can anyone accuse an atheist of "just wanting to sin" with a
straight face? It has to be a deliberate insult, or else I would have to
question your mental faculties. If you thought about it even for a second, it
would be obvious that the accusation completely misses the point!

Firstly, thanks very much for apparently suggesting that my ethics are so
corrupt that I would thoughtlessly lie about my motives for being an atheist,
that sinning - or the kick of breaking the rules - was the driving force behind
it, and that simultaneously I'm stupid enough to believe that becoming an
atheist would actually help me with that. I note your slanderous intent. Now
imagine a rude reply from me..... no! A lot ruder than that!

Secondly, believe it or not, I am actually trying to do what's right and good
and helpful and appropriate. I try to be a good person! I just happen to have
come to the conclusion that this idiotic sin idea is totally unhelpful. See my
article on Sin. The best you could say about it, is that it can serve as an
educational bad example for ethics.

Sin is not on my mind at all in my life. When I do something or refrain from
doing something, I don't spend even a split second with the question whether it
is sinful or not. I think about whether it is useful or not. Useful not just for
me, but useful in general. Or harmful, to look at it from the other side. I
think Kant's categorical imperative is quite helpful here: "Act only according
to that maxim whereby you can at the same time will that it should become a
universal law." In other words, don't follow your self interest, but the
universal good. Who needs religions and Gods when you can have Kant?

This means that I'm trying to be good even though there is no threat of
retribution when I don't. I know that I could be bad without threat of hellfire,
and I'm still trying to be good. Because I want to. Because I want others to do
the same. Because I enjoy a social environment where people try to be good. This
provides enough reason for me.

But if you think you need the threat of hellfire to be good, then may I suggest
that you sit down and have a few hard thoughts about your own stance, please? If
the threat of hellfire is the only thing that stands between you being nice and
you being an asshole, then you've got the ethics of a dog. You can train a dog
how to behave by spanking, no intelligence needed. But I wouldn't trust anyone
with such an ethic makeup. I would fear that the asshole would always hide
underneath a very thin veneer, ready to break through whenever desire overtakes
fear. You aren't doing what you want to, you're doing what you have to. That's
perpetual repression of desires. It is unlikely to work.

And while you are at it, also think about whether you might be projecting your
own wishes on me. Do you think I want to sin because you want to sin, and regret
that you're not allowed? If that should be so, I recommend you stay a Christian,
because there, you can be saved while still being a sinner. If I believed what
you believe, I would do the same. Leaving the faith only makes sense when you
don't want to sin, when you want to do good and be good for the sake of it.

If you actually want to be good, if you enjoy the company of people wanting to
do good, I posit that you don't need the concept of sin, let alone hellfire. And
both of them will wither away and disappear from your mind eventually.
