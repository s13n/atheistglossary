# Evidence

Scientists are often portrayed as those people who follow the evidence in their
quest for truth, as opposed to believers who stick to their belief no matter
what. This is often used in apologetic battles between atheists and theists, and
the expectation is that it speaks for atheists. But no matter who uses the
argument, and on which side of the debate, it is too simplistic to hold water.

I was recently watching a recording of a debate between two eminent elderly
scientists, one atheist and the other theist, about what science can explain,
and where the limits of it are. I won't say who it was, because that's beside my
point. The atheist gave a rather unflattering display of his apparently dogmatic
stance, saying openly that no evidence would convince him of the existence of a
God, and he'd probably rather go mad than believe it. Needless to say that some
Christian apologists latched onto it as proof that scientists weren't following
their own rules they hypocritically expect believers to obey.

I'm not sure the old man fully understood what he had done there, but I have
every reason to assume he was sincere, and certainly not hypocritical. Neither
do I think his stance is unscientific. I rather think he is being misunderstood
and weaponized in an ideological battle that is fought with much cruder
instruments than what he is used to. I more or less share his opinion, and could
have said something similar, except that I would have been careful to phrase it
in a way that is less prone to abuse. So let me try here.

The point I'm trying to make hinges on the difference between evidence and
proof. While a proof, in its strictest sense, leaves no doubt when successful,
evidence only ever shifts plausibility. Having enough evidence means to have so
much more reason to believe something over its opposite, that it appears silly
not to. There's no definite rule where this line is to be drawn, but it is clear
that there will be some evidence on either side, just not of the same weight.
Sometimes a single piece of evidence is strong enough to shift the balance
decisively to one side, but in many cases it is many pieces of evidence coming
together, and only taken together they manage to justify a confident decision.

Think of a court case trying to prove a murder. Usually, a multitude of evidence
is collected that needs to be sighted and evaluated in order to build a picture
of what has most likely happened. You can only convict the accused when that
picture is so conclusive that the only reasonable explanation is that the
accused is the murderer, and other alternatives can be confidently dismissed.

Science works in similar ways. It doesn't usually rely on single pieces of
evidence, but on a jigsaw puzzle of evidence that taken together approaches a
clear picture. There are usually pieces missing, and pieces not quite fitting
properly, but as long as they are few and far between, confidence is not
significantly impaired, and the scientific community by and large accepts the
underlying theory, i.e. the prevalent explanation.

When the atheist scientist stated that there was no evidence that could convince
him, he meant a single piece of evidence, and couldn't imagine one that would
have been enough to tip the balance for him. This is merely another way of
saying that the evidence for his own stance was so overwhelming, that his
imagination failed to produce an example of counterevidence that would have a
chance of undoing that. That the kinds of evidence that were overwhelming enough
to achieve this, would have to be so extraordinary, that they'd cast him into a
kind of cognitive dissonance, something he wouldn't be mentally able to process.

I have been atheist for over 40 years, and the evidence for my conviction is
similarly overwhelming, so by now I would also guess that a single piece of
evidence strong enough to convince me otherwise, would instead throw me off the
rails. It would be much more likely that a whole series of evidence from
different scenarios and topics, occurring over a period of time, would
eventually be able to tip the balance for me. It would have to make sense to me
in such a way that I can integrate my previous knowledge, rather than having to
dismiss it, which means that at the very least I would have to understand why I
had been wrong before. What I know can't just be undone, it has to be given a
new sense in an expanded understanding.

To use an example: Not only have I learned that the earth is approximately a
sphere, I have also understood why it has to be. And I have collected numerous
pieces of evidence confirming it. If you wanted to convince me now of a flat
earth, you would have to present numerous plausible explanations why my
understanding and those pieces of evidence do in fact not point to a spherical
earth. To point to an alleged conspiracy won't cut the mustard.

In short, a lifelong experience sets the bar extremely high for anybody who
wants to turn me around. I appreciate that this also applies to theists. This
doesn't amount to hypocrisy, and doesn't subtract from the notion that science
is evidence-based.
