# The Fall

When Christians talk about "The Fall", they invariably mean the story in Genesis
3 where Eve gets persuaded by the serpent to eat the forbidden fruit, and has
Adam eat from it, too. This precipitates their exit from paradise, and their
eventual death. As they are the ancestors of all humanity, they pass on this
"original sin" to all their offspring, which is why we are all in need of
salvation, even though we weren't personally involved in this transgression.

In my opinion, the story is quite interesting, beautiful and deep, but Christian
theology badly maims it in its misanthropist interpretation. Let me offer my own
view:

I see the story as highly metaphorical, it almost goes without saying. Nothing
in the story is "real", everything is a metaphor for something. There never was
a literal garden, a literal God, a literal Adam and Eve, a literal tree or a
literal serpent. The story is about humans becoming humans, i.e. acquiring their
human traits, and the consequences thereof.

When Adam and Eve stroll around in the garden, naked and unaware of it, they
don't need to take care of anything, to worry about anything. When God makes a
helper for Adam, it isn't clear what he would need help for, since he hasn't got
anything to do. The only task he has is to name the animals.

This is the state of human beings on the verge of becoming conscious. It reminds
me of dogs strolling around. There's hardly anything that would set Adam and Eve
apart from dogs, except the task of giving names to animals. Like dogs, they are
unaware of their own mortality, and they don't need to plan for the future. They
live from moment to moment, and find everything they need in their environment.

The "tree of the knowledge of good and evil" symbolizes consciousness. Eating
from its fruits means to become conscious. The serpent is right: It opens their
eyes, they become aware of their nakedness, and - more importantly - also of
their mortality. Hence, when God warned them that they will die, he was right in
a figurative sense.

Becoming conscious also means to become responsible. This is the flipside of the
human condition, and this is what the ejection from paradise symbolizes. What
Christians call "The Fall" therefore symbolizes humans separating from animals,
and acquiring the traits and abilities that make them human. The depth of the
story is that it shows the negative side of this, too.

In spite of all the problems that consciousness causes for humans, this isn't a
story about a "Fall"! It is the story of a rise, the rise of humanity above the
state of animals, the rise to dominance! It presents Eve as the first human,
because it is her who makes the first conscious move, who drives the action. She
drags along Adam, who presumably would have been happy to carry on living
carelessly like a dog. She is also the first scientist, who decides to test her
hypothesis with an experiment. She personifies the modern human. She's my hero
in this story.

Once you become conscious, there's no way back. This is why there's no way back
into paradise, at least not during our lifetime. But I wouldn't want it any
other way. I want to be human, even if that means to worry and fear, to be
responsible and to fail, to work and fight. A careless life like a strolling dog
in paradise would be no option for me. Particularly since we now have a very
comfortable life, with all the help science has provided us, compared to the
time when humans separated from the animals, when people usually died early, and
often died a painful death, without any idea what is happening to them, and why.

That Christians manage to abuse this beautiful story to motivate both
scapegoating Eve (and by extension subduing women), and gaslighting everybody
into a sense of inescapable guilt that requires divine salvation, angers me. In
my view, this adds insult to injury, for the purpose of keeping people in a
psychological bind that can be exploited for good and bad ends.
