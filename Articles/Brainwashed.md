# Brainwashed

Why is it that Christians mention hell so quickly when they actually think
theirs is a religion of love? Isn't this revealing? A kind of "Freudian slip"?

I have been thinking this for a long time: The real reason why people still
carry on believing in Christianity is fear of hell. Nothing else really
convinces them, but hell scares them into believing Christianity is all just
dandy.

Now, that's an exceptionally miserable reason for believing anything, but it
seems when you fear that mere thoughts can already end you in hell, you
effectively stop thinking. Too dangerous. Instead of thinking, you stick to the
railings that were provided to you by your religious thought minders and avoid
any thought that could endanger your afterlife prospects. Even looking beyond
the railings feels like looking into an abyss.

There's another way how this manifests itself in conversations between believers
and unbelievers. Maybe you've noticed, too. I mean how the believer seems unable
to even hypothetically entertain a thought that his interlocutor puts in front
of him. For example the simple thought that there is no God. Most normal people
would have no difficulty playing with this thought in a hypothetical, what-if
way. But the believer comes back with replies that make clear that he hasn't
actually followed through on the hypothesis at all.

Such is the case when they accuse the atheist of wanting to sin. If you are
capable of hypothetical thought, you can put yourself into an atheist's
position, and if you do that, you realize at once, that the concept of sin
doesn't work with the atheist. If you're an intelligent person who finds the
concept of sin important, and you genuinely want to understand how on earth
atheists think they can get by without it, you won't act as if the atheist
clandestinely believed in sin, and just lied about it. You would try to find out
what the atheist thinks he has in his arsenal to keep wrongdoing at bay.

But the capability of hypothetical thought seems to be what Christopher Hitchens
called the "ironic mind". Religious dogma seems to kill that. Some people may be
genuinely unable to think hypothetically from the start, but I believe that most
would be capable if they allowed themselves the fun of irony. This is perhaps
what I resent most in fundamentalist religion: The shackling of minds, so that
they can only entertain literal thoughts, and are threatened by irony, wit,
hypothetical thought and playful questioning of rules and taboos.

Or, in short, the brainwashing.

If you are a believer, test yourself: Can you hypothetically work through
atheist thought in all its consequences, without feeling odd at every corner,
uncomfortable, as if in danger? Can you hypothetically remove God, Jesus, sin,
hell, salvation and all that from occupying your thought, and not feel your
knees wobble? Could you play the role of an atheist on a theater stage in a way
that would look authentic and not overblown? If you can't, let me submit to you
that you are likely brainwashed.

Just to forestall a knee-jerk reaction: Many atheists I know could put
themselves into the role of a believer, because they have been there. They would
merely play their former self. If they have psychological difficulties, it is
because of the damage that has been inflicted on them while they were religious.
