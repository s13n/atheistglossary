# Suicide

Has Jesus effectively committed suicide, by arranging for himself to be
crucified? Is it suicide when others do the killing, but you arrange the
situation so that you will end up dead? Aren't you just using others as tools in
such a case? What if it really were suicide? Would that somehow diminish it? Why
indeed is suicide bad, or rather, when is it bad?

Of Jesus you would maybe say that suicide as a sacrifice for the benefit of
others is good and commendable. If you can save someone else that way, giving
your life away for it is noble. This also seems to be the idea behind people
sacrificing themselves when in service in the military or police or other
security services. The image of someone throwing himself in the path of a bullet
or grenade so that someone else can live, is this not the ultimate demonstration
of love?

But then, would it be noble to kill yourself in order to cause a handsome life
insurance payout for the benefit of your family? Say you find yourself unable to
provide for your family to a satisfactory extent in the usual way, so you resort
to such an alternative. It would be the ultimate sacrifice again, wouldn't it?
And it would be out of love, right?

Most people would probably instinctively object. They'd say that no, it isn't
loving or noble to bugger off in such a way! They would say that it would be
much better to stay around and try to work with the others in the family to find
a way out of the quagmire. The loving thing to do would be to stay with the
people who love you, and whom you love.

But then, why doesn't the same apply to Jesus? Isn't it curious that when Jesus
has himself killed, it is the utmost loving act, and when you are doing it, it
is an objectionable act that violates the love others have for you? It isn't
really an escape, either, to distinguish between you and someone else doing the
killing. If you arrange the situation, it is you who is responsible. Like Jesus,
because he arranged it, didn't he?

I can't give you a clear way out of this. What I can say is this: I think you
have a right to end your own life, but you also have the obligation to not drag
others into it. Don't use others as tools to implement what you are too afraid
to implement yourself. You might make them feel guilty without them really being
responsible. It is only natural that when others witness you trying to take your
life, that they will try to prevent you from doing it. The very reasonable
assumption is that you're doing it out of a whim, that at the moment you don't
see the way out, but that there really would be a way out and you would be happy
to live if you only saw it. They would want to prevent you from making a
mistake. You would, too, in their situation, right?

To kill yourself, you have to take on some responsibility. You have to think
about what it means for others. You have no right to make the life of someone
miserable when pursuing your own decision. What have they done to deserve it?
What have the train drivers and the rescue workers done wrong to be forced to
scrape your splattered remains from the front of a locomotive? Is that a job
you'd want to do for some anonymous other? What have your parents done wrong to
deserve having to grapple with the loss for the rest of their lives?

You see, killing oneself responsibly isn't easy. You needn't be too concerned
about whether it lands you in hell, that's the wrong concern. If you are
determined to kill yourself, be aware that it will be the last thing that you
can be responsible for, in other words that you can perform in a responsible
way. If you go, this last act should be exemplary. There will be no opportunity
to improve on it anymore. It will be the last impression you made. It follows
that it isn't an escape from responsibility, but a litmus test for your
responsibility. If you think you can't leave the world a better place by living,
you should strive to leave it a better place by dying, i.e. by dying in the
right way.

I am compelled to think about this whenever a suicide attacker makes news. The
scariest thought is that these persons actually, honestly thought that they
would leave the world a better place by killing a few people completely at
random. I find this a staggering thought. Some sort of religion, I suspect, is
needed for people to be this much deluded.
