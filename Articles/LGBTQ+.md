# LGBTQ+

This topic makes me cringe. I know this is going to annoy some people, and the
best I can hope is that it annoys people equally on both sides of the
ideological war.

The initialism started out as LGB and then LGBT before acquiring its
contemporary form. Some even extend it to LGBTQIA+ and other variants, at which
point the disaster comes into full view.

I cringe because many people don't seem to get the self-defeating irony in this
initialism. Its motivation is simple enough: List the different sexualities and
gender identities beyond the heterosexual majority, in order to make people
aware of their existence and normalcy.

Now, starting with a simple fact is certainly not wrong, but the problem that
gradually raised its head is that you can't list every variation, because there
are too many. Real life is complicated, right? More complicated than even gender
activists initially acknowledged. So the original goal of coming up with a
concise moniker that everybody can be expected to understand, gradually morphed
into an overcomplex mess, where not even the hardcore activists can agree upon a
common term.

It is the plus-sign at the end that expresses this irony in the most concise
way. The entire justification for the initialism in its various forms is to
explicitly list the different variations in order to make them visible. But the
plus-sign suddenly introduces a generic placeholder that is supposed to account
for "all the others". But what is the justification for listing some varieties
explicitly and separately, and others not? Why is it LGBTQ+ and not LGB+, for
example? If it is important to list the varieties, why is it not important to
list them all?

Actually, the Q is already a generic placeholder all by itself, making this even
more hilarious. In a way, you could spell out LGBTQ+ like this: Lesbian, gay,
bisexual, transgender, some others, plus the remaining others.

Again, I fully understand the motive for wanting to make the full variety of
sexual identities transparent, particularly in the face of oppressive religious
dogmatism, but the road to hell is paved with good intentions like this.

The effect this has is that insiders quarrel about petty interpretation and
representation differences, while the outside majority views this as
increasingly sectarian elitist intellectual masturbation, gradually
transitioning into ideological warfare.

It illustrates that what we really need is a totally generic term that
encompasses all sexual identities, including the heteronormative ones. There are
enough variations even within heterosexual majority identities that treating it
as a monolith in opposition to the diverse and colorful non-heteronormative
spectrum would be wrong, too. Human sexuality is diverse, and that's that. It
normally isn't anyone else's business, and it shouldn't determine the way we
interact in public, unless of course when sexuality is part of the topic.
There's no reason why sexuality and sexual identity should matter at all in a
social interaction, for example when I'm shopping, or when I vote, or when I
engage in a discussion. I want a single category, not as many different ones as
possible.

To the religious gender bigots I want to say that the God I don't believe in
created humans as one, not two types. We now believe in equal rights and
opportunities across all variations of gender, race, belief and nationality, so
in most cases there's no reason at all for distinguishing. They're all human,
that's all we need to know.

When you actually want to boink someone, I trust you will have ways and means to
find out whether you match up. If not, don't consult the bible, ask a therapist.
