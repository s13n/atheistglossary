# Destiny

Some people think that our future is prescribed in advance. That our destiny is
fixed. I wonder what the psychological motivation for this is. What emotional
reward does such a stance offer? Why would you want it to be this way? Is it
about there being somebody who has a plan for you? The belief that your life is
being looked after by someone else, someone hidden, acting behind the scenes?

Others believe in a guardian angel who doesn't interfere unless there is
something about to run off course, at which point he acts to get it back on
track, preventing serious harm to you. I find this more appealing, because it
leaves the trajectory open. A fixed destiny wouldn't leave me with the option of
changing course. There would be no room for my free will. Free will would have
to be an illusion. I would execute someone else's program. I hate that thought.

But to be honest, as desirable it may be, I don't believe in guardian angels,
either. I have seen things turn out for the better in unexpected ways, and for
some people that is evidence for their existence, but I see this balanced by
other things turning out for the worse, so I see this as examples of randomness
in nature. And that randomness is not fair.

I find this easier to bear psychologically than a belief in predestination or
guardian angels, because when things go wrong I would have to assume that either
it was planned to turn out that way in advance, or some guardian angel failed to
do his job. I find this utterly frustrating, and in effect more unjust than
being subjected to random influence.

If some deity had planned all my life's trajectory in advance, and I happen to
develop a serious illness, for example, it means that this deity was malevolent
in my case. I don't see how I could have any responsibility for that, so what
reason could the deity have to put me at such a disadvantage? I only see
arbitrariness, which from my perspective is the same as random. It is still hard
to accept that I have been put at such a disadvantage, but at least it wasn't
intentional. I had bad luck, and I find that easier to stomach than being the
victim of a bad practical joke of a deity.

Effectively, I get dealt a random hand at the beginning of my life. It was
random which sperm hit which egg when I was conceived, and from there on a lot
of other random events had an influence on what and where I would eventually
turn out to be. But I can still play this hand in a way I control. I can play
better or worse, in the confines of what I was given. However, there's no
destiny or angel or God I can blame.

Don't we all bemoan that we haven't been dealt the hand we would have liked?
We're not rich enough, not beautiful enough, not talented enough, not
hardworking enough, and therefore not as successful as we would have liked.
Something is always wrong, and if you believe in someone prescribing your
destiny, you have reason to be miffed, to say the least. Christians usually try
to tell you that God isn't to blame, but if he really created everybody and
everything, he is! It is an obvious attempt at deflection, and I don't buy it!

If someone fixes your destiny without you knowing, and you can't complain about
even obvious unfair malice in setting things up for you, then you might as well
believe you got dealt your hand randomly. Christians, as usual, try to have it
both ways at once. God creates and knows and plans everything, yet is
responsible for nothing and can't be blamed for anything. That's gaslighting, it
makes no sense.

I believe that there is no such thing as destiny. Things are random to some
extent, and nobody can escape that. It isn't fair, but it is impartial. It isn't
fair when you roll a dice and it shows a one, and someone else rolls it and it
shows six. He wins and you lose, but nobody is to blame. But even a game
involving dice isn't entirely random. All the randomness aside, there is still
some room for your skill. The randomness will tend to even out on the long run,
but your skill can accumulate an advantage for you.

That's representative for live as a whole. There is a lot of randomness, a lot
of incalculable influences, but there is also room for your skill to play itself
out. There are rules, there's structure and there are chances. A good life is
one where you make good use of the hand you've been given. That's not an exact
science, but I appreciate it much more than suspecting hidden forces which are
controlling your life in ways you don't understand.
