# Reliability

Apologists are wont to call the bible reliable. It is the sort of fuzzy feelgood
language with no tangible meaning that raises my suspicion. It makes me wonder
what they're hiding. The same kind of leeriness that a lot of marketing evokes
in me.

The question, of course is what can I rely on here. How is a book reliable? I
would expect that it reliably contains the same text each time I open it. But
that's what books commonly do, right? Can I rely on it being true in what it
says? No, definitely not. Some of the texts in it claim to be written by
somebody who didn't write them, for example. Can I rely on it containing the
text as it was written by the respective original author? No, not really. We are
dealing with a checkered transmission history, and we have no original of
anything in the entire bible. Even the oldest copies we have may be many
centuries from the original. Can we rely at least on the bible giving us an
accurate account of what Jesus said? No, neither. All of the sayings were
written down almost a lifetime after they were uttered, and we don't even have
those original texts. Nobody was there to keep a stenographic record when Jesus
was on his ministry.

So I really don't see a practically relevant interpretation of what reliability
is supposed to mean here. In no sense I would care about is the bible reliable.

Apologists point at how accurately the text of the bible was preserved over the
centuries. The implication is that this is something extraordinary, and evidence
for the bible's reliability. But when I have a closer look, the accuracy doesn't
look that impressive anymore, and at any rate we can only really tell how
accuracy it was copied when we have earlier manuscripts to compare it with. But
since we haven't got any originals, we can only tell how accurately the later
text transmission was, but not the all important early text transmission. And it
would be wrong to project the later accuracy back in time linearly.

The later accuracy, i.e. the one we can check because we have extant texts we
can compare, is mainly determined by how diligent the scribes were who did the
copying, how well preserved and legible their original was, and how many such
copying stages there were in the course of text transmission. When you have a
text that is considered important, even divine, you may assume that people tried
to be careful. In such circumstances, I don't find it all that extraordinary
that the copies are reasonably good. Nevertheless, a myriad of differences have
crept in for various reasons, which shows that the copying is not miraculous, in
fact it suffers from just the kind of mistakes you would expect, given that it
was painstaking manual work.

Needless to say, when we discover really old copies, such as what happened in
Qumran in the 20th century, we know that all the errors in them must have
happened before the point in time when they were buried or abandoned. But that
still leaves a considerable amount of time for them to have gotten corrupted in
one way or another. And how do we know whether the copying was done in the same
diligent way in the centuries before Jesus? Would we really want to assume that
a book like Isaiah, for example, survived the destruction of the first temple in
Jerusalem, and the exile in Babylon, without change to the text?

In fact, when you analyze the texts closely, they often show signs of having
been edited in some way. Isaiah for example seems to have been amended twice, by
different people. The original version would have ended before chapter 40. We
don't know who did that, and why they didn't write their own books instead. Is
this an example of fraud, or was it a more noble motivation? We do know that
these amendments must have happened before the oldest copies were made that we
have. So inevitably, lots of speculation and complex, intricate arguments emerge
about what it was that has happened, and how close the text we have is to the
actual historical reality.

That's not what I would call reliable. And neither would I expect a text like
this to be reliable in the first place! The term is misapplied, even using it
with the bible misses the point. I don't think the bible is about reliability.
You might say it is about identity, about meaning, about loyalty and trust. But
I don't see how reliability comes into it. It is an apologist sop to believers.
