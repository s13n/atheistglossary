# Martyrdom

What makes a fundamentalist believer so fond of martyrdom? Have you ever
considered this? The suicidal terrorists of the 9/11 attacks triggered this
question for me, and I'm sure for many of you, too. Why do people happily
sacrifice their life for a cause they can't have any way to know if it is true?

Both Atheists and Christians have told jokes about the 72 virgins that allegedly
await islamic martyrs in heaven. A more sober reflection might uncover that in
fact the martyrs themselves are often people young enough to be virgins, in a
metaphorical way. Old people are much less likely to sacrifice their life in
this way, even though they have much less left of it. Some older people,
however, for example preachers/mullahs, are keen to indoctrinate younger people,
so that they can exploit their naivity to become martyrs. In this manner, those
older people advance their own power game with no danger to their own life,
which they would of course not sacrifice.

The Christian, like the atheist, would of course see immediately that those
muslim martyrs are fools. They are sacrificing their life for a phony cause. The
Muslim, however, will probably think the opposite. Clearly, someone must be
mistaken here. The religious fans of martyrdom ought to think hard what makes
them so damn sure that their own party is right here. This is important, because
martyrs obviously do much damage. They often kill or wound others with them.
Sometimes many others, who they often don't know anything about, and who have
nothing to do with the reason for their act of self-sacrifice.

For me, as an outsider with regard to all those religions, and an advocate of
individual freedom, I would grant everyone the right to take his own life, and
if it is because of martyrdom, so be it. I would argue against it when given a
chance, but I would respect the choice of an adult and sane individual. But
nobody has a right or even duty to take someone else's life with him. If you
want to be a martyr, do it for yourself and leave others out of it.

So when Paul or Peter or some other early Christian wanted to die for their
belief, that's fine with me. They apparently didn't attempt to take any
bystanders with them. They wanted to make some sort of point. Others find other
ways to make their point. Good for them when they succeed, but had they asked me
for advice, I would have advised against. I respect their choice, however.

But that's not the usual type of martyr. Usually, matryrs seem to want to die in
battle. Christian history is full of those, and in Islam it seems to be the main
type. This type of martyr is a soldier who has been so fanaticised that he stops
caring about his life, and uses his own self as an expendable weapon. His
fanatical belief has turned into a drug that blocks all natural inhibitions.

And that's really the essence of what I think of martyrs: They act as if their
belief was a drug. And the dealers who sell this drug have less than charitable
motives. It is no accident that young people are both more prone to drug
addiction and to martyrdom. From this angle, the preachers who encourage the
martyrs and drive them to their self-sacrifice are the equivalent of drug
dealers. They exploit a vulnerability of their prey for furthering their wealth
or power. It is a big part of why many atheists think that religion is a source
of evil.
