# God

In ancient times, Gods seemed much nearer than they appear today. I put Gods in
plural here, because back then people found it completely natural to believe in
a multitude of Gods. You probably know this from Egypt or Greece, but the
Israelites also believed in multiple Gods. They had only one "own" God, but when
you read the bible texts, it is quite obvious that they assumed that the Gods of
other peoples also existed, they just thought their own God was stronger.

Mythical people like Noah or Abraham interacted with their God directly. Abraham
famously haggled with God over the number of righteous people in Sodom &
Gomorrha. The covenants mentioned in the bible are between God and his people,
which has at least an inkling of a mutual situation, i.e. mutual promises,
mutual dependencies and mutual duties. In a certain sense, God and his people
are eye to eye.

This wasn't monotheism. It didn't deny the existence of other Gods, it required
loyalty to the one God of the Israelites. If there is only one God, there's no
point in a covenant, because what alternative would the people have? With only
one God in existence, the worship of another God is a mistake, an illusion. When
there are multiple Gods, and you have a covenant with one of them, worshipping
another God is a disloyalty, i.e. an offense. The difference is important. The
harsh reaction of the Israelite God to disloyalty, such as in the story of the
golden calf, would make no sense if the Israelites had only been in error. It is
the competition between Gods which makes loyalty important. But at the same time
it makes God a more human character, with a human psychology.

The technical term for this is henotheism. How this morphed into monotheism is
an interesting question for cultural history, but for our purposes here I only
note that the insistence of Christians that there's only one God is totally
unsupported by the Bible. If you're a Christian, you should wonder how you could
be coerced into believing that God's word says this, when it is obviously
untrue.

Of course, many basic tenets of belief are simpler when there's only one God.
For example, there's no conflict with the notion that the universe had a single
creator. In a polytheistic mythology such things are more complicated. So there
was a motive for this kind of simplification. But to be honest, we don't even
seem to have arrived at a complete monotheism now. Muslims accuse Christianity
of being a polytheism of sorts, and they have a point. To argue that the trinity
is really only one God is a bit of a fisherman's story. And what about all the
angels, the devil, the saints and such? They populate a space somewhere between
a human and a God, thereby diluting the clear distinction.

In a way that brings us back to the beginning of this article: When the one God
is so remote and inflated, people don't see it as near and accessible anymore.
How do you interact with an omni-everything God that's bigger than life in every
respect? You can't do what Noah or Abraham did! In fact Moses did everything to
discourage people from interacting with God directly. He insisted on being the
medium, which of course was supposed to secure his power. And the later churches
and religious authorities did likewise. The inaccessibility of God for the
ordinary believer is the basis for their power. Catholicism, confusingly and
hilariously, manages to have it both ways: You are supposed to have a personal
relationship with God through prayer and such, and at the same time the only way
to God supposedly leads through the church!

I conclude that God's description is being twisted and shaped by the power
aspirations of his earthly self-appointed deputies. It has been so for millenia.
Faith is a power game.
