# Blessing

What is a blessing, really?

Christians often say that it means to be favored by God. But in reality, it is a
kind of ritual that people perform on other people. It is a form of well-wishing
pronounced by people. Or have you ever witnessed an actual reaction by God? I
certainly haven't.

Now, I don't object to people wishing well for others. Quite the opposite, I am
very much in favor of benevolence and amiability. But that's not the same! To
me, it seems that the blessing takes a detour via God, whereas my concept of
benevolence goes directly from one person to another. My question would be, what
has God got to do with it? You wish someone well, good! Why don't you tell him
directly? Or if you don't want to tell, show it to him?

Somehow, I suspect that many people find it easier to ask God to favor somebody,
than to tell this person directly that they wish him well. OK, I get that there
are situations when telling someone directly isn't viable. This someone may be
far away, may be dead, may be unapproachable for some reason. But maybe you just
can't muster the courage. You still want to express your well wishing, and God
serves as a proxy that you can address instead.

I guess my question would be why you are taking this detour via God? If you are
so made that you need some sort of ritual that makes use of a proxy, couldn't it
be a figurine instead, or some other object? After all, you are projecting,
aren't you? I'm not bashing it, I realize that feelings often need some
conduit. You need it to get out, and sometimes the obstacles are disabling.

I sympathize with this, I hope you can tell. The odd thing for me is the role of
God. If you can take a back seat on this question and have a detached thought
about what God's role in this is, you may agree with me that God is kind of an
intruder here. His omniscience should mean that he has the right opinion about
every person already, and someone pronouncing a blessing couldn't possibly
change this. But here you are trying to change his will or mindset by way of a
blessing. Why do you think that might work? What does this mean for the image
you have of your God?

I mean, I really appreciate when you wish me well. But it would feel even more
genuine to me when you cut out the middleman. Own up to your feelings, there's
no need for a proxy! If you don't want to tell me, just show me your sympathy,
and I will feel blessed.
