# Worshiping

Can you explain to me what the point is in worhiping God? Isn't this just
sucking up to someone much more powerful, hoping for some breadcrumb falling
from his table? Why would God even appreciate this? Why would a creator want to
be praised by his own creation?

I am a creator of sorts. I write software. I could very easily create software
that praises me each time I start it up. If I did, I would expect you to think I
was bonkers. I mean, what would this praise be worth? I would effectively be
praising myself!

Maybe you object that my software doesn't have free will, so its praise isn't
worth anything. But praise from a being with a free will would actually mean
something.

So suppose you have kids. I imagine you actually welcome their having their own
free will. Would you want them to praise and worship you? I can assure you that
it would annoy the heck out of me! I'd suspect they suck up to me because they
want something from me! Even worse, I would be upset that they seem to think
that this sort of behavior actually achieves anything with me! It would feel
like I totally failed in educating them.

In my opinion, worship makes no sense. If you worship a higher being, it is a
pathetic and unnecessary display of submission, since the higher being would
know anyway how inferior you are. It doesn't make it any better that you show
you're conscious of it, because the higher being would likely know that, too,
and at any rate is unaffected by it. If you worship a being at the same level,
what is it you are trying to achieve? Do you hope for a favor? Would you like
the praise to be returned? And if you worship a lower being, you probably
wouldn't even be understood, so why bother?

What actually makes you so sure that your worship is appreciated by God, and why
should he appreciate it? Could you imagine that he's rather annoyed by it? Are
you doing it in hope of a favor being returned by him? If so, wouldn't this
appear rather cheap? And if all this doesn't apply, would it not be totally OK
to just stop this praise and worship altogether, and just live a good and decent
life? Why would God not be satisfied with that?

I am also puzzled by the idea that Christians seek to have a personal
relationship with their God (see my article on personal relationship). I believe
it is impossible to have a personal relationship with a being that is so much
more powerful than you! It would be grossly out of balance, something like a
personal relationship between an elephant and a bacterium on his skin. But maybe
you hope for a much more balanced relationship, remembering that you're
allegedly created in his image. But what would this mean? God as your peer, a
bit like a pen pal? But what is the point in him being God then? Why not have a
real pen pal instead?

The real purpose of worship, I think, is rather more mundane. You are to be
coerced into a submissive position, not for the benefit of God, but for the
benefit of your religious leaders. God doesn't need the praise, but they do. It
helps them lead and control you, and secure your active support. And rather
frequently, your religious leaders are psychologically prone to manipulate you.
That's likely why they've chosen this job.
