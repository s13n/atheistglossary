# Know vs. Believe

Is Atheism just a belief? Is it just another religion? Do atheists have the same
kind of faith as religious believers? What does it actually mean to know
something, rather than just believe it, however strongly?

The intellectual term for this is of course "epistemology". If this sounds Greek
to you, that's because it is. The question is very old, and ancient Greek
thinkers have come up with answers, or attempts at an answer, that are relevant
today, even though the topic is still being debated amongst present day
philosophers. That should tell you that is is anything but simple.

Indeed, what is the truth worth, when you can't know it? When you are thrown
back to merely believing? When everyone merely believes, we can all believe
different things with equal justification. Essentially, everybody would then
possess his own truth, and nobody can dismiss the truth of his peer as invalid.

Such a state of affairs would render truth and knowledge useless terms with no
relevance to reality. That's not what we would usually subscribe to, unless we
cling to our beliefs so desperately that we want them to be true even when they
show themselves to be false. Only then would we say we "know" something that's
demonstrably false (which we would of course ignore).

But we all know that calling something true doesn't make it so. Truth and
knowledge simply aren't such personal matters. I have written about "Truth"
elsewhere and recommend you read that, too. From that you will glean that my
concept of truth hinges on confidence, and isn't absolute. This makes knowledge
an accumulation of experiences, an outcome of a process.

If this is so, knowledge is acquired by making experiences, which makes the
passing on of knowledge by word of mouth, or by written texts, a dubious
endeavor. I don't dismiss this outright, but what really gets passed on in this
manner is belief. If you read about the workings of a car engine in a book, or
have it explained to you in school, you afterwards have a belief how a car
engine works. You haven't experienced it in such a way that you *know* how it
works. The explanation may have convinced you, it may have looked plausible to
you, and you may be able to reproduce the explanation to pass your exam, but it
remains a belief all the same. It only becomes knowledge when you have some
actual experience with the workings of such an engine. It may be experience
using the engine, experience with it failing, experience maybe from taking it
apart.

In this sense, it is belief that is passed down from the elders, not knowledge.
If you would just have students read texts and listen to your explanations in
school or in university, you would do the same as religious teachers. You would
teach your students a belief, even when you are teaching science. They would end
up believing in science in the same way they perhaps believe in their religion.
If you are an atheist who grew up amongst atheists and picked it up from the
elders, it would be a similar belief as with a theist.

A much better school would make sure you make experiences that corroborate what
you learn by reading your books or listening to an explanation. You back up
biology classes by working with actual plants and animals. You back up physics
by doing experiments that show what you have explained is actually working that
way. And you instigate curiosity in your students so that they have ideas how to
devise and conduct such experiments by themselves. That way, they make
experiences that gradually convert their belief into knowledge.

Religions frequently do the same, i.e. they try to make sure that their young
believers make experiences that they take as corroboration for their belief. One
example of this are the experiences that surround a "born again" event, or the
various feelings of Jesus' presence or God answering a prayer. We all know that
people tend to experience those when they work themselves into a suitable
psychological situation. But there is no reason to believe that there is more to
it than psychology.

So, really, knowledge is a relative term, and there is no clear line that
separates it from belief. The one thing that can be said in favor of science is
that it has found a way to tie knowledge to experiences that are communicable
and replicable, and hence relatively independent of human wants and needs. The
difference to religion is therefore mainly one of method, and the scientific
method has demonstrated over and over again that it leads to dependable
knowledge. That's something religion never reached.
