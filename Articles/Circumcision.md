# Circumcision

As a Christian in my young years, I wasn't subject to circumcision, so I still
have my foreskin. But I learned in school that the Jews are circumcised as a
sign of allegiance to their faith. Since then, I wondered why a God would want
his followers to cut off the foreskin he had created them with.

Ordinarily, I would have assumed that God's will would rather be to leave your
body the way it is, since who are you to want to improve something that the
perfect creator has presumably devised with more wisdom that you can ever hope
to have? But there was something apparently more important: To show, indeed to
document, your group membership and loyalty as party of the Jewish covenant.

But that's odd, isn't it? God is supposed to know your innards, your secrets,
your loyalties better than yourself, so why would anyone need to document it in
this way? In fact, you could later change your mind and still remain
circumcised! If you add the fact that circumcision happens at an age where you
are incapable to make up your own mind about it, it follows that it isn't,
indeed can't be, about documenting your own allegiance! Your opinion doesn't
matter in the entire process. You are being presented with a done deal!

So it dawned on me that the whole thing is rather more like a brand. It is
inflicted on you when too young to object, it is irreversible, and it is
outwardly detectable even though it isn't in plain sight. Like a brand on a
cow's ass cheek, it marks ownership. You are the property of the Jewish God, and
you will be for the rest of your life. You don't necessarily reveal it each time
you present yourself in public, as it is somewhat subtle, but it is
unmistakeable to yourself and to anybody intimate with you. Whenever you pee, or
have sexual intercourse, you are being reminded of your assigned identity.

Other cultures and other communities have other customs to document a claim to
someone, some even more outwardly obvious and irreversible, some rather more
symbolic and invisible. The Christian baptism can be interpreted in this way,
and at least the Catholic church views this as permanent, too. I can't get
un-baptised even if I wanted to. But of course I can very easily ignore it,
which would be impossible had I been circumcised. In a way I am considering
myself lucky, but I still resent the idea behind it. It violates my autonomy and
integrity. It asserts an ownership claim over me. Someone decided where I should
belong, and my opinion doesn't even matter when I'm grown up and have developed
a mind of my own.

I am of course aware that some people derive some comfort and reassurance from
this kind of identity assignment. You literally know where you belong. If you
accept this decision someone else has made for you, and regard it as part of
your identity, you will probably see all this in a positive light. But I have a
more independent and freedom-minded nature, and this sort of thing itches me. At
the very least, everybody should have his own say in it, and freely opt in
rather than being confronted with the unalterable fact. But of course, not
giving you any say is a big part of the whole point of it! You don't ask a cow,
either, whether it would want to opt in to having the brand burned into its ass.

I haven't even mentioned the health part of it yet. Technically speaking,
circumcision is a form of bodily injury. A culturally sanctioned one of course,
but does this make it any better? The process does bear the risk of damaging
your health, diminish your sexual pleasure or affect your psychological
wellbeing. This is especially true with female genital mutilation (which is thus
aptly named, even though circumcision is also a form of mutilation, albeit less
invasive and grievous). In women, the practice is rather obviously designed to
severely impair sexual pleasure, which sets it apart from the male practice,
where this may play a role, but not such a prominent one. Both practices can,
and many people argue should, be regarded as a crime, given that bodily
integrity is a universal human right.

I wholly agree in the case of female genital mutilation, but I argue for more
tolerance with circumcision, but partly in order to not appear antisemitic. I
still regard it as wrong, but not being a Jew, I hesitate to assert my own
opinion over and above theirs. I would want Jews to give their own religious
practice serious scrutiny, and be honest about what the whole thing is actually
for. Man up to the realization that no tradition is a valid excuse for something
that would otherwise be regarded criminal! It simply is no good to flatly
declare that this is required by tradition and that's that. Nothing is written
in stone like that. If you're honest to yourself, pretty much everything can be
changed (and has been changed) without suddenly making you a different person
and an outcast. At any rate, how ridiculous is it to think that your spiritual
attachment and cultural identity hinges on the absence of a foreskin?
