# The apocalypse

Have you actually read the last book of the bible, John's apocalypse? What do
you think of it?

What, do you think, has it got to do with Jesus? Read the gospels, and how they
depict Jesus! Then compare it with the apocalypse, and how Jesus comes across
there. Where's the connection?

The apocalypse is full of bombast. Jesus inflated beyond all proportions,
portrayed as the avenger in a story that reeks of contempt and frustration. The
author obviously channeled his disappointment into phantasies of violent
revenge! No trace of a meek and mild Jesus, no turning the other cheek, no
mercy. That's basically the negation of what Jesus taught in the gospels.

No wonder this book is popular with those Christians who never had much sympathy
for forgiveness, clemency and love for the enemy, and much rather prefer the
thundering, avenging, fire-breathing superhero. This is all driven by contempt.
The apocalypse serves as a vent for frustrated people. When they speak of
justice and judgement, they mean revenge.

It could be funny if it wasn't dangerous. If it was being read like a comic
book, like an episode of Superman. But it seems to capture the imagination of
some people to such an extent that they start seeing signs everywhere, that the
story in the apocalypse is unfolding. They crave so much for the second coming
of Christ, which is overdue for some 2000 years now, that they seriously
contemplate using force to bring it about. In other words, they try to sow
discord and violence in order to twist Jesus' arm to make him abandon his
procrastination and finally reappear. Mad!

Of course, Jesus won't reappear, because he's dead and has decomposed for 2
millenia. Everybody with a sober mind knows that. Even many Christians do,
although they may not be keen to admit it publicly. For a Christian, his message
should be the thing that counts, not his potential reappearance, and whatever
else the apocalypse dreams up. If you put your hopes on the prospect of his
reappearance, you'll be disappointed for another 2 millenia.

Now, people often believe that Jesus' second coming must be imminent, because
things have allegedly gotten so bad nowadays that, surely, it can't get much
worse anymore, or so they think. But this is a familiar motive for at least the
last few millenia. The ancient Greeks complained already that things are
declining, the young people don't quite match the quality of their elders, so
things generally go downhill. And so it was throughout known history, it seems.
Clearly, this must count as evidence that everything must have started in
paradise, when things have been deteriorating constantly ever since.

But mysteriously, if you look at it from greater distance, things have actually
improved markedly. People who believe that it was all much better in the past,
are invited to try living without central heating, clinics and medicine,
toothpaste, soap, fertilizer or pesticides. If you fancy dying a painful and
agonizing death at 25 from childbirth complications, or from a tooth infection,
you're welcome to try. Alas, all those improvements have nothing to do with
Jesus.

Bottomline: I can't take the apocalypse seriously, and even less can I take
those people seriously who pin their hopes and cravings on it. It is rather sad
to picture John as an old and mad man, writing it to vent his frustration about
a complete failure of his highest hopes to materialize. But to fall for the same
mistake 2 millenia afterwards is embarrassing.
