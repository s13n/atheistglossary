# Materialism

There has been a long standing antagonism in philosophy between materialists and
idealists, and if you are somewhat interested in philosophy, you will probably
have some idea where you stand in this debate. For most people this seems to be
an intuitive matter. In simple terms, perhaps overly simple terms, you either
are comfortable with the notion that the material world is all there is, or you
feel that this can't be all, and there must be something that transcends matter.
In my impression, it is rarely a conscious and fact-based decision, it rather
seems to be a matter of personal preference, since nobody appears to have a
compelling or even decisive case that would settle the question.

If I had to decide between materialism and idealism, I would probably come down
on the side of materialism, but I don't want to do that, because I think that
the problem lies with the two concepts, and the dichotomy is entirely
artificial.

The problem starts with the question what the "matter" actually is that
materialism got its name from. In the old times, when we didn't yet know
anything about how things are in the very small, we had no idea what molecules
and atoms and particles and fields are. We had an intuitive grasp of what matter
is, because we can usually touch it, see it, otherwise experience it. Matter
forms objects and occupies a portion of space, and the same space can't be
occupied by multiple objects at the same time. With a concept of matter like
that, there's clearly something missing, and the idea comes very naturally, that
there must be something entirely different, that complements matter and gives
the world ideas, concepts, order, purpose and life.

But we have a different level of knowledge now, and we realize that matter isn't
what we thought it was. Matter consists of atoms, and they're mostly empty.
Particles can fly right through it, fields intersperse it, mass and energy can
be converted into each other, and shapes vary with speed. The question arises
whether fields are matter, too, or whether they are something entirely
different. Perhaps they even belong to the concept of ideas?

For me, those are fruitless questions. I'd much rather drop the concept of
matter, and instead talk about everything that is physical, i.e. that can be
measured and observed, that can interact in some form to create our reality. The
physical includes all of matter, as well as fields, forces, waves, information,
order and randomness. All of that is part of the same reality, and is
intertwined in ways that make it nonsensical to draw a line between such
concepts like matter and ideas. Only when including all this in the notion of
matter, I am a materialist.

This means that for me, there is no supernatural, no distinct realm of ideas or
spirits. All that is part of a single, physical reality.

You may object that this is just another preference, and I have nothing to show
that my view is any better that the view of anybody else. While that's true, I
do believe that my view is more parsimonious than others. It obeys Occam's razor
by not introducing a superfluous and artificial distinction. Hence it is
simpler.

In general, I find that when we face questions that seem unanswerable, and
people fail to crack the problem for centuries, despite a lot of effort, the
real problem often is that the terms and underlying assumptions are wrong. The
question is being asked in the wrong way. It needs a fundamentally different
approach, and with the right approach the problem often vanishes.

But this is difficult, of course. How do you even become aware that there's a
problem with the question itself? That your assumptions are leading you down the
wrong path? You need to be ready to question even your most basic notions of how
everything works, how the world hangs together. That can be scary. But it can
bring you closer to the truth. It begins with trying to understand what science
has found out about our reality. There's a lot of weird and totally unintuitive
knowledge by now, that invalidates our everyday notions on a very fundamental
level. A level that you may never have explored conciously.
