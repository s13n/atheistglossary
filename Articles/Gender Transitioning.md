# Gender transitioning

A disclaimer: I am not personally affected and can certainly not claim to have
any expertise on this. Consequently, I may end up with egg on my face. I am
trying to not be ignorant, however, and I strive to acquire an informed, but not
necessarily an expert opinion on this matter, which became prominent only in the
past few decades. I wish others would refrain from spamming their own ignorant
stance to the world, and try to at least understand the problem before offering
their embarrassment of a solution.

The first thing to understand is rather trivial: The genetic sex, the sex
organs, the gender identity and the gender expression are all distinct things
with no automatic concordance. The notion that you have to feel, think and
behave in a certain way, because you have a certain set of genes, or a certain
set of sex organs, is misguided.

It is no surprise that the bible doesn't help here. Firstly, the insight took a
very long time to put down roots in our thinking, and that is in part because of
the overemphasis on the male vs. female dichotomy that pervades the bible. But
it is also overemphasized by people who interpret the bible. There's a
difference between showing something to be generally or mostly true, and making
a moral and absolute law out of it. It would not be difficult to treat
right-handedness the same way, because the bible clearly depicts the right hand
as the superior one. Treating left-handed people as inferior would be easy to
justify on this base, and indeed there have been attempts to "reeducate"
left-handers into right-handers, in a mistaken attempt at "helping" or even
"curing" them. But most people in ancient times weren't quite that dogmatic, and
that also applies to a certain extent to the bible itself. So we suffer today
from biblical oversimplification and from theological overinterpretation
combined.

We should know a lot better today. It was Simone de Beauvoir who very clearly
argued that gender is a social construct more than 70 years ago. It is about
time that this became common knowledge. It would do everybody a favor if we
climbed down from our strict and sometimes extremely binary notions of gender,
and allowed some more fluidity in society. Why should otherwise free people not
be in charge of their own body, their own identity and their own expression in
society? Who are you to impose rules on someone else regarding their gender?

Indeed, what business does anybody have deciding on the gender of someone else?
At child birth, it was and still is customary across the world to have the
child's sex determined by some official, typically a doctor. This is usually
needed for the birth certificate - a questionable practice in my opinion. And
what's recorded there fixes your gender for at least the time until you are
mature enough to be allowed to mind your own matters, and in practice a lot
longer than that, because you would be facing numerous unnecessary obstacles
that people somehow feel entitled to impose on you.

Now, even this seemingly straightforward sex determination isn't always clear,
and in case of ambiguity, which happens frequently enough to matter, people may
resort to the scalpel to "rectify" something that may not need rectifying,
thereby fixing a sex that may not agree with the developing gender identity. How
can anyone expect a developing mind to adjust to what the scalpel did? Do you
seriously think that gender identity follows your outward bodily features? You
don't become a man or woman by looking down your body and check whether you have
got a dick!

So nobody should be surprised that every now and then, somebody finds him- or
herself in the wrong body, not just for the reason mentioned above, but for
whatever reason. That's not the result of some kind of ideological
indoctrination, it is the result of one's development, of one's maturing. It is
not a matter of preference, it is a matter of self-awareness. You don't choose,
you discover. The choice is whether you accept reality and admit it to yourself,
and to others.

When you're in the wrong body, it is only natural that you want to do something
about it. Our society should accept this rather than trying to interfere with
it. There's no harm involved for anybody else, therefore they should at least
stay out of the way, and preferably be helpful. It is very frustrating to be in
that situation to start with, but a busload of clueless people who think they
can give you advice or even pressure you into something that suits them, but not
you, is agonizing.

If you think you need to protect someone, who you believe is too young, from a
mistake that he or she may grow to regret later, take note that it is quite rare
for people to regret their decision in these circumstances. People, even young
people, do not tend to take those decisions on a whim. They realize what's at
stake, in the vast majority of cases, and they know better than you. It can be
even more damaging to prevent people from doing what they know is right, than to
let them do something they may regret. Respect them and don't think you know
better. When in doubt, ask an expert. The expert's ideology is likely not worse
than your own.

And in any case, develop a relaxed attitude towards this issue. It isn't your
body and your problem. Let people mind their own business, and their own life.
Everybody will be happier for it.
