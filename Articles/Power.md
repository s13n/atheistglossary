# Power

Amongst Christians, I see two different kinds. One kind sees the Christian
message as an escape from, and an alternative to, earthly power. The other sees
it as way to assert power. The former accepts this life as wanting, and puts all
hope in the afterlife. The latter wants Christianity to dominate this life
already. The former pursue salvation, the latter pursue power.

I was only exposed to the first type for a long time during my growing up and
thereafter. Christians were in the majority in my homeland Germany, and although
the numbers were declining, it wasn't alarming for most people, so the
atmosphere was relatively relaxed and tolerant. But when I look at other
countries, predominantly the US, things look very different. Even though overall
the Christians are in a larger majority than I experienced in Germany, they are
splintered into more different groups, and they seem to have decided decades ago
that they want to rule. They want power in this life. They want to tell
everybody how to live.

What is the right way to follow Christ? What is his message? Is it to usurp
worldly power if possible, and ensure everybody becomes a Christian, if
necessary by force? I don't think so! You can't enforce salvation, so what is
the point in forcing others into a belief system they're not willing to adopt?
What is the justification for effectively degrading others to a lower rank, and
thereby depriving them of at least some of their rights?

The answer is simple: It is power. Part of Christianity attempts a blatant power
grab, democracy and fairness be damned. It is not about arranging oneself with
plurality anymore, about compromise and tolerance, it is about domination.

This is a sign of weakness. Several developments and trends conspire here to
give Christians a feeling of being beleagured, being under threat, being on a
downward slope. The feeling grows that there's no room for compromise and
tolerance anymore, it is a struggle for survival. Either we win, or they win.
The Christians who believe this resolve to crave for a decisive battle, rather
than accepting the decline. A showdown of sorts. High noon!

Of course, this means civil war, if taken to its conclusion. To attempt to
impose an authoritarian doctrine on a pluralistic society is guaranteed to
become violent. It may work for a while if administered ruthlessly and
mercilessly, but those who are wronged by it will not go away, and there will be
many! The result would be a totalitarian state that has only violence as a tool
for maintaining power, because it has no legitimacy. We don't need to speculate
here, as there are various examples in our present world.

This would inevitably be a situation where power is concentrated with a small
ruling elite, who uses oppression and intimidation to keep people from open
rebellion. The irony of the situation is that all those ordinary people, who
feel threatened and thus crave for a dominating ruler who safeguards their
position, will turn out to be the useful idiots who help the autocrat winning
his position of dominance, at which point they will be treated the same way as
everybody else, i.e. with violent disrespect. In a way, they will not get what
they want, but what they deserve. Unfortunately, the others will undeservedly
get it, too.

The more intelligent Christians realize that a theocracy, that heaven on earth
is a pipe dream. You will never have God himself rule over you, it will always
be humans. And humans pretending to represent God's will are generally the
worst. They abuse God for their own power. And since they're accountable to
nobody else, they end up as autocrats. A theocracy is always an autocracy in
practice, with a brazen liar at the top.

This is Moses, not Jesus. Old testament, not new. This is wars and genocide. And
it is defeat, as the Israelites have experienced multiple times in their
history, including being displaced and decimated, and I fear neither many of
them nor many Christians have learnt the lesson. The temple in Jerusalem was
destroyed twice, and if it had actually been rebuilt after the second
destruction, it might well have been destroyed a few times more in the meantime.
The second destruction was brought about by an attempt to erect a theocracy.
This is the outcome of a totalitarian mindset that believes it can wipe out or
rule over neighbors. The result isn't more power, but less. A smoking pile of
rubble doesn't provide much power or protection. Only cooperation and compromise
does that.
