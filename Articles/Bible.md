# The Bible

When growing up in a Christian environment as a child, you usually learn from an
early age that the Bible is the book which God wrote to teach people what he
wanted them to do. You may not yet understand it, so the adults and especially
the priests and teachers are there to explain it to you.

For me, the first questions about this surfaced when I was able to read "real"
books, i.e. books that were not specifically aimed at children. I found that the
bible wasn't like other "real" books. The language was different and difficult,
and it wasn't written in a way that I expected from a book that teaches things.
School books explained things, but the bible often seemed to explain things in a
way that I simply didn't understand. A few well known passages were easy enough
to understand, but the vast majority did nothing to teach me in any way relevant
to my life. Furthermore, I gradually realized that there were large parts of the
Bible that never appeared in mass or in school. It was such a thick book, but
the same parts were being cited over and over again, and in a way that made no
reference to the bigger picture.

Why did God not write a book that was easy to understand? Why was there so much
stuff in the Bible that apparently didn't matter?

I then learned that Jews only used the books from the old testament, and didn't
regard the new testament books as holy at all. And different confessions of
Christians also disagreed regarding which books in the Bible they considered
holy. Muslims, finally, regarded a still different selection of books as holy,
but only the Quran as the final and true word of God. Clearly, there was a lot
of disagreement about what the Bible actually was, and in what way it
represented God's will. So how could I know I was on the right side? How do you
actually know what God wants, when not only the texts are hard to understand,
but people also disagree fundamentally about their status and interpretation?

Add to that my realization that Christians, and even the clergy, appeared to
take most teachings of the Bible not very seriously. Jesus apparently wanted
people to not worry about earthly fortunes, to give away their posessions to
benefit the poor. But the Catholic Church was undeniably very rich, and there
was considerable pomp in the catholic liturgy and ceremonies.

By that time I was well in my teens, and my faith had started to weaken and
dissolve. The Bible being the word of God? No, it didn't look like that at all.
You would expect a book that was written by a perfect being to look very much
different. Much more perfect. Easier to understand, clearer in its teachings,
coming to the point much more directly. And above all, full of wisdom and
knowledge that couldn't be found out easily by humans. None of that applied to
the Bible. And if that was because it was written by humans, and only inspired
by God, then what does "inspired" actually mean? What exactly is God's
contribution, and how can we separate the wheat from the chaff?

It took a while, but the realization ended up being that it all was human. There
was no contribution of God. Nowhere in the Bible was anything to be found that
couldn't have been written by humans alone. Heck, some of it was plainly wrong,
for example in the first book, Genesis, it presents a description of the
Universe that has very little to do with what we knew about the Universe in the
20th century. It was no surprise that people 3000 years ago didn't know better,
but God should have known better, and could have taught his scribes, i.e. could
have inspired the true makeup of the Universe. But he didn't. The result was the
same as if he hadn't taken part at all.

The Bible had started to look like a fraud to me. The church was bullshitting
me. They seemed to know, because they didn't take the Bible at face value, they
were selective what to read and preach about, and they presented their own
context and interpretation. Nobody really seemed to care. Both the church and
the people seemed to somehow use the Bible as a quarry for finding the boulders
they could use to build their own belief. Whether it made any actual sense
didn't seem to matter much. For the church, ceremony and ritual seemed to matter
more than content. Content was formalized to a large extent.

Later, in my twenties, I regained some interest in the Bible, but this time as a
historical text with no holiness or authority attached to it. It was material to
study history with. It has remained this way. In this mode, the Bible could be
interesting again, but all my veneration had vanished, and when there was
something wrong in the text, it was wrong, full stop. No hard feelings, no
guilt. That's one of the benefits of atheism: You can "just" read the Bible as
it is, analyze what it says, draw your conclusions, and change your mind again
when you learn additional context. It is a book, nothing more. A very important
one for our cultural history, but still only a book, and best interpreted with
reference to a lot of contextual information you can get from other books.

If you are looking for a book that tells you the truth, look elsewhere. I
recommend Euclid's Elements for a very early example of a book that contains the
truth. It doesn't tell you how to live, and that's a plus. It merely tells you
what you can rely on. Over the course of the last centuries, we've built an
enormously successful edifice of real, useful knowledge on this foundation.
