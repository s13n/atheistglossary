# Apologetics

It seems to me that there are many more Christian apologists in the USA than
there are in Europe. I'm not sure why that is, but I suspect it has to do with
Christianity having a more fundamentalist bent in the USA, and apologetics are
needed more to defend such a strict and dogmatic position.

When I listen to those apologists, it frequently strikes me how little they
appear to understand the atheist position. They clearly can't see it as it is,
but view it through the lens of their own faith. Consequently, the chance that
they will actually reach me, let alone convince me, is very remote. So I get the
impression, that this isn't what they are after, anyway. The whole effort seems
to target doubting believers. The apologists appear like sheepdogs who target
individuals who appear to want to stray, and strive to keep the flock together.

In the process, the apologist arguments reveal the Christian thinking, i.e. they
reveal what makes Christians stick to their faith. It says nothing about what
would convince unbelievers. If anything, much of what apologists say would
actually reinforce my unbelief, if that was even possible.

For example, the fear of eternal damnation shines through many apologetic
arguments, even when they strenuously try to avoid the topic. This not only
leaves me unconvinced, it actually serves to show me what the Christian
religion, or at least the variant the apologists are trying to defend, is about
at its core: Fear. Even when they speak about love and salvation, it looks to me
like they are papering a thin layer over their fear. It shows when they idealize
love to such an unreal extent that the wishful thinking, and the divorce from
reality, become palpable. When they idealize their God as the ultimate
incarnation and manifestation of love, they try to brush away the largest part
of their own faith and scripture that speaks a very different language.

It often makes me think: Dude, do you actually know your own religion? Do you
believe I'm clueless?

At the end, apologists manage almost invariably to appear manipulative and
dishonest. Manipulative in that they don't want to look reality in the eye and
paint a truthful and realistic picture of their own religion, including their
practical manifestation in their own denomination. And dishonest in their
depiction of the facts they're arguing about, which they should be familiar
with, such as evolution, science, atheism, society and more. And they appear to
manipulate and lie to themselves more than towards others.

In some way they actually do me, and other atheists, a favor. They end up being
their own worst enemy, in that they demonstrate what religion does to people.
They may succeed in recapturing the occasional straying believer, but they make
the flock uncomfortable and frustrated at the same time. There's a fine line
between educating people and indoctrinating them, and apologists easily cross
it. Even when people can't quite put a finger on where the problem is, they
sense unease and discomfort, and feel bothered and shackled. Overdo it, and they
want to break free.
