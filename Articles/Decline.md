# Decline

Are you among those who believe that our society, or humanity in general, is in
a decline? That we have passed the crest, and it's all downhill from here? Well,
congrats, you are part of a broad coalition of cultural pessimists, some of
which are resigned to it, and some use it to scare you into accepting their
recipe for turning it around. To me, this seems to be the territory of
self-fulfilling prophecies. And I can't help observing that this kind of
pessimism has a very long history. If history has anything to teach us, it is
that the cultural pessimists existed for millennia. Ironically, while the
cultural pessimists have been warning of decline for millenia, our culture has
risen to new heights in the meantime. If this is the kind of decline they mean,
I am all for it.

I find it especially strange to see Christians warning of a decline as the
result of people leaving Christianity. I wonder, is there any evidence
confirming that? I see evidence the other way: Atheist countries are amongst the
most prosperous and also amongst the most peaceful. Countries with a dominating
Christian majority fare worse both in terms of social standards and in violence.

Others who think our western culture is in decline have a different hunch: They
think that Christianity is going down with our western culture, and Islam or the
Chinese or the Indians are going to take over. Some think that going back to
Christianity is our only chance to save our western culture from that fate.

I think this is all hysteric bullshit. Our western culture isn't in decline, it
is more prevalent than ever! The world over people crave for it! People want to
migrate towards it, not away from it! They understand the promise it holds, and
they understand how they aren't getting it from the corrupt, violent and cynical
leaders of their own countries.

No, it is the religious alternatives that are in decline! And they know it! All
over the world, they resort to violent oppression in order to keep their
privileges. Look at Iran! There we have a religious gerontocracy that has
nothing to offer to their own people in terms of positive utopia. They use
religion to whip them up and keep them in the fold, but it isn't working and
they have to use blunt force to prevent people from jumping ship. The theocracy
isn't a paradise, it is a nightmare.

In Russia, the orthodox church conspires with the dictatorship and the secret
services to uphold an autocratic system that couldn't care less about the
wellbeing of the people. They are being used as cannon fodder in a war, as
useful idiots for keeping the wealthy elite in power, and as immature serfs that
have to do what the elite wants. All this while the church tells them that they
are doing the Will of the Lord. On top of that, Russia actively tries to export
it autocratic model to third world countries. They deploy mercenary forces in
Africa to help autocrats oppress and exploit their people there, which also
causes or exaggerates the very misery that drives people to flee the country,
and try to reach Europe.

In China, we have a secular autocratic religion that lets people pursue their
own capitalist interest, while officially preaching communism, and making sure
that the autocracy remains unchallenged. If they have a chance, many people flee
the country, because of the pervasive surveillance and repression there. Even
when you see China in an economic ascendency, you will hardly be able to
diagnose a cultural ascendency. What is it that this Chinese system has to offer
by way of cultural progress? If you see any, let me know! Compare that with the
situation in Taiwan, which has made great strides not only economically, but
also culturally, after the political system loosened up, and became more liberal
and more democratic. That's what a cultural rise looks like, and it is directly
associated with western cultural values!

I could add many more examples to those few most prominent ones I mentioned, but
you get the point. Western culture isn't in decline, it is under fierce attack!
That's not because it is not working, but because it challenges the oppressive
power structure in many countries, and the power elite there. Pervasive
communication systems have made it easy for even remote people to compare their
own situation with the situation elsewhere, and they realize what a rough deal
they've got from their own leadership. Western culture gets attacked because of
its appeal, because it works, because it offers a utopia that most other systems
don't have on offer! Because it offers real-world advantages rather than
promises of other-world advantages that may well never materialize.

The main problem is that many who enjoy the advantages of western civilization
seem to have forgotten what they have. Have forgotten what bedrock of values our
culture is built on. And, no, I don't mean capitalist greed and reckless
egotism! Greed and egotism are universal. You find them in every system, both
secular and religious. I mean personal freedom and responsibility, education,
dignity and human rights. Those are not Christian values, and they can't be
defended by returning to Christianity.
