# Explosion

Explosions loom large in the mind of at least some Christian apologists. There
is the Big Bang, of course. And there is the "Cambrian Explosion". Both are
being used as evidence for God, falsely so. But that shan't be my aim here. I
would like to show how they aren't explosions, and how the language used serves
to mislead the layman.

An explosion is a lump of material that "ignites" at some point, whereby a
chemical or nuclear reaction starts, which proceeds so rapidly, and produces so
much heat, that the lump of material expands so violently that it causes
destruction in its surrounding. When used figuratively, it is usually the rapid
expansion that is meant, not so much the destruction or the heat. We want to
express that something went extraordinarily fast, or suprisingly fast.

For example, when we talk about a population explosion, we don't want to say
that people exploded in great numbers, we just want to point out a population
growth that proceeded unusually quickly. The population growth came about in the
usual way: People procreated. Nothing really surprising. It was just that the
balance between people getting born and people dying got disturbed such that
much more of the former than the latter occurred. This could be because of fewer
wars, better health care, more food supply, or a combination of those. It is
only surprising as long as you don't look at the causes. Once the causes are
clear, the effect usually appears self evident, and the moniker "explosion"
seems greatly exaggerated.

An interesting example of an "explosion" is the explosion of our knowledge. If
it doesn't feel like one, it is because we're still in the middle of it, and
because it is about knowledge, which we always think we have enough already. But
if you look at it from some distance, it is wholly remarkable. Christians should
really make more use of it for proving God. Because whenever something
extraordinary happens like this, it must be God's work, right? Except that in
this case, it is the opposite. The knowledge explosion got underway in earnest
as soon as God was ostracized from scientific investigation some 400 years ago.
He wasn't ostracized because of atheist ideology, by the way, but because of his
apparent uselessness.

This goes to show that an explosion of knowledge, or populations, or indeed the
Cambrian Explosion, may happen simply because a crucial obstacle vanished, or
was overcome. It might have been triggered by a random event, and when the
resulting effect feeds on itself, you get a geometric progression, i.e.
exponential growth. You get this all the time in nature, but usually the
progression runs into a limit rather quickly, for example because something runs
out that would be needed. But if there's ample supply, it can go on for quite a
while, and the growth can reach monstrous proportions.

Consider the development of a virus. Some random mutation modifies a virus in a
way that it can infect a new species. Only a single individual is infected at
the start, and it spreads in a geometric progression from there, as long as
enough individuals are still around that can be infected. We have all had the
privilege of witnessing this explosive effect in a pandemic not too long ago. As
"normal" as a geometric progression is, as surprising it is for many people when
it actually happens. They're completely lost and suspect nefarious activities by
unidentified hidden actors.

I don't know what caused the Cambrian Explosion, I'm a layman in that respect.
But I have zero difficulty imagining a completely normal reason for why such
things might occur, because I know about exponential growth, and what is needed
for it to happen (spoiler: not that much). My guess would be that some sort of
mutation happened to a cell somewhere, that made it significantly more likely
that variations occur. It is a fairly naive thought, I know, and I don't want to
be seen as disrespectful towards the scientists in that field. But I don't see
this in urgent need of explanation. I rather see it as a normal occurrence that
would happen at some point, given how evolution works. But if you know, or can
find out, all the details about it, even better! At any rate, no divine
intervention would have been needed.

This is connected to my opinion that randomness is a feature of reality, which I
discuss elsewhere. If you allow for some randomness in the world, without
suspecting a hidden actor at work in the background, a lot of seemingly
implausible occurrences appear rather normal. If you combine small scale
randomness with some self-reinforcing effect, a small random trigger can bring
something in motion that ends up having a large effect. You would think that
such a large effect can't be random, but in some way it is, because the trigger
that started it was random. From there, a mindless process goes on, and expands
as long as it has something to feed on.
