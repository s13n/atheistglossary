# Blasphemy

Blasphemy is a ridiculous concept. Think about it: The omni-everything creator
of me is offended about something I say about him. Why should he?

I mean, if you are an ordinary human being today, you have some experience in
social media receiving abuse for basically nothing in particular, just because
some pathetic idiot out there feels like insulting you. If you are not
exceptionally short tempered, you shrug and move on.

So why can't God? If you, as someone on the same level as the offender, can
shrug this off, the all powerful God should be infinitely more able to ignore
it. He is totally unassailable, isn't he? What does it say about him when he
still gets worked up by even mild forms of abuse?

Of course, it isn't actually God who gets worked up. It is the believers. They
feel offended, but they realize that this wouldn't carry much weight, and would
quickly appear pathetic. So they inflate the offense by imagining that it is God
himself who is offended.

In other words, they lie. They try to present themselves as the selfless
defender of God, rather than admitting the truth, which is that they are
butthurt.

This, of course implies that God needs to be defended, which is where this whole
thing backfires horribly. It portrays God as weak and petty. He needs to be
defended by his own lowly creatures. But, of course, merely stating this would
be regarded as even more blasphemy. So the believer tries to get out of the
self-inflicted tangle by being even more upset and offended.

Now, we atheists see this all the time. Religious feelings are somehow sacred,
and offending them is NOT OK. Nobody ever explains to us why this only applies
to religious feelings, and not our non-religious feelings. Why should believers
be spared the kind of shit everyone else has gotten used to endure? Couldn't
they just grow up?

The problem wouldn't be one if only a few believers would be offended. The
actual problem kicks in when large groups of people get worked up about it to
the point where they start to riot. And, of course, quite frequently, they get
incited deliberately by religious leaders. Muslims seem to be particularly prone
to rioting because of blasphemy. In my eyes, this makes their God appear
particularly weak. And it makes their religious leaders appear particularly
perfidious.

Only recently, one such deluded believer stabbed the renowned novelist Salman
Rushdie because of an alleged blasphemy in his novel "The Satanic Verses". I
have read it, and I find the blasphemy rather mild. Needless to say, the ones
who are so upset about it haven't usually read even a single page of it. But
some islamic authorities are hell bent on using every pretext they can to whip
up their followers, and to threaten those who don't subscribe to their specific
type of delusion. As we all know, fear is an often used instrument of power
politics. That should tell you all you need to know about what blasphemy really
is about: Power politics. It is all about lowly human affairs, and the power
aspirations of wannabe leaders. No divinity is involved anywhere here.

If I had any say, I would pass legislation that punishes those who accuse others
of blasphemy, because they are clearly mendacious and try to use unfair means to
assert power over others.
