# Cultural Christians

I sometimes call myself a catholic atheist. This apparent contradiction is
supposed to show my upbringing as well as my current conviction. I am not
optimistic regarding my ability to shake off everything that catholicism has
imbued me with. This, after all, is what religious indoctrination wants, when
starting with small children already. The roots that are thus planted are
intended to grow too deep to be able to rip them out again.

Does this mean that I'm a "Cultural Christian"? Yes and no. Yes, because some of
my values and opinions are formed partly on the basis of, and partly in contrast
to, Christianity. No, because I'm not regarding this as a desirable state of
affairs. I don't want to be a cultural Christian. I don't see any benefit in it,
I rather see it as a form of hypocrisy.

What does it mean to be a cultural Christian? Does it mean to outwardly take
part in its rituals without any belief in it? Does it mean to believe in the
usefulness of the Christian religion for societal cohesion, without subscribing
to its doctrinal tenets? Does it describe an opposition against other religious
"cultures", a kind of rallying behind a religious flag?

In some way a cultural Christian seems to believe that while the Christian
theology and doctrine is mostly bonkers, and would need a fundamental update for
our modern times, so that belief in its literal truth is a sign of an
intellectual problem. Its "values", though, are a useful basis for a society to
circle the wagons around.

In my nostrils that reeks of culture war. Some people are quite open and frank
in this regard. Ayaan Hirsi Ali recently outed herself as a Christian with
essentially this motive. Her public statements didn't indicate any commitment
for the Christian doctrine, or the tenets of its faith. They were centered
around a notion of cultural antagonism with Islam on one side, and a certain
"progressive" mindset that gets derided as "woke" on the other.

The underlying feeling seems to be that there is such a level of threat, that it
is necessary to rally around the Christian flag, circle the wagons around the
Christian values, and put aside not just the doctrinal differences, but also the
faith part of religion. Essentially, those people are scared. That's my
perception, at least. I find it ironic that fear drives those people into the
arms of a religion that is based on fear. They are jumping from the frying pan
into the fire.

I suppose it would be a good idea for those people to sit down and calmly think
about what their core values actually are, and what that has got to do with
Christianity. Some people like to describe Christian values in the most
all-encompassing, general, humanistic way in order to draw the circle as wide as
possible, but that's either wishful thinking or a dishonest ruse. You are not
automatically living Christian values when you are simply a nice person. And
when you are spooked by religious fundamentalists amongst Islam, you should be
equally spooked by Christian or Jewish fundamentalists.

If you are sympathizing with the notion of being a cultural Christian, do
yourself the favor and identify what it is that makes you a Christian. What is
specifically Christian about it? Simply being a good person isn't. Don't look at
Christianity through rose tinted glasses. It is not a spiritual wellness scheme.
There are different offerings from young earth creationism to mysticism, and you
can pick your favorite to some extent, but when push comes to shove you will be
pulled into the flock and made a representative for something that likely goes
further than you are comfortable with. The archaic, awkward, backward stuff will
not go away.

You are a Christian when you believe in the resurrection of Jesus, in his being
God himself, in his eventual return to erect his kingdom. If this is a fairy
tale for you, you are not a Christian. If you have values in common with them,
fight for them together with them, but that doesn't mean you have to join them
and swallow their ideology hook line and sinker.

When we can't escape fighting a culture war, it must not be a war between
religions. It must be a war for values, for what is good about our culture. The
freedom, first of all: Freedom of thought, freedom of religion, freedom of
movement, freedom of speech, freedom of assembly. And community values where it
is regarded as virtuous to care about one's peers, respect individuality and
differences, and foster growth and education.

When you want me to join you in circling the wagons, to rally around a common
theme, don't try to conscript me into cultural Christianity! I do not want to be
seen fighting for the baggage that comes with it. I understand that atheism
isn't enough substance for you, indeed it isn't for me, either. Atheism is a
negative stance that needs to be augmented with a positive outlook. But I won't
swallow an outlook that harks back to much of what I regard as thoroughly
misguided.
