# Evolution

I learned a joke in my school days, around the time when we learned about
evolution in our biology class, more than 40 years ago:

Little Fritz came home from school and met his father, who asked: "So you're
back, what have you learned in school today?"

"The teacher taught us that we descend from monkeys."

"From monkeys? Ridiculous! Well, perhaps you, but not me!"

One shouldn't need to explain the point of a joke, but here I need to do it to
make my actual point: The father's reaction is an emotional one, he doesn't know
anything about the topic, but he has an emotional dislike of the result. And he
is too thick to realize how he shot himself in the foot.

This is exactly how I see most evolution deniers amongst the more fundamentalist
parts of Christianity. They are clueless about the topic, but viscerally dislike
the result, and are too thick to realize when they shoot themselves in the foot
in a discussion.

For example when they try to tell me that evolution is "just random", i.e. it is
inconceivable that something like a human being can result from a random
process. This reveals both the emotional stance that is behind this response,
i.e. that it is somehow diminishing to be the result of a random process, and
the cluelessness of what evolution actually teaches, i.e. that randomness is
only a rather small component in the overall process, and not even an essential
one. The point is not that randomness is necessary, but that it is sufficient.
It works **even though** the mutations are random.

In fact, if the mutations weren't random, but cleverly selected for maximum
effectiveness, you would arrive at a certain result much quicker, because you
don't waste time exploring dead ends. Non-random evolution is possible, and it
is still evolution. That should show you how dumb this critique really is.
Evolution works regardless of whether mutations are random or not. If they are
random, it just takes a lot longer. On the positive side, random mutations also
are bias-free, so they don't overlook anything. If you wait long enough,
everything that's possible will be tried.

Most Christian apologists bashing evolution seem too thick to be in a position
to discuss evolution in any sensible way. This unflattering characterization may
not be polite, but it is at least honest. I am not a biologist or otherwise
particularly knowledgeable about evolution, but those critics of evolution
strike me as total morons. Even those who are smart enough to google for
articles written by the Discovery Institute.

Or what should I think of people who feel they can rebuke hundreds of thousands
of very knowledgeable scientists and experts with a sneering note about the
alleged "randomness" of evolution? It is the Dunning Kruger effect on steroids!
The tail is wagging the dog! Even if you understand nothing of evolution, you
should be able to see the dumb arrogance of those apologists!

You can discuss the scientific aspects of evolution if you are knowledgeable
enough, and this includes its deficiencies and contested points. But this isn't
a debate for the general public, and certainly not for those who wouldn't ever
accept a scientific result that contradicts their religious dogma. Whenever
somebody attacks evolution from the pulpit, you know something is wrong.
