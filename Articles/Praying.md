# Praying

When I was little, they taught me to pray. We prayed at home, and in
kindergarden. I don't remember much about it. At that age, I guess when we sing
together, we sing together, and when we pray together, we pray. It's just
something we do, and doing it together it is more fun, and better to memorize,
right? The prayer wasn't much more than thanking God for something. At least I
don't remember much more.

Sometime later, when in school, the teaching became more detailed. Now, I was
supposed to reflect upon my mistakes, or sins, so that when praying to God, I
could ask for forgiveness. I wouldn't want to present myself to God all dirty,
right? Gradually, this led to confession (I was Catholic, remember!). Preparing
myself for a confession was to sit in church, outside the confession box, and
reflect upon my sins that I have, or might have, committed. They called it
"examination of conscience", and the prayer and song book contained some
guidance. The guidance for kids was shorter and simpler than for adolescents and
beyond.

My problem was usually that I didn't think I had done anything wrong, and trying
to scrape my conscience for any signs of a wrong thought felt awkward. I was
supposed to pray to God for help, and that felt even more awkward. But of
course, I could hardly enter the confession box and tell the priest that I had
nothing to report. My sinful nature meant that there always had to be something,
right? So I typically confessed something rather harmless, like not doing
immediately what my parents told me to do, or forgetting to do my school
homework. I wasn't telling a lie, and didn't want to, so this seemed the easiest
way out.

When talking or reflecting about prayer, those confessions often spring to my
mind. Praying and confessing somehow got linked in my brain. Maybe that's
because both evoked this feeling of awkwardness. As a child I prayed
effortlessly and playfully, but also rather mindlessly, and I had no bad
feelings about it. Later, along with confessing, this changed. It was always a
monologue. There never was any response.

I regret this to some extent, because I appreciate the meditative aspect of
prayer, i.e. the collecting and structuring of one's thoughts. Rationally, the
idea of praying to God nowadays appears to me to be somewhere on the spectrum
between ridiculous and preposterous. Psychologically, however, it may well have
a beneficial function that has nothing to do with God. If praying causes you to
reflect on what you have done and what you wish for yourself, you may well be
better able to improve yourself and to pursue your aims, rather than leaving it
to the subconscious, where it perhaps works destructively. It helps to raise
your self-awareness.

It doesn't work for me, however. There's too much baggage in it. It is sad,
because I have to achieve the same effect in another way, and indeed I do
reflect frequently about myself, and my goals and wishes, and my situation, so
that I don't dig myself into a hole. But it isn't part of a routine or a ritual.
It competes with all the other things that happen in a day. Sometimes I miss a
non-burdened, non-religious form of prayer that is an automatic part of daily
routine, but I realize at the same time that it isn't going to happen, given my
past experience. It could be a form of meditation, but knowing myself I would
have to get past some deeply rooted suspicions, and in addition to that I'm not
good at establishing a routine.

I'm old enough for having made my peace with myself in this respect. It is as it
is and it is OK. I guess it is one of those things you have to find a substitute
for, once you realize you're an atheist.
