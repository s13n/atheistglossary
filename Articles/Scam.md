# Scam

How do you recognize a scam? I mean, how do you figure it out before it is too
late, before they've ripped you off? It can be quite difficult, right? Many
people find it so difficult that they resort to only dealing with people they
trust. You don't want to wreck your brain over complicated legal text that seems
deliberately crafted to be unintelligible (up to the point that it is printed in
all caps and/or small print to make it as awkward to read as possible), and
think about where in this mess there could be a pitfall you wouldn't want to
accept. So you resort to dealing with people whom you trust. Or think you can
trust. But of course, this is an instinctive method, not a rational one.

The scammers know that, of course. So they have the following options:

1. They can do everything to present themselves to you as trustworthy. This is a
  marketing problem first and foremost, but they can also use it by pretending
  to be someone who they aren't. Think grandparent scam for an example.
2. They can make the rational assessment as complicated as possible, to make you
  lose your patience or stamina in going through the due diligence. Complicated
  terms and conditions that only lawyers can understand are an example.
3. They can cause a sense of urgency to make you believe that you can't afford
  to take the time to study the offers in detail, and make you decide
  impulsively. Time limited bargain offers are an example for that.
4. They can try to use your envy by pointing to role models who allegedly bought
  it, too, making you believe that you would lose out if you didn't follow.
  That's why they use celebrities in their propaganda, for example.
5. They can inflate the potential disadvantages of you not bying into it, so
  that you are effectively scared into the scam, while playing down the cost for
  you. Insurances often use this to make you believe you need an insurance you
  don't really need.
6. They can make you believe that you stumbled upon a rare opportunity that
  others don't have, leaving you in a position to make a steal, out of sheer
  luck. They might tell you that you were drawn in some sort of lottery, or it
  might be a Nigerian Prince seemingly wanting to part with a chunk of illegal
  money if you help him with retrieving it.

This is not meant to be exhaustive, my point is that there are patterns to scams
that you could recognize if you wanted to. But in order to recognize them, you
need to think soberly about it, and put your own cravings aside. This means to
think about it rationally, and that is the very thing the scammers want to
prevent. They prey on people whose instincts they can manipulate, and who decide
without sober reflection.

I'm not trying to tell you that I am somehow immune. I'm most definitely not. I
fell for scams, too, and usually this had to do with me wanting something so
much that I let it override my rational assessment. Fortunately, the damage was
relatively small, so you could say it was an appropriate fee for a lesson
learned. But there are decisions in life which are so momentous and so unique
that you can't really afford to make a big mistake there, and you won't get the
second chance to apply the lesson that it taught you. For example, buying a
house is often such a unique event. You have to decide without prior experience,
and need to figure out whether the deal is fair without being an expert in the
matter. Long running insurances are a similar case. You don't change those every
few years.

So when you are about to make a momentous decision, you need to look at the red
flags to get an idea whether you might be dealing with a scam. Now lets do that
with the decision to "follow Jesus", i.e. to dedicate your life to the Christian
faith. Before I do it myself, I invite you to pause for a moment and reflect
honestly to what extent the Christian churches use the scamming patterns I
listed above. Read on only after you have gone through it by yourself.

Now, you will not be surprised to read that I see all six scam patterns present
in Christianity:

1. God is being presented as father. Can it get more trustworthy?
2. The bible is so complicated to read that most people only use a few verses.
3. You can die at any point in time, so time's running out, they remind you.
4. They tell you that believers are happier and try to show that to you.
5. They try to scare you with the prospect of ending up in hell.
6. They claim that you don't deserve it, but you get the free gift of God's
   mercy if you buy into it.

So for me all the red flags are up. How is this not a scam when all indicators
point that way? Why would a benevolent, all-loving God set up things like that,
when they allegedly are about the most important decisions in your life? You
want to tell me "well it may look like a scam, and all other religions are, but
this one it isn't!"? Why should I believe this? Why should I believe what looks
like the prototype for most scammers to learn from? If you had to teach a
scamming class, would you not use religion as a prime example?
