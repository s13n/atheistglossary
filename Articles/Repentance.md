# Repentance

"Repent before it is too late", Christians implore me. The concept, of course,
is that I need to repent before I die, so that I don't get cast into the lake of
fire once I'm dead. To be honest, I'd much rather wait until I'm dead and have a
clear view of what actually is going to happen. Being an atheist, I can't quite
bring myself to take this threat seriously, in fact I find it quite obnoxious.

So my topic here is how mean-spirited and devious this threat is! This is not
what I would expect of a benevolent deity! I mean, think about it: I am supposed
to do something rather drastic on the mere claim - without any accompanying
evidence - that something horrible is going to occur to me if I don't, and when
I finally can get some evidence it will be too late to do something about it.
Whoever comes up with such a notion ought to be punished for it, and not
worshiped! This is precisely the opposite of how a benevolent, let alone loving,
actor would treat someone!

Face it, there is zero reasonable argument why a deity should act in this way!
The right way to treat people would be to provide them with the evidence they
need to be able to confidently choose their actions.

When I think I need an insurance, I want to know beforehand what the conditions
are, what the cost is, what ensures that the insurance is going to keep their
promises, and what my options are when I should change my mind at some point,
for whatever reason. I want to be able to estimate my risks and benefits. What
is such an insurance even worth when I can't? Nothing! It would be
indistinguishable from a scam!

And that's what this "repent, repent!" calls are for me: A scam! Nobody would
stack the odds and the communication in this way, unless they were intent on
defrauding people! It is the worst possible deal, or else they would provide
some tangible reassuring evidence, communicated in a transparent way.

Of course the promoters of this scam will claim that God is entirely reliable,
hence the deal is as honest and reliable as can be. But any fraudster would say
that! You need to already believe it to trust it, and everybody ought to
recognize the circularity here. I object that I have every reason to distrust
it, not least because it is so heavily leaning on wishful thinking. This is
exactly what fraudsters prey upon, so why should it be any different with
religion?

This is how this method works: They try to scare you out of your wits, so that
you react in an impulsive, unreasonable way that runs counter to your proper
interests. Fear was invented by nature to make you flee a dangerous situation,
but you should make sure you don't flee into the claws of the predator.
