# Miracles

Some atheists would admit that if they really witnessed a miracle, they might
well start to believe. At any rate, many apologists seem to be interested in
atheist's stance on miracles, and if they could be converted in this way.

I'm not one of those atheists. I reject miracles on definitional grounds. For
me, everything that exists is a part of this one, single reality. If a God
should exist, he would be part of ordinary reality. I would not invent a special
category for divinity. A being is a being, and there is only one way to be.

This means that there are no miracles, there are only things we can't explain.
Or can't explain *yet*. Many things that used to be regarded as miraculous at
some point in the past, have totally normal explanations now. There's no reason
to believe that there are things that will always be beyond explanation.

So would witnessing a so called miracle make me a believer? No. Scientists often
encounter things they can't explain. That's what makes it interesting, and leads
to new theories. Does it make them believe in miracles? No. Like scientists, I
have no problem admitting that there are many things that I don't know and can't
explain, so why should witnessing one more of them make any difference? Take the
discovery of dark matter and dark energy, for example. Science has been unable
for the last couple of decades to explain them in any satisfactory way. If
science was religion, this would be regarded as a miracle. Science regards it as
an interesting problem to solve.

Add to this that many alleged miracles in the bible are quite ridiculous. Take
the prominent example of the resurrection of Lazarus of Bethany. Note that such
an outstanding miracle (relative to other miracles allegedly performed by Jesus)
is only reported in a single gospel, namely John's. That should raise an eyebrow
already! But more importantly, consider what the whole thing is actually for! I
mean, if there is any truth to Jesus' message of his afterlife kingdom, raising
Lazarus is only going to prevent him from entering that kingdom for a while
longer. And during his prolonged time on earth, he is wandering around as
someone who has been dead already. Perhaps even partly decomposed and smelly,
reminiscent of a zombie. Wouldn't this rather feel like a bad joke at Lazarus'
expense?

When you read the bible text it rather seems that Jesus did the miracle
specifically to show off his abilities, "for God's glory so that God's son may
be glorified through it". He deliberately waits for a few days before visiting
Lazarus, so that he will have died by the time he arrives, instead of hurrying
and healing him before his death. And Jesus explicitly says that he acted for
the benefit of bystanders, so that they believe. If I was to personally witness
something like that, I would assume that someone tries to trick me, but I most
certainly would not believe that I was witnessing a genuine miracle!

So what do you expect me to think about such an alleged miracle in a gospel
that's 2000 years old and was written by someone who wanted to get a theological
concept across, rather than trying to narrate history as accurately as possible?
I mean, it is bad enough that you don't immediately recognize the bullshit, but
how can you assume anyone else would buy it?
