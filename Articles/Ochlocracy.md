# Ochlocracy

This is what the ancient Greeks called a degenerate form of democracy, where the
people aren't interested in the common good anymore, but rather in their own
advantage. It is mob rule, where a majority or a large minority rule by violence
and intimidation, with the democratic institutions malfunctioning or out of
service. Any similarity to the state of affairs in some contemporary countries
is not entirely accidental.

The Greek historian Polybios, living some 150 years before Jesus, came up with a
theory of statecraft that expanded on Aristotle's three types of government:
Monarchy, Aristocracy and Democracy by adding degenerate forms corresponding to
each of them: Tyranny, Oligarchy and Ochlocracy. They go through a cycle with
ochlocracy following on democracy as a consequence of a rise of egotism and a
decline of a sense of the common good. Arguably, a number of erstwhile
democracies of our time have made this transistion and would thus better be
called ochlocracies. I leave it as homework for you to figure out which ones.

I find it both striking and humbling to see someone from more than 2000 years
ago describe the current situation in our countries so convincingly. Indeed, the
works of Polybios have had some considerable influence on the architects of our
democracies, for example the US founding fathers. But somehow we either have
forgotten what they still knew, or they were unable to overcome the forces
inherent in the cycle Polybios described.

But I don't want to blame the founding fathers. The demise of democracy is our
own collective deed. The look back to Polybios can teach us what we get wrong.
We don't emphazise enough the common good, and we let our egotism run wild. We
let our democratic institutions rot away, and are too tolerant towards people
and groups who want to dominate through intimidation and violence.

Most importantly, we seem to have forgotten what democracy is, how it works, why
it works and why we all want, or should want, it. People came to think that
democratic institutions are there to be plundered and taken advantage of. That
the state doesn't represent the common good and deserves to be starved and
incapacitated. That it just wastes everybody's money.

Polybios tells us what the consequence is: Mob rule. This should worry those
super rich who think that they shouldn't have to pay taxes. Do you really think
you could sit out mob rule on your lonely island?

And when people are sick of that (which can happen fairly quickly): Monarchy and
Tyranny.

Just as a side note to doomsayers who see this as a sign of the approaching
apocalypse: The fact that this was diagnosed and described more than 2000 years
ago should show you that it is indeed a cycle, i.e. it won't be the end you
crave for. The world will keep turning, and you will be in the same shit as
everybody else.

Why am I including this amongst articles about atheism? Because I see a cabal of
unlikely allies conspire against democracy, amongst them religious
fundamentalists of different pedigree. I wonder what they hope to get, because I
find it very hard to imagine they will be happy with what they're going to get.
Some seem to take part in the destruction just for the hell of it. Because
they're bored with the status quo, maybe. Because they believe in the creative
force of destruction. Others crave for the second coming of the messiah, and
want to twist his arm. Others want to settle old grievances. Yet others are just
mad. All of them are about to mow down the tree they're sitting on. The tree
we're all sitting on.
