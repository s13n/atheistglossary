# Context

Recently it has become customary in debates between atheists and believers to
accuse the opponent of missing or disregarding context. It has become rather
annoying, I confess, not because I disagree with the notion that context is
important, but because it has become a blanket accusation that gets used with no
justification.

If someone accuses me of missing context, I expect him to tell what context he
regards important for discussing the matter at hand. I don't accept having to
guess what he might mean. What is it that I allegedly neglected, and how does it
change the argument?

In a way I accept that Christian apologists are retaliating in style here,
because as far as I have observed, the accusation of a lack of context first
appeared on the side of the atheists. In many cases this is justified in my
opinion, because Christians are very frequently throwing isolated bible verses
at you instead of making an actual argument. Understanding the verse would
usually require to look at the context in which it appears in the scripture. But
the Christian often doesn't bother to provide such context.

This conveys the impression that the Christian regards the bible verse as
something that speaks for itself, like a fortune cookie. Like a magical
incantation that works miracles by way of being recited. But if the atheist
would ever dare to use a bible verse by itself to illustrate a point, the
accusation of missing the context would almost be guaranteed.

But it has progressed beyond that. By now, you can provide ample context in your
argument, and still get accused of missing context. It has deteriorated into a
knee jerk reaction, an attempt to turn the table in the cheapest possible way.

The most blatant form Christians have come to use is to assert that the whole of
the bible is the appropriate context, which basically means that you have to
read the entire bible before you're allowed to comment on even a single verse.
This is hilarious, and an obvious attempt to simply shut you down. The bible is
a collection of books by different authors who often didn't know about each
other, and didn't know about what the other wrote. That's trivially true when
one writes after the other, how could the earlier author know what later authors
would write? It took hundreds of years until the entire collection of books that
we now know as the bible had been compiled. If that's the context you rely upon,
it is an afterthought!

No, the context you may need to understand a bible verse is the book it appears
in, perhaps also the books it refers to, the life and environment of the author
(as far as known) and the motivation and aim of the author (also as far as
known). Not the entire bible! And some important context may not appear in the
bible at all! You'd have to look at different sources, perhaps from archaeology
or from other historical accounts. In particular, the new testament is not the
context you need to understand the old testament, for very obvious reasons!

Fortunately, the ruse is easily spotted. Whenever somebody accuses you of
missing context without providing the context you're allegedly missing, you know
it is a dishonest way of debating. You can be prepared for it, recognize it,
react appropriately to it.

In many cases, it will be best to finish your point and not let your opponent
throw you - and the audience - off your train of thought. The missing context,
if there should be any, can be addressed thereafter. But more likely you are
just a spectator, in which case you can recognize how honestly somebody is
debating, by observing his use of the "context" argument.
