# Justice

Christians tend to have a very narrow-minded idea of what justice is. I am
frequently horrified by how fundamentally different it is from what I regard as
real justice.

For me, a basic tenet of Justice is proportionality. The punishment for an
offense must be proportional to the offense. This sounds like basic fairness.
For most people this is so self-evident that they barely spend a thought on it,
but it is anything but. Just imagine what would result if victims could judge in
their own case, and you immediately see that all fairness and proportionality
would fly right out of the window. We also know how this extends to the social
realm, where punishing someone from the out-group tends to be more severe for
the same offense than for someone from the in-group. Think about racism.
Proportionality of justice is a hot topic!

Now look at Christianity. The way justice is interpreted there makes you think
it is a regress from the earlier notion of "an eye for an eye". You have to give
the old testament the credit that it tried to come up with a simple formula for
proportional justice. It seems to be somewhat crude and barbarian to us, but
compare this with Christianity's "ultimate justice" of you burning forever in
hell for all your sins in your first life. Proportionality? Nope!

If you think about it, Christianity's notion of justice as "afterlife
retribution" is pretty much the opposite of what we regard as justice today. We
generally agree that, apart from proportionality, justice should be administered
as soon as possible, to maintain the connection between offense and punishment.
It is a psychological thing that we've learned over time. Christianity does the
opposite. Apart from being totally disproportional, it saves all punishment for
the end, when you have no more chance of improving your ways.

Improving your ways is the keyword! What is punishment there for? The Christian
idea seems to be purely about retribution. But we nowadays see punishment as a
way of inducing change in people, change for the better, hoping that the
offender learns from it and becomes a better member of society. The word for
this is social reintegration, where in Christianity it is ultimate and terminal
ostracism. Question: Which method do you think is more ethical?

Once you focus on social integration, and on repair rather than retribution, you
inevitably come to realize that punishment is not what justice is about. If
justice isn't about punishment, what is it about? At the end, it is a means to
help people live together in safety and in harmony. To segregate people by
jailing them must be the last resort when safety can't be achieved otherwise.
Justice is there to help put right what is wrong, and not to provide the revenge
some are lusting for.

This could be a Christian idea if it wasn't for this harebrained Christian
obsession with hell as the "ultimate justice". In this way, they knock down with
their ass what the Jews as their predecessors have built with their hands.
