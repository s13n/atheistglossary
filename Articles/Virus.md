# Virus

A virus in biology is a piece of DNA, packaged in a way that allows its
successful transmission from one organism to another. The DNA inside encodes
information that induces the organism to replicate and redistribute the virus.

The important terms here are "information", "replicate" and "redistribute". It
isn't important how the information is encoded. The virus is a pattern, i.e. the
pattern of information causing its own replication and redistribution, by using
the facilities of a host organism. The same pattern has emerged in computing,
and it became prominent there with the widespread adoption of the internet as
the ideal redistribution medium.

So, in a way, you could say that the virus isn't the thing, it is the phenomenon
or the pattern. Therefore, when the pattern emerges in another context, as has
happened in computing, applying the same term is appropriate, even though the
thing itself is quite different -- a biological virus and a computer virus are
very different things, but they lead to very similar phenomena.

This sort of thing hints at a more fundamental principle that is involved here,
which does not depend on the details of how information storage, replication and
retransmission work. The principle applies as soon as suitable conditions are
present, i.e. as soon as there are practically working means of information
storage, replication and retransmission. If the conditions are there, a virus
will eventually appear by way of some sort of evolution.

Note that this does not (yet) contain any value judgment. I'm not saying viruses
are bad or good. I'm not talking about sickness or cure. I'm just describing a
phenomenon, which, arguably, can be beneficial as well as harmful. You can very
well study the phenomenon, and the details of its workings, without applying
such a judgment, and indeed that would be the scientific way.

Once you recognize the pattern, you can go searching for other situations when
it occurs. This is how the term "virus", which originated in biology, got
transferred to computing. But it doesn't stop there. Richard Dawkins made a
similar move by transferring the biological term "gene" to culture, creating the
term "meme" to describe the cultural analogon to the biological gene.

A gene and a virus are similar things in biology, in that they store information
that an organism is replicating. The big difference is that the virus is being
transmitted separately, in a packaged form specifically suited to being
transmitted, whereas the gene only exists within cells of an organism, and often
enough doesn't even get replicated on its own, but only in conjunction with
other genes contained in the genome.

For me, this means that "meme" is kind of a misnomer. Instead of deriving it
from the gene, it should have been derived from the virus, because the way memes
are being replicated and transmitted in a culture resembles the way a virus gets
transmitted much more than it resembles the replicating of genes. I'm
essentially saying that cultural concepts, ideas and tenets spread much like
viruses. They are packaged pieces of information that collaborate with the
facilities of an organism to create and transmit copies of itself. If you pick
up a new idea from somebody, and proceed to tell others about it, you execute
the mechanism. You spread a virus.

I therefore consider it absolutely correct to say that something "went viral",
meaning that it replicates and spreads in a manner similar to a virus.

In the very same sense, religions also went viral. A religion is a virus. It is
information that causes organisms to replicate and retransmit it. It is also
subject to evolution. The information changes, thereby adapting to changing
environments, and improving success rates in spreading. Christianity is a
mutation of Judaism that dramatically improved its success rate in spreading
over the Roman empire, and later over the world. Many other mutations that
sprang up over time didn't enjoy that success, and withered away. We can read
traces of it in old scriptures, where sects, heresies or religious teachings are
mentioned that we don't have anymore today, but which did have a following for
some period of time in the past.

Of course we can witness the same phenomenon now, when we see sects spring up
and disappear even within a person's lifetime. This means that there is no lack
of mutations, and most of them only have a short lifespan. Every now and then,
however, a mutation seems to make the religion more appealing, and makes it
spread faster and wider. But in the long term, it seems that all of them share
the same fate. No religion has survived from the beginning of time, even the
most enduring have developed from previous religions, and have gone through
their own internal mutations. In all likelihood, they will also wither away
eventually, and be displaced by new mutations.
