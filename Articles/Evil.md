# Evil

Christopher Hitchens, Richard Dawkins and other "new atheists" have been much
criticized for their strident stance, proclaiming that "religion poisons
everything" and "faith is one of the world's great evils, comparable to the
smallpox virus but harder to eradicate." Many found this critique overblown,
claiming that religion was also the root of quite a lot of good.

But we now have another example to look at, which those atheist advocates didn't
foresee. We have the war in Ukraine, and the Russian Orthodox Church that
supports it very vocally. And, I have to say, Hitchens and Dawkins apparently
had it absolutely right. It is as if the faithful in Russia had set out
specifically to prove them right. They openly promote naked, totalitarian and
unmitigated evil in the name of the Lord. They present their version of
Christianity as just as murderous and merciless and uncompromising as
fundamentalist Islam.

If you haven't yet watched the many examples of this orthodox Christian pro-war
propaganda that appeared on Russian TV since at least the beginning of the war,
please do so, lest you believe I'm exaggerating. An excellent source of original
material with english subtitles can be found in the Youtube channel "Russian
Media Monitor" by Julia Davies. This is what the ordinary Russians have been
treated with on a regular and very frequent basis, without any noticeable
opposition within Russia.

As I'm writing this, I'm watching an example from early August 2023, where the
leader of a religious radio station sits together with half a dozen people in a
TV discussion about what to do in Ukraine, and openly and earnestly recommends
to "slit their throats" and "burn them out" with thermobaric rockets, because
they "position themselves as pagans", are beyond healing, and hence need to be
eradicated by force. He denies that this isn't Christian, telling us that it is
the ethics of the old testament that need to be applied here, not those of the
new testament.

While he's advocating genocide, the others sit and nod.

Again, this is not an outlier in Russia! It is but one example of a great many
examples, which all make clear that the Russian Orthodox Church has put itself
firmly behind the authoritarian regime of Putin, behind his war against Ukraine,
and behind his war crimes. They drive the genocidal rhethoric against Ukraine,
and against the "godless west" in general. They are not followers, they are
leaders of those atrocities. And they have no trouble finding the right snippets
of biblical text to support their dehumanizing propaganda.

So, please tell me: If knowledge of good and evil flows from God, from Christian
teaching, from the bible as God's word, then how come that as an atheist, I can
so readily recognize the evil that those Russian clerics promote, and those
clerics can so easily justify their abominations with references to that very
bible? Clearly, something is wrong here about the notion that we need God to
tell good from evil, when those who should know best get it wrong so
fundamentally!

Hitchens couldn't have said it any better: "Here is my challenge. Name one
ethical statement made, or one ethical action performed, by a believer that
could not have been uttered or done by a nonbeliever. And here is my second
challenge. Can any reader think of a wicked statement made, or an evil action
performed, precisely because of religious faith? The second question is easy to
answer, is it not? The first - I have been asking it for some time - awaits a
convincing reply. By what right, then, do the faithful assume this irritating
mantle of righteousness? They have as much to apologize for as to explain."

You may not feel obliged to apologize for a different denomination than yours,
but that would be a copout. It is the same Christianity, after all. The same
ethics. The same Jesus. The same bible. If the orthodox can turn it into such
evil, any denomination can, because they all draw from the same well.
