# Misogyny

I was raised in a catholic environment, where worship of Jesus' mother Mary is a
big thing. In some sense I believe that the catholic church needed a female role
model. Even churches that don't hold Mary in such esteem have to answer the
question what the role of women is supposed to be in Christianity. Maybe it is
just me, but I have the impression that churches who sideline Mary are even
worse, because they end up looking more at the Old Testament to find the proper
role of women in life.

But even with Mary, it is rather bad. Have you ever tried to collect everything
that Mary is reported to have said in the bible? I can tell you, the list isn't
long. She basically is there, but hardly says anything. She has no voice.
Catholic teaching has her represent the role of a silent, enduring, loyal and
ever-loving mother in the background. But of course, all this is only a
projection. Mary serves as an empty canvas onto which the church paints its own
picture of the ideal woman. And this picture is totally dominated by male
interest.

I find it striking how a woman can be portrayed as the pinnacle of womanhood,
when she basically lacks pretty much all female attributes! It isn't enough that
she is reduced to her role as a mother, she is actually even deprived of that,
since she isn't a "real" mother, she merely serves as a vessel. Today we would
call her a surrogate mother. And, indeed, with the exception of John's gospel,
she only plays a role in Jesus' birth and childhood. And in John's gospel, when
she stands beside the cross, Jesus addresses her as "Woman", as if he wanted to
deny that she was his mother.

The old testament, of course, is even more blunt. The ten commandments list
women amongst the property of men, and if anyone violates a woman, for example
by raping her or killing her, it practically amounts to a property crime against
either her father or her husband. Notwithstanding a few examples of opinionated
or semi-autonomous women in the bible, the overall gist is clearly the woman as
the subordinate of the man.

As much as you try, this can't be credibly used as a mold for modern women. But
there's no lack of attempts in religious circles. To effectively reduce women to
their biological and social function of bearing and raising kids in the service
of their husband is misogynistic to the core, there's no two ways about it. It
is a blatant power grab of men, who pretend to be entitled to show women where
their place in life is.

Hilariously, the creation story in Genesis 2:21 has the woman created from
Adam's rib. This practically inverts the biological fact that it is the woman
from whose body the man is created. This is yet another attempt at depriving
women of their womanhood. It shows that the power grab starts right at the
beginning of the bible, and carries on throughout.

The one example that provides an exception from this in the bible is the song of
songs, where femininity is actually being celebrated. I find it totally
implausible that it should be written by Solomon. I'd say it was written by a
woman whose identity was lost to a male priesthood whose womb envy wouldn't
tolerate women as authors of sacred books.

And, please, don't give me this transparent excuse that this was normal in those
times, and that Christianity was actually an improvement for the rights of
women. The same thing is being claimed by muslims. It is historically ignorant,
since greater liberties predate Christianity by far, in Greece as well as in
Egypt. Even in Rome, despite the virginity cult there, women enjoyed greater
liberties than what both Jewish and Christian doctrine mandated.
