# Inspiration

God is said to have inspired the authors of the bible. But what does this
actually mean? Does it mean that he dictated the text, so that the atuthors are
really only scribes, and the actual author is God himself? Or is it that God put
the general thoughts into the authors' minds, and the way they put them into
writing was their own choice? What is inspiration, anyway? How does it work?

Some seem to think that it really is like dictating the text, which is why they
talk about the bible as the "inerrant" work of God. Which is actually quite
audacious, because even when you think that inspiring is like dictating the
text, you'd still have to assume that the authors didn't make mistakes when
writing it down. Do they think the authors handed their text over to God for a
round of proof reading? What point is there in declaring a text inerrant when
there are still errors owing to the humans involved in the process? Isn't that
like saying "It would have been free of errors if God had written it by himself,
but, alas, since he used imperfect people as scribes, there will be errors!"
Well, duh!

The ethymological root of the word "inspiration" lies in the Latin "inspirare",
which means to breathe upon, or blow into. It links back to the old idea that
life itself is associated with the breath. In Genesis, God breathes life into
the first human, Adam, whom he made from dead matter. You could say that God
inspired life here. When it is the breath that makes something alive, then it is
clear that "inspiration" really is a metaphor. In a figurative sense, to give
inspiration is to give life.

If you believe that people don't really have a life of their own, and no
thoughts of their own, i.e. that all this comes from God, then it follows
logically that your thoughts are really God's thoughts. You are are an empty
vessel until it is filled with life and thoughts by God. God lives and thinks
through you! In this sense, everything you think is God's word, and you are
merely the transmitter!

Some people take this notion very seriously, and even believe it is a humble and
deferent stance! But it means the opposite! It means that they think they can't
be wrong, because the source of their thoughts is God! They realize that this
would be ridiculous, so they need Satan as the excuse for their own mistakes!
Satan here has the role of an alternative source of inspiration, somebody who
breathes the wrong thoughts into your mind. It strikes me that people who have
such ideas effectively deny that they have their own original thoughts. All the
thoughts they have are blandishments they receive from external beings. Which
means they're never responsible for anything, the only mistake they can make is
to fall for the wrong temptation. But the real blame will always be on Satan!

I know that the feeling that you don't think your own thoughts, that you don't
live your own life, that somebody else pulls all the strings, is not uncommon in
people. It is a psychological condition that can easily become pathological. I
have encountered people who really believed that someone else was sitting in
their heads, or had planted a device in their heads, which gave them access to
their thoughts and cognition. I'm not a psychologist, but I can imagine that
this can come in various different styles. It can feel like a form of remote
control, remote supervision, or indeed remote inspiration. The feeling of a loss
of control of yourself can be very real. I figure that it doesn't take a lot for
people to associate those external agents with God and/or Satan.

For someone like me, who doesn't have this feeling of external involvement in my
own thoughts, it is an odd experience to interact with people who are convinced
of such external agents. I wonder if this psychological condition is also behind
the notion of inspiration that believers hold. I wouldn't deny that I can be
inspired, too, but for me it has a completely different meaning! When I'm
inspired, I get ideas from what I experience. But I recognize the ideas as my
own, still. What I see, hear or otherwise experience triggers associations in
me, from which ideas flow. My own ideas. To give you an analogy, the ispiration
is like the whiff of some food, the idea is the image of a meal with this food.
The whiff triggers an association, but the idea fleshes it out into a story.

In this understanding, the biblical authors were inspired by the "whiff" of
divinity, but the story is entirely theirs. Much of what went into their stories
is their own ideas, therefore it is only normal that different people have
different associations and ideas, and hence write different stories that don't
agree with each other. If someone told you to write a story about the moon, with
no further instructions, you would take your own associations and ideas, and
work them into a story that you invent. Two different people would come up with
quite different stories even though the inspiration was the same.

This understanding of inspiration wouldn't mean much, even if it did come from
God.