# Fine tuning

Astonishingly, numerous atheists regard the fine tuning argument as one that
gives them pause and could convince them of theism. I don't quite understand
that. For me, the argument has it wholly backwards. It is like saying the foot
was fine tuned for the shoe.

If you believe that the universe was finely tuned to have us in it, you believe
that we were the purpose behind the creation of the universe. That strikes me as
self-importance to an absolutely ridiculous extent. If you are soberly looking
at the facts as they are known today, you realize we are apparently a minuscule
side-effect in a somewhat random corner of the universe. The most you can say is
that the universe allowed our appearance, i.e. it allowed for conditions
suitable for our evolution. But we have no idea whether that's special in any
way.

If you feel this slights you, well, take it as a sign that the whole thing
originates in your own psychology. See the entry on Anthropocentrism. We haven't
a clue how universes are made, but somehow we imagine God twiddling a number of
knobs, to dial in the conditions that are just right for us. That's vanity! I'd
say this amounts to finely tuning God to suit our ego.

Consider: We're not only appearing in a completely unremarkable corner of the
universe, at some random time. We also have been there for a very small amount
of time, relative to the grand scheme of things. And it is not at all unlikely
that we soon will disappear again, sharing the fate of lots of species that have
gone extinct before us. Does this look like part of a grand plan that has us in
mind?

I can't bring myself to believe that! Even if the universe was finely tuned, it
certainly wasn't tuned for us! But what was it tuned for, then? What was the
goal of the tuning? If anything, it could have been to make the universe
"interesting". But that presumes an observer, including his particular taste in
what's he finds interesting. Once more I smell anthropocentrism here. At best, I
would be disposed to assume the agnostic position: The universe might have been
tuned, but we have no idea what for.

But what is wrong with assuming that the "tunable parameters" acquired random
values in some way when the universe began? Is there anything that would rule
out randomness in principle? I don't think so. The odds might be very small that
the parameters ended up the way they are, but we are in no position to assume
that there was only one try. We only know the single universe we're in, but that
doesn't permit us to postulate that it is the only one. We simply can't know.
And if there really was only one try, any outcome was possible, so why not this
one?

Really, nobody is in any position to calculate odds here. All attempts so far
have been grossly unsound. Nobody knows which parameters are tunable, if any.
Our current physical knowledge, which we know is incomplete, has a number of
parameters that appear to be independent. But we know that we do not yet have
enough knowledge to tell. So trying to conclude anything in a situation like
this is jumping the gun.

I have no problem with randomness. Randomness is all around us, a normal feature
of our environment. But some people are uncomfortable with it. They feel better
when they postulate a hidden agent that restores determinism. I frown upon this
cheap trick. Postulating something that can't be observed, in order to avoid the
simplest explanation, is specious. It also doesn't help, since when the effect
is the same as randomness, we might as well call it randomness.

Equally possible is the scenario that the constants need to be what they are,
due to reasons we haven't found out yet. They may depend on something we don't
yet know about. For a somewhat contrived and oversimplified example, take the
number π which is defined as the ratio between circumference and diameter of a
circle. Is this value fixed or tunable? Its value seems arbitrary anyway, it
isn't even a rational number! For a long time it was considered fixed, but it
turns out that it depends on the geometry. Its common value pertains to a flat,
euclidian geometry, but it was found that there are other geometries where the
value is different, and not even constant. This dependency is a relatively late
discovery. Or to put it the other way around, the value of π is fixed because we
are effectively assuming a flat geometry as a precondition, we just didn't know
we did, until we found that there are other geometries.

Why should it not be like this with the other constants we are wondering about
right now?
