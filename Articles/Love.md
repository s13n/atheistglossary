# Love

Christianity is the religion of love. This sets it apart from all other
religions, which are about --- something else, surely?

But what does Christianity mean with love? What is love in Christian teaching?

Certainly not sex, at least if you ask fundamentalist Christians. Sex is only
for procreation. It doesn't really have anything to do with love. Not directly,
anyway. To "make love" must therefore sound rather odd for a Christian.

The ancient Greek distinguished four different kinds of love, and had different
words for them. In English and many other languages, this distinction is somehow
lost, leading to some confusion and ambiguity.

**Eros** stands for erotic love or sexual desire.

**Philia** stands for affection or deep friendship.

**Storge** stands for brotherly or parental affection.

**Agape** stands for divine, non-personal love.

If this distinction had been preserved in Christian teachings, instead of
translating all of those into the same word "love", maybe it would become
clearer that Christian teaching really emphasizes one form of love, agape, over
the others. Eros, in particular, enjoys no respect at all, and the best you can
say is that it is being ignored. It is clear from context, however, that
sexuality and desire is actually reviled.

In translations, where this is all represented by the same word "love", it looks
like Christianity is trying to redefine the meaning of "love" to exclude
sexuality and desire. This may not have been the original intent of the texts as
they were written in Koine Greek, but it does seem to have had this effect
throughout Christian history.

In my opinion, all those different interpretations of love are part of human
life, and you ignore or suppress any one of them at your peril. None of them is
"higher" than any other. In Christianity this false emphasis takes on absurd
forms at times.

Allegedly, it is out of love that God sacrificed his own son in order to atone
for humanity's sins. That makes no sense. It looks like agape winning over
storge, but there's no justification why this should be necessary at all.

Jesus has the same message when he demands in Luke 14:26 that in order to become
his disciple, one has to hate one's family. That's not just agape ranking higher
than storge, it is agape at the expense of storge. So the Christian message
seems to boil down to the glorification of agape, and the sidelining or even
suppression of all other meanings of love. When Paul famously explains love in
1 Corinthians 13, he uses the word *agape*.

I didn't get this for a long time. The Christian concept of love made no sense
to me and appeared utterly damaged. The realization that there are different
types of love, which have different words in the language the new testament was
originally written in, cleared up for me a lot of what's going on here.

I still think that emphasizing one type of love over all others was mad to begin
with, but only the translation-related loss of distinction creates the harmful
failure of Christianity in the USA and elsewhere to understand love in its
entirety. It effectively made agape completely replace all other forms of love,
and created the impression that the new testament was speaking about love in its
totality, when it actually only spoke of one type of love.

We can all see the psychological debris of this demolition of love. Many
Christians, including clerics and church officials, seem deeply confused about
love. They don't seem to understand why the pinnacle of love that the bible
teaches to them is such a failure in practice, leaving them unable to deal
openly and confidently with their own desires and dispositions. They literally
miss three quarters of what love is.
