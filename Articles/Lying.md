# Lying

Do you think that a lie is OK when it is for a good cause? And how do you react
to a lie when you become aware of it? Are you always upset? Or does it depend on
the motive of the liar?

There can be no doubt that the motives of a lie can be very different. You may
lie for your own advantage. You may lie in order to avoid an embarrassment.
You may lie in order to assuage someone's feelings. You may lie on behalf of
someone else. Or you may lie to yourself. Is that all equally bad?

Christians typically point at the ten commandments, where it is ruled that you
shall not bear false witness against your neighbor. This is often taken as
meaning that you shall not lie, but that's a dubious generalization that isn't
reflecting what the text says. If you wanted to reduce it as much as possible,
rather than generalizing it as much as possible, you would perhaps conclude that
it only speaks against testifying falsely in court, and only when it pertains to
your neighbor.

This, in fact, mirrors our custom today. Lying by itself isn't punishable by
law. Only when testifying in court, you are compelled to tell the truth. Other
places in the bible corroborate this, indicating that people held similar views
even thousands of years ago.

Consequently, there has been disagreement ever since, about what kind of lie was
bad. Fundamentalists like Augustine claimed that all lies were sins, unless they
were accidental. Others like Origen thought some lies were beneficial and
therefore justified. Even God himself could beneficially lie, he maintained.

You notice the slippery slope: With a "flexible" approach such as Origen's, you
can potentially justify just about any lie. And, indeed, some Christians find no
problem with lying for the benefit of Christianity, i.e. "Lying for Jesus". If
it leads to salvation, it must be good, right?

But the fundamentalist stance is slippery, too! Imagine your spouse is
threatened by a murderer, and he asks you where he is. You're compelled to tell
the truth, but would you? You might say that telling the truth is still
required, and all that happens is God's will, but come on! This wouldn't be
pious, it would be irresponsible! (Never mind that some would jump at the
opportunity to get rid of their spouse...)

And what about creating a false impression, without actually lying directly? You
can effectively spread a lie by deliberately telling a half truth, by leaving
out crucial parts of the whole picture. While you didn't technically lie, i.e.
say something you knew was false, you nevertheless wanted to deceive. Thus, your
motive was the same as if you had lied, just the means where subtly different.
Does that count as a lie, too? Or as a sin, if you're a Christian?

If so, you probably can't work in advertising or in politics, because spreading
false impressions is their daily occupation. You say what is advantageous for
you or your employer, and keep your mouth shut about what's disadvantageous. You
are trying to create an impression that is more positive than warranted. You
deceive.

This spreads more widely the more you think about it. Christian apologists and
ministers are doing it. Lovers are doing it. Friends are doing it. Colleagues
and managers are doing it. Everybody is doing it. If you are dressing up, and
apply makeup, you do it. Arguably, it all is about creating a favorable but not
entirely truthful impression. Putting something into a good light is more
important than putting it into the right light. It absolutely pervades society.
Not just ours, but equally so the ancient society in Jesus' time.

Is that bad? Does it matter? Clearly, you need to draw a line somewhere. Most
people would probably agree that being totally truthful in all aspects of life
would not really be possible, or even desirable. We would miss out on a lot of
beauty and style, temptations and indulgences, most notably the harmless and
enjoyable ones. I wouldn't want that. It would throw out the kid with the
bathwater.

On the other hand, a lie requires perpetual maintenance, or else the truth may
break through. Even the harmless lies can morph into a menace over time. That
kills trust and messes with relationships. It comes back to haunt you.

So we're back at the point where we need to consider the circumstances. What's
the harm and what's the benefit? The answer will depend on the situation. No
general rule will get us around making a judgment. Lying isn't bad by itself. It
is the motive and the consequences that may make it bad. Perhaps the most
important rule is to be truthful to yourself. It will help you make better
judgments. And it may make you both more trusting, and more trustworthy.
