# Supernatural

In my article about Dualism, I already hinted at my opinion that there is no
supernatural. You may object that I'm just arbitrarily defining it away. Hence I
better explain what I mean with that, and why it isn't just a cheap trick.

Of course, by saying that everything that exists is natural, i.e. exists in
nature, I define away the supernatural. If God exists, he's part of what I call
nature. You could also call it the universe, but some people think God exists
outside, and separate of, the universe, which is why he can create universes.
So, regardless of the actual word used, with "nature" I mean everything that
exists. So I'm not really defining anything out of existence, I just expand the
meaning of "nature". That makes the term "supernatural" superfluous, but doesn't
remove anything from existence that you would call supernatural.

There's a reason why I'm doing that. The reason is that I don't know how to
distinguish the supernatural from the natural. How would I know that anything or
anybody I encounter in my life in whatever way is natural or supernatural? How
do you tell them apart?

You might say that the natural is what is governed by the laws of nature, and
you recognize the supernatural when something happens that doesn't follow the
laws of nature. But there's a rather obvious problem with that:

How do you know that the event actually violates the laws of nature? Do you know
all the laws? Do you know all the relevant factors? Can you be sure you haven't
overlooked anything?

In science, it is totally normal to encounter events that can't be explained by
the currently known laws of nature, or at least appear so. Science is actively
looking out for such cases, in order to expand our knowledge. A very prominent
recent example is the discovery of dark matter and dark energy in the universe,
which so far can't be explained by the laws of nature as we know them. Yet
nobody in the science community would think even for a split second of calling
it supernatural or suspecting a supernatural origin. Everybody is like:
Something big that we can't explain yet, how cool is that! Finally there's a
chance of expanding our laws of nature by another significant step, after we had
some stagnation since the big leap of relativity and quantum mechanics!

So, rather than postulating a supernatural explanation, they openly admit that
they don't know, and consider it an exciting new problem to sink their
metaphorical teeth into.

Why can't we treat all seemingly unexplainable events like that?

If I witnessed someone rising from the deathbed right in front of my eyes, I can
assure you that I wouldn't suspect a miracle. I would find it puzzling, but I
would assume that there is a natural explanation for it that I just can't see
(yet). So Jesus appearing in front of me and doing his miracle thing wouldn't
work with me. I'd try to figure out what exactly happened, what he had done,
i.e. I would try to find out the trick. And even if I fail, I wouldn't conclude
that it must have been a miracle. I would just resign to the fact that I don't
know.

And that's perhaps the difference between me and a believer. I don't need to
know. I am curious and would like to figure things out, but I don't postulate a
solution when I fail. I can live with such a failure. I consider that normal.

So if you want to convince me of the supernatural, you first need to show me how
to distinguish it from the natural, and that includes the cases where we don't
know. It is not enough to have no natural explanation, it would be necessary to
establish that there can't be a natural explanation, not even with laws of
nature that have yet to be found. Good luck with that!
