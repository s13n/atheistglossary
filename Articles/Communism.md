# Communism

I'm not a communist, and never was one. But I realize that this doesn't buy me
much in the current US, where you are labeled a commie already when you oppose
wearing arms in public, or think that a public health care system is a good
thing. Even worse, Christianity is being painted as in fundamental opposition
with communism, which is nonsense both historically and ideologically. It goes
to show how far off the original Christian idea many US Christians are.

To recap, Communism originally was the utopia of a society based on social
equality and freedom, with collective ownership and collaborative problem
solving. Communist theorists like Marx, Lenin or Engels added their economic
theories and expanded the utopia to a classless society without a leading caste.
If that reminds you of the original Christians, the followers of Jesus, you are
not far off the mark. It is no accident that the communist utopia has produced
Christian answers in the form of "Catholic social teaching", Christian
Socialism, and various worker's movements based on Christian ideas. A number of
Christian sects are based on similar ideas, like collective ownership and living
in communes. Such ideas and ideals can be traced back to Jesus and his
teachings.

Communism according to Marx, Engels and Lenin opposes religion, famously calling
it the "opium of the people", but the critique isn't attacking religious ideals,
it attacks the role organized religion has assumed in the defense and
stabilization of oppressive and unfree social circumstances. Instead of helping
people to throw off the shackles, it had ended up helping to preserve the
shackles by making them easier to bear. The focus on a better afterlife made
people tolerate more hardship in this life, and thus helped the capitalists as
the exploiters and beneficiaries of human labor. In this sense, organized
religion betrayed the ideals of its founder, Jesus.

While many people were in agreement with the diagnosis of the problem, they
weren't prepared to jettison religion, as prescribed by communism, and instead
the various socialist religious alternatives emerged, some persisting to this
day. In this sense, many Christians, particularly in Europe, are still close to
the communist idea, and engaged in socialist projects and groups. They view
Jesus as a socialist pioneer of some sort.

In the US, communism is largely equated with the communist countries, such as
North Korea, China or the former Soviet Union. But those are only communist in
name, and have actually transitioned into a dictatorship of some sort. Indeed, I
would argue that it is a birth defect of communism, that it has a strong
tendency to decay into dictatorship. But that wasn't the original idea. The
fault lies in an unrealistic idea of man.

It is ironic that, using the perversion of the idea of communism as a bogeyman,
Christians in the US went on to pervert Christianity into an authoritarian form
that flies in the face of what Jesus taught. A faux communism is pitted against
a faux Christianity, thereby burying the meaning and sense of both under the
rubble of the ideological battle.

Also ironically, many people don't even seem to have realized that countries who
once labeled themselves communist, or still do that, have long ago ceased to
have anything to do with communism. Likewise, many organizations who label
themselves Christian have long ceased to be Christian in any discernible way,
without people noticing. People mistake capitalist values for Christian, and
Christian values for communist.

But this only adds into a great confusion that has taken hold in the political
landscape. Nobody seems to know anymore what anything means. Words are being
used as weapons, not to denote something clearly. Feelings are more important
than facts, and what you are is more important than what you think and say and
do.
