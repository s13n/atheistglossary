# Preaching

When I was a teenager and still went to church on a Sunday morning, I frequently
was annoyed about the one-directional format of the mass. The repetitive liturgy
was boring. The most interesting thing was usually the sermon, not least because
it was something to think about. But many times I would have liked to interject
a question, or perhaps even an objection. But there's no room for that in
church, at least not in catholic church. The church knows, the priest talks, and
the congregation listens.

But I also realized that for a discussion, the mass was the wrong format. For
starters, it would have woken up the many churchgoers who didn't really listen
anyway, and just wanted the time to pass, while they were thinking about other
things, or nothing at all. The people who went to church because one goes to
church, and who wanted to see who else went to church. Or rather who didn't. No,
to discuss those things there needed to be a smaller meeting, only with people
genuinely interested, and on a more egalitarian level. That wasn't happening in
my village, so I got into reading, which was helped greatly when I went to
university and had much better access to bookstores and libraries. Remember,
there was no internet yet at the time.

While reading changed my life and my outlook on religion quite a bit, I came
around to wonder why people preach, and perhaps even more why people let it be
done to them. I appreciate a good lecture, so I came to wonder what
differentiates preaching from lecturing, what annoys me in one and pleases me in
the other. At what point does a lecture turn into a sermon?

Ask yourself, what makes you want to listen, instead of speaking? What turns me
off is hearing the same boring stuff over and over again. Commercials in TV,
radio, on websites or social media for example. Repetitive attempts to hammer
something very simple into my mind, by repetition, not by persuasion. I have
grown allergic to it, and I try to avoid or block it if I can. I don't like to
be spoken to willy nilly by strangers, and certainly not loudly.

So a lecture is something I attend voluntarily, by choice, just like I read a
book of my own volition. A sermon is something that happens to me while I'm
captive. And the sermon comes with an odor of obligation that the lecture
doesn't have. The lecture respects me, the sermon doesn't. The lecture is
education, the sermon is propaganda.

Where do I draw the line between the two, since clearly they overlap? What I
like in a lecture is when the lecturer appears to have made an effort to
understand a topic, and tries to help others to understand it, too. When the
emphasis is on insight. It doesn't all need to be facts, although they certainly
help. I also appreciate opinion, particularly when it is well reasoned and
informed. I like when the lecturer also presents important objections and deals
with them fairly. But I can also appreciate quips and satire, as long as they're
not malicious.

But sermons are different. What annoys me is when preachers get corrected, and
turn around and repeat the same falsehood again. For example about evolution.
When preachers think they have the truth, are in no need of checking or
discussion or correction, and expect everybody to swallow whole what they
present. When they are selling something rather than offering something. When
they expect me to do something they wouldn't do: Listen.

I have come to think that preachers are often weak people. They preach not only
to others, but also to themselves. Preaching as an act of self-reassurance. They
preach against their own doubts, so that they don't need to listen to an inner
voice that tries to speak to them. It shows when they are louder than necessary,
more categorical than necessary, more commanding than necessary, more judgmental
than necessary, more dismissive than necessary. When they use rhetorical
trickery rather than persuasive argumentation. When they want to win rather than
convince.
