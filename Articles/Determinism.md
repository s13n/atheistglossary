# Determinism

Do you think that the world is largely deterministic, i.e. that if you only knew
enough about its current state, you could predict its future accurately? In
other words, that the fact that we are not able to predict the future
accurately, is only due to our limited knowledge about the current state of the
world, or the universe.

Many believers seem to believe that while we may never be able to have complete
knowledge about the state of the world, and therefore may never be able to
produce accurate forecasts, God does have this complete knowledge, and therefore
can make completely accurate predictions, i.e. he has perfect foresight.

I don't believe this at all. I do not believe that we are living in a
deterministic world. There is some amount of genuine randomness in play, and
therefore precise forecasts are impossible in principle. Being a god doesn't
help here.

I actually wonder how people can have this notion in their minds at all, since
we all experience the indeterminism all the time! We would like to have
determinism, but the fact that we haven't has given rise to all sorts of
trickery like horoscopes and religions and other scams that fake a sort of
determinism that effectively puts the blame on you when a prediction fails,
rather than admitting that the prediction wasn't going to be accurate anyway!

Even rather mundane things like a weather forecast is anything but accurate!
Despite all the science behind it, it only works relatively well over a day or
two, but when it comes to predicting your local weather a fortnight in advance,
you might as well roll dice. This isn't a failure of science! It results from
the fact that genuine randomness is part of the phenomenon. No matter how
accurately you know the state of every molecule involved, and how accurately you
can simulate its future behavior, you will still end up with unpredictable
behavior.

For a long time it appeared that you could at least rely on a principle
formulated by Leibniz: "Natura non facit saltus" (nature doesn't jump). But even
that has become dubious in light of quantum theory. We do in fact observe nature
making jumps at the level of fundamental particles, for example when a nucleus
of an atom spontaneously disintegrates in a radioactive decay. There doesn't
seem to be something in the state of the atom that would allow a prediction of
when the decay will happen. It just happens spontaneously, with no way to tell
in advance when and why. That's genuine randomness. At a macroscopic level,
nature doesn't jump, but at the level of elementary particles, it does.

If you think that a God would be able to forecast that, you're just fooling
yourself. You would do that purely in order to rescue your determinist world
view. Or to rescue what you think is "common sense". But you have zero evidence
that you're right! What you should do instead is to look at reality, recognize
that it isn't accurately predictable, and draw your conclusions from that,
instead of inventing something to fix a reality you're not OK with.

I feel I must add that this genuine randomness that pervades our world does not
mean that everything is random. That is clearly not the case! This is only a
problem for black-and-white thinkers. People who can only think that the world
is either completely random or completely deterministic. That's their own
problem, and not mine. I have no problem with the notion that reality is partly
random, in fact I would maintain that this is exactly what reality is like.

The weather forecast is a good example. On the short term it can be quite
accurate. On this scale, nature indeed doesn't jump. But the influence of
randomness accumulates over time, and makes forecasts ever more inaccurate the
farther into the future you try to go. In a sense, randomness blurs the view
into the future in a way that becomes increasingly obstructive, like fog. In a
fog, you can still see quite clearly over short distances, but from a certain
distance onwards, you basically can't distinguish anything anymore. But other
than in a fog, which hides things that do exist, because it is a fog in space,
and not in time, randomness hides things that only will or will not exist later
in time. With a real fog, you can make hidden things visible by moving towards
them. With randomness, you can only move forward by actually letting the time
pass, and then you can't move backwards anymore and try again in a different
direction.

What does all this mean? Well, it means that all philosophical or theological
arguments that are based on a deterministic world will fail. They are based on a
wrong premise. If you think that since everything has a cause, there must be a
first cause, you are making this mistake. The premise is wrong. It simply isn't
the case that everything has a cause. Some things haven't, their appearance is
genuinely random. If you care to look, you can see that playing out all the time
in your own environment. If you postulate a God to explain the randomness away,
you are begging the question.
