# Paradise

My late father used to say, that the fact that things keep going downhill, for
example that each generation appears to be more decadent and incompetent than
the previous one, is proof that the bible is right: It must all have started in
paradise.

Funnily enough, it didn't seem to me that things were actually going downhill.
Maybe I was in a privileged place, but when you look at medicine, natural
sciences, technology, even art, music and literature - wouldn't you agree that
we had a lot of progress? I mean *real* progress, that makes lives better!

As curious as I am about how people were living a couple of thousand years ago,
I would never book a time travel journey back to biblical times, without a
guaranteed return trip to the present. Imagine you get a tooth infection! And
instead of a competent dentist, all you find is mental healers or brutal
plumbers that are very likely to make matters worse!

I can't help it, but I somehow suspect that people from biblical times would
regard our times in the 21st century as a form of paradise. Not all people, of
course. Some would be too dogmatic to recognize the advantages. But most would.
Life seems so much better! It is much longer, to start with. In biblical times,
people could get very old, but it was a small minority. Today, in the most
developed countries, a *majority* of people reach an old age of 75 years or
more, and lead a much safer and more comfortable life up to that age.

You may say that this isn't what paradise means. That paradise isn't about
getting old and leading a safe and comfortable life. But then, many descriptions
of paradise have been beside the point. People imagined paradise as a place
where the fried doves are flying right into your mouth, where milk and honey
flows in streams. That's the dream of poor people who are engaged in a daily
struggle for survival. That still is the case today for many people, but for
many people in developed countries it is a way of life they never had to endure
or even worry about. For them, how is paradise different from fine dining at a
posh restaurant?

So what does paradise mean, and why would I want it? What's the promise? What's
the promise *for me*? I have gone through rather a lot of imaginations about
paradise, none of which seem particularly appealing to me. You would have to
brainwash me to make me like it, and what would be the point in that?

No, to me a concept of paradise like this is wholly backwards. It starts with
the wrong premise altogether, which is that things were good at the beginning,
and have deteriorated since, so that you'd want to get back to where it began.
No, in fact it was very bad at the beginning, and we collectively have been in a
perpetual struggle to rise from the dust and the agony and the fear. And we have
made progress. Incidentally, we have made much faster progress since we
decoupled our concept of knowledge from religion.

So, for me, the *future* is something to aspire to, not the past. My paradise
lies in the future. But it isn't perfect by any means, and can't be. It is not
about living forever, it is about being able to bring things forward, to help
progress, to make something easier or more enjoyable. In one word: To be
constructive. Paradise isn't just there. It gets built. By ourselves! It is our
collective construction site.
