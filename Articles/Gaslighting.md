# Gaslighting

This is a fairly recent word that has come to describe something similar to
brainwashing, but it seems useful enough to distinguish them, hence the term
stuck. If you have seen the 1944 movie that lent it its name, you'll have an
inkling what it means. Ingrid Bergman famously gets manipulated into doubting
herself, her own sanity. Gaslighting is a brainwashing method, which strives to
undermine self-perception and self-worth of an individual, in order to make it
more dependent on others for interpreting perceptions. It is an attack on the
mental autonomy of the individual, with the aim to make it easier to manipulate
and abuse.

In simpler terms, it is about making you doubt your own sanity, so that you
cling on to your abuser as a stability anchor. You thus become mentally
dependent on your abuser, and you become vulnerable to his manipulation.

Why am I explaining this here? I think you need to know about those techniques,
because they are increasingly being used to manipulate you. It is a matter of
mental self defence.

