# Son and Father

Few people know that Yahweh, the old testament God, who's supposed to be the
father of Jesus, is himself the son of another ancient God, El. The bible
attempts to gloss over this old legend and tries to spin it as El and Yahweh
being two names for the same God. But there are traces left where the
distinction shines through, like a pencil imprint after the eraser had removed
the black line.

So, in a way, the idea that Jesus is God's son, picks up a notion from a
millenium back, when Gods had offspring. El was the Father-God that predates the
time described in the biblical Genesis legend by at least a millennium. The
creation story harks back to El as the creator, but since the bible has been
edited to reflect the theological understanding of much later, the original
polytheistic past has been recast as monotheistic. It is a view of the legendary
past through the eyes of the Israelites after babylonian captivity.

But this is ironic, since the Israelite scribes were so keen in the millenium
before Jesus to unify El and Yahweh, and cast him as the one and only God of the
Israelites. In a certain sense the Christians undid that with their notion of
Jesus as the son of God, and it may well be that they picked up traces of the
old polytheistic past that were still left beneath the surface. It should come
as no surprise that the Jewish authorities were opposed to that notion, as it
effectively wound back the monotheistic innovations of the previous centuries.

Muslims also scoff at the Christian notion of God having a son, while still
selling itself as a monotheist religion. But what really gets critics going is
the notion of having one's own son killed as a sacrifice. This also harks back
to an ancient story, namely Abraham and Isaac. Here, Abraham's loyalty to God is
demonstrated, when challenging him to kill his son Isaac. In some sense, it is
Jesus' loyalty that is being demonstrated with the crucifixion. Both cases
demonstrate the ideal of the unwavering submission of the son under the will of
the father, even if it means the son's death.

This strictly hierarchical notion of a family runs through all of the bible. The
father is the autocrat of the family, and the son owes him absolute loyalty and
unquestioning submission under his will. This applies to Jesus despite him being
regarded as God's equal, or another manifestation of God. Deutoronomy even has
the rule that a rebellious son is to be stoned to death. And the epistle to the
Ephesians makes clear that the autocratic structure of the family continues to
apply in Christianity.

For me, the bible is full of autocratic propaganda, trying to teach that a clear
hierarchy is the required organizational form for all reality. The family is a
kingdom in the small, just as religion is a kingdom in the very large. It is
distinctly patriarchal and authoritarian. Things aren't decided in a cooperative
way, after discussion amongst equals, but top-down by command. This is how you
would organize a military. It is toxic for any society that is organized in an
egalitarian and democratic way.

Reality, of course, is quite different. In the family, even a patriarchal father
will get old and frail, and perhaps develop Alzheimer or dementia. At some point
it would be better for him to obey the son's commands. What prevents the son
from becoming the tyrant his father has been? In many cases you depose of a
tyrant by murdering him, because that's the only way. So isn't this strictly
patriarchal power pyramid forcing people into violent rebellion?

I suggest we have a much better way, one that takes into account that everybody
has something to contribute. Cooperative systems produce better results than
dictatorial ones. Sons grow and become fathers, and fathers expire. A family
isn't a platoon, but a collective, and the family members play roles and deserve
room to grow, and not commands to fulfill.

It is no different in society. Rather than serving as the plaything of an
inflated ruler, it is the space in which people cooperate for mutual benefit,
and ideally where everybody can find enough space to thrive.

Do you think that in our world there are too many sons rebelling against their
fathers, or too few? Too many subservient followers or too few? Are you craving
for a leader or for a buddy? Do you run your own life or have it run by others?
