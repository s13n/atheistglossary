# The Kingdom

Why are Christians so obsessed with kingdoms, I wonder! What is so desirable in
a kingdom that would make you want it so much?

Maybe people in Jesus' time were craving for a Jewish king rather than being
reigned by Roman occupiers. I get that. I also guess that most ordinary people
back then had very little exposure to democratic ideas that had been developed
centuries earlier in Greece. So speaking about a kingdom would have addressed
Jesus' contemporaries with their own dreams and aspirations.

But trying to sell a kingdom to people like me today, who have experience with
democracies and elections, with sharing and dividing power, with an independent
justice that can also keep the executive in check, well, that seems fallen out
of time to me. Why would I want a kingdom, of all things?

Of course, many Christians just recite what they hear in mass or read in the
bible without giving it much thought, but others must have occasionally
entertained the thought that this kind of language could be repulsive to some.

Now, Christians tend to advertise their imagined kingdom as much different from
ordinary kingdoms. Their king, of course, is Jesus, or God, what allegedly
amounts to the same thing. So somehow the criticisms one could direct at
ordinary, earthly kingdoms wouldn't apply to their kingdom. But what's the point
in using this vocabulary then? When the difference is so big, why use the same
word for it? It is only going to be confusing!

I suspect otherwise. I think they really mean a kingdom, and the difference
isn't as big as they want you to believe. The king, of course, reigns
absolutely, opposition isn't allowed for, the role of the underlings is reduced
to approval and acclamation. The king is thought to be omnibenevolent, so of
course opposition is thought to be entirely superfluous, and people would not
feel any need for it.

But if that should be so, why have a king at all? With such a level of approval
and harmony, there wouldn't be any need for a hierarchy. An egalitarian, even an
anarchical system would work. If anyone should feel the urge to praise the Lord
and sit at his feet, he could do that, without anybody compelling him. Others
could stroll around and use the opportunity to have a chat with Jesus, or have a
beer with him, without hierarchy or protocol getting in the way. Wouldn't this
be much more attractive?

I suspect that this is no accident. The imagery of a kingdom is deliberate,
since churches want to dominate. The flock is being prepared for a hierarchical
church, by presenting the kingdom as the desirable state of affairs, the one
right and godly way of government. The idea seems to be that people who crave
for a divine kingdom are more tolerant of an earthly kingdom. Particularly when
those earthly kings can claim to represent God. The catholic church springs to
mind, but also all those countries who have the head of state blessed somehow by
religion. Theocracies are the most extreme form of this, but even England can be
seen as an example, where the monarch is also head of the Anglican church.

If you look at the reality of today's theocracies, or any other hierarchical
system of power with a religious representative at the very top, you will
probably agree with me that this is not what you would want, unless you are a
member of the power elite there. Wherever on earth people or countries get to
demonstrate what a religious kingdom would be like, they demonstrate why you
definitely don't want that. Even worse, we have several examples today for how
easily a hierarchically organized church can enjoy a symbiotic relationship with
a fascist or protofascist state.

Hence, the Christian vocabulary puts me off. At the very least it sounds grossly
outdated to me. More likely it exposes a fundamental disagreement regarding how
things ought to be organized in the large.
