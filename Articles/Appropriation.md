# Cultural Appropriation

Is it OK to use cultural achievements of a different culture, or a different
cultural group? This question has been a matter of public debate in recent
times. Some people argue that it is a form of theft.

I admit straight away that I am all for cultural appropriation, in fact this is
what makes culture advance! We are in the realm of ideas here, and if an idea is
worth stealing, that amounts to a veneration of the idea.

Imagine if we had to desist from using democracy as the founding principle for
our societies, out of respect for a Greek copyright on it! We would have to fall
back to our own native barbarism! Of course, it seems that this is happening
right now, in many democratic countries. But I think it is wrong!

Somehow, it seems to me, that the copyright idea has been generalized into
absurdity here.

But there is a valid core in the debate. While I am all for cultural
appropriation, I am against obfuscating the actual source. The problem isn't in
stealing the idea, it is in attempting to present it as your own, and in erasing
the actual source from history. Thou shalt steal ideas, and thou shalt admit who
you stole it from!

By all means, play the blues as a white person, just don't pretend you invented
it. Be conscious of history, of your roots and the roots of your ideas, and be
honest about it in public. Give your predecessors, your teachers, your idols,
their due respect. That doesn't subtract from your own worth, because it shows
that you are conscious and responsible in what you are doing.

In other words, don't be religious about it. Religions also tend to inflate
their own originality, and play down or even erase their dependency on earlier
religions, and on outside influences. Their God has got to be the origin of
everything, therefore having adopted outside influences is against dogma and
must be denied. It must be denied even when it is obvious.

Judeo-Christian religion has internalized lots of outside influences and
presented them as their own invention. Their God is a development from earlier
Gods from Mesopotamia and Palestine. Their creation myths are harking back to
earlier myths from the same regions. The flood myth has its predecessor in
Mesopotamia. The morality incorporated in the commandments draws from earlier
concepts in Egypt and Mesopotamia. The 7-day week is taken from Babylon.
Christian theology draws a lot from Greek philosophy. It goes on and on.

All of this, pious believers will assure you, is originally theirs. It is their
God who made all of it. The mythology in the bible is drawn up to "prove" this,
i.e. it is written and dated to make it appear as if their God came before all
those other external influences, and this wants to suggest to you that it's the
others who steal from the Israelites, and not the other way around. They can't
give their sources their due respect, because they hate them, and it would
detract from the greatness of their God. Even the great scientific discoveries
are declared to be derived from Christian teachings, as absurd as it may appear!

And there you've got another, rather lowly, motive: Envy. Taking a good idea
from your enemy creates an ego problem. You wouldn't want to pay your enemies
their due respect. Denigrating them, and if possible erasing them from memory,
is the aim of the dishonesty in the first place! The Israelites have been
conquered several times in history, and in a way have retaliated by erasing the
cultural achievements of their invaders and turned them into something to hate.

So in a way, the more you are trying to claim all achievements for yourself, and
try to erase the contribution of others, the more you show your inferiority
complex. You try to conceal your own inferiority by trying to rob others of
their achievements. That is the type of appropriation that is harmful,
offensive, unfair and embarrassing. But many believers seem to buy it. It is
flattering for their own God, and their own religion, so it must be true.

I am interested in cultural history, which basically boils down to the question
where things come from, and how they got to be what they are now. And the more I
learned about it, the less unique and original the Judeo-Christian religion
appeared to me. They have appropriated from their precursors and from their
adversaries much of what is now presented as their original contribution. And I
still see it happen today.
