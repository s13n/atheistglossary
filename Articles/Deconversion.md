# Deconversion

When I first heard this word, I was somewhat puzzled. My intuition said that it
must mean to reverse a previous conversion. This implies returning to a stance
or belief that you had at some previous time.

But I grew up as a catholic. When I gradually lost my Christian faith during my
adolescence, I didn't convert back to anything, unless you assume that I was an
atheist in my mother's womb. In fact I don't even see my transition to atheism
as a conversion. Both my growing into, and growing out of, catholicism was a
development that I see as progression, not conversion, let alone deconversion.

That may have a lot to do with the fact that there wasn't a certain point in
time when I "deconverted". No sudden crisis that led to an abrupt change of
course. This may set me apart from a lot of Americans that have their
frustration with their religion bottle up in them, until they can't go on and
the cork pops.

It may also have to do with many Americans experiencing their religious
"conversion" in their adolescence or even later, when they are "born again". In
a way, deconversion presupposes conversion, doesn't it? This is a term, and an
event, that was unknown to me from my experience in Germany. I knew of no
example of someone in my environment who was "born again" in this sense. I still
don't quite understand, how somebody can convert to Christianity, especially
fundamentalist Christianity, when he's already acquired a thinking, critical
mind. My experience is with growing into a religion from the very first moments
of life.

So, really, both concepts are similarly alien to me: Conversion *and*
Deconversion.

It may just be my character, but isn't it the case that opinions, beliefs and
convictions *develop*? Which implies that it happens gradually! If your belief
changes quite abruptly, isn't this evidence that you have held it back for too
long, until the situation becomes untenable, and your change of tack becomes
rather cataclysmic? Have you asked yourself why it is so? Why can't you just
gradually shift your position as your understanding evolves?

Of course, I assume here that the *gradual* evolution of one's opinions is the
natural state of affairs. Indeed, that's what I am convinced of. So an abrupt
change of belief hints to an unnatural state of affairs. Something hinders
nature here, leading to sudden or near-sudden "conversions" or "deconversions".

You kinda get the gist: I'm frowning on both conversions and deconversions. But
really, I'm frowning on the conditions that produce them. So I'm not advocating
or supporting such de-/conversions, I advocate questioning the conditions that
lead to them. Those are manipulative, constraining, unfree conditions, and the
main aim ought to be to escape those conditions, rather than changing one's
belief.

In short, it is not that important what you believe, it is rather more important
what condition you're in. Are you living in a state of freedom, where you are at
liberty of following the thoughts and insights you have, rather than following
the trails (and rails) someone else has laid for you?
