# Priest

When I was growing up, my catholic parents, especially my father, conveyed to me
that they would appreciate me becoming a catholic priest. Indeed, in my greater
family there were a few nuns, and they were respected and revered, so that when
they came to some family event, they were visibly treated with greater attention
and esteem than the other guests.

The same applied to priests. The local priest was treated like a dignitary, as
if he was on a different level than the others. Comparable perhaps with an
aristocrat or a rich businessman. They received more attention than the others,
more deference than the others, and were more immune from criticism and
pushback.

It took a long time for me to shake this off. And then, I started to realize how
inconsistent this stance of my parents was. I remembered the story they related
from their own parents how things were in the village around the beginning of
the 20th century, more than a hundred years back. Allegedly, the mayor and the
priest conspired to disburden people from their property by having them build
debt while drunk, and had them sign a bill of sale right there in the pub, which
they notarized on the spot. When the poor soul woke up the next morning, having
slept off the intoxication, he realized he was one patch of land lighter. The
priest and the mayor thereby helped the rich of the village to increase their
wealth at the expense of the poor.

Clearly something didn't quite fit together here. How can you hold those two
different views of priesthood in the very same head, like my parents did? Well,
fortunately I quickly realized that I wouldn't want to be a priest, well before
their cognitive dissonance became obvious to me. And, equally fortunately, they
weren't really serious with their wish, either. So no rift ensued.

But what has remained with me is a very sceptical stance on priesthood. If
Christianity did nothing to deter this old priest in my village, whom I never
met because he had died long before I was born, what was it good for? The
messenger of God acting as a fraudster who conspired with the rich against the
poor! His job should have been to make his flock good Christians!

Of course I don't generalize this. I am quite aware that most priests are not
like that, and I was even as an adolescent. But what it did for me was to show
that priests are ordinary humans with nothing setting them apart from the rest.
So why should I pay some extra respect? Why should I look up to them? They're
doing a job, and may do it well or badly, just like everybody else. They don't
even understand the message of God any better than I do. Well, they may, but I
will have to determine that myself. It is no good having blind trust in them.

But, I conjectured, if that's so, why do I need priests at all? Why does anyone
need a priest? More and more it seemed to me that if priests are shepherds, and
people are the sheep, it is all about shearing them.

Over time, I realized how deeply ingrained in religions this is. Take Moses, for
example. The bible doesn't hold anything back here: Moses had murdered a person
before being "called by God" to his mission. He turned out to be a talented
tyrant who knew and used all tricks to maintain his power and his control over
his people. He had his party kill indiscriminately 3000 of his people to shock
and submit them. He carefully deterred his people from directly engaging with
God, and installed himself as the arbiter and medium between God and his people,
leaving no way around him, and making himself indispensable. He thereby
installed the role model for priests up to our times, i.e. "there's no way to
God without (or around) me". The priests are the tollkeepers on the path to God.

Not surprisingly, power corrupts. And absolute power corrupts absolutely. No
need to be surprised about the widespread abuse accusations the various churches
have been engulfed in across the last several decades. I wasn't. When the news
broke, I already was prepared. I didn't expect any better of them. To be sure, I
didn't expect any better from others, either. I'm not holding priests to a
higher standard than others, because I already know they are no better than
anybody else. The abuses of priests don't discredit them as human beings any
more than they discredit any other who commits the same offense. They discredit
their profession. It shows their profession to be redundant.

If there was any truth to religions, people would realize it directly. There
would be no need for a middleman. Even less for a middleman who falls for the
same vices as everyone else.
