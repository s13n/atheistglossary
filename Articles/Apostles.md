# Apostles

I still remember how I was confused as a child about what made an apostle. I had
learned that there were twelve apostles, but then there was also the apostle
Paul, who wasn't one of them. I needed to get older to untangle this mess
gradually. Luckily, I had reasonable courses and teachers at school, which were
explaining this relatively unburdened by dogma. But the full extent of the
situation dawned on me much later still.

So, OK, an apostle is just someone who follows Jesus' teachings and acts as a
messenger or missionary for Christianity. But somehow, that doesn't describe
what I read in the bible. There, the twelve follow Jesus, but often enough don't
seem to understand him, and nowhere do they do any teaching of their own. They
were disciples, but as far as I can tell they weren't apostles. Paul, in
contrast, never met Jesus, so didn't receive any teachings other than his
Damascus moment, but he did lots of teaching.

Granted, after Jesus was gone, some of his disciples carried on his message and
taught it to others. So they turned into apostles, for example Peter. But most
of the "twelve apostles" simply disappear from sight. This makes the notion of
the "twelve apostles" look artificial, a post hoc invention. I regard it as much
more likely that a varying number of people accompanied Jesus, and people joined
and dropped out again as they went. The number twelve is just a "special number"
justified by theology and symbolism, and not related to historical fact.

The fact that after Jesus' crucifixion most apostles disappeared, struck me as
particularly remarkable. The gospels make a point describing Peter as wavering,
as someone who was tempted to deny the relationship with Jesus, but he's
depicted as the rock onto which the church is built. In contrast to that, other
apostles simply vanish. Have they wavered, too?

Later Church tradition has it that they turned into martyrs for Christianity,
but this smells of a later invention. Obviously, people had the same realization
I had, and felt they needed to fill in a few blanks. But I conclude something
different: Those apostles dropped out entirely. They had followed a promising
and charismatic leader, whom they expected to do away with the Romans and erect
a just Kingdom, and then he was killed like that! I think they found their hopes
dashed, and they eventually went their ways, perhaps looking for another leader
to follow.

I find it even possible that some of Jesus' disciples ended up in the ranks of
the rebels that later were embroiled in a war against the Romans. I mean, how
sure is it that those people became dedicated, pacifist Christians? They hadn't
been such pacifists before, they hoped for a new kingdom, and I doubt they
expected the transistion to be entirely nonviolent. One of the twelve was even
called "the zealot". So what holds such people back from becoming guerilla
fighters, after Jesus had shown to be ineffective?

Somehow, believers seem to assume implicitly, that once someone had met Jesus
and listened to his teachings, they'd be invariably hooked, and remained a
disciple for life. That's wishful thinking. If Jesus had been so riveting, so
convincing, he would hardly have had reason to complain that his disciples don't
get it. It is much more reasonable to assume that many of them didn't get it
even after his death. Doubting Thomas, hilariously, is described to have doubted
even though he was standing directly in front of him. He needed to put his
finger into Jesus' wound to convince himself. That's hard to believe, unless
Jesus somehow had a marked change in his appearance that prevented even his
apostles from recognizing him readily.

So I conclude that many of them didn't get it, and hence dropped out. Paul, in
contrast, got it in a rather different way, and taught things that evidently
didn't find the approval of those who had known Jesus. And at the end he shaped
Christianity in a major way. This means that the thing about Christianity that
you have to "get", has little to do with Jesus' appearance and teachings. Having
known and listened to Jesus may even have been counterproductive, at least from
the perspective of a post-Paulinian church.
