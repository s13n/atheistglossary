# What if?

People sometimes ask: What if you're wrong? What would you say when you die and
find yourself facing God?

Now, my immediate reaction is that I'm annoyed about the ignorance underlying
the question. I am annoyed that people don't realize how preposterous the
question is. It would become obvious if they would hypothetically replace their
own God in the question with some other imagined vindictive superpower they
don't believe in. If the prospect of them having to face this other superpower
after their death doesn't worry them, why should the prospect of facing their
God after my death worry me?

Clearly, the question in a way presupposes the existence, or at least the
plausible existence, of the God they believe in. The question doesn't make any
sense for Gods or other superhumans when they are entirely implausible. You'd
have to ask yourself millions of similar questions regarding all sorts of
hypothetical beings. That's a rabbit hole you would never get out of anymore.

Essentially, the question is pointless. It makes sense for people who already
believe in the existence of the God of their question, but it doesn't for
others. As an argument for the existence of God, it is circular.

But, of course, instead of just being annoyed, I could actually play along and
consider the question nevertheless. And indeed I have reflected on what I would
say in such a situation. In all honesty, I must admit that despite thinking
about it, I don't really know what I would say. Is there anything that would
make any sense to say? If Christianity is right, it would be pointless at that
moment to say anything, as my fate would already be sealed. So perhaps I would
just shut up.

At some point I thought I would ask God why his teachings are such crap. What
his point was in arranging things such that honest and diligent thinkers would
come to disbelieve, only to be shown wrong when it is too late. I figured that
if God had any sense of fairness he would actually give a useful answer. But I
came to realize the silliness of my thought. God had already shown he was unfair
by arranging it in this way, so why would I expect anything different once I'm
in front of him? Chances would be that he would just contradict himself once
more, leaving me none the wiser.

But, of course, it would not be inconceivable that I would be so dumbfounded or
awestruck or any combination thereof that I would be effectively unable to utter
anything coherent. So even thinking about it makes no sense. I may or may not
say something, who am I to know?

Let me offer this as a comparison: Say you would suddenly be confronted with a
fleet of Vogons appearing here in our solar system, announcing they would blow
up our planet Earth in a few minutes, to clear the path for a hyperspace bypass
that has been in planning for some decades. I trust you can look up what I'm
referring to, if you are unfamiliar with Douglas Adams' works. What would you
say? Oh shit? No please? What would there be to say?
