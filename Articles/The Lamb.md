# The Lamb

What is the difference between a goat and a lamb? When a goat is ritually
sacrificed as a scapegoat, we see it as negative. When a lamb is ritually
sacrificed for the same end, Christians regard it as "The Good News".

Isn't it about time to relegate this disgusting and long outdated imagery to the
dark and dusty cellars of history? Even Christians would be disgusted by
something like this, unless it concerns their messiah! There's a very strange
and peculiar schizophrenia about it, or call it a cognitive dissonance. The very
same thing that would raise people's hairs is regarded as the pinnacle of
ethical behavior when performed by Jesus!

The truth is that substitutionary atonement is a crazy concept that should have
been past its heyday during Jesus' lifetime already. Now, 2000 years later, it
still is being peddled by religious shitheads who haven't gotten around to think
about how grossly out of whack this is. Wouldn't it be about time we all finally
arrived in the present?

Look, if the Christian God can't find a better way to atone for the mistakes
humans make because they are humans, than to beget a son specifically in order
to simulate his killing, and present him as simultaneously a sacrifice and a
king and himself, pretending this stunt saves sinners from the punishment for
their sins, even though it actually doesn't, because they afterwards still can
end up in hell for them, well if he can't find a better way then he ought to
leave me out of this entirely. It isn't my fault, and I want nothing to do with
this convoluted shambles.

This whole thing reeks of very oldfashioned blood rituals, of cannibalism, of
magical thinking and twisted logic. We have moved on! Some may not yet have
realized it, but it is actually the 21st century now! We don't do blood rituals
anymore, and we prefer to have people pay for themselves, which is what we
consider fair.

I don't want to be held responsible for the fake self-sacrifice of a God for
dubious theatrical reasons! Had he asked me beforehand, I would have advised
against. I would have told him that he is more valuable alive than dead, and
that this kind of sacrifice wouldn't change anything for the better, fake or
not. And I feel vindicated by 2000 years of history since then. What exactly has
gotten better since the crucifixion? According to Christian teaching, I still
need to believe and repent in order to get saved from hell. Well, it wasn't much
different before, was it? In fact, it seems that the punishment for not
believing, or not believing properly, has become worse rather than better. So
where's the progress, the benefit?

In fact, things have started to get better eventually, after more than a
millennium of stagnation. Somehow, life conditions improved gradually as soon as
people unshackled themselves from Christian doctrinal domination, and started to
do actual science. What a peculiar coincidence, right? Just for a giggle,
compare the first 400 years of Christianity with the last 400 years. Which part
would you prefer to live in, and why?

Anyway, if you're a Christian and want to bring me the "Good News", pause for a
bit and think about what's good about it, and what's new. And think about how
people are bound to understand it when they aren't brainwashed into believing
that blood rituals are still a thing.

Sorry, this ended up as a rant. Never mind.
