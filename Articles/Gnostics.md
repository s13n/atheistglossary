# Gnostics

The Gnostics were a sect (or multiple sects) deriving from and related to
Christians. They don't play any significant role nowadays anymore, but they had
quite some influence throughout many centuries, and their teachings were
persecuted as heresies by the church. Why do I bother mentioning them here?
Because their teachings are quite interesting, particularly to show how the
Christian message can be, and has been, interpreted entirely differently from
what nowadays tends to be regarded as self evident.

Who is right? Well, of course the church you belong to is right, otherwise you
wouldn't be part of it, right?

Lets go through the essentials. The word "gnosis" is Greek for "knowledge". It
isn't how the sect would have called themselves, it was the "others", the (now)
orthodox Christians, called them, mainly to emphasize that orthodox Christian
belief was supposed to be about faith, whereas the Gnostics thought they had
superior knowledge that informed them what everything was about (life, the
universe and everything). And the Gnostics thought that the others had it all
wrong.

Today, gnosis is often associated with esoteric thinking, with people who
believe they are privileged to be in posession of some secret and superior
knowledge or insight, which puts them on another plane than normal people. I
find that as arrogant as offputting, and I would never be attracted by such a
view. In fact the very notion of secret knowledge being superior is patent
nonsense in my opinion. Knowledge gets created and improved in public, where as
many minds as possible have access to it, and can scrutinize it, add their ideas
to it, and interpret it in novel ways. If you think you have secret knowledge
you likely have ended up in an echo chamber of narcissists who think they are
above the "normal" people. The odds are that you dig yourself in there and your
thinking becomes hermetic.

It is, however, interesting how they viewed the world, and in particular their
differences from the Christianity we know. Perhaps the most striking difference
is that their God isn't the creator of the universe. The universe is the
creation of the Demiurge, a malevolent or at least incompetent deity that is
identified with the old testament God. The savior God, in contrast, is
identified with Jesus, or the father of Jesus. The two are antagonists.

This world view regards everything physical as part of creation and therefore
evil, a trap that keeps the divine in us in captivity. The salvation is
therefore a liberation of the divine within us from the body. I find such a view
emotionally relatable, given the background of suffering that must have been
prevalent in ancient times. People who experience the world as a whole as a
shitty place full of misery and pain, and who are looking for an explanation for
this sorry state of affairs, might well be drawn to such a view that dismisses
the entire physical world as evil, including one's own body. For people deeply
troubled by their existence, it may provide a ray of hope and aspiration, by
directing the attention to the divine spark within. But of course, it may
further estrange you from your own existence, your own physicality. That appears
like a road to schizophrenia to me.

This is a faith that redistributes the roles that we are used to from
Christianity. It doesn't need a devil, because it projects the dichotomy between
good and evil onto the duality between God and the Demiurge instead. The stark
personality difference between the old testament God and the new testament God,
which is a problem for ordinary Christians, is a feature, a confirmation of
Gnosticism. Salvation isn't the entry into heaven, it is the exit from physical
captivity. Judas isn't betraying Jesus, he helps him out of that captivity, and
is the only one among the disciples who has an inkling of the secret truth.

A lot of Christian religious tenets are reinterpreted, and sometimes turned on
their heads, by Gnosticism. It comes as no surprise that their teachinge were
opposed by the church fathers in antiquity, particularly since they called
themselves Christians, too. We have our disagreements what the "real"
Christianity is today, but the differences were much greater in antiquity. The
version or versions of Christianity we have ended up with today are not due to
their inherent truthfulness, or faithfulness to what Jesus taught. They are the
result of a power struggle that went on for centuries. Had the Gnostics won
instead, we would now regard their teachings with the same self-evidence and
normailty as we regard the orthodox Christian teachings now.
