# God's Image

I wonder what that actually is. I mean, this expression is used by Christians as
if they all know perfectly well what it means, and no explanation is ever given,
nor necessary.

But, of course, they don't give a clear explanation, because they don't know
either! Ever since this expression was written down in Genesis 1:27, people have
wondered what exactly it means. It is a mystery! And at the same time, it is
central to how Christians understand human nature!

The most direct interpretation would be, that God looks somewhat like us. The
Jewish image ban put aside, it would mean that depicting God as an old bearded
dude would be quite OK, at least as long as you're male. (Why bearded, you may
ask! Does he not want to shave? Surely, some angel would be at hand to have him
groomed! But, at second thought, you realize that he would want to avoid being
mistaken for a woman at all cost!)

But, of course, this must be seen from the other direction: We are a bit like
God! The point of the expression isn't to tell us what God looks like, it is
telling us something about ourselves! To put it bluntly, it is about human
self-glorification! Genesis tells us: You're special, even a bit divine! Who
wouldn't like to hear that?

Of course, Christianity promptly goes on to teach us that we're not like God at
all! We are failures, sinful by nature, in need of salvation! Like so often,
Christianity teaches us a direct contradiction, and tries to sell it to us as a
deep truth. It gives us a glimpse of the divine, it tickles our vanity just
enough to make us crave, and then withdraws the carrot and replaces it with the
stick.

To me that just makes the expression in Genesis 1:27 hollow and insidious. It
makes me feel tricked. I suspect that's exactly what it is there for.
