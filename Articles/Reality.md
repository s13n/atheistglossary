# Reality

Reality is supremely important for me. I strive to be in contact with reality,
and I find it embarrassing when it turns out I was fooling myself, I fell for an
illusion. I want to see things the way they are, not the way I would like them
to be, whether consciously or subconsciously. That's why I don't like
mind-altering drugs, for example, as I view them as a way to escape reality.

I understand that people might want to escape reality when the going is tough. I
can't guarantee that I'm immune. If at some point I would have to endure such
hardship, I might want to flee reality in some way. But so far that hasn't
happened, and I'm grateful for that.

Now, this presupposes that there is a reality, i.e. a common reality for
everybody, as opposed to everbody's own separate reality. I take this as a
given. We can all have different perceptions of it, but that doesn't change the
fact that there is a reality out there, that is there independently from me and
from what I might want out of it. I may be able to change that reality in a
small way, but for the largest part it won't budge for me, and it is going to be
me who has to adapt.

But that's not a problem, it is a great big adventure playground. There's so
much to find out, to investigate and understand, to try out and improve. There's
also some kind of spirituality associated with this, because there is this sense
of connectedness that stems from the fact that I'm part of the same reality,
that I'm made from and for that reality, i.e. that reality fits me like a glove.
Or was it the other way around? This is an immense source of confidence.

I can only guess that it is similar to what people feel when they feel connected
to their creator. I have heard people ask atheists how they can trust their own
mind when there's no creator. The argument seems to be that we can rely on our
mind because God made it so. That strikes me as a non-answer, i.e. as a way of
shutting down the question rather than answering it. I believe that I can rely
on my mind because it is the product of the very reality that I'm in and part
of. They fit each other, so why shouldn't I trust it? By striving to remain in
contact with reality, I maintain that fit as well as I can, and thereby uphold
trust. This is my faith, that me and reality are for and from each other.

You are and remain in contact with reality by engaging with it, interacting with
it. The more you pull out and engage with fantasies and fiction, the more you
are in danger of losing that contact, and I believe that you risk fooling
yourself. This doesn't mean that you shouldn't read fiction or watch movies, but
that you should prevent it from impairing your sense of reality.

In the ideal case such fiction can give you new ideas and a new perspective on
reality, and hence improve your contact with reality. I'm sure most of you will
have such an experience when reading, that strikes you as capturing something
really well. A poem that makes you feel as if the poet could read your heart. An
explanation that makes a lightbulb turn on in your head. A statement that makes
you think: Bingo! I couldn't have said this any better! I feel that such reading
is reality-enhancing.

But it can also be the opposite. You might be led down the garden path. And I'm
not talking about phantasy fiction, about openly phantastic stories. Those
actually enhance your perception of reality. Alice in Wonderland, for example,
is wonderful for sharpening your mind in this respect. Have you ever asked
yourself how a cat can disappear and her grin is still there? Thinking about it
sharpens your wits, and is fun! The problem is with stories that latch onto your
wishes and fears, and abuse them. They creep into your system and try to take
over. If you aren't attached to reality, they might succeed. I think the
religious teachings can be like that. If they manage to replace reality in your
mind, you're hooked.

I once held God for real. That was when I was a child and trusted my parents and
teachers. But I never had any success with interacting with God. The easter
bunny was more real than God, until I found out that the eggs were put there by
my mother. At some point years later it was clear to me that God wasn't part of
reality. It is something we imagine. Like people imagine demons, witches, magic,
and all those things that disappear when you don't believe in them. I was a bit
embarrassed that I had been fooled, and a bit proud that I had figured it out.

This is one of those experiences that I remember as empowering, as a step
towards becoming a grown up. I had figured out what many adults apparently
hadn't. I felt smart. Not overconfident, but smart. And I had fallen in love
with reality.
