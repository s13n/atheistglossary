# Dilemma

I have come to see the way people deal with a dilemma as a sign for their
morality. Many people seem to actively try to make one side of the dilemma
disappear. That makes the other side appear as the obvious and only solution.

I guess this has lots of advantages for people. They can attest righteousness to
themselves, can conveniently ignore the ambiguity, can display a principled
stance and can more easily dispell lingering doubts. But they effectively throw
the other side under the bus. I have written about this in the article about
abortion.

But as much as those people present themselves on the moral high horse, I
consider them immoral. They take the easy way out, the way that makes themselves
look better, at the expense of those involved in the dilemma. They are high on
principle and low on compassion, and prefer a "clean" solution over a balanced
one. They abuse the plight of others for their own elevation.

What do morals demand in the face of a dilemma? The first thing that comes to my
mind is to appreciate both sides of it, i.e. to look at the entirety of the
problem, instead of obfuscating or belittling one side in order to make the
solution look obvious, and thus effectively denying there is a dilemma at all.
This means accepting and sustaining the pain involved.

The second is that one should strive to reduce the severity where possible,
instead of adding even more weight to one of the sides, thereby driving up the
cost or sacrifice inherent in the dilemma. This may sound self-evident, but it
is surprising how often people associated with a dilemma abuse it to pressurize
other people, often those closest to them, or try to stack more guilt onto it,
trying to stress the affected person to the breaking point. This amounts to
maliciously abusing a weakness for personal gain, and is the polar opposite of
what a moral person would do. I find it even worse when people hide their
personal motive behind a screen of religious righteousness by claiming that they
represent God's will.

The third is to try looking forward and prevent such a dilemma from arising in
the first place. Often a dilemma presents itself because of a previous mistake
that can't be undone. Or even worse, the dilemma may be the result of a
deliberate move. Creating a dilemma for the opponent is a war tactic, and
therefore it is also part of war tactics to try to foresee an opportunity for
the opponent to create a dilemma, in order to forestall it, and thus avoid being
put into an awkward position. Personal relationships shouldn't be akin to war,
and the moral thing to do would be to help avoiding a dilemma and the awkward
situation it creates. But more often than one would wish, personal relationships
turn into opposition, and descend into warfare.

The fourth is to remember that you could be in such a situation yourself, where
you are in need of moral and emotional support in the face of a dilemma. Someone
who recognizes the awkward position you're in, and can share the burden. Someone
who actually tries to help instead of preaching. Someone who doesn't instruct
you but tries to understand you. Someone who joins you in the search for a way
out, for a compromise that minimizes the inevitable bruises.

Many Christians know this because they remind themselves of Jesus' example of
the adulterous woman he saved from getting stoned, as told in John's gospel.
This is the kind of moral behavior I am talking about. Jesus recognizes the
awkward situation and defuses it. But frequently people miss some important
points in this parable. They focus on the aspect of the people not being without
sin, and therefore in no position to throw the first stone. But let me probe
this a bit further:

Would it have been OK for sinful people to throw the second stone? Why didn't
Jesus himself throw the first stone? If what the woman did was so wrong, he
would have been justified in doing so, right? He could thus have done what many
people would regard as just, and at the same time allow everybody else to join
in. A hilarious catholic joke hints at that:

"Jesus had barely finished his sentence when a stone came flying from the back
of the crowd, hit the woman on the forehead and struck her dead. Jesus
complains: Oh Mother, you can be really annoying sometimes!"

But why didn't Jesus throw the first stone? Out of mercy? What has she done to
deserve mercy? If this is supposed to be a good example for everybody to follow,
it would mean to make exceptions from rules, out of compassion for someone in
trouble, to defuse a dilemma someone is suffering, thereby taking the rules with
a grain of salt. In such cases, you can achieve more by not following the rules
than by sticking to them. I wish more people would understand that this can be
the moral thing to do. And to do it without climbing the moral high ground.
