# Faith

Faith is a kind of belief that is extra strong, even unquestioning. The
distinction between faith and belief isn't even there in some other languages,
such as German. And it most certainly is gradual. When does belief become faith?
I guess when you put some kind of unquestioning confidence or trust in it. When
you don't spend even a moment's thought on whether it could be false.

Usually, when I sit on a chair, I don't spend a thought on whether it might
break down. Except when it appears wonky from the outset. You could say I have
faith in chairs. Why? Because my experience with chairs has generally been good,
so I have no reason for concern when using one. Chairs have behaved predictably
so far, notwithstanding a few instances of chairs that did break down underneath
me, and some instances of chairs I didn't trust from their appearance, so I
didn't want to sit on them.

So, while I wouldn't call my faith in chairs blind, I certainly trust them
enough to refrain from checking their condition every time I sit down.

In fact, we are living our lives surrounded by things that might break, people
that might misbehave, appearances that might deceive. It would be totally
debilitating to always doubt, and always check. If your expectations tend to get
frustrated very frequently, so that you basically can't rely on anything, you
can't live your life anymore. This is essentially what gaslighting is. We all
depend on events being generally predictable, and exceptions being infrequent
and not catastrophic. We need to be able to have faith in that. In this sense,
faith is necessary, and we all have faith to some extent.

This sort of faith is a result of experience. From our earliest age we learn how
the world around us behaves, and develop our faith accordingly. If you are
subject to some serious chaos and insecurity in a young age, you might get
damaged for life.

But this kind of faith is not the one religion talks about. Religion asks us to
have faith in something we have no experience with. We are faced with claims
that are somewhat arbitrary, originate in some ancient text, have no way to
verify them, and don't have an impact on our daily life. We are supposed to have
faith in order to improve our distant future. It is a dogma-based faith, and not
an experience-based faith.

I fail to see why such a dogma-based faith should be particularly virtuous. For
me, faith in general isn't virtuous, it is a practical necessity. But
questioning things every now and then is actually beneficial, so I would rather
say that it is virtuous to have doubts. You can't doubt everything all the time,
but you ought to doubt things every now and then, particularly those "truths"
that are being marketed as self-evident and indubitable. Ask yourself "how do we
know?" and you may end up with some deeper understanding.

Be especially wary of people trying to tell you that doubt is bad, and that you
are going to be punished for doubting this or that dogma. There is absolutely no
good reason to punish doubt. It rather ought to be recommended as leading to
insight! If someone commands faith and condemns doubt, he most probably has
something to hide, a hidden agenda that is less than charitable.
