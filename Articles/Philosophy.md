# Philosophy

There was a time when philosophy and theology was pretty much the same. People
took the existence of gods for granted, so the search for truth automatically
involved gods. That time has long passed, and philosophy separated from
theology. The difference is that theology continues to take the existence of
gods for granted, while philosophy treats this as one of the open questions that
deserve study.

Arguably, by doing so, philosophy maintained the connection with what we now
call science, particularly the natural sciences, while theology gradually got
into a more and more oppositional position vs. science. This is often denied,
but the actual debates and practical relationships show otherwise. Many people,
both theologians and philosophers, are discomfited by this rift, and try to
argue a conciliatory position. In their view, philosophy and religion really
work on the same problem, and can contribute alternative or complementary
viewpoints, thereby advancing knowledge better than each side alone.

I have read books and articles by several of those people, and I am unconvinced.
Those people seem to be driven by a desire, rather than actual evidence of a
tangible benefit. In other words, I believe they see - or like to see - things
as they want them to be, rather than as they are. They are following a project
of reconciliation rather than one of truth-finding.

I don't want to bash this. There's a time for the truth, and there's a time for
comfort. But I prefer to keep them apart, remain honest and accept the truth
where it leads me. And I expect this stance from philosophy. Putting the
existence of gods as a dogmatic given in front of all thought is not acceptable
for me.

For me, philosophy should be concerned with reality. In this sense, it is an
empirical science, because when its results disagree with reality, reality wins.
Therefore, philosophy has an obligation to take in the results of the other
sciences, like physics, biology, geology/cosmology, mathematics, information
science and so on. I have little patience with philosophers who ignore the
scientific results of the last 100 to 150 years, and carry on believing they can
look at the world through pure, detached thinking. If you could only draw one
lesson from sciencific research of the last century, then it would be that
reality is immeasurably weirder than even the brightest thinkers could have
foreseen. This should humble every philosopher, and remind him that thinking
about the world starts with looking at it.

For example, I think that the notion of a deterministic universe is totally
indefensible by now. Quantum level processes are inherently random, and they
generate information. This may seem contradictory to some, but it isn't. For
example, when a random event creates a particle that flies off towards you, you
can detect the arriving particle, which conveys information to you of the event
that created it. Producing information doesn't need a mind, it happens randomly
all the time. Information can also be stored and accumulated, in a natural,
automatic process that doesn't need a mind, either.

It turns out that information is a fundamental aspect of nature that only
relatively recently has acquired a central position in the scientific landscape.
This wouldn't have happened without the emergence of communication and computing
technology. The general feeling is that we are walking through an open door to a
large new area of knowledge that we hadn't appreciated before. Witness the
relatively new quantum computing technology, which uses insights from quantum
mechanics for processing information.

This is a challenge for all philosophy. If you are a philosopher, you head for
obsolescence if you ignore such major insights. At any rate, those new insights
are a much richer source for philosophical study, than the n-th resumption of
tired old theological arguments.
