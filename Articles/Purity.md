# Purity

Some religious people are obsessed with purity. They seem to regard purity as a
virtue, but why? What are the benefits that make it virtuous?

I want to approach this from a pragmatic angle rather than a moral one. I try to
look at the role purity plays elsewhere in nature, in order to get a feeling how
"natural" purity is.

As a disclaimer, I want to admit upfront that I'm an engineer, a technically
minded person, so my examples will come from technology. But I regard technology
as a part of nature, i.e. of reality.

If you look for purity in nature, you'll inevitably think about a mountain
spring, about a clear crystal, about fresh white snow, clear and clean air
affording a wide view from a mountaintop, and similar motives. But you'll be
surprised how impure those are when you look at it in detail. Even more
importantly, you will be surprised how important **impurity** is for just about
anything that's in existence.

For example, have you ever tried to drink purified water? If you have, you know
how bland it tastes. For water to taste good, it needs some minerals and salts
in it. In other words, there need to be some impurities. It is the same nearly
everywhere in nature. Crystals shine in much more interesting colors when there
are impurities. Air is a mixture of gases and hence impure by definition, even
when we perceive it as "clean".

In technology, impurities are what makes things interesting. Pure iron is not
very useful. Practical steel contains impurities in the form of carbon, and
often further metals like chromium or nickel. The interesting and useful
properties of many materials come about because of impurities, or by creating
mixtures. The entire field of electronics with all its myriad uses depends
crucially on impurities. A pure silicon crystal is pretty useless. Its
interesting functionality comes about through the controlled introduction of
impurities, which give rise to transistors and diodes, which make up the
functionality we find in smartphones, computers and all other products that
depend on semiconductor chips for their functioning.

You see: It is all about impurity. That's where the action is. That's where the
great benefits are found.

Even in biology, it is all about impurity. If it was about purity, we wouldn't
have sexual reproduction in nature. Sexual reproduction basically enforces
impurity, because it only works when two separate organisms combine to
contribute their DNA, rather than asexual reproduction where purity is
maintained by passing on the full set of DNA from one parent to all offspring.
It strikes me as downright ironic when humans, who reproduce sexually, insist on
purity. It seems to me they are missing the entire point.

Now, that doesn't mean that I embrace the opposite view that you can't have
enough impurity. That would be throwing out the kid with the bathwater. In order
for impurity to work its magic, there needs to be some amount of purity as a
starting point. Thus, purity isn't entirely useless. Purity has the role of a
precondition, it is akin to a raw material, from which you start when doing
something interesting. But it isn't an end in itself, let alone a virtue.
There's absolutely no reason to elevate purity to the status some religions want
it to have.

If you look at it, the reason why religions obsess on purity, is that they want
control. Sexuality, in particular, is something that appears hard to control,
so they try to rein it in. This is essentially unnatural, and can be rather
damaging when overdone. When girls are addressed, it can also perpetuate a
patriarchal standard that submits women under the domination of some man.

The upshot is that purity isn't a virtue. You don't owe it to anyone. By itself,
it is useless, since it is merely a precondition for impurity to work its magic.
This is apparent in technology and nature alike. We're all special and
individual mixtures, with our individual signature of impurities, and that's how
it should be.
