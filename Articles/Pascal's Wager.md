# Pascal's Wager

Blaise Pascal was a French mathematician and philosopher who lived a rather
short life in the 17th century. His now famous wager appears in his book
"Pensées", which means "Thoughts". It was only published posthumously from
fragments that he prepared. He died before he was 40 and was unable to finish
his work on the book.

I tell this because I find it utterly unfortunate that this outstanding thinker
is nowadays best remembered for his worst achievement, his wager. I trust you
know what it is about. If not, I trust you find good descriptions on the
internet.

I don't think he deserved this bad fame. I'm not even sure he would have come up
with his wager, had he not been painfully ill for much of his short life, an
illness that may well have been curable in today's world, given the medical
achievements we have seen in the past 400 years. For an intellectual caliber
like him, the wager is an embarrassment. It does a disservice to himself, to his
religion, and to his god.

Many people realize instinctively that there's something wrong with the wager,
even when they can't immediately tell why. It just appears cunning, cold,
calculated. And it is. Pascal tries to reduce the question of faith to a
question of calculating the odds. And the way he stacks the odds gives the
intention away immediately. For an atheist like me it feels like he wanted to
insult my intelligence.

The most insulting ruse is, of course, the gratuitous introduction of an
infinite. It is like a nigerian prince scam, only that the alleged prince
promises you an infinite amount of payback. The trick is otherwise the same: You
are offered a fantastic, but remote reward, if you pay your contribution in
advance. But you are offered no security, and if you were honest to yourself,
you would realize that the reward is unlikely to ever materialize. Does the
infinite promise make up for that? You don't seriously believe that, do you?

I also feel insulted because the argument appears to assume that in matters like
faith, I can be lured by promises of personal advantage. The argument
presupposes that I'm selfish and corrupt, that I don't have ethical values, and
that I orient my beliefs according to pure self interest. Maybe Pascal believed
that this is what atheists are like, but it is an insult nonetheless.

Perhaps most importantly, however, the wager paints the Christian God in a very
unfavorable light. Honestly, if this sort of calculated bet convinces God to
admit me into heaven, I don't want to be there. This is absolutely the wrong
reason for anyone to get admitted into heaven, if Christianity is worth
anything. I don't want to be associated with such a God.

And, of course, the argument plays with the fear of hell. This is another
damning verdict for the Christian religion, when hardly any argument for it can
do without a reference to this sword of Damocles. It is a visible reminder that
Christians don't trust their own positive message, when they think they can't do
without even a tiny hint of eternal torture. It shows what Christianity really
is: A death cult, and what really drives Christians: Fear.

Maybe I should welcome Pascal's wager because it exposes this apologetic train
wreck to those who are still able to think for themselves. But I can't. I feel
sorry for the man. He deserves a better legacy, even though he himself is beyond
pain and fear, for more than 360 years now.
