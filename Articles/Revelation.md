# Revelation

One of the most hilarious aspects of many religions is how the most important
information that God allegedly wants everybody to know and follow, is revealed
in private to a select individual who then has the job of educating the rest of
the world about it. The pattern repeats itself: Whether you are talking about
Moses or Mohammed, about John Smith or David Koresh, they all have their
revelations in private, in a setting that makes it impossible to independently
verify what happened, so that people are left with the choice between believing
them blindly, or not.

Wouldn't you agree that this is a very dubious state of affairs for messages
that claim such an immense importance for everybody's life? The highest truths
are coming upon us in a way that is practically indistinguishable from lies and
fraud! Surely that's supposed to tell you something!

What's more, I have yet to meet someone who treats all prophets and their
revelations with the same respect and reverence. Every believer practically
chooses from all available prophecies the ones he's prepared to believe. People
just don't give credence to Mohammed and Smith at the same time! But why? What
makes one prophet more trustworthy than the other? What makes one revelation
more credible than the other? They both talk about similar things, they even
simulate each other's language and style, they claim a similarly miraculous
setting for the revelations, and funnily enough they often claim that they are
the last one, the final prophet. So if you believe in one of them, why not
believe in all of them?

I take the much simpler route of not believing in any of them. Im my view, those
revelations don't just look like frauds, they are frauds. As much as people may
convince themselves that God had spoken to them, I don't follow. They may just
be mistaken or full of shit, I don't care. A revelation that can't be
substantiated, and shown to not be erroneous, can be summarily ignored. A God
who has something important to say ought to be able to find a better way.

Think about what the implicit message is in such private revelations: The
prophet tries to tell you that you weren't worthy of receiving the divine
message directly. Only he, the prophet, was deemed worthy, meaning that God for
some reason selected him over you to be the one to speak to. And you have to
believe what the prophet tells you about it, because there's no way to check.
How is that any different from somebody with an inflated ego abusing divinity to
elevate himself to a dominant position?

I propose everybody who claims to have privately received divine messages, which
he claims he must inform others of, should be tarred and feathered and driven
out of town. He doesn't deserve the reverence of a holy man, but the treatment
of a scoundrel.
