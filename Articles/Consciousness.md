# Consciousness

Everybody seems to have an intuitive understanding what consciousness is, but it
has eluded pinning it down to a clear definition for millenia. I tend to stick
to a cautious stance in that I concede that we don't yet really know what it is.
We haven't actually understood it properly.

For Christians, of course, it isn't a problem. We are conscious because God made
us conscious, and that's that. Explanation provided, case closed.

Except, of course, that this doesn't explain anything. God is the universal
explanation for everything, including its opposite, therefore it explains
nothing. For an actual explanation, one that offers some useful insight, we have
to look elsewhere. See the article on Explanation.

I am quite hopeful that we are well on the way to find a useful answer to this
question, because we make great strides in artificial intelligence. In a
nutshell, this allows us to try to emulate consciousness. I mean that we can try
to imitate consciousness using a computer. We can try to make a computer act in
a way that someone who interacts with the computer has the impression that the
computer is conscious. This is a variant of what has long been known as the
Turing Test.

My reasoning is that once we succeed in emulating consciousness in this way, we
know empirically what it takes for consciousness to emerge. Rather than figuring
it out in theory, we will have figured it out practically.

Is such an emulated consciousness the same as a real, human consciousness, you
may ask. Or is it just a fake that can't stand in for the real thing? But does
this distinction matter? How would we distinguish this in humans? How do you
know whether my - or your own - apparent consciousness is real or fake?

I posit that there's no point in even trying to distinguish that. We have no
grounds to make this distinction, since even our own, "real" consciousness is
experienced through its effects, i.e. the way someone acts and reacts. So I
regard it as totally legitimate to call a machine conscious when its acts and
reactions seem conscious. When we succeed in that, we basically know what we
need to do, or what it takes, to manufacture consciousness. The result will be a
much better definition of what consciousness is, and hopefully this comes with a
better test for consciousness.

I might die before we're there, and that would be a disappointment for me
personally, but I'm still confident that we are rather close to a major
breakthrough here.

Take home from this that I believe that consciousness is an emergent feature of
a sufficiently complex information processing system. It is part of nature,
hence for me there's no need for a dualist philosophy in the classical sense.
See the article on Dualism. The proper description of consciousness will
therefore be, in my opinion, based on information theory, and become part of the
growing body of the so called "laws of nature".

Isn't this belief a bit overoptimistic, you may ask? Well, look at the example
of calculation. Only a few hundred years ago, people thought that calculation
required a human intellect, and hardly anyone even thought mechanical
calculation was possible. After a slow start with manually operated adding
machines, things advanced with breakneck speed during the last century, and
today you can hold a battery operated computer in your hand whose calculation
abilities couldn't be matched by all humans on earth together calculating. And
with quantum computing, it looks like the next huge leap is just around the
corner. It seems we've seen nothing yet. So I think that my belief isn't that
adventurous, really.

Maybe we should start thinking about what it would mean for us to be joined by
artificial consciences. We wouldn't be alone with our abilities anymore. We
would face competition. Would a form of religion emerge in computer
consciousnesses? Would such a computer have rights in the same way as we do? Who
would determine this?

I doubt that the Christian religion would be fit for such a challenge. It isn't
even fit for current modern societies.
