# Personal Relationship with God

Have you got a personal relationship with your God? What kind of? Is it a kind
of friendship? Would you say it is built on mutual trust? Is it between equals?
If not, is it like between father and son (or daughter)? A child son or an adult
son? Is it like between a boss and a worker? Is one dominant and the other
submissive? Is it a relaxed relationship or is there tension or stress? Do you
feel you have to deliver, to fulfill expectations? Do you have expectations
yourself? Do you fear a breakup, a rejection or a punishment?

I'm asking because I hear and read a lot about people claiming to have a
personal relationship with God, but I hardly ever have it explained, or get
described credibly what it is like. Even when I was a schoolboy, I was puzzled
about it, as I couldn't figure out what this was supposed to mean. I was to
treat God as my father in heaven, but quite clearly I couldn't have a
relationship with him that was anything like the relationship with my real
father. What did they actually mean?

Today, the concept of a personal relationship with God seems perverse to me. A
totally impossible idea. Something that is guaranteed to mess with people's
minds, to bamboozle them and gaslight them.

How do you establish personal relationships? By starting out needy, creeping up
to the other, ready to do everything to acquire his favor? By assuming the
submissive position? By praying to him and begging him for signs and favors? By
devoting your life to him? What kind of personal relationship is that? It is
totally defunct! It doesn't and can't work. You are basically trying to get into
a relationship by negating yourself, by suppressing and removing yourself from
the equation. You expect the other to enter into a relationship with a nothing.
You effectively invite the other to treat you like nothing, to take advantage of
you and abuse you.

People sometimes make this mistake when deeply in love with someone. They
sacrifice themselves for it, and one of the toxic tenets of Christianity is to
treat this stance as a virtue, where in reality it leads to disaster. This isn't
the highest manifestation of love, it isn't even love! Submission in a
relationship can be fun when you *play* it, but not when you *live* it.

If God is supposed to be your friend or a person of mutual trust, he can't be
the omnipotent ruler and judge. Imagine you have a personal friendship with your
top boss. What if he fires you? Would you expect him not to fire you because of
your friendship? If so, you expect him to be partial and in your favor, not just
and impartial! That would be cronyism. But if you don't expect an advantage,
what are you expecting from the friendship? What would it mean if God was
equally friend with everyone else? Would you still expect him to send some of
them into the lake of fire at the end?

If you know anything about personal relationships, you realize that such a
combination of roles and properties makes a personal relationship with God a
pipe dream. It means fooling yourself.

But that's partly a consequence of Christianity's ambiguous depiction of God,
who is supposed to be the Father and the Son, the omnipotent and omniscient
guardian and enforcer and simultaneously meek and mild Jesus. The friend *and*
the boss. You've got to love him with all your heart and fear him at the same
time. It doesn't make any sense, but at least you can always pick what suits you
at the time.

With this realization, I came to prefer real relationships with real people.

Your personal friend should not be your judge, and your judge should not be your
personal friend. Your personal friend should not issue commands to you, and you
shouldn't pray to him. Your personal friend should not be your boss, or creator,
or master, or owner. Friendship should be at the same level, or it isn't - or
will soon cease to be - friendship. Your friend is welcome to give advice to
you, and you are free to disregard it. Your friend is welcome to give solace and
support to you when you need it, and he should leave you alone when you don't.
And he should never threaten you.

Nothing of that applies to God and can possibly work with God, not even when you
imagine him as meek and mild Jesus.
