# 9/11

You may wonder what an article about 9/11, the terrorist attacks of Sept. 11,
2001, has to do with atheism, i.e. why it belongs in here. But it actually is
quite central to the topic. Many atheists have come out of the closet as a
result of those attacks, in fact the whole "new atheism movement" was in some
way triggered by them. There's nothing new about the atheism, but the terrorist
attacks cast a sharp spotlight on the dangers of religious fanaticism, and
indeed religion in general, so that the "new atheists" felt that something
needed to be done.

In fact, the situation is even worse today than back in 2001, and something
still needs to be done, only much more urgently.

I still remember the day of the attacks. Europe is 6 hours ahead of New York
City, so the news reached me in the afternoon while at work. Soon, there was no
way of thinking about work anymore, and I left. I had an hour's drive back home,
during which I listened to the radio and was in thought about what had happened.
I still remember clearly how I came to be worried about how the US would react.
I had a distinct hunch that the reaction would make things worse. That the
attackers effectively wanted to lure the US into a reaction that would end up
putting the world in jeopardy, and that the US would take the bait.

In the following months I thought I had reason for hoping that it wouldn't
happen. I thought that fighting in Afghanistan against the Taliban who harbored
the terrorists was a measured reaction, and I was content with the fact that
NATO as a whole participated. My frustration with that particular operation came
later. But in 2003, when the US invaded Iraq on a false pretext, I saw my
worries fulfilled. This wasn't a measured and proportional reaction anymore, it
was abusing the prevalent sentiment for a different cause. And religion played
an important part, on both sides.

We all know what happened. After the 2001 attacks, George W. Bush, the POTUS at
the time, a born again evangelical Christian, declared the "war on terror" that
carries on to the present day. He also alluded to the crusades, which made clear
what the mental framework was that governed his politics. It made clear that the
aim wasn't justice, but retaliation. It wasn't about finding those responsible
for the crimes, and putting them in front of a court. It was about using this as
a pretext for fighting a much more broadly defined enemy. Or, rather, an
undefined enemy.

If you don't know who your enemy is, you don't know when you've won, or lost.
The war goes on forever. And the danger is that it creates its enemies, that it
feeds on itself. Declaring the war on terror set this dynamic in motion, and the
US hasn't found a way out ever since.

It also has shown the world at large, that the US isn't about the rule of law,
i.e. about justice. Not only has the US lied in order to justify the Iraq
invasion, it has pressurized prospective allies and mucked about the entire UN.
At a time when it would have been wise to listen to advice, the US thought that
they were on a righteous path, and whoever didn't agree was part of the problem.
The US was hurt and needed to beat somebody up, whether it was right or not. As
I write this, the same pattern repeats itself between Israel and the Hamas.

Now, I'm certainly not going to defend Hamas or Saddam Hussein! To depose Saddam
was justified even though he didn't have the weapons of mass destruction that
the US claimed he had. But it wasn't worth the bloodshed, it didn't justify all
the lies, and it couldn't hide that the real reason for the US to engage in this
war wasn't Saddam's overthrowing. The US had collaborated with his ilk on many
occasions. At best, this was a geostrategic war. And waging such a geostrategic
war on a pretense of justice is a sham. Everybody knows it is.

The curse of the evil deed is that it carries on searching for a justification,
and the justification gradually expands into what now is often described as a
war of civilizations. Osama Bin Laden would have been very proud of himself!

So what used to be a war against terror morphs into a war along religious fault
lines, because pretty much everyone seems to equate a civilization with its
predominant religion. And not just religions, also denominations are a factor.
In Russia, the Orthodox Christian church imagines Russia as a civilization all
of its own, and as in combat with "The West". Apparently, more and more
"players" on the international stage are hell bound to battle out their idea of
"civilization" using military force. Which could mean that there won't be much
civilization left when the dust settles again.

It annoys me no end that despite not having a horse in that mad race, atheists
are almost universally being drawn into the battle. I have zero interest in
fighting battles on behalf of a religiously defined "civilization", not least
because the ideals I would find worth fighting for are almost guaranteed to not
be those the religious actors are fighting for! My idea of civilization revolves
around human rights, individual freedom, fact-based and levelheaded discourse
and social justice. The religious actors I see leading the discourse are rather
more interested in dominance, everything and everyone else be damned.

This is the road to hell. We urgently need to remember what we are actually
standing for, what the basic tenets of our civilization are. It isn't, and can't
be, a God of whatever ilk. What happened to democracy?
