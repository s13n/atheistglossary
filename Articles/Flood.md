# Flood

When I was a child, and taught about Christianity and the bible, of course I
also was taught the story of Noah's ark and the flood. Perhaps not accidentally,
it was taught to me at a young enough age where I wasn't yet able to realize
that it couldn't have happened in this way. But I still remember vaguely that I
found it somewhat odd that God even drowned all the animals, except for those
who found refuge in the ark. Why? What had they done wrong? I still remember
that the teacher answered something like that it didn't matter, since they
weren't all killed, the animals were growing back thereafter. And there I was
trying to grapple with an answer that somehow felt wrong, even though I wasn't
able to articulate it.

From there on, each time I came back to the story, it felt wrong in even more
ways that I was aware before. Today, I am totally at ease with it because I know
it is legendary and probably never was intended to be taken literally. But this
leaves the question what it actually wants to tell us. Is there a moral of the
story, and if so what is it?

Note that I don't even start to argue against the literal interpretation of the
story. If you believe that the flood story was literally true, you are beyond a
point where I could reach you with any sort of argument. You must have
eradicated your critical faculties entirely, because it is obviously wrong on so
many levels, yet you still cling to it.

The question I'm asking is why would God almost, but not totally eradicate life
on earth, when he could just as well have started over. Hadn't he created
everything in six days, out of which living beings only took a fraction? If it
is so easy, why not do it again, starting from a clean slate?

Or, to put it in another way, what was the worth of those individuals that were
preserved in the ark? Why were they worth enough to warrant the whole effort of
building and maintaining the ark? I mean, look at Noah, his character as
described in the bible. He drank, and he cursed the wrong son for it. Not what
you would expect from a man whose character was so exemplary that he was the
single human being worthy of being saved from perishing in the flood! And if you
take a step back or two, you come to think that, seen as a whole, things were
just as bad after Noah as they were before. People were just as wicked
thereafter as before, thereby making any thought of an improvement moot. When
seen from this angle, the flood was a failure!

Interestingly, when you look at it with evolution in mind, it looks entirely
different! In evolution, when you eradicate all lifeforms, you essentially wind
back the clock all the way, and evolution would have to start from before
abiogenesis. But if you only eradicate most individuals, keeping just enough
that they can procreate, you essentially keep all the information evolution has
accumulated over time, and start from there. This kind of thing has happened in
history on earth. Perhaps most well known is the eradication of the dinosaurs as
a result of the impact of a large meteor some 66 million years ago. It was a
mass extinction event, but some species survived, keeping their associated
genome, and providing the starting point for further evolution.

Could it be, I wonder, that people in the bronze age had a hunch that you needed
to preserve some individuals to allow further development to occur? It would be
in contradiction with the Genesis creation account, but that might not have been
so apparent to people back then as it is to us today! Does the story of the ark
and the flood contain the nucleus of an evolutionist world view?

From this viewpoint, the moral of the flood story could be this: You can kill
everything and everybody, but if you want things to continue thereafter you need
to preserve a few individuals of different species. When they can procreate,
they can multiply and quickly populate the land again. But if they can't
procreate, they'll be gone forever. And this applies to each species separately.
If you only keep a few dogs, you won't get any horses afterwards. Not any time
soon, anyway.

That's not exactly evolution as we understand it today, with mutation and
selection and all. But it casts a bright light on the fact that life and traits
are being passed on from one generation to the next, so individuals that can
procreate are able to pass on the baton. And potentially a few of them are
enough to start. But it doesn't work from nothing. Hence an individual has an
inherent worth. Today, we would say it's in the genes, but back then people
didn't know that.
