# Satan

I really don't get how people can take Satan for real. I mean, think about the
story, and see if it makes any sense! Where does Satan come from, and why does
he have the position he has in Christian doctrine?

Satan, so it is believed, is a fallen angel who rebelled against God and now
tries to tempt people and reigns over hell.

That's obvious bullshit. What's a fallen angel, to start with? What's the point
of having angels at all when they are just as fallible as ordinary people? What
sets them apart from the rest of us?

Doesn't this mean that even in the afterlife there's opposition, even rebellion?
Why? I mean, isn't the afterlife what comes after judgment, when this should all
have been dealt with already, and people are segregated into good and bad for
the rest of eternity?

And why does God tolerate this? God is supposed to be more powerful than Satan,
but for some reason he keeps him around rather than conquering him once for all.
As if he had a soft spot for him.

Some people say that God keeps Satan around in order to have him as a bad
example to teach people. That's even greater bullshit. We don't actually see
anything of that bad example, except what religion tries to make us believe.
That's not an example, that's propaganda.

No, this is too transparent a ruse to be fooled by. God and Satan are just
needed to personify good and evil. If you want a perfectly good God, you need a
perfectly bad Satan to oppose him. If you want the good to win over the bad, you
need God to be stronger than Satan. You need a bad cop to go with the good cop.
You need a stick to go with the carrot.

This is all the result of human wishful thinking. Astonishing why people don't
see right through it!

I'm bored by this. How can you take people seriously who honestly believe such
nonsense?
