# Suffering

For as long as people were able to think, they presumably wondered why they had
to suffer. This is a very basic and universal phenomenon of our human experience
or our human condition. No wonder several ways of explaining it can be found in
our earliest written sources. It definitely precedes the bible, i.e. the bible
is most certainly not the first, and not the last of it.

People of course noticed that life tended to begin and end in pain, making pain
and suffering a kind of bracket that framed, and to a certain extent defined,
life. So one obvious way of dealing with it is to accept it as an inevitable
component of life, which means that it is best for you to embrace it and
integrate it into your life. It is not necessarily easy, since your natural
reaction is to avoid hurt and pain. Indeed, that's why evolution came up with
pain in the first place: As an incentive to avoid what endangers your life and
impairs your ability to pass on your genes. Pain has a biological function, and
it depends on your wanting to avoid it. If you didn't experience pain from
hurting yourself, you would hurt yourself too often and die as a consequence. It
is a mechanism that is much more basic than reason and consciousness, and
predates it in evolutionary development. Thus, it works regardless of reason and
thinking.

But here we have a contradiction already: If life is laced with, and even
defined by, pain and suffering, why do we want to avoid it? Or to put it the
other way around, why is something that we want to avoid so pervasive and
definitive in life?

For some people this meant that the creator of the world, who arranged things in
this way, could not have been benevolent. This is what the Gnostics thought, for
example. For them, we were all born into a shithole, to use a modern term, so
they summarily dismissed our earthly existence as wrecked, and the way out was
through enlightenment and rising above this earthly quagmire. Even where others
didn't assume such a malevolent demiurge, many believed in capricious Gods who
couldn't be relied upon, and were playing their tricks on us. This was quite
prevalent in ancient Greece. For them, we suffer because we are at the receiving
end of practical jokes by the Gods at our expense.

In some way this idea of capricious Gods most directly relate the behavior of
the Gods with the behavior of humans, i.e. they have the same defects of
character. And just like people can suffer from the actions of capricious humans
with power, they suffer from the actions of Gods with even more power.
Suffering, therefore, is just the consequence of being on a lower rank in the
pecking order, i.e. of your relative lack of power. Your lack of power means
that you have to endure what you can't avoid, but that you still have your wits,
your intelligence, that you can use to dodge the blows wherever possible.

The interesting consequence of this stance is that it empowers you. Neither does
it presume that you are suffering because of your own fault, e.g. your sinful
nature, nor does it leave you without an option other than endure it. You will
not be able to avoid *all* suffering, but you certainly can succeed in avoiding
*some* suffering! If you are clever and knowledgeable, you can use foresight to
avoid getting into bad situations, you can use knowledge to treat and cure
illnesses, or you can investigate the sources of suffering in order to find a
way around them. Suffering thus becomes a practical problem you can attempt
to solve! People are virtuous when they both are prepared to endure the
suffering they can't avoid, and able to circumvent the suffering they can
avoid. Suffering isn't a punishment, it is a challenge.

Look at the story of Ulysses, who personifies this notion. His odyssey shows
both his ability and willingness to endure the suffering the Gods inflicted on
him, as well as his shrewdness and prowess in dealing with the obstacles thrown
in his way. Compare this with the story of Job in the old testament, where the
idea is that you are at the mercy of God regardless what you do, and the best
you can do is to stay loyal to your God whatever happens. So while Ulysses is
shown virtuous because of his endurance and his prowess, Job is shown virtuous
because of his enduring loyalty. Note the crucial difference!

The Ulysses story tells you that there is no particular sense in your suffering,
since it may result from the incalculable whims of the Gods, who might want to
test you, or they might not, without you knowing. But you can do something about
it. You may not be able to avoid all of it, but the better you are at learning
and thinking and trying things, the better your chance to get around it. The Job
story tells you that whatever you do doesn't matter, since God follows his
impenetrable will regardless. Your only hope is to find God's approval through
your unwavering loyalty. This means that learning and thinking and trying out
things isn't going to help, and doesn't give you any advantage.

Now, I don't believe in any of those Gods, whether Greek or Jewish or Christian,
but I do find the Greek position a lot more convincing and likeable, especially
the empowering in it. Look at how much science, and in particular medicine, has
reduced suffering, after taking this empowerment as its mission. In comparison
to Ulysses, Job looks like a fool to me. From today's perspective, it seems very
clear to me that in this respect, the Greeks were right and the Jews were wrong.
And it turns out that Gods are entirely superfluous to that.
