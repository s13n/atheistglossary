# Law

You would hope that you don't need to explain to anybody what a Law is! But it
apparently is necessary, since people seem to confuse different usages of the
word. So forgive me if I seemingly explain the obvious, but perhaps you might
still draw some benefit from it.

The conventional meaning of the word is that it is a set of rules for human
behavior in a society. Laws usually come with regulations for how to enforce
compliance, too. And there is a system of justice surrounding those laws, which
has different roles for different people in a process. Enforcing a law is not
the job of everyone, there is the police, or even different kinds of police, for
that job. It is easy to see why it is so, when you imagine a situation where
every hillbilly with a gun could enforce laws as he sees fit.

Note that those laws are invariably a human creation. In an authoritarian
setting it might be a single person that devised the law, but we know for a long
time now that it is better to involve a larger number of people, and implement a
law-giving process that allows for negotiating conflicts of interest, and for
participation of all parties concerned. This is what parliaments are for, and
elections for those parliaments, rather than having self-appointed dictators or
kings.

The so-called laws of nature, or scientifical laws, aren't really laws. They are
a misnomer. There is no system in place to enforce them, and there is no
authority to devise them. They merely represent some kind of consensus amongst
scientists, and they don't bind anybody. You may disagree with them, but you may
have to accept being ridiculed, unless you are clever. If you manage to defeat
one of them successfully, you're not jailed, you're hailed! A natural law is a
formulation of some fact of nature, i.e. some behavior that nature seems to obey
in general. The fact itself tends to stay, but the formulation of the law might
change, as a consequence of our increased understanding. For example the fact
that objects fall to the ground when released has been explained in various
different ways over time. The explanations have changed fundamentally, but the
fact remains the same. The "law" may seem to change, but this only reflects
increased knowledge, it doesn't imply changing facts.

So nobody "devises" a scientific law. At most, they are proposed, and debated,
and in the best case they are accepted by a wide consensus after some
considerable time. And if you think you have valid objections, you can put them
forward freely. You might lose your reputation, or even your job, if you make an
ass of yourself in the process. There's nothing wrong with that.

If you think that you can counter with God's law, which allegedly doesn't
change, think again. It is mere propaganda that God's law doesn't change. If you
look at religious history, you realize that the allegedly unchanging law of God
is being interpreted in ever changing ways. So much so that it becomes
questionable what the unchanging core actually is. And it not only changes over
time, it is also different between different creeds and denominations, and
groups of adherents. So the alleged laws of God are a false claim, they have
dubious origin, dubious justification, and are often self-contradictory or vague
to the extent of being useless in practice.

The Ten Commandments tend to get mentioned in this context (see my respective
article). But they only show their uselessness in practice. Hence, and
hilariously, Moses can go on to make a mockery out of them right after he has
received them from God. With God's approval! Or what else should I think of his
ordering of the killing of thousands of his followers for a crime his brother
led? And of his appointment of his brother to the post of high priest
thereafter? Clearly, the law here is Moses. The words of the commandments don't
really matter - he shatters the tablets! We're seeing a tyrant making his own
laws, pretending that he is acting on behalf of God. Why aren't more people
seeing through this extremely cheap trick?
