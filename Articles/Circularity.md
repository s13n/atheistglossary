# Circularity

A circular argument, simply enough, is an argument that has its conclusion
amongst its premises. It is a fallacy, of course, but it is quite astonishing
how often people just don't notice that their argument is in fact circular. It
seems to me that you need to have some amount of intelligence, and alertness, to
spot it. Or, maybe, it is a matter of training, I don't know.

Circular arguments are frequent in apologetics. An example is the way miracles
are explained. Many Christians would try to tell you that the miracles Jesus
performed are evidence for his divinity. Is this argument circular? I'd say yes!
To talk about miracles presupposes the existence of the supernatural. It is in
the definition of the term "miracle". Without the supernatural there is no
miracle. You can't start with the premise that there is no supernatural, and
then accept the notion of a miracle. It would be a contradiction.

Hold on, I hear you say. Doesn't a miracle show that the supernatural must
exist? Well, no, because one wouldn't call it a miracle, one would call it an
instance of an unexplained phenomenon. I hope you won't assert that everything
you can't explain must be a miracle! I trust you would recognize that this would
be ridiculous.

The problem is in how you distinguish between the things you don't know, and
those that are impossible to explain in a conventional way. When you say that
some occurrence is a miracle, then you effectively assert that there can't be an
ordinary explanation. Not just that you don't know of an ordinary explanation,
but that there can't be one! I guess you'd agree that as long as an ordinary
explanation is possible, the claim of a miracle is at least premature!

If that is so, how can you assert with confidence that an ordinary explanation
is impossible? You'd rather overplay your hand here, right? You'd effectively
have to claim that you know as much as necessary to declare an ordinary
explanation to be impossible, and that's one heck of a claim! I'd perhaps buy
that an ordinary explanation looks unlikely, but that's still a long way from
knowing that it is impossible.

Having driven home that point, maybe you can now acknowledge that miracles
aren't evidence for the divinity of Jesus for the simple reason that miracles
presuppose divinity. The argument is circular. If you don't accept the premise,
you simply conclude that Jesus did a few unexplained things. It may not be
possible anymore to reconstruct what actually happened, but there would be no
compelling reason to assume that it couldn't be explained in ordinary terms.

Now, I happen to think that this is one of the easier examples of circularity,
which is why I employed it here for the purpose of exposition. There are much
more insiduous and circuitous examples of circularity in apologetics. In my own
experience, however, it is much easier for the skeptic to spot them than for the
apologist. I have seen entire books written by apologists that end up being a
long winded circular argument for the existence of God. Once you've realized
that, the whole house of cards collapses, but the author doesn't see that. Maybe
that's because he doesn't want to see it.

In a jocular mood I once defined my own intelligence metric: I defined the
"Heinzmann Intelligence Circumference" as the shortest path length of a circular
argument that the subject is just unable to recognize as circular. If some
apologist has to write a book of hundreds of pages to conceal the fact from his
own mind, that his argument is circular, then this is a testament to his great
intelligence. Less intelligent people would need a lot fewer pages. Or a lot
fewer lines, even. As with the argument from miracles.
