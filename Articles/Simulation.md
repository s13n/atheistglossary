# Simulation

Lately I came across multiple people apparently fascinated by the possibility
that we're living in a simulation. It may be the movie series started by "The
Matrix" that kicked off this fad, but whatever, the question is intriguing
enough to also have fascinated me.

There is the argument that we likely live in a simulation, because there will
most certainly be God-like, higher intelligences, who have developed simulation
to such an extent that they can run a lot more simulations, even simultaneously,
than there are real worlds, so just from statistics we would most likely be in
one of those simulations rather than in a real world.

That certainly sounds plausible, but I'm not sure it makes sense. In order to
approach this, lets first talk about what a simulation actually is. As a
computer scientist, I have some experience with simulators. Those simulators are
computer programs that represent real world items and their behaviors and
interactions within the computer's storage, and whose code allows the processor
to animate those representations in a way that mimics their real-world behavior.

For example, there are electronic simulators where the real world items are
components with which electronic circuits are built. You know, transistors and
such. The simulator can exercise the circuit in such a way that its behavior can
be observed without building the circuit in the real world.

That's useful for several reasons. It allows you to look into aspects of the
behavior which would be very hard to observe in the real world. The electric
currents involved, and their changes over time, can be plotted on diagrams, and
analyzed in various ways, so that the behavior of an actual real-world
implementation can be predicted in advance with some confidence. In this way,
you can avoid having to build real prototypes until you have found a candidate
that's so promising that actually spending the effort to build it is justified.
You can also subject the cicuit to abuse that would likely destroy a real-world
exemplar, without having to write off the expense involved in making it. This
helps in making the circuit as robust as possible.

But here's a thought: Isn't a real-world prototype built with actual components
a kind of simulation, too? A prototype is typically constructed in a way that
facilitates observing its behavior in some detail. It is being subjected to some
abuse to check its robustness. It has this in common with a simulation.

The important difference seems to be that in a simulation, the components
themselves are rebuilt as a representation inside an entirely different system.
And they are rebuilt there only to the detail we're interested in. The
components are a simplification of a real component, which makes both simulation
simpler and the simulation result easier to interpret. But it also means that
reality can be much more complex, and the simulation isn't very accurate. As
with all simplifications, you might have simplified too much, and the result
becomes not just imprecise, but incorrect.

To summarize this point: The simulation is "indirect" in that a representation
of it is made within another system, and it is "simplified" because this
representation doesn't encompass every detail of the component. The indirection
allows observations that wouldn't be possible in reality, but this is afforded
by a greater effort, both in programming and in energy to operate the simulator.
And the simplification brings the effort into a range that is affordable.

If we should live within a simulation, we would be like the representation of
components whose behavior is being simulated. The higher intelligence would be
expending considerable resources to construct such representations, and run the
simulation, presumably because they would be interested in our behavior for some
reason. We would be a simplified version of what we could be if we were real.
The simplification would allow them to more easily come to the result they
desire, i.e. the aspect of our behavior they'd like to find out about.

The question that springs to my mind is what would they be interested to find
out in this way? You don't expend those resources for nothing. It could be some
kind of game, which they play for fun, like we play computer games, which in
most cases are simulations, too. But it would still be a simplification.

If we were in such a simulation, would there be a way to know it? I don't think
so. The artificial world that is built inside the simulator is the entire world
for us. We can only perceive what is part of the simulation.

However, if we allow outside interference with the simulation, i.e. the superior
intelligence changes data midway in the simulation, what would that look like
for the simulated beings? Obviously that depends on what exactly is changed, but
most likely it would look nonsensical. Of course you could resurrect somebody
who just died, reintroduce an intact piece of china that had just been
shattered, and things like that. Things that would look like a miracle to an
inside observer. But what would be the point of meddling with the simulation
like that? You'd only defeat the purpose of the simulation, and invalidate its
result!

I don't think this is realistic. I simply fail to imagine why a higher
intelligence would go through all that trouble. What would they try to find?
