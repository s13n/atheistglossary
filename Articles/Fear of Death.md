# Fear of Death

I'm afraid of dying. There's no point denying it. I have seen people dying from
close range, members of my family, people I loved and was close to. I have had
an instance where I nearly died myself. I am old enough to realize that the
majority of my years are over, and that it is only going to go downwards from
here for me. Dying is ugly, agonizing, potentially painful, and terrifying.

But I'm not afraid of death.

It is absolutely essential to be conscious of the difference between the two.
Being dead doesn't hurt. It means to not exist, which is the same state we were
in before our conception. Do you remember the time before your conception? Did
it hurt? You see?

I don't get why people are afraid of being dead. Being afraid of being dead
seems to me to indicate an inflated sense of self-importance.

So what am I supposed to think about Christianity, which tries to sell us the
notion that it has conquered death, that it promises us a life after death, or
actually a life after dying? What is this good for? Who needs that?

Christianity promises to solve the wrong effing problem!

As a Christian, you still die! Christianity doesn't solve this problem, which is
the real problem! Instead, it solves a problem that isn't one, except when you
are one of those people whose self-importance demands their eternal endurance!

For me, who is afraid of dying, but not of death, Christianity has nothing to
offer. Science has a lot to offer, however. In this case, it is palliative
medicine that matters. This is addressing the real problem, for a change. It
doesn't solve it in the sense that it allows you to escape dying. But it makes
dying a whole lot less agonizing and painful, and it manages in many cases to
return some amount of dignity to the dying, and relieve the relatives of much
hardship. This is really valuable, in contrast to useless Christianity.

Have you ever honestly thought about why you accept getting sold a non-solution
to the problem of dying? Do you really think that the promise of an eternal
afterlife somehow compensates for this failure to address the actual problem?
Especially given that this promise is exceptionally cheap!

I haven't even addressed the question of what the afterlife would be like, if it
should exist at all. I have left out the question of heaven vs. hell, and how to
make sure you end up in the right place. For me, even if there was only heaven,
and my admission there would be guaranteed, the prospect wouldn't lure me. I
simply wouldn't know what to do with all this eternity. I've tried to picture me
there. If I can meet a lot of interesting and stimulating people there (and I'm
not sure about that), I can imagine myself enjoying it for a couple thousand
years perhaps, but eternity is a lot longer than that, and my terminal boredom
would be inevitable.

I feel it is necessary for everyone to face the hard questions of dying and
death and one's stance, fears and wishes head-on. It is no good fooling
yourself, especially when the religions are too keen to fool you with precisely
those topics. They know exactly what people desire and abhor, and they had
several millennia to hone their strategy. So what exactly is it you're afraid of
and why? What would you wish for yourself, and why? Do you dare to think things
through, or do you push the hard questions aside and stick to a convenient
illusion?

This is a very old problem, which we might have lightly brushed aside in our
modern times. Memento mori, this ancient trope should serve as a reminder that
there is something inevitable that we shouldn't delegate to somebody we can't be
sure is acting in our interest.
