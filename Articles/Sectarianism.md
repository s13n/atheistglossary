# Sectarianism

Has it ever puzzled you how many wrong religions and denominations there are? I
mean, the only right one is yours, right? At least for a Catholic, like I was,
this is a somewhat credible position, since you can look at your own
denomination as the biggest one, and all the others just splintered off at some
time.

But, of course, you soon realize that even this doesn't amount to much. There
are entire religions, like Islam, with a similar size, and of course they're
also Right (TM), despite the splintering in their own ranks. And isn't
Cristianity itself actually just a branch off the Judean tree?

So, if they are all oh so right, why don't they all agree with each other?

Well, guess what, it's because none of them are right. You can only agree on a
single common truth, but you can disagree over a vast kaleidoscope of untruths.
All religions rely on the same wrong sources of truth: Revelation, Ancient
Scripture, Lore. And, inofficially, on wishful thinking.

So how does a splinter group survive in the face of such competition? How did
you come to assume that yours is right? How did you come to think that it is
appropriate for your group trying to teach others, to proselytize them, to wean
them from whatever group they're in? How did you come to presume or accept that
it is them who are on the wrong side? Did it ever cross your mind that they
might have just as much reason to teach and proselytize you?

If you don't ever come around asking yourself such questions, or you feel guilt
while you're asking yourself such questions, consider yourself brainwashed.

The question of why there's such a diversity of religions and their teachings,
when there supposedly is just one truth, is the elephant in the room of
religious belief. If people notice it at all, they often brush it aside with the
mentioning of the devil, which of course explains nothing. If God really should
want people to follow his truth, then setting up things like that, with such a
bewildering multitude of disagreeing factions, is a really devious way of
treating his followers.

Some of those factions try to turn this into an argument. The Jehova's
Witnesses, for instance, teach that only 144000 people will be saved in the end,
thereby taking the apocalypse literally. With a membership of around 8 Million,
they are setting themselves up for failure. It this were true, not even one out
of 50 of their own sect would make it at the end. That's a totally hopeless
aspiration, since how could you even hope to qualify above and before your own
local elders?

But the gist, which appears in most religious factions, is the message that you
are members of a privileged few, that you are closer to the truth than almost
anybody, and are thus in a privileged position for getting redeemed. It turns
redemption into a competition, or more frankly into a rat race.

This makes religious factions like football fan groups. They compete against
each other for no particular reason other than competition itself. They hate
each other for completely spurious reasons. And they love hating each other
passionately. It gives them a sense of belonging and purpose. The theological
underpinnings are nonsense. Most members care very little about them, anyway.
