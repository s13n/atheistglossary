# Wrath

The wrath of God seems to loom large in the minds of some believers, and I
struggle to square this with the alleged love of God. How would you call someone
whose love for you can switch that quickly into wrath? Capricious? Irritable?
Insanely jealous? Petty?

How exactly does such short temper help the love?

If you ask me, it doesn't. If you have ever been in a relationship with a very
jealous and irritable partner, you know what I mean. The love you feel is bound
to become stale and wear out rather quickly. You quickly ask yourself whether
the love is real when it can be displaced by a temper tantrum so easily.

When we're talking about an allegedly perfect God, this turns into a rather
ridiculous discrepancy. What's perfect about such a disposition for rage, I ask?
When a God is as powerful and unassailable as Christians assume him to be, the
rage is completely pointless. It is also very unfair. If someone is that much
more powerful than you, and you depend on his benevolence, the worst that can
happen to you is a fickle and unpredictable temper.

I know of course that people moderate this by pointing out not only God's love,
but also his alleged justice. But how does that help? Now you can choose between
totally incompatible traits, all of which God is supposed to have. It is the
usual Christian ruse to have it all ways at once, so that you can always tell
your opponent that he's getting it wrong. They emphasize the wrath when they
want to scare someone into submission, emphasize love when they want to woo
someone, and emphasize justice when they want to reassure someone.

So the wrath of God is a tool in conversation, preaching and apologetics. The
nefarious aspect is that this wrath tends to feel just and justified, where it
is merely fuelled by personal frustration and powerlessness.

This is why I suspect that it is such a helpful tool for people who are
frustrated and feel powerless. They know full well that they are not able to
develop any significant impact on their own. So they borrow power from their
God, by invoking his wrath. They feel right and righteous where they're just
angry. The same motive is behind a curse or a magical ritual. It is an attempt
to amplify one's own insufficient power by some kind of reference to God. As
pathetic as it seems, there are apparently enough people who fall for this kind
of bullshit, and they sometimes even feel licensed to act out God's wrath in his
name.

Take John's apocalypse as an example. He dreams himself into a violent fantasy
where he has those people punished by Jesus/God whom he wants punished. He laces
his own rage with lots of magical thinking and procedural excess that appears so
pompous that, in my view, it crosses into the cringey and ridiculous. It fails
to scare or impress me, and reminds me of Don Quixote instead, who gets carried
away by his own delusions. I wish people would react to it a bit more like
Sancho Panza, who provides his earthy and practical wit to counterbalance the
mad and overblown rhethoric.

I used to get annoyed by those threats of God's wrath, but nowadays is doesn't
usually evoke more than my eyeroll. It's not God's wrath after all, it is the
wrath of someone much smaller, who is trying to hide behind his imaginary big
brother, trying to appear a lot bigger than he is. I'm like: What's your
problem, dude? What are *you* angry about that makes you want God to dish out
punishments on your behalf?
