# Lore

So you're a member of a fairly new sect that originated with a charismatic
leader who is now not around anymore. But you have some elders around, people
who knew the leader when he was still there. You don't need scriptures, because
you have people who can tell you and whom you can ask. The creed is passed on by
word of mouth. You're not really part of a religion, it is more like a
conspiratorial group.

This is of course advantageous when you are under close suspicion and survey,
because you leave very little evidence that can be used against you. Given the
societal tensions in Jerusalem in the first century, I imagine that the early
Christians there had to operate under such conspiratorial circumstances, because
if their leader was executed for rebellion, his followers will remain under
suspicion.

But as the number of followers grows, the groups split as a matter of
practicality. When relying on oral transmission of the creed, the creed will
grow variants. People will start to disagree, and with a diminishing number of
first hand witnesses, there will be fewer people who can rein this in with
authority. So some decades after the crucifixion, the problem will become
important how to keep a common creed, how to establish a structure that can keep
up with an increasingly numerous membership. You start to need competent
management, and one of its biggest challenge is to come up with common lore that
can prevent a splintering of the new religion into distinct factions that
disagree with each other.

How do you do that? I guess you need scripture, which you can pass on, have
copies made, and read out to your members, most of which will not be able to
read it themselves. Scripture doesn't get inadvertently altered like when
passing on the story from mouth to ear. And it separates the members into those
who write those scriptures, and thereby control the creed, and those who are at
the receiving end. In other words, there will be an elite forming in the nascent
religion. It will not be egalitarian anymore, and I am sure that will cause
friction amongst those who knew the earlier times.

I think that is one of the reasons why Paul had a head start with his mission to
the gentiles. He had to rely on scripture right from the start, i.e. he used his
letters to control the creed he had brought to those various places on his
travels. And he was the leading figure for his flock right from the start, so
there would have been fewer authority battles in the part of Christianity he
catered for.

The situation amongst the Jewish followers of Jesus in and around Jerusalem
would have been quite different. They would have started to need scripture, and
accept scripture, only much later. Many of them would still have believed that
Jesus would return during their lifetime, so why write anything down? And they
were close enough together for oral transmission to work. By the time it grew on
them that they needed scripture to pass their creed on to the next generation,
the Judean War was in full swing and Jerusalem, their center, was destroyed,
thereby scattering the group around. Maybe they had some written material, but
it got lost.

Paul's followers were located elsewhere and rather unaffected by the catastrophe
in Jerusalem. I believe that this situation is the origin of the dominance of
Pauline theology in Christianity. It also partly explains why Christianity
didn't take root amongst the Jews, even though Jesus himself was a Jewish
reformer intent on appealing to the Jews. The gentiles weren't on his target
list. He didn't talk their language, didn't approve of their lifestyle, and
didn't expect them to become Jews, at whom his teaching was directed. For
Christianity to take root with Jews, scriptures in Aramaic or Hebrew would have
been necessary, and teaching would have had to take place in and around
Palestine, where other Jewish factions were also active, and where the Rabbinic
school that descended from the Pharisees ended up winning.

The end result was a new testament that was strongly dominated by the Pauline
tradition, and written in Koine Greek, which makes quite clear that it was
directed at the gentile audience, and not the traditional Jewish audience.
Ironically, Christian lore ended up getting defined by the very people Jesus
didn't want to target, using means that he didn't master, i.e. producing
scripture. Jesus, who wanted to reform Judaism, ended up splintering off a new
religion instead. A new religion whose lore would have been quite different if
he had controlled it. And the new religion endures after 2000 years, even though
Jesus thought he was living in the end times, and everything would soon come to
a close.
