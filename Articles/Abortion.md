# Abortion

Even though abortion is never mentioned explicitly in the Bible, which
presumably does say something about how important the authors thought the topic
is, Christians can get so worked up about it that it dominates their thinking!
Something is going off the rails here!

Now, abortion is a complicated ethical matter that is not specific to
Christianity, and indeed one can reason about it without ever mentioning
religion. To me, it seems that when religion comes into it, things get
oversimplified to the point where it becomes utterly absurd. As the saying goes:
For every complex problem, there is a simple solution, which is wrong.

We are clearly dealing with a dilemma, and the simplest solution to any dilemma
is to ignore one side entirely. The solution is then usually obvious - and
wrong. If you instead look at it soberly and responsibly, you recognize that
there is a value judgment involved. In your set of values, some contradict each
other, and you need to strike a balance. This is what real life looks like, in
contrast to religious doctrine.

What's the value of an unborn fetus? What's the value of the pregnant mother?
How do you properly strike a balance between the two?

To put the value of the fetus squarely on top of the list is very likely
hypocritical for a Christian. This becomes apparent when the Christian advocates
for abolition of abortion, while at the same time endorsing the death penalty,
or defending soldiers going to war. One immediately recognizes that there are
different standards in play here. If you put the value of life as an absolute
that trumps everything else, you have to apply it universally, or else your
position rings hollow.

In practice, most people instinctively do apply some judgment to the value of a
life, and all sorts of factors play into it. Tribe, race, age, gender, health,
wealth all play a role, whether we like it or not. It would be foolish to deny
it. As much as you think you are above this ethically, in a pinch you will start
to fall back to such categories. Hence it is easiest to maintain a "pure" stance
for those who aren't affected. Whom the consequences don't reach. The theorists.

To see this playing out, you may want to read through the bible while asking
yourself: What (relative) value does the bible appear to assign to the various
humans in the various bible stories. It is very far from the truth that it holds
life as the highest value! In Exodus 21, for example, you can see quite a few
examples of how different values are assigned in different circumstances. For
example, a miscarriage due to a beating of the woman is treated like material
damage, and not a killing. The difference between slaves and free men is also
instructive.

Of course, we wouldn't use the same scale of values today, our ethics have
progressed, and we judge things rather differently. But the fact remains that we
don't always treat a life as an absolute value that trumps everything else. We
should therefore pause for a moment and reflect on the basis for such a scale,
rather than pretending it doesn't exist.

It may sound harsh, but I regard the value of a born human higher than that of
an unborn fetus, and the value of an adult higher than that of a child. The
Bible seems to agree. I was told that Jewish custom has it, that if one has to
choose between the life of a fetus and that of the mother, it is the mother's
life that is to be preserved. I think this is reasonable. Nature itself doesn't
seem to assign a very high value to a fetus, given the frequency of
miscarriages. Before we had the sophistication of today's medicine, it was usual
to lose a substantial number of children at a young age, due to various diseases
or due to hunger and malnutrition. In some parts of the world it still is like
that. Losing a young child removes the burden from the mother, and indirectly
the whole clan, and readies her for the next pregnancy. Not a lot of investment
is lost when the child dies young. The death of an adult is a much greater loss.

This sounds heartless, I know. But nature is like this. When the Bible advocates
conquering and killing enemy tribes, it actually follows the notion of the
egoistic gene, even though its authors had no idea of this concept that Richard
Dawkins popularized. It is not the Bible that makes us rise above the pure
struggle for survival playing out in nature, it is culture and civilization
amongst us humans, which allow us to look beyond the immediate material need,
and pursue values that are more universal, and more transcendent. But we still
are grounded in nature and must take it into account. And we also should be
conscious of the values we apply.

The cultural concepts I would like to put up high on the value list, is
self-determination and compassion. Noone has rights over me and my body, other
than myself. The same applies to everybody. So nobody can require a woman to
bear a child against her will, just as nobody can require you to donate blood or
an organ to save someone else's life. You are compelled to render assistance to
save someone's life or health, but not at the cost of your self-determination.
This fundamental right comes with a responsibility, the responsibility to avoid
the dilemma as much as possible, in order not to create the situation in the
first place, that would require a difficult and hurtful decision later on. It
demands your foresight, and implies the responsibility to decide with the
greater good in mind, as opposed to your purely personal convenience.

And I would remind everyone else to be helpful rather than condescending or
imperious, to be humane and forgiving rather than dogmatic. A dilemma rarely has
a completely satisfactory solution, and what counts at the end is that you feel
you have done your compassionate best, in the given circumstances, and within
the means that are at your disposal.
