# Being gay

I find it rather weird that many Christians don't seem to be able to make up
their mind whether being gay is part of an identity, or a lifestyle, or an
illness, or a moral failure. They seem to oscillate between those in order to
reconcile their stance with what they think is what the bible tells them. It
makes them appear utterly confused.

The lifestyle interpretation is pretty pathetic for a start. There's absolutely
no reason why anyone would voluntarily choose a lifestyle that causes him
nothing but trouble. I mean, choosing the lifestyle of a thief also potentially
causes a lot of trouble, up to ending in jail, but you at least have the
prospect of being rich to counterbalance that. But what would counterbalance the
trouble of choosing a gay lifestyle?

If being gay is part of your identity, the only choice involved is whether you
show it or not, i.e. whether you live your life in harmony with, or in
opposition to your identity. In other words, you decide whether you live the
truth, or live a lie. You can of course decide to live a lie, thereby avoiding
any negative repercussions from society. But if you aren't a totally cynical
person, you will find this hard to pull off. It would feel like living the life
of someone else, and betraying the people around you, possibly even the ones you
are closest to. Witness the people who engage in heterosexual relationships,
including marriage and raising children, who at some point come out as gay, to
the great surprise and consternation of their family and immediate peers.

Treating being gay as an illness is a particularly nefarious stance. It implies
that there must and can be a cure, and recommending and administering it would
be a good deed. In reality it is psychological abuse. Even worse when you regard
it as a communicable disease, and others, particularly children, need to be
protected from catching the bug. That's industrial scale nonsense!

Imagine you're a left-handed person. It is accepted now that this is a
completely ordinary occurence that afflicts about 10% of all people. But there
was a time not so long ago when people regarded it as a deficiency that ought to
be corrected, if only to afford people a more successful life. After all, many
tools are made for the right-handed majority, and the minority is automatically
at a disadvantage. So people were compelled to learn and train right-handedness
to overcome their natural left-handedness. In other words, their left-handedness
was treated as a kind of disability that needed to be compensated for by
additional training.

We now know that this is nonsense. Left-handed people don't suffer a disability
or an illness. They are perfectly normal, just different in this particular
aspect. There's no reason to inflict any cure or special training on them, or
discriminate against them in any way. Being gay is very much the same. It just
happens, and it is neither an illness nor a lifestyle nor any kind of failure.
It just isn't the majority condition, that's all. You don't choose it, but you
best acknowledge it and live accordingly.

Being gay can therefore not be morally wrong. Parts of your identity and
personality can't be immoral. Yor acts can be, namely if they are harmful to
others. But that applies to everybody in the same way. Some Christians therefore
apply the notion "don't hate the sinner, hate the sin". There is an element of
truth in it, because if your disposition is dangerous or potentially harmful,
for example when you are a kleptomaniac, you have no right to act it out, you
instead have an obligation to keep it in check, so that harm is avoided. But
that's not the case with being gay. I have not yet seen a Christian make a
credible case what the harm in it would be.

Why should the bible even have a bearing on this? Especially since people
writing 2000 or more years ago can't be expected to have such an enlightened
view on it! We should be glad we now have a better understanding, instead of
being thrown back to the ignorance of millennia ago! I mean, the bible counts
amongst fish what clearly are mammals, and we don't take that as authoritative
for what a fish is and what a mammal! In such cases we have no difficulties
using our new, scientific knowledge. Why not here?
