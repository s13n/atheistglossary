# Ascension

Surprisingly few people, Christians included, seem to know that after his
resurrection, Christ allegedly stayed around for another 40 days until he
ceremoniously ascended into heaven. The story is found at the end of the gospels
and the start of the book of Acts. But maybe it is good people don't know, since
if they did, it may lead them to a few questions, like it led me.

I read there, that "After his suffering, he presented himself to them and gave
many convincing proofs that he was alive. He appeared to them over a period of
forty days and spoke about the kingdom of God." Yet, when you recollect what he
actually is supposed to have done in those 40 days, there's very little. It
would easily fit into a couple of days if necessary.

John, whose Gospel has the most detail, has the story of doubting Thomas, which
is strange in itself, since Jesus told Mary Magdalen to not touch him, since he
wasn't yet ascended, yet shortly afterwards, still not ascended, he told Thomas
to touch him to make him believe! Equally strangely, Jesus meets the disciples
several times, and each time they don't seem to recognize him. Thomas, having
already probed Jesus with his finger, does not recognize him a few days later
when fishing on the lake! Anyway, the newly resurrected Jesus goes fishing with
his erstwhile disciples, who don't recognize him! What is the point in that?

And what do the "many convincing proofs" mean? How many proofs does one need for
determining that someone is alive who is standing directly in front of you and
talking to you? Did they already have holodecks back then? Granted, the
resurrection was supposed to be a miraculous thing, and therefore incredible,
indeed it sure would have been for me, had I been there at the time, but those
were his apostles, to whom he had explained and predicted what would happen
beforehand, if the gospels are to be believed! So when the very thing happens
that he has predicted to them, they are completely incredulous and need many
proofs? What's the point in choosing and having apostles like that? Did he
choose them for their exceptional thickness? And nowadays many people fancy they
would recognize Jesus immediately!?!

Even stranger, what is the point in spending 40 days on earth trying to convince
his own apostles, who should have been his mouthpieces before the crucifixion
already, and yet still obviously don't get it? My goodness, what could have been
done in those 40 days instead! Imagine if Jesus had paid Pilate a visit! "Ave
Pontius, I'm back already! What do you say now?" I would have wanted to see his
face! Even better, he could have visisted the high priest Caiaphas, who
allegedly wanted him killed for the benefit of everybody else! Convincing *him*
would have been a much more effective act, and it still would only have taken a
day. I imagine him saying to Caiaphas "Don't even think about killing me again,
I'm dead already! And yet I'm standing here in front of you! Will you believe in
me now or shall I send you to hell?"

But no, nothing. For someone who wanted to erect a glorious new kingdom after
his death and resurrection, that's a wimpy start. Why didn't he get it going
immediately? What was there to wait for? Maybe that's why his apostles couldn't
believe it. They had expected something much bigger! An actual revolution
instead of a picnic and a bit of bible study. Jesus should have been better at
expectation management! He should have told them, that after his resurrection,
he would come back and carry on explaining to them what he had somewhat
unsuccessfully tried to explain to them before.

And then, after the 40 uneventful days were over, he was lifted up and
disappeared in a cloud. I know what this is like, I've been doing paragliding
for a while. I wouldn't want to be sucked into a cloud, though, because that's
very dangerous. Two men in white robes then explained to the small crowd that he
would come back in the same way he ascended, which I can also relate to as a
paraglider. It didn't happen, though, so he must have missed the landing patch
and come down somewhere alone, hopefully not stuck in a tree.

Fun aside, the more you think about this, the less plausible it becomes. Once
the disciples recognize him, once they don't, even though they had before. Jesus
delivered proofs that apparently weren't that successful, so he needed many of
them during 40 days to even convince those he was standing right in front of,
and who knew him very well from only days before. And when he ascends into
heaven, he leaves them back, still having to wait for the holy spirit, before
they can start to testify for him, and carry his message into the world. So his
40 days of explaining still weren't enough to get his disciples on their way!

What's that supposed to mean for everybody else? Why should I believe all this?
