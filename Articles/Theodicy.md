# Theodicy

Theodicy is the brainy codeword for the problem of evil. Why is there evil when
God has made everything, and is omni-everything? Leibniz coined the name in the
early 18th century, but of course the problem is much older. The Greek Epicurus
expressed it succinctly in his famous trilemma some 2300 years ago. 2000 years
after him, David Hume found the problem still unsolved:

"Epicurus’s old questions are yet unanswered. Is he (God) willing to prevent
evil, but not able? then is he impotent. Is he able, but not willing? then is he
malevolent. Is he both able and willing? whence then is evil?"

The problem is only really a problem when you want to hold on to your God-belief
at all cost. When the straightforward solution is somehow unacceptable to you.

If you merely follow logic, and put your own psychological neediness aside, the
clear conclusion is that God can't have all the attributes ascribed to him.
Simple logic leads to the conclusion, that when the premises of a syllogism lead
to a contradiction, at least one of the premises must be false.

You know what the atheist's solution is: God doesn't exist. There, problem
solved! Indeed, for me this is the most straightforward solution, and I really
don't see what should be wrong with it.

But I realize of course that this solution feels like an amputation for many.
They can't let go of their belief, and would feel phantom pain after the
operation. Consequently, people experiment with all sorts of mental gymnastics
in their attempt to circumvent the problem. Amongst the simpler attempts is to
realize that only one premise must be discarded to remove the contradiction,
thereby keeping God, and the question is which one. Epicurus leads the way:

- Perhaps God is unable to prevent evil, i.e. he isn't omnipotent. In a way this
  is a more general question that goes beyond the theodicee. For example, is God
  able to defy logic? Or is logic so fundamental that even God is subject to it?
  Of course, if God can defy logic, any shit is possible, and even thinking
  about it is bound to fail. You can believe this, but it basically makes God
  totally incomprehensible, and any reasoning about him futile. If you don't
  quite want to go that far, you have to draw the line somewhere, i.e. you have
  to decide for yourself where God's omnipotence ends. That feels a bit
  arbitrary and slightly blasphemous.

- Perhaps God doesn't want to prevent evil, i.e. he isn't omnibenevolent. This
  flies right in the face of Christian doctrine. If God can't be relied upon to
  be absolute love, he might as well be Satan at the same time. It would be a
  bit like the old gods of the Greek or the Romans, who aren't perfect at all,
  who have moods and throw tantrums, are unfair and sometimes jealous. The God
  in the Old Testament is still like that in some respects, but that's not the
  God the Christians advertise.

- If God is simultaneously omnibenevolent and omnipotent, then you are left with
  the option to explain away evil. You could assert that what appears evil to us
  is really for the best. Who are we to judge, God knows best! But this sounds
  rather hollow when you witness a child dying from cancer, or a reckless
  scoundrel making a fortune. If that's for the best, what does "evil" even
  mean? It is like declaring a circle to be square, and believing you've squared
  the circle. The only thing you've achieved is to fool yourself.

Of course, if you're not bound to Christian doctrine, still other solutions are
possible. You could believe that the creator of the universe (the demiurge)
isn't the same guy that you worship as the omnibenevolent God. The Gnostics
selected this solution during early Christianity. For them, the material world
was inherently evil, since it wasn't made by God, but by the demiurge. For
Christians, Satan is a similar trick, since all evil can be attributed to him,
thereby keeping God untainted. But that's too cheap a trick, because then you
have to explain the existence of Satan or the demiurge, and that's basically the
same problem again.

It makes me cringe to think about the amount of thought and effort, i.e.
precious lifetime, that has been wasted on this question over many centuries,
only because people don't want to accept the most obvious solution. All this
while Epicurus had it right all along, before Christianity even started.
