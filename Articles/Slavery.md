# Slavery

In Europe, slavery isn't much of a topic anymore. Of course, the US is different
because of its own history of slavery, which persisted a lot longer than in
Europe, and because the descendants of the numerous slaves that were forcibly
brought to the US still live there, and still are fighting for equality.

So I understand the actuality of the topic I paid little attention to. And I
acknowledge that the bible shows a stance towards slavery that is, shall we say,
unhelpful. It is something that did occupy my mind at a young age. I remember
hearing the Exodus story, where the Israelites escaped their own slavery, and
fled from Egypt to the promised land, where they were to live in freedom.

And before they even settled, before their journey was complete, they conquered
other people and took slaves, with not just God's approval, no, following his
explicit command!

What's a young mind supposed to make of this? When is slavery OK and when not?
Is slavery OK when God's chosen people do it, and not OK when it is done to
them? Apparently so! And Jesus did nothing to clear this up for me.

The story of the babylonian captivity of the Israelites basically repeated the
same scenario once more. Effectively, the Israelites were enslaved twice, and
regained their freedom twice. At least twice! Yet, there was no clear rebuke of
slavery, not even in the new testament. Little wonder that christianity went on
to accept and embrace slavery up to fairly recent times.

The gist of all this is indeed, as far as I understand it, that slavery is OK,
unless you try to enslave the chosen people. You only enslave the out-group, not
the in-group. No wonder this is today called racist - which is, of course, a
misnomer in theory, since it isn't race that is the crucial factor here, it is a
combination of ethnicity and creed for Jews, and creed alone for Christians. In
practice, for the USA in particular, slavery is of course racist, for obvious
historical reasons.

Doesn't this strike you as peculiar? The bible, or Christianity, is supposed to
represent the best in morality, the rule set to live by, the role model to
follow! And it can't bring itself to have a clear and unambiguous stance on
slavery? As if slavery wasn't always wrong! Pretty damn convenient for the
masters, huh?

I was brought up with the notion that all humans are basically equal. I
originally believed that this was a Christian notion, that this was what
God/Jesus wanted! I now know that it is part of the concept of human rights,
which is quite recent, and met with some resistance from religious circles.

And it gets even worse. At some point I realized that Jesus was comparing the
relationship between God and humans with the relationship between a master and
his slaves or servants. "The kingdom of heaven is like a king who wanted to
settle accounts with his servants" is what we read in the gospel of Matthew.

Does this sit well with you? It casts a rather different light on the notion of
the Father-God, right? The father in this notion is the master, the king, the
patriarch, the autocratic leader who isn't to be challenged or criticized. I
didn't have such a type of father, wouldn't have wanted one, and wouldn't wish
it on anyone else. It is a father figure that is, and should be, utterly
obsolete.

At this point you may realize that it all hangs together. The notions of the
father, the king, God, the husband, patriarchy, family, marriage, slavery,
obedience and the entire structure of society are all related to each other,
they collectively form and inform a christian worldview that looks and feels
wrong and obsolete in its entirety. If you reject one notion, you might as well
throw out the whole rotten tangle. We deserve something better than that.
