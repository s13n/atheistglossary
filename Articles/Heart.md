# Heart

As a schoolboy, I learned that the thoughts and emotions are located in the
brain. That wasn't obvious, indeed one of my close relatives believed it was the
heart. In retrospect I have no doubt that she got it from her Christian faith.
She wasn't educated in any significant way, having grown up in a rather simple
rural environment with little opportunity to learn stuff. A simple, kind, well
intentioned, likeable woman with some really silly beliefs. Some were in fact
racist, but not in a mean way, she simply didn't know any better. The bible
represented everything that was right for her, even though she rarely read any
of it herself, and went by what she heard in church instead.

For some reason, the bible appeared to work on the assumption that it is the
heart where the thinking and feeling and decision making happens. For a book
about the truth, this appears quite quaint. Now, I haven't got any problem with
accepting that the people back then, 2000 or more years ago, really believed
that the heart was the seat of emotion and reason. After all, it isn't obvious
where it sits. Picking the heart reflects that emotions and thoughts tend to
have an effect on how fast it beats. The brain, in contrast, doesn't make itself
and its function noticed in such a way. Perhaps the most direct hint would have
been that if you hit someone over the head with a club, he tends to pass out.

In fact, the Greeks had reasoned about this stuff quite early on, but that
hasn't had any influence on the bible, it seems. Pythagoras was an early example
of people who located the mind (or the soul) in the brain instead of the heart.
But, admittedly, it took a long time until the notion was accepted by the wider
population. That explains how the old notion ended up in the bible, if you take
the bible as the work of ordinary human beings, restricted by the knowledge
available to them at the time of writing.

But it doesn't explain why God would have subscribed to the same old notion.
When you read in Exodus that God "hardened the heart" of the Egyptian Pharaoh,
this is clearly not God, but a human author speaking. Even when you think God
was using a metaphor, you would have to ask yourself what the point of that was.
If God had wanted to be truthful and understandable at the same time, he could
have told people that the Pharaoh used his brain to think, and that God was
hardening his brain, or actually his stance. It wouldn't have subtracted from
the message, and would have been much more truthful. And on top of that, it
would have shown an inkling of divine knowledge, which would have made the
notion more plausible that the text was divinely inspired.

But the text as it is conveys the opposite message: That there isn't any divine
inspiration involved, that it is 100% within the scope of what people would
have known at the time of writing.

When believers use the same language even today, parroting iron age ideas about
how people think and decide, apparently giving outdated notions more credence
than what we now know to be the case, they appear just as quaint to me as their
holy book. They don't do themselves and their cause any favor.

But that's the curse of lying, isn't it? If you tell a high stakes lie, like the
divine inspiration of the bible, you have got to follow through, no matter what.
You can't even openly admit that thinking happens in the brain, because that
forces you into some elaborate waffling about why God apparently didn't know
that, or why he missed all opportunities over the course of centuries to educate
people in a case of practical importance, and instead elected to keep people in
their ignorance by using their faulty notions instead of the actual truth. God
looks bad no matter what. You need to claim that he of course knew, but didn't
want to tell, even though there would have been no downside in telling. What's
the point in having a divinely inspired book when it only tells you what you
already know, or think you know?
