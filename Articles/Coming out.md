# Coming out as an atheist

Most atheists who were raised in a religious environment come to the point
eventually, where they feel they need to come clean and tell their environment,
their partner, their family or their friends, that they are an atheist. For some
people that's a fairly benign experience, while for others it borders on the
catastrophic. If you are at, or right before, this point, you will surely ask
yourself how to go about it. But since the situations and the people are
different, there's no universal advice anyone could give. But some food for
thought might just do the job.

My own experience was pretty mild in comparison to what I heard from others.
That may be because I wasn't focusing on the doctrines or religious content, but
on the relationship. In a way I was in the process of growing up and becoming
more and more autonomous, and religion was just one aspect in that. You could
say that the main point I was trying to get across, to my parents in particular,
was not that I don't believe in God, but that I was old enough to decide this on
my own. I did mention that I doubted various religious doctrines, but I tried
not to make a fundamental, make-or-break issue out of it.

I was too young to plan this in any conscious way, I just felt that being overly
confrontative would get me nowhere, and only create a situation that was bad for
everybody. By keeping the religious doctrinal questions from acquiring a high
profile, I got away with pushback on a level that I could handle. In hindsight,
I believe it was totally the right way to do it. As much as my parents wanted me
to be and remain a Christian, they also wanted me to grow up, and having one's
own opinions, doubts and reasons is a part of that. It was all about making my
parents accept the level of freedom and autonomy that I wanted. Topics that are
at the very core of one's belief are ill suited as the thin end of the wedge.
You need to start at the other end.

Of course, that doesn't always work. If you have such a controlling environment
that you are given no chance of getting this wedge in, you may not be able to
avoid confrontations, but even then you need to have prepared the arena.

Psychologically, coming out as an atheist to religious parents feels to them
much like you joined a cult. This is ironic, of course, as in reality it is the
opposite: You leave a cult. But everbody thinks he's the normal one, and the
others are wrong. So if you put your atheism to them bluntly, they will think
you've been brainwashed, and they are likely going to want to "cure" or "save"
you. The result will be that they are alarmed, and are going to keep a closer
eye on you, thereby reducing the freedom you are trying to get. You need to boil
that frog slowly.

Consider, too, that you are inevitably going to have a substantial lead on them,
because you have been thinking about this in the closet for quite a while. You
had all this time to work it out for yourself, before you arrived at the point
where you think you want to go public. For everybody else, it is likely to
appear much more sudden when you do. This creates shock and opposition.

As much as you may want to tackle their superstitions, their doctrines and their
religious bigotry, because that is what upset and bothered you the most, you are
better off keeping that for later. First, you need to keep them closer to the
comfort zone, where they can accept and perceive on the emotional level that you
are OK. That you are doing your own thing, and that's natural, and acceptable.
That you may be straying and wobbling a bit from the path they would prefer, but
not in a bad way. That you can handle your increased freedom. You train their
tolerance.

This may need physical distance, so that you can escape surveillance. A good
pretext is when you go to a college or university that's far enough from home to
require your own flat or shared house. Or when you get a job that's further
afield. This is generally what people do when their environment is too stifling
and tight, but the obvious downside is that you may have to sever or freeze a
few relationships that may be dear to you. Well, no pain, no gain. Freedom has
its cost. I don't know where your optimum is, only you can decide. All I'm
saying is that often, before you can come out, you need some amount of freedom
and autonomy. This will often be the first problem to solve. And it will help if
you don't need to solve all problems at once.

When the going gets rough, and you face significant pushback and attempts to
rein you in and put you back in control, it obviously becomes harder to handle.
It is usually better to focus on the autonomy part and not try to push an
atheist agenda, or come out with force. But sometimes that may be the only
option. It will most likely lead to a breakup of some sort, with all the
associated tribulations. You may find yourself in a situation where no
meaningful conversation is possible anymore, and you have to get away. If this
can't be avoided, you'd better have some material and emotional independence,
otherwise you may find yourself in a very difficult situation. Avoid, if at all
possible.

But when it happens, seek help. You will need support, and you shouldn't be
ashamed of that. If you fear this sort of development might occur in your case,
plan for it.

But above all, don't despair. You have the right to your own opinion, your own
life, and your own happiness, and you wouldn't have become an atheist if you
didn't know how to think. That's a good bedrock right there.
