# Enlightenment

I sometimes come across people who try to claim enlightenment as a Christian
invention. A variant of it claims science as a Christian invention. I find that
quite annoying, because it is a brazen attempt at denial, i.e. denying the
conflict between Christianity on one side and Enlightenment or science on the
other. It is an attempted distortion of history to get Christianity out of the
firing line, according to the motto: "If you can't beat them, join them!"

Now, as dishonest as this is, there is an element of truth in it. Good
propaganda is often like that: A grain of truth at the core, embedded in a
heavily biased and deceptive narrative. So bear with me while I attempt to
disentangle the mess and expose the fraud.

For a long period during medieval times Christian dogma had such a monopoly on
thought and imagination of people, that independent thought which was not
influenced in one way or another by Christianity was practically impossible. In
this sense, any thought that led away from Christianity was at the same time
dependent on, and derived from, Christianity. In fact, I would argue that in the
beginning, openly confrontative thought would be crushed by Christianity. The
only chance was to gradually erode Christian domination from the inside.

Not that people like Giordano Bruno or Galileo Galilei were out to do away with
Christianity altogether, and realized that they could only do it gradually from
the inside. They didn't have such a grand plan. They just recognized how wrong
Christian authority was in some repects, and challenged orthodoxy in this
limited sense, in the belief to lead back to a more original and better version
of Christianity. Those rebels frequently saw themselves as reformers, as
improvers of the faith, and not as its destroyers.

A lot of that goes back to Martin Luther, who himself drew to some extent from
another rebel, Jan Hus, who preceded him by a century. They were out to combat
the corruption in the Roman Catholic Church of the time, and tried to lead back
to a pure version of the faith that didn't depend on Rome. In effect, Luther
managed to find enough allies in politics to succeed in establishing a competing
power structure that robbed the Roman Church of its monopoly. He succeeded where
Hus failed. It is this political success that created the intellectual and
political breathing space for the Enlightenment to succeed in Europe. But, of
course, Luther didn't want to foster the Enlightenment, he wanted to reform the
Church. Yet, in a way, he is a precursor of the Enlightenment.

The thing is, once you have liberated the mind, it pursues its own course. Once
you have learned how to think properly, investigate properly, debate properly
and draw proper conclusions, you invariably apply those skills to other topics.
And so it should come as no surprise that enlightened thinking ended up turning
against not just the Roman Church, but Church and faith in general. People saw
how this way of thinking started to yield great insight into how things are and
behave, and they became bolder and more adventurous, and accepted authority even
less. The realization gradually dawned, that you didn't need religion at all to
conduct proper thinking and reasoning, and that on the contrary faith was often
in the way of achieving progress. The conclusion was what we today call
"methodical atheism". It means to conduct scientific investigation as if there
was no God. For the purpose of doing science, regardless of your actual belief,
you were supposed to leave God out of all equations and reasoning. This was a
momentous decision, because it made possible science as we understand it today,
and led to the extreme richness of scientific insight that we profit from today.

This decision was momentous because it condenses the realization that God is
useless in science, that including God in reasoning about nature and its
behavior can't lead to any profitable insight, but rather tends to obscure it.

So in a very real sense the Enlightenment was an anti-Christian movement, but it
started out as a Christian reformation attempt. There's a certain irony in this,
but it equally means that the Enlightenment indeed has Christian origins, in the
sense that something that arises out of protest or critique against some kind of
establishment, owes its existence to this establishment. In the same sense
Marxism owes its existence to capitalism, and democracy owes its existence to
tyranny. But it obviously would go too far to demand thankfulness or reverence.

But it would be wrong to view the Enlightenment as just an anti-Christian
movement, or even mainly as an anti-Christian movement. Enlightenment isn't an
ideology, it is a method. It is anti-Christian only as much as Christianity is
anti-Enlightenment. Enlightenment emphasizes the individual mind, it empowers
the individual mind over and above dogmatic dominance. As long as Christian
reformers did the same, they were themselves enlightened. At the very moment
when they came back to assert their own Dogmas, the Enlightenment turned against
them.

Hence Kant's motto of the Enlightenment: "Have the courage to use your own
understanding". Understanding, not faith! It is about pursuing one's own
insight, not about adopting someone else's dogma. Luther started it in a way by
making the text of the Bible available for everyone, so that people could make
up their own mind about it, rather than being at the mercy of priests and
bishops. But when you can read the Bible by yourself, and discuss it in public,
you can also debunk your religious dogmas. More often than not, this erodes
clerical authority and Christian faith, because they depend on you adopting
their explanations and interpretations.
