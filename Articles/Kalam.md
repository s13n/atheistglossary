# The Kalam 

The Kalam cosmological argument has enjoyed a disproportionate amount of
attention in apologist circles, given its gaping holes. If you don't know what
it is about, please peruse the links in the description, as I won't offer an
introduction here. But see my entry on the Cosmological Argument. Even some
skeptics and atheists seem to think the Kalam is at least a plausible and
interesting argument, if not entirely convincing. I think it is hogwash, and not
worth much in-depth study, but you can certainly indulge in it if you're so
inclined. After all, even some eminent philosophers have invested their time in
it.

The basic Kalam argument attempts to show with a syllogism, that there must have
been a cause of the universe. An extended version of it attempts to equate
this cause with the Christian god. A syllogism is a simple logical argument
that starts with some premises and ends with a conclusion that logically follows
from them. The premises appear to be very simple, obvious even, which is to say
that they reflect some sort of common sense. Many people will instinctively
accept their validity.

Now, it should immediately trigger your bullshit detector when somebody purports
to solve a famously difficult problem this easily. Indeed, the problem of the
origin of the universe, or the problem of proving god, has been discussed for
millenia without a consensus emerging. You can bet that someone is either trying
to fool you, or himself, or both. You don't need to be a philosopher, or a
cosmologist, to smell a rat:

"For every complex problem there is an answer that is clear, simple, and wrong."

The Kalam argument is wrong in more ways that I can touch upon here. See the
linked literature. Many rebuttals go much too deep for the average punter. In my
view, it suffices to observe how the allegedly common sense premises are being
applied to a problem that is way beyond their realm. In particular, how can you
just assume that a sentence that is trivially true for everyday objects that
surround us, apply equally to the universe as a whole? The universe isn't a
thing just like other things. In fact, it is fundamentally different, because it
is the container of all things, a property that doesn't apply to anything else.

In short, the Kalam argument greatly overstretches its premises. It thereby
presupposes things it would have to show first. In doing so, it fools the
unwary, but not the expert. It is the layman's idea of a clever argument.

In fact, if we have learned one thing from science in the course of the last two
centuries or so, it is that the further we depart from our own realm of
perception and experience, the weirder and more incomprehensible everything
gets. Regardless of whether you zoom out to the very large and very far, or zoom
in to the very small, you find that common sense fails spectacularly.

This goes down to the very basic concepts that most of you will take for
granted. It starts already with the notion of up and down. This notion is so
enmeshed in our language and thinking that we often don't realize it is not
universal. Religious language in particular is full of imagery around the idea
of up and down. Hell is thought to be below, heaven above, the sinner falls, the
saint rises, we look down on the former and up to the latter. But as soon as we
leave our own planet, this notion loses all meaning. It suffices to be in orbit
around earth to make this apparent, like in the International Space Station ISS,
where there is no sense of gravity anymore. People live and work there with no
particular orientation being necessary or useful. You'd need to look out of the
window to remind yourself where our earth is, which could tell you where "down"
is. In space, one of our most fundamental mental concepts that pervades our
thinking just disappears - along with the basis for all the metaphors that
draw from it.

But this is only a tiny beginning. Going further, you find that our notion of
time dissolves, our notion of what "matter" is, of what "things" are. Even the
meaning of "nothing" becomes rather unclear. Hence it becomes unclear what it
means to exist, including what it means to begin to exist and to cease to exist.
With such fundamental problems it becomes doubtful whether we actually have the
proper language and mental tools to speak about such topics in a sensible way.
It is perhaps unsurprising that mathematics with its formal language has been
more trustworty than ordinary language when it comes to describe all this.

You will have noticed that when scientists attempt to explain such difficult
things to laypeople, they use a language full of analogies and metaphors, in an
attempt to ground their presentation in some common sense concepts they assume
their audience is familiar with. They want to start from within your comfort
zone. But this is a two-edged sword, since it may reassure you in your trust in
those common sense concepts, where such trust is entirely unwarranted.

This brings us back to the Kalam. The origin of the universe is an open question
at this point. Various scientific hypotheses are being debated with no consensus
in sight. The relatively recent discovery of dark matter and dark energy has
shown that we are far from understanding all this. One thing is clear, however:
With every major discovery things become weirder. It would be wise to suspend
judgment on how the universe came about, until we have gathered enough evidence,
and science has figured out a plausible theory that integrates all these
findings. Against this background, the Kalam appears both ignorant and arrogant.

### Literature

* Wikipedia entry: https://en.wikipedia.org/wiki/Kalam_cosmological_argument
* Entry in Stanford Encyclopedia of Philosophy: https://plato.stanford.edu/entries/cosmological-argument/
* Dr. Craig's Lecture in Birmingham: https://youtu.be/Dqc42ZB24ew
* 2-part refutation by scientists: https://youtu.be/pGKe6YzHiME & https://youtu.be/femxJFszbo8
