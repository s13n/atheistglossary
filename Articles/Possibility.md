# Possibility

What does it mean when we say that something is possible, or impossible? In the
strict sense it would need an incontrovertible proof, and that's generally only
available in math. In this sense it is impossible that 2+2 is 5. But in real
life we haven't got the luxury of such certainty. So we may have many things we
deem impossible, but a tiny uncertainty will remain. You could say that even
though it is theoretically possible, it is practically impossible. The odds are
just so low that it happening can be ruled out with considerable confidence.

I am addressing this topic because I frequently encounter people in discussions
who appear to derive some comfort from the fact that nobody can prove that God
is impossible to exist. The argument seems to be: "You can't prove that God is
impossible, so I am justified in believing in him".

Now, I think this is one of the weakest possible defenses of a God that exists.
If that's the straw you hold on to, you are in trouble. But this may only become
apparent when you compare it with other cases which aren't quite as loaded as
the God claim. So lets talk about pregnancy. Suppose your wife or girlfriend
tells you she's pregnant and you know it can't be from you. Would you say it is
possible that she had an immaculate conception? Would you believe her if she
claimed it was?

Most people I know would say to her that she should stop bullshitting and admit
the truth! In all practical senses of the word they would hold immaculate
conception as impossible, and hence would assume she was lying.

So is it *theoretically* impossible? I don't think we can say, because how can
we be absolutely sure? Are we justified in assuming that it is *practically*
impossible, and in accusing her of lying? I think yes, we are!

In the vast majority of cases it is the *practical* impossibility that matters
to me. The residue of insecurity is usually too small for me to take it into
account. Therefore I can call something impossible that strictly speaking isn't,
and I know it isn't. I'm not lying there, I merely omit a detail that I deem
irrelevant. If you object, you would have to show that not only is there a
possibility (however small), you would also have to show me that (or how) it is
relevant!

This also applies to God. I usually call God impossible, and I want it to be
understood like if I called immaculate conception impossible. I am not making a
theoretical statement, but a practical one. God is practically impossible. The
odds that go the other way are too small to matter. It is like immaculate
conception, like gravity suddenly reversing direction, like the earth being flat
after all, like the sun not reappearing tomorrow morning any more, like all of
the animals on earth having evolved from the few individuals that were in Noah's
arch a mere 5000 years ago. Like all the stuff you're completely convinced isn't
real, even though you wouldn't be able to prove it.

To be sure, I DO know that I have no proof, and that a small chance of being
wrong remains! If you can make a convincing case you have a chance of turning me
around, because I'm not dogmatic! My emotional wellbeing doesn't depend on any
of those, and I can admit I was wrong. But don't hope winning me over with the
mere assertion of a remaining possibility! "You can't prove God doesn't exist!"
doesn't work with me, and you should think about why it works as an agument for
you, given how weak it is!

I'm sure you are like me in many of the other life questions, and if you can
doubt whether your girlfriend really had an immaculate conception, you know how
I can doubt that there's a God.
