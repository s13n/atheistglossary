# War

Sadly, wars are becoming a normality once more in our world. We were granted a
few decades of relative calm and security, but this seems to have drawn to a
close. And, unsurprisingly, religions are intimately intertwined with it.

Have you ever wondered how it can be, that religions almost universally claim to
be about peace, and invariably end up involved in war mongering when it comes
down to it? Look at pretty much every war that has been fought in the last few
decades, and you will see that people called it a just, or even holy, war, that
they have God on their side, whichever side, and that fighting in it is the
righteous and pious thing to do.

Muslim terrorists drove the passenger planes into the world trade center with
religious conviction, and the USA retaliated with a war against terrorism that
still endures, and repeatedly was called a crusade, or similar. Regardless
whether you look at the wars in former Yugoslavia, Iraq, Iran, Syria, Jemen,
Libya, Mali, Sudan, Somalia, Ukraine, or the conflict in Palestine, it is always
fought by people who see God on their side, and religion calling them to arms.

In Ukraine, there are Orthodox Christians on both sides of the war, and the
patriarch on the Russian side went on record with his calls for support of the
war against Ukraine, which he paints as a war against the western world, with
its globalization and its decadence. To which he sees Russia as a "very
attractive alternative", a country where traditional values are being
exemplified, such as family, sense of duty, and patriotism. And yet I look at
what that country does, on the battlefield, in politics and government, and in
its own society, and I can't help finding it all totally rotten.

Do we need any more examples for the total and persistent hypocrisy and
corruption of religious doctrine through the centuries and millenia? You have to
admit that it just keeps reoccurring, and flies right in the face of the
pervasive self description of the religions. And, in the case of the Bible, one
has to add that the topic is overtly prepared there. Jesus notwithstanding when
he commanded his followers to love their enemies, the fact is that this in no
way stands in the way of killing them. People just kill them out of love, or so
they pretend. No remorse. God even says so: Spare nobody!

Rather than deflecting to the rotten nature of humanity, thereby taking the
blame off religion, you ought to be honest and see religion as what it is:
Complicit, and often instrumental, in the wars of this world. Religions are just
as rotten as humanity itself.

If, like me, you see religions as purely man made, this is unsurprising. But if
you see them as divine or divinely inspired, you've got some explaining to do.
