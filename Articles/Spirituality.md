# Spirituality

Spirituality has been a hot topic in society for a long time, and yet I'm still
not sure I understand what it is. It seems to be a blanket term that mops up
everything religious outside of the big religions. There's no real distinction,
however. The spirituality of the big religions seems more or less the same as
that outside of them. The vocabulary used may vary, but the topics are
invariably about a kind of connectedness in two directions: An inwards direction
with what can be called an "inner self", and an outward direction with the
"cosmos", or "another dimension", or a "higher sphere".

My issue with this is that it sounds like bullshit bingo to me. Yet it is so
widespread that there has got to be something to it, right? I better dig into
it, lest I miss something important.

In my observation, spirituality seems mainly to be about two things: Insight or
Knowledge, and Control or Power. Insofar it resonates with me, because to some
extent that's what I'm interested in, too. Those are basic human interests. Seen
from this angle, spirituality crosses over into science effortlessly. But while
science is a collective endeavor that strives to increase insight and control of
humanity, spirituality has a distinctly egocentric bent to it. Spiritualists may
tell you that they have some universal solution, but they almost invariably sell
it to you as a means for your personal empowerment. Which in turn means that
they (probably correctly) assume that that's what people want: More knowledge
and power for themselves.

I think I understand that. You are being tossed around in your life by external
forces out of your control. Things happen to you, big or small, for no
particular reason. The things you crave most urgently seem to be those most
prone to failure. And at the end you die. What's the point in all this?

Knowledge and power can give security, and thus combat fear. But it isn't
necessary to have actual power and knowledge. Imagined power and knowledge has
the same reassuring effect. It is reassuring to have some "deeper" knowledge, no
matter if it is right or wrong. As long as you don't submit it to a rigorous
test, you don't know the difference, and often you don't want to know, because
the important point is not to be right, but to feel in control.

Many spiritual schools of thought parade this openly: They present themselves as
paths to a deeper knowledge and a means of achieving greater power. God-like
knowledge and power, in some of the cases. As ridiculous as this may seem, it
gives away the egocentrism at its core. The main question isn't whether it is
right, but whether it works for me. And so I think people eventually end up in
the esoteric sect that best mirrors their own psychological cravings. Those with
the biggest aspirations will fall for the biggest promises.

I believe that when you are attracted to some spiritualist offering, you are
revealing something about your inner self, a bit like a Freudian slip. Others
can pick up the clue. For example, are you attracted by "secret knowledge"? Why
do you think there's a secret in the first place? Why wouldn't people want to
reveal it? Why would they offer it to you, of all people, and what do they
expect in return? Would you be thrilled to know something that others don't? Do
you want to be superior to others, perhaps even capable of controlling them?

If you're into spirituality, I recommend some introspection. Try to be honest to
yourself, and think about what it is you are trying to find. Is it just about
becoming a better person in your own self image? Or is it about becoming more
successful, more influential, more powerful, less vulnerable? Don't get me
wrong, I'm not dismissing this summarily, those are all common human cravings,
and it doesn't help pretending they're wrong or nonexistent. My point is
self-awareness and honesty. I am also convinced that this makes you less prone
to becoming the prey of charlatans.

But I'm sure you will agree that while self-improvement is certainly positive,
increasing power and control may be a mixed bag, particularly when other people
are involved.
