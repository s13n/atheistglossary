# Woke

Isn't it fascinating how quickly the term "woke" has been turned against its
original meaning? I still wonder how it could arouse such emotions, given what
it actually means. Obviously there was a lot of mongering of various sorts
involved. The term only became prominent in the aftermath of the shooting of
Michael Brown in Ferguson in 2014, amongst the "Black Lives Matter" movement,
and I'm writing this article only 10 years later.

I still can't find fault with what it was originally supposed to mean: Alertness
to injustice and discrimination. Shouldn't that be universally desirable? But,
apparently, some think the opposite. Nowadays you can run an election campaign
with anti-wokeness, and get applause from half the population. Does this mean
that half the population is actually **against** alertness? Or **for** injustice
and discrimination?

Some would say that, yes, this is racism raising its ugly head. As soon as black
people raise their alertness, the racists cry wolf. I would like to believe that
it isn't quite as simple. But the more hysteria is being whipped up around the
term, the more pessimistic I am about it. It really seems that the debate has
become divorced from the original meaning of the term, and "woke" has become a
label that is being slapped on everything that one wants to vilify.

Before you read on, please stop for a second to reflect on what "woke" means to
you. Have you followed its early usages or are you just encountering it as a
loaded term in ideologically driven debates? Note that I don't implicate that it
is you who is the ideologue. Quite the contrary, I do concede that the original
users of the term may have ideologized it themselves. I just wanted you to
introspect if you let yourself be drawn into this ideological battle.

Specifically when you think you are against "woke", what does this actually
mean? What is it you are against? Do you regard it as an unjustified blanket
accusation or presumption of racism or discrimination? Dou you feel unfairly
indicted?

I'm not living in the USA and therefore have a bit of a struggle with
understanding all this hysteria. I would have thought that if you're not
actually a racist, you wouldn't need to get worked up about "woke" at all. It
isn't you who is being accused. Sure, as with all political fads that come and
go, some exaggerate and over-generalize it, but you can usually sit it out. And
as the core of the term is quite sensible, i.e. there actually is injustice and
discrimination deserving attention, and needing to get reined in, fundamentally
opposing the whole idea would be over the top anyway. You could be woke
yourself, just not with a vengeance.

I often cringe, even despair, with the tendency of people to exaggerate a good
idea into absurdity, losing all measure. But this goes both ways, as the counter
movement exaggerates just as much, or perhaps even more. Couldn't we all be a
bit more relaxed about it?

But, of course, once the term is a moniker for one side of a culture war, it
doesn't need to mean anything in particular. It just needs to serve to
distinguish between the in-group and the out-group. The term isn't the problem
then, it is the war that is the problem. But the question becomes even more
important: What is it you are actually fighting for, and why? And how much of
that is real, as opposed to the hullabaloo that is being whipped up for
political gains? Are you allowing yourself to be drawn in, or are you focused on
the actual underlying issues?
