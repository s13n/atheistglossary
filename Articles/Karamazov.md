# The Brothers Karamazov

Dostoevsky's last novel is famous for its treatment of the question of evil,
i.e. the theodicy. The key quote from the novel is "If there is no God,
everything is permitted"; at least that is the form everybody seems to "know",
and attribute to Ivan, the middle brother who serves as the poster materialist
and nihilist in the novel. It isn't a literal quote, but it attempts to capture
the materialist position that Ivan personifies.

Great praise is heaped upon a couple of chapters, namely "Rebellion" and "The
Grand Inquisitor". Ivan talks almost obsessively to Aljosha, who barely gets a
word in between, where Ivan scathingly condemns human bestiality through a list
of examples of most cruel infanticides, and uses it to dismiss human free will.
He recites a poem that he wrote which has a Spanish inquisitor debate Jesus on
the topic of freedom, where the former accuses Jesus to have done a disservice
to humans with freedom, because they can't handle it, and arrests him.

Religious apologists are quite fond of this depiction of the tension between
faith and reason, between theism and atheism. But Dostoevsky composes the
episode in a way that stacks the odds in favor of theism. Aljosha has barely a
chance of making a fool of himself because he says very little, and after all
this rant by his brother Ivan, it suffices that Aljosha gives him a kiss, which
echoes Jesus' kiss to the inquisitor in Ivan's poem. In a way, the poem already
contains Ivan's craving, which Aljosha merely needs to understand. This is how a
theist imagines a moral and spiritual vacuum in atheists.

If you read the passage, you invariably note how much Ivan uses theist language
in his rant. This is not how a genuine atheist would speak. It is the language
of someone who is deeply disappointed by religion, and by mankind, too. It is a
stage in a deconversion. It still uses the mental framework of religion, but he
has become aware of its inadequacy, without having progressed to a state where
he could put something in its place. It is a destructive phase, and the
constructive phase hasn't yet begun. It is the phase where one vomits out what
has caused pain, but after having vomited, one feels the worst, one is the most
exhausted and vulnerable, yet it is the start of a recovery and a healing.

Dostoevsky brilliantly captures this psychological state of frustration and
exhaustion, but doesn't seem able to see beyond it.

The conclusion from this isn't that without God, everything is permitted! It is
not that without God, all those horrible infanticides are OK. In light of all
the bestialities that have been and still are being committed in the faith that
it is God's will to do so, this is completely implausible! Evidently, God can
very easily be used for justifying such acts, and you can't even claim them to
be against God's will, because it would just be you saying so! As I write this,
in Dostoevsky's Fatherland Russia, the head of the Russian orthodox church,
patriarch Cyril, openly endorses the war against Ukraine, in which a large
number of children have been abducted and brought to Russia in order to
indoctrinate and "Russify" them. I wonder what Dostoevsky would have said about
that!

To put it bluntly, it appears that *with* God, i.e. with the idea (or delusion)
of doing God's will, *everything* is permitted. Humanistic atheists in contrast
tend to believe (and I exaggerate only mildly), that *without* God, everything
is *forbidden*, unless one can be confident to not encroach upon the rights of
others. This, of course, turns Dostoevsky's notion on its head!

Even when you are not quite as charitable towards atheists, you have to admit
that bestiality may afflict both sides, and God doesn't appear to help. Rather,
it seems to me that God serves as a vindicator and amplifier. If you are already
of the meek type, you will find justification for that in God. If you are of the
cruel type, you find justification in God, too.

The realization from this is that what is permitted doesn't really depend on
God. The rules that are attributed to God are just as man-made as the rules an
atheist would follow. This is not a problem of faith, but of ethics, and ethics
is a feature of culture. Much has been written and debated about it, and at the
end you realize that it is a collective job to uphold and develop culture, and
that those barbaric acts Ivan is so frustrated about, are just what happens when
culture breaks down, and the animal in every human comes to the surface.
