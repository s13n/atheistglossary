# Abiogenesis

It is not that long ago that I learned what this word means. I was of course
aware of the question how life emerged from non-life, but somehow I missed the
term for it. This might be because it didn't rank very highly on the list of
problems that interested me. For me, it was basically a special aspect of the
more general topic of evolution. And I took evolution for granted.

But it isn't that simple, I came to realize. For evolution to work, you need a
mechanism that replicates stuff. Only then it makes sense to have another
mechanism that selects between the (imperfect) copies, preferring the better
ones and getting rid of the worse ones. But how can you have such a replicating
mechanism without having life in the first place?

To put it in a different way, the question is if you can have evolution when you
don't (yet) have life. Intuitively, I had equated evolution with life, so you
have either both or you have none. But is that so?

If you can have evolution without having life, I figure getting life would
simply be a matter of time. Once you have the process running, it would crank
out ever more sophisticated results, and at some point we would consider it
alive. So, for me at least, the main problem is how to get evolution going
without needing anything living.

What I do take for granted, is that there is no jump from non-life to life. What
we regard as life is not some unique property that you either have or don't
have. Something that gets breathed into you, some mysterious magical force that
animates otherwise inanimate matter. I simply regard life as something that is
sufficiently complex that its appears intuitively "live" to us. It is far from a
clearly defined concept.

Biologically, it requires some sort of metabolism. Some matter is ingested,
processed, and resulting products are excreted. But that's a weak definition of
life. A car engine also has a kind of metabolism. It mainly ingests fuel, air
and oil, and the occasional spark plug, processes it, and excretes fumes. It's
alive, right? We even say "the engine died" when it has stopped working! But it
isn't biological. It isn't "real" life! So, it appears to me, we first need to
tighten up our definition of what life means, before we can talk about when and
how it started.

For me, it is quite obvious that we could have some kind of replicating
chemistry way before we would intuitively speak of life. More to the point, we
could have all necessary ingredients for evolution way before we would call it
life. And if this is so, it wouldn't suprise me at all if this evolutionary
process would come up with something that we would call life, given some
considerable time. Because, it would just gradually get more complicated and
sophisticated, and at some point it would look like life to us. No spark needed
to ignite it, no sudden change, just a drift towards life.

To recap, what is required for evolution to work? A replicating mechanism that
makes mostly faithful copies with the occasional error creeping in. A process
that selects between the copies, putting some of them at an advantage and others
at a disadvantage, depending on the outcome of the replication process. And some
feedback of this into the replicating process.

That's not such a lot to demand. In a way, you have a replicating process
already when matter crystallizes. What was chaotic to start with, accumulates
into a regular, replicating pattern. Think snowflakes forming from water
droplets. Order emerges from chaos, in a mindless, natural process. It usually
takes a seed, a small particle where the crystal starts, and it grows from
there. Isn't that already a tiny bit like life? Is life perhaps a marvelously
complex kind of crystal?

If this sounds ridiculous to you, perhaps you think of human life. Think of
something much simpler, like bacteria, instead. Or simple plants. Isn't there
some kind of similarity between a growing colony of bacteria, and a growing
crystal? Or between a growing fern and a growing snowflake?

This isn't supposed to be an explanation. I can't explain how life formed. It is
an open scientific question. But I can try to convey my intuition. This
intuition says that there's nothing magical about this. It can very easily start
with simple processes gradually getting more complex as time goes by. As a lot
of time goes by. And my intuition tells me that life started with crystals.

But what do I know?
