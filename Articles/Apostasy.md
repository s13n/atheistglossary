# Apostasy

Apostates are every dogmatic religion's worst nightmare. Plain unbelievers can
always be assumed to never have received the "good news", and if they only
would, if they were properly introduced to the faith, they would of course
convert with flying colors! Or so the faithful believe. But that doesn't work
with apostates! They used to be believers, and usually know exactly why they
don't accept the "good news" anymore. They aren't unbelievers out of ignorance,
but out of experience and knowledge.

This is so bad for many believers and apologists, that it needs to be wiped off
the face of the earth. The cognitive dissonance it triggers is so disturbing to
them, that active measures are taken to combat the apostates. It can easily get
nasty, and even dangerous for health and life of the apostate!

Some Muslim countries outlaw apostasy outright, and sometimes the penalty is
death. It means that apostasy is regarded as at least as harmful to society as
murder. If a society is built on a religious faith, apostates from that faith
are regarded as antisocial, as corrosive to society. And in a way that's true,
since they challenge the foundation of such a society.

In secular societies, you can't have people killed for that reason, and this
plain fact is a major civilization advance that had to be wrestled from the
hands of the religious. But religions in such societies have developed other
coping methods. Amongst those is "shunning", i.e. the ostracism of the apostate
from the community, with two motives. Firstly to make the experience as
terrifying as possible for the apostates, hoping to make them give up. Secondly
to protect the remaining believers from the "bad" influence of the apostate.

Of course, this lays bare the weakness of the religion, showing how little
confidence believers have in the persuasiveness of its teachings, and how
quickly they abandon their own loving and benevolent stance they otherwise brag
so much about. It shows how all the niceness is a rather thin veneer over a core
that is built mainly on loyalty and obedience and, of course, fear.

Ironically, this confirms what the apostates have found out, and what brought
many of them to leave the faith: The hypocrisy of it all. That reality doesn't
match the outward facade, the love and care they are so eager to display. That
the religion is just a very well polished turd. And the apostate has broken
through the polish, and thus "betrayed" the faithful.

Another coping mechanism is to attack and decry the apostates. They have been
misled (by the devil, usually). They gave in to their sinful nature, or wanted
to sin. They weren't serious in their search for the truth, and neglected
context, or didn't read the right books. They were never "real" believers in the
first place. Anything goes, as long as it detracts from the fact that somebody
who was very much a dedicated believer, had left the faith as the result of a
conscious, often painstaking examination of the religious tenets and scriptures.

The apostate demonstrates with his example that one can leave the faith as the
result of applying rational thought, while following truth where it leads. For
the faithful, this is the ultimate challenge. If accepted, he would have to
concede that a rational path can lead *out of* the faith. The tenets of faith
usually have it that all rational paths invariably lead *towards* the faith!

To many believers, this is so serious a contradiction, that to wipe it off one's
consciousness and heal the cognitive dissonance, they are prepared to kill or
shun, as mentioned above.

If you are an apostate, you will know what I'm talking about. And if you are
about to leave your faith, be prepared for some real nastiness!

In my opinion, the humanity of a religious faith is measured in how the
believers treat apostates. That would be my litmus test. Do they shun them?
Attack them? Threaten them? Decry them? Heap unjustified reproaches on them?
Ridicule them? And how do they square this with their faith?
