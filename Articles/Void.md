# The Godless Void

If you are a doubting believer on the verge of deconstruction, you may very well
be concerned about a perceived vacuum that awaits you on the other end of it. A
moral vacuum, a lack of purpose, a lack of grounding or connectedness, a feeling
of getting ejected into a void. If there is no God, what replaces it? What fills
the God-shaped hole in you? Or in society?

This topic is related to the notion of nihilism, so you may want to read that
article, too. The common thread is that without God, there's something missing.
Something important, which requires replacing with something else, or else there
will be real damage of some form. It is one thing to try to assess what the
actual damage, or the actual threat is, or whether it is even real. But
irrespective of that, it is very relevant that many people feel it is important!
Even if that should only be a psychological effect, it would have real
consequences on both the personal and the societal level.

What is this void? What are people missing without God? Why are some people even
coming back to religion after having recognized what a lie it is?

To be frank, I don't miss anything. It doesn't afflict everybody. If you think
that since you need God, everybody needs God, you are mistaken. You shouldn't
extrapolate from yourself to others. But even though I am unaffected, I
recognize the problem. If a substantial proportion of the overall population
feels they need a God, religion is unavoidable, and will prevail regardless of
its veracity. Pointing out that it isn't true will achieve nothing.

There is a void in atheism that is imminent and should be obvious to everyone,
but many people miss it. Atheism is not a worldview. It is merely the rejection
of a belief in a God or Gods, but nothing positive offsets this. As a common
ground, it is pretty minimal. If you expect atheism to provide a basis for your
worldview, you are in for a disappointment. Atheism doesn't tell you what is
right and what is wrong, what you should do, what you can hope for, and what the
answer is to life, the universe and everything. It merely says that it isn't
God. It tells you that God is the wrong answer, but it doesn't tell you the
right one. It doesn't even tell you how to ask properly.

I believe God is a projection, i.e. the result of people externalizing their own
subconscious and picturing it in human shape or form. That's why God has so many
human traits. The problem is that you can see through that, recognize that it is
just a projection and not real, but you still can't stop projecting. You can
switch off the light, but then it gets dark, to put it jokingly.

The way to deal with this is to realize that it actually is a part of you that's
speaking. The way you see God tells us something about you. You can analyze
that, or have someone else analyze it. You see a loving, kind and endlessly
patient God? That might be your inner desires speaking. Perhaps that's what you
miss in your life. You like a strict, wrathful, vindictive God? Perhaps you've
been hurt, you are angry, and don't have a specific target, no lightning rod to
discharge it. You like God as a righteous judge? Perhaps you feel there's too
much injustice around you, and you are in no position to put it right, even
though you'd want to. Either way, God may serve you as a substitute for
something you're missing in your life. It would be a good start to be honest
about that. Honest to yourself, and ultimately also honest to others.

Perhaps even more importantly, you need others to communicate with. Your
cravings and frustrations can more easily turn into delusions when you are left
alone with them. The worst is when you have become so locked into your views
that others don't want to talk to you anymore. That's usually not their fault,
but yours. If you had something interesting and valuable to say, people would
enjoy talking to you. If you are rambling, shouting, preaching and cantankerous,
people will avoid you, and you are likely to disappear even deeper in your
self-dug rabbit hole.

Religions usually have an elaborate social network that ensures that you meet
others of your faith. That is a good thing as long as it doesn't become
exclusive, i.e. it prevents you from meeting people of other persuasions. We are
social animals and don't thrive alone, but we can also be led down the garden
path when we are only allowed to have contact with like-minded people. Atheists
are not immune from that, and, having escaped one cult, can get caught in
another. As atheism isn't a worldview, there usually isn't anything matching the
social infrastructure that churches provide. Atheists need to meet their own
social needs in a different way. You might have to become more proactive here.

Such social environments will not necessarily help you answer the "big",
"ultimate" questions, but you might find that they don't need a definitive
answer. Maybe they are the wrong questions to ask altogether, maybe they have
more than just one answer. There's no one, single plug for the void. Don't
expect anything as rigid as in a religion. Wasn't that one of the reasons you
left, anyway?
