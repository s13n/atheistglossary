# Confession

As a teenager, when I still attended catholic mass regularly, I used to like the
sermon best, because it was different each time, and many times I found it
thought-stimulating. I wouldn't always agree with what was preached, but I
usually found it worth thinking about. The other parts of mass bored me
increasingly, because it was all so repetitive.

But I gradually realized that even the sermons eventually became repetitive. And
I had become critical about the teachings, too. Much of what they taught just
seemed to be nonsense. I became increasingly doubtful and disenchanted, and
thought: Why am I here? What's the point?

And then, one sunday morning in mass, while I was openly reciting the apostles'
creed formula with everybody else, it occured to me that I was lying. I went
through the individual points in the creed, and I realized that I didn't
believe any of them. Not one.

* Believe in God? Nope.
* In Jesus? Nope.
* Conceived by the holy spirit and born of a virgin? Nope.
* Descended to hell and rose again? Nope.
* Ascended to heaven and seated at God's right hand? Nope.
* Will come again to judge? Nope.
* Holy spirit? Nope.
* Holy church? Nope.
* Forgiveness of sins, resurrection and eternal life? Nope.

So here I was, lying openly in church, in the middle of mass. I felt some guilt,
not because I didn't believe, but because I lied.

And I wondered, how many around me are doing the same? Who believes this stuff
in earnest, and who just plays theater, like I do?

I suddenly understood how this works on the level of mass psychology. How people
are brought in line, not by force, but by ritual. I understood why I lied: I
didn't want to cause a stir. I didn't want to disrupt. So I went along with a
ritual that expected something from me that wasn't true, and I lied out of
respect for the ritual. And I had to assume that many others did, too.

I understood that the catholic church didn't care whether I lied or not. It
cared whether I complied with their rules and teachings. It didn't matter what
was in my head. It mattered what came out of my mouth.

I understood that mass is an instrument for manipulating people. Your engagement
and contribution is welcome, but only within the boundaries defined by the
church in a top-down manner. Dissent is not welcome. When the liturgy has you
recite a creed or confession, the content is prescribed by the church, and not
by what you actually believe. You're not supposed to confess, but to comply.

I went into that mass as a doubter, and came out as a full-on atheist. Not
because of a significant change in what I believed, but because I had understood
how religion works.

I wasn't quite ready to openly announce my atheism to my environment. I had
respect towards my believing parents, and didn't want to cause any unnecessary
upset. But from there on in mass I kept my mouth shut whenever I was supposed to
recite something I didn't believe. And I gradually reduced my mass attendance,
against some pushback from my parents. I wanted to be truthful, not because I
thought a God wanted this from me, but because I thought I owe this to myself. I
may be an unbeliever, but I'm not a liar.

I still attend mass today in some rare cases, for example when somebody from my
environment and family dies. I think this is an act of respect for others and
their belief. But I am not going to recite anything, or do anything, that I
don't believe in, even when some ritual expects this from me. And there's some
part of me that still resents that I have to make those compromises, that
the churches create an environment where this is made awkward for me. It shows
to me over and over again that they value compliance higher than truthfulness.
But at least this confirms over and over again that I was right as a teenager,
and still am right today.
