# Biblical Literalism

When reading the bible, it doesn't take much to realize that it consists of
various different books that obviously were written by various different people,
some known and some unknown. It is also fairly easy to see that they're not all
representing the same opinion. It seems they don't even have the same character
and/or temperament. You don't need to be a scholar to come to this realization,
it is apparent from the text itself.

Yet, a lot of Christians firmly declare that the bible in its entirety is the
"Word of God", and as such free of errors. There's even the "The Chicago
Statement on Biblical Inerrancy" from 1978 which basically tries to elevate this
claim to a dogma. In many churches it is basically a required belief.

I can't overstate how ludicrous this appears to me. The whole idea is so riddled
with nonsense, it boggles the mind.

As a start, why should a God who allegedly is the creator of everything,
including humanity, even have to, or want to, write a book to explain his will
to men? Is **this** the best idea he could have come up with? Why didn't he
implant his will into people's mind directly? After all, it is said that God
wrote his moral law onto people's hearts, even those who don't believe in him.
If he can do that, what prevents him from inscribing in this way all that's
important to him?

Instead, he "inspires" a bunch of writers to write a confusing and convoluted
mass of text that is full of apparently unimportant material that has no bearing
on his will, stays unclear or even silent on important matters, and is
notoriously hard to interpret unequivocally, leading to numerous different
schools of thought that disagree on what God's will actually is. As a textbook
for educating his followers, it fails spectacularly. Clearly, inspiring a set of
human authors to write it was the wrong method. He should have written the thing
himself. He has shown to Moses on mount Sinai that he could if he wanted, hasn't
he?

In fact, I would argue that creation itself should be regarded as The Book, as
the "Word of God". It is his creation, and why shouldn't it be able to speak for
itself? Treating nature as the word of God bypasses the imperfections of human
scribes and doesn't necessitate the strange dogma that what they wrote must have
been planted into them by God. All you need to do is to learn the language of
nature, in order to decipher God's will. If you believed in God's creation, you
would embrace science as an attempt to do exactly that. No need for a bible!

Seen from this angle it seems utterly foolish to regard the bible as more
authoritative than nature itself, God's direct and authentic creation! Why
should God commission a book as his creation's user manual that is harder to
understand than the user manual of a cheap Chinese TV set, when he could just
have his followers read the book of nature directly?

Would it not be much more appropriate to regard the bible as a human commentary
on God's creation? As a collection of opinion pieces and anecdotes that
unsurprisingly disagree with each other here and there? It would require a
slight readjustment of how to interpret the meaning of the "Word of God", and of
"inspired". Why can't the "Word of God" be interpreted as "The word that speaks
about God", or "The word whose topic is God"? The bible would be inspired by God
in much the same way as your love letter is inspired by your loved one! In other
words, the inspiration isn't active, it is passive! God would inspire people to
write about him just because he is there as a topic to think and write about!
Heck, the moon can inspire people to write about it, just by being there and
doing nothing!

Don't take this as my tacit admission that God must exist, however. I don't
claim that in order to inspire somebody, that thing must exist. People can be
inspired by completely fictional things. In fact, those are often the most
interesting inspirations.
