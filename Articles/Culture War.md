# Culture War

Recently, a prominent erstwhile atheist came out as a Christian, and according
to her public confession it was not so much because of Jesus, but because of
"civilizational war". I happen to think that "civilizational war" is just an
even more dramatic term for what became known as the "culture war". They're the
same, except for the degree of madness involved.

Now, I can understand, sort of, that people who were locked in an abusive,
fundamentalist religious environment are quite mad when they finally get out.
There's a good reason they're mad. It is a natural reaction to what they
endured. But it isn't sober, balanced or - umm - healthy. The pendulum is
swinging violently. You'd wish the person encountered, and immersed themself, in
an environment that is level headed and tolerant, and thus provides an
opportunity for the rage to cool down and give way to a more seasoned stance.
Alas, to those people such a stance appears lukewarm and accommodationist.
They're in a belligerent mood, and want to safeguard others from the fate they
had to endure.

I wish those people would focus more on their own personal and emotional
wellbeing than on waging a greater war on behalf of society.

But, sadly, and admittedly, there's an increasing overabundance of people who
seem to be intent on waging some sort of ideological war. It doesn't bode well
for our collective future, and I fear we're heading for a new period of real,
physical wars because of it. Needless to say, none of the ideologues would come
out of this as the victors, they will just have destroyed much of what they
previously had, and find themselves in the same ruins as everyone else.

Because what culture, or what civilization, can we defend in a real war?
Justice? Forget it. Truth? That's going to be the first casualty of war.
Tolerance? Gimme a break! Freedom? Well, that might be the only thing you can
win in a war. If you survive it. It might still be worth fighting for, though.

Seriously, if you are being threatened by an adversary that wants to subjugate
you and rule over you, you have a good reason to fight. But I mean real
subjugation, not imaginary subjugation. I don't mean you having to endure people
who disagree with you. That's not subjugation! I don't mean you having to obey
the rules of your society, such as paying taxes and accepting the results of a
democratic election. That's not subjugation! Subjugation is when you are being
deprived of rights that other people in your society have. And if you think that
other people in your society should be denied some rights that you have, it is
you who wants to subjugate others!

So what is a legitimate motive for a culture war? I'd say it is freedom and
equal opportunity. Not adherence to any ideology, religious or otherwise. And
freedom doesn't mean freedom from doubt or freedom from criticism. If you want a
form of freedom that doesn't equally apply to others, it isn't freedom, it is
privilege.

You don't fight for freedom by removing it from others. You might think you are
justified, because you are fighting someone who you recognise as a danger for
your freedom. But from the outside, you will very quickly be seen as fighting
against freedom, too. Always remember: Freedom isn't a zero sum game. It is not
a cake that you compete over, trying to get the biggest slice. Freedom isn't
freedom if it isn't for everyone. If you fight for freedom, you don't fight for
yourself, you fight for everybody. Even for those who don't agree with you!

This is meant as a warning for culture warriers on all sides. Take yourself back
for a moment and think about what it is that you are fighting for. Is it your
freedom or everybody's freedom? Is it about having your way or allowing
everybody to have their way? And then think about what brings your aim forward!
What would make people reconsider their position when they don't yet share
yours? What would reassure them that you're not going to deprive them of their
freedom to disagree with you? Which positions do you consider valid and
admissible, even though they differ from yours? And what about the others, what
makes them inadmissible for you, and why do you think you can say that?

And, while you are at it, consider who your adversaries in this war actually
are. If it is the extremists, fair enough. But all too easily, you divide the
landscape into a simple "for or against", and nolens volens you're fighting half
of the world. If not more. If you fight islamic extremism, for example, you
might have a million adversaries. If you fight muslims, it will be a billion.
I'm not claiming the numbers are accurate, I want to illustrate a point. If you
lump people into an ideological category, you basically signal to them that you
expect them to behave according to the category. They will understand that they
won't have any benefit from assuming a sophisticated position, as you'll not
recognize that anyway. So they will stick to their party. You have made them
fulfill the role you assigned to them.

And you'll find you have multiplied your enemies. Is that wise? Think about it!
