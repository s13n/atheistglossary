# Young Earth Creationism

So you, the young earth creationist, think the true age of the earth can be most
truthfully calculated by adding up a few genealogies in the bible, combine the
result with some rather legendary datings of kingdoms and reigns, and derive
from that a literal number of years since the creation. And all that is
supposedly superior to the work of many thousands of scientists in such varying
disciplines as cosmology, archaeology, geology, physics, biology, genetics,
chemistry and so on?

Give me a break! The genealogies and other datings in the bible weren't even
written with an overall chronology in mind, this much is fairly apparent! To
even attempt to derive the age of the earth from this was already dubious at a
time where there was absolutely no other way to tell, for example at the time of
Bishop Ussher who produced such a famous bible-based calculation. But now that
we have so much better methods at our disposal, why even waste a single thought
on this desperate and ridiculous method anymore?

I know why you hold on to it, of course! The bible must be true because it must
be true because it must be true! Even when it is not. Especially when it is not!

It is a dogma after all, and the distinguishing feature of a dogma is that it is
true even when it isn't. Otherwise we could just call it a fact.

The actual dogma, of course, is that the bible must be literally true, since it
is the Word of God. Literally true means that the numbers must also be right.
When the bible says that Methuselah died at the age of 969, then this means that
he lived for 969 years. How else could it be? Would God use figurative language?
Would he exaggerate? Nah! Never!

But wait! Lying for Jesus or for God is OK, right? Origen, the church father
writing in the 3rd century put it like this in his refutation of Celsus: "Don't
you say yourself, Celsus, that deception and lies may be 'used as a cure' at
times? So would the use of such a cure have been absurd if such a cure could
have accomplished salvation? For some are of such a nature that a few untruths,
such as doctors sometimes use towards their patients, are more likely to put
them on the right path than the pure truth. [...] For it is not unreasonable
when he who 'heals sick friends' has also 'healed' the people he loves by using
such means, which one should not use preferentially, but only according to
circumstances"

Following Origen, even God himself lying occasionally for a good cause would be
OK. And, indeed we have witnessed numerous young earth creationists lying for
their good cause, albeit more than just occasionally.

But there's a snag! If it is OK even for God to lie for a good cause every now
and then, why is it such a strict dogma that the bible must contain the literal
truth and nothing but the truth? Could it not be that God cheated a bit in his
creation story, for the good cause of making himself appear a bit cooler when he
speaks everything into existence in just six days, rather than laboriously
manufacturing a whole universe and needing 13 billion years for it? Furthermore
who in biblical times would have understood what a billion is, and how it was
different from "a lot"?

So perhaps you young earthers should be a bit more consistent, and allow for God
and the bible what you have been practicing all along! Grant the bible what you
grant yourself, namely a flexible approach to the truth. Six days or 13 billion
years, the difference isn't all that great if you think about it properly.
Perhaps God's nap on the seventh day ended up taking a bit longer than planned.
Such things happen, and who knows, it could well have been for the better.
