# Sin

The concept of sin has been taught to me from very early on. But as far as I can
think back, it never sat easy with me. I have been thinking about what the
problem with it is, as far as I am concerned. It took a while for me to work it
out.

I used to think that sins are merely wrong deeds, and that's that. But then
there was the "original sin", which meant that I could be sinful without
actually doing any wrong deed. In this instance, sin was a condition, not an
act. And that made no sense.

Gradually, I came to realize that what I recognized as sin was what others
declared to be sin. Sins where things I could read about in religious
literature. There were catalogs of sins, which said that this was wrong and that
was wrong and I shouldn't do this other thing because God doesn't want me to,
and that even thinking about some stuff was a sin, too. Sins were rules God
allegedly made, which I had to obey. Usually there was no explanation, it was
enough to note that God didn't want it.

The concept of a wrong deed was different. Whether something was wrong was a
result of my own thinking. I didn't need to read it up somewhere, I needed to
think about it, about its consequences, about whether I would still find it
right if I was on the other side.

I know this is my own interpretation and rationalization, but it makes sense to
me. Sin is an externally devised law, while right and wrong is an ethical
judgment I make on my own and for myself. To learn about sin, I need to read the
scriptures and other religious texts, while for learning right and wrong I need
to educate my sense of ethics and justice. For the former I need text
comprehension skills, for the latter I need understanding, and good judgment
skills. The latter makes higher demands on myself than the former.

It follows that it can be right to sin. It is not difficult to find examples in
the bible. To pick a famous example from the old testament: It would be a sin to
disobey God when he commands the extermination of the Amalekites. Disobeying God
is always a sin. But in this case it would be the right thing to do. God's
command is clearly unethical and wrong, and a good person would refuse to do it.

Now, of course I know the usual answer to this: Who are you to think you know
better than God? God is by definition both more knowledgeable and more good than
you are, so when you disagree it can only be your own fault. Failing to see it,
the best you can do is to obey.

This is wrong for two reasons. Firstly, there's a difference between not
understanding something, and being complicit in it. If I genuinely disagree with
your demand, I will not obey, even though I may not understand it fully. I can
hardly think of something more wrong and despiccable than killing people without
understanding why, just fulfilling orders of a superior. I would be a hitman, a
criminal, trying to absolve myself from any ethical responsibility by reference
to my principal. Receiving a command doesn't absolve me from my own moral
responsibility. It is even more absurd to do it for a God who ought to be able
to do the dirty job himself.

Secondly, people never get their orders directly from God. Some believe they do,
but I think they're either a fraud or mistaken. The orders come invariably from
some self-appointed earthly deputy of God. Sin is a concept that is (ab)used by
power hungry humans who try to hide behind a God to make their own goals seem
authoritative. The bible is no exception. It was written by people with their
own agenda, trying to use God as an amplifier and facade for their own goals.

The story of Moses at Mount Sinai gives it away when you read it in the right
spirit. He pretends to have received the commandments from God, having made sure
nobody followed him on the mount who could find out. Having come back down he
shattered the tables when he saw his people worshiping without his permission,
and went on to cause a bloodbath as an act of terror to get them back in line.
Moses deliberately positioned himself as the middleman between his people and
God, so that he could control them, and used every stratagem available to him to
secure his position. Had the tablets been from God he would never have smashed
them. He realized that they were ineffective for securing his power, so he got
rid of them and resorted to blunt force and terror. For him, the commandments
were not about right or wrong, but about power. He tried to impersonate God so
that he could make the rules. Consequently, and rightly, his rules are called
the law of Moses, and not of God. God only served as his facade.

So for myself, I resolved to do what's right, and not worry about sin. As long
as I'm doing what's right, I'm happy to sin with abandon. And if you still think
that this is contradictory, you haven't understood.
