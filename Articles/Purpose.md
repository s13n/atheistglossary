# Purpose

So I'm living my purposeless, pointless life as an atheist, and guess what? I
don't care.

I genuinely fail to understand why people feel the need for an external purpose
that sombody else imbues them with! Is it a general lack of creativity regarding
what to do with one's life?

I've been thinking about this. I imagined my own parents in the act of
conceiving me. Would I feel better if they had planned to conceive me, and went
about it with an earnest sense of duty and a prayer on their lips? Or if it had
happened in a semi-intentional way when they were just having fun with each
other? Of if my father had raped my mother (just hypothetically. I'm not saying
he might have - he was never violent against my mother in any way)? Or if it
turned out that someone else entirely was my father, and my mother just kept
quiet about it (just hypothetically. I'm not aware of anybody who would have had
a chance)?

Would any of those alternatives matter for my own life, for my sense of purpose?

I mean, from my own perspective, I am who I am, no matter how I came about. I
can only play the cards life has dealt me. When my parents have had their idea
of what kind of son they would have liked to have, and what that son was
supposed to do, would that actually have made my life purposeful?

The way I'm wired, I would rather not do what others want of me, I have my own
ideas of what to do. Although what has become of me is sort of OK in the eyes of
my parents, it is rather different from what they had imagined and wished for.
In a certain sense I have thus blasphemed against my creators - taking my
parents as my creators.

So why would I draw any kind of comfort from being created by a divine being?
From my perspective, it is totally irrelevant who created me, how, why - and,
indeed, whether. I am there, I am me, and I am in charge of my life. The purpose
of my own life is of my own making, regardless. Even if I had been a completely
random creation, it wouldn't subtract from my purpose.

So if you are amongst those who believe, that it is their being created by God
that gives them purpose, what do you think this purpose is, and how does it
differ from a purpose that doesn't derive from creation? To put it simply: What
is the purpose you think God gave to you?

I found it surprisingly difficult to get a clear and concise answer to this
allegedly fundamental question. There are various answers to it. You are
supposed to be fertile and multiply, in order to rule the earth. But why? You
are supposed to worship and praise God, but why? Why does God need you to do
that?

At the end, it seems to me that people think that God gives them purpose, but
they don't really know what that actually is. Religion certainly gives them
something to do, and something to hope for. But that serves to distract you! It
doesn't answer the underlying question.

But maybe that's the trick! You don't need to know what the purpose is, you just
need the comfort that there is one. It is about accepting that you are an
underling, and the lord, the king, the God knows what you don't. But it
doesn't matter, because he has a secret plan, and it isn't your job to know it.

For me, it boils down to something I have touched upon elsewhere, the
distinction between someone who craves for a king, a leader, and is ready to
submit himself to his authority, thereby also getting rid of much of his own
responsibility, and someone who is free, doesn't accept authority, and is left
with his own responsibility.
