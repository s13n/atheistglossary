# Guilt

If I should want to regard the ominous concept of "original sin" in a charitable
way, to interpret it as a potentially meaningful and valuable notion, rather
than the abomination Christianity has made of it, I would like to see it as
another term for "tragical fate".

The tragedy I'm talking about is that we do things that we might regret
thereafter. This is inevitable. We do not have perfect foresight, so we can't
possibly be aware of all consequences of our action or inaction. So even with
the best of intentions, we may end up causing great harm and misery. This
situation is not of our making, it is inherent in our being humans, or - if you
want - we inherited it all along from our earliest ancestors. It isn't a sin, so
it can't be an "original sin", but it is an "original condition". It can
sometimes feel like a sin, however!

Imagine for the moment that you have inadvertently caused an accident that
resulted in the death of another person. If you are a conscientious person, you
will feel guilty. You will accuse yourself of neglect, of being responsible for
the death of another person, and for the misery of that person's friends and
family. And often there are things that you objectively did wrong, something
that you could, and should, have done differently to avoid the disaster. Some
people are greatly debilitated by this feeling of guilt, which they can't escape
from, since they can't wind back the clock and change their fault retroactively.

Clearly, there is a blurred line between fate and responsibility. If you could
have done differently, if you had been more careful, you could perhaps have
prevented the misery. But how reasonable is it to expect this from you? How much
is that your fault, and how much is is an inevitable consequence of your human
condition? How much are you guilty? How much feeling of guilt is appropriate?
What exactly has your fault been? Or your sin, if you prefer that term.

Like me, you may subscribe to the notion that everybody is responsible for his
own actions, and accountable for his own mistakes. I maintain that this is a
good moral baseline, which contrasts biblical notions of collective
accountability, where people are held accountable for the mistakes of their
ancestors and/or their tribe, or vicarious accountability, where someone else
bears your responsibility. But this notion of personal accountability has its
limits where it is too much to bear.

In such a situation, your feeling of guilt may make you dependent on
forgiveness. But there may be nobody available to forgive you. Or those
available may not be prepared to forgive you. They aren't obliged to! After all,
would you forgive someone else in this situation? Would you be able to rise
above your hurt and brokenness, and relieve the guilty of his guilt?

We are in real tragedy territory here. There may not be a solution to a dilemma
like that. You may be jealous of people who haven't got such a conscience that
torments them, and who can move on after this. But that would seem unfair, even
immoral. Those self-centered people exist, but we don't see them as role models.

For conscientious people, the problem is very real: How can I carry on after
that? Why do I live when the other one is dead? How can I walk under the eyes of
those who have lost a loved one because of my fault? How can I pay for what is
not payable?

I can understand why people want a way of atoning for such mistakes, a way to
shake off, or work off, this guilt, so that one can live on in a meaningful way.
For a way to get back into balance with oneself, without appearing uncaring or
irresponsible to those who suffer. I think that religion can provide such an
escape. But in the case of Christianity, I think that religion replaces this
mental bondage with another one. It isn't really a liberation, it is a
substitution.

I think this guilt can only be dissolved with compassion. The guilty needs to
feel that he's not being ostracised for his mistake. It could happen to anyone
else, so how should we be licensed to judge? A tragedy is often just that: A
tragedy. Something that happens despite nobody wanting it to happen. The guilt
isn't wrong, it is an appropriate reaction of a responsible mind, but there's a
limit to it, namely when it becomes self-destructive and debilitating. It is
similar to grief in this respect. At some point you need to move on, and if you
think about it, even the dead ought to be willing to allow you to move forward.
There's no use in adding more damage to what was done already. Better to use the
experience to learn from it, to improve things, to turn things into a
constructive direction.

If you feel guilty, rather than looking for someone to take it off your
shoulders, for a savior who atones it in your place, maybe you can bring
yourself to think about how to turn things for the better. You may not be able
to help those who suffered from your mistake, but you can pay back with your
compassion and positive energy, with which you can make a small part of the
world a better place. I think that's atonement in the best sense.
