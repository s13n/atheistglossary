# Science

Science is a much abused term. That's unsurprising, given its undeniable
success, which tends to make other disciplines envious when they are concerned
with knowledge. There are religious sects that carry the term in their name,
without having anything to do with it. Of course, religions are also dealing
with knowledge, or at least they pretend to. This means science is a competition
for them, or even a threat. The relationship between science and religion
therefore has been, and probably will always be, fraught with tension.

Science has challenged religion's erstwhile monopoly in the world explanation
business, and by now dominates it in parts of the world (sometimes called the
"enlightened" parts of the world). The reaction was partly to fight it, and
partly to embrace it. "If you can't beat them, join them" - you get the gist.

Part of this game is to confuse people about what science actually is and how it
works. So let me explain. Forgive me if I state the obvious.

When talking about knowledge, what it is and how to attain it, we usually speak
of "epistemology". That's the philosophical term. Science fits into this as a
method. It is a way how you can accumulate knowledge. It is an extremely
successful method, but it doesn't purport to be the only one. However, it is
important to think about why it is so successful, because it tells you something
about knowledge in general, and about other ways of attaining it, and why they
fall short. Including religion.

Some people try to tell us that there's no real antagony between science and
religion, because they are about the same goal: attaining knowledge. But that's
wishful thinking. In practice, they do compete, because their methods compete,
and they are anything but equivalent! Some other people try to tell us that
there are separate domains of knowledge, and they require different methods. In
some domains the scientific method is appropriate, in others the religious
method is. I think that's also wishful thinking. For starters, there's no
inherent line which science can't cross. If you draw a line beyond which
religion is the only contender, you don't know how you can know anything at all
beyond this line. For all practical purposes this line is the boundary of what
can be known.

What do I mean with that? Well, how do you distinguish knowledge from belief?
This is the topic of another article that is a kind of sibling of this article,
and I'll defer you to it here. It turns out that this is the fundamental
question that every method of attaining knowledge must answer. "How do you
know?" is the very question that defines and begets science. Science is what
results when you turn this question into action, into an endeavor, and finally
into a method.

It took surprisingly long for humanity to realize that Gods don't help here. It
is conspicuously easy to say: "I know because God told us so", thereby deferring
to some holy scripture. But even the less gifted have realized that scriptures
aren't that clear and unambiguous, and gradually people came to realize that a
deity which was supposedly the explanation for everything, actually explains
nothing. God is an epistemological dead end.

This gave rise to "methodical atheism" several centuries ago. This term means
that regardless whether you actually believe in a God or not, for the purpose of
attaining knowledge about nature you ought to ignore Gods altogether. You
conduct science as if no Gods existed. This realization basically coincides with
the beginning of the spectacular rise of scientific knowledge that continues
unabated up to the present day. This should tell you something important about
the relationship between knowledge and Gods.

It is fair to say that methodical atheism released an avalanche of knowledge
that is still rolling, and it is impossible to say where it will eventually
stop. Compared to that, Christian knowledge seems to be stagnating. That's not
entirely true, but much of what brought Christian thinking forward actually
derives from science, particularly from applying the scientific method to
religion itself. I am talking about archaeology, textual criticism and history
as well as psychology and cultural history, which may not always count as hard
science, but which all have drawn from the scientific method, and its focus on
checking theories against reality, and eliminating disturbing factors such as
bias and ignorance.

It is important to realize that the scientific method doesn't tell you when you
are right. It merely tells you when you are most probably wrong. When you check
your theory against reality by means of an experiment (perhaps even a thought
experiment), it isn't admissible to conclude that your theory is right when the
experiment succeeds. A successful experiment isn't a proof, it is evidence. It
doesn't remove all doubt, it tips the balance. A lot of successful experiments
build up confidence in a theory, hopefully up to a point where doubting the
theory becomes rather irrational. Typically, an experiment with an opposing
outcome carries a lot more weight than a confirming experiment, and rightly so,
because it likely points to a flaw in the theory, i.e. a case that isn't
explained adequately by the theory. If the result is genuine, and not the result
of some error in the experiment, it will be taken seriously by a diligent and
serious scientist.

And that's the secret: You've got to check your theories against reality. A
dogma doesn't help. An experiment does.
