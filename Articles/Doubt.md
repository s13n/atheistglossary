# Doubt

So you have doubts. How do you feel about it? Does it make you feel
uncomfortable? Guilty? In danger?

But why? What's wrong with doubting? You've been told something, or you have
assumed something, and you've come to suspect it may not be true. Isn't this a
completely normal, even beneficial, state of affairs? I mean, it makes you
search for evidence, doesn't it? It makes you inquisitive rather than
complacent. Active instead of passive. In my view, that's entirely positive!

Apologists also tell you that doubt is normal. Everybody has his doubt's, they
say, don't worry, it is God's way of testing your faith. So all you need to do
is keep the faith and the doubt will fade away.

But that's the wrong way to deal with doubt. It is the attempt to blunt the
doubt, and lead the way back to complacency. The way to deal with doubt is to
follow it, and collect information that may shed some light on the matter. In
short, to gain knowledge.

It is of course true that doubt is normal, in the sense that you will likely
retain some amount of doubt even after you have collected all sorts of evidence.
Life usually doesn't provide you with complete and indubitable certainty. But
that doesn't mean you should accept the doubt without any attempt to gain
further insight. You might of course decide that the doubt isn't important
enough to warrant the effort of collecting all sorts of additional information.
That's OK, but don't fool yourself! If the doubt makes you feel uncomfortable,
it must be important for you, and it is worth putting in some effort.

Are you afraid that your inquiries may uncover some unwelcome insight that
unsettles you further, rather than putting your doubts to rest? Do you fear to
open pandora's box? This may of course happen, particularly when a whole world
view is at stake. But then, odds are that your world view already hangs by a
thread. It isn't the doubt that's the problem, it is a world view on a sticky
wicket. You'd be better off not to prop up a house of cards, but to search for a
better foundation.

In my experience, brushing off doubts for too long will only tend to turn a
correction into a collapse, and multiply the pain. The right answer to when to
deal with doubts is always: Now rather than later. A disappointment is always
better than a shock and a crash.

Christian apologists are, unfortunately, mostly unhelpful with this. When it
comes to doubts in the Christian faith as a whole, they tend to send two
opposing messages: Firstly that doubt is normal and no big deal, something that
most Christians experience at some point, a phase that they go through. Secondly
that what is at stake couldn't possibly be more important! It is an attempt at
calming you and alarming you at the same time. Like telling you that your house
is on fire, but there's nothing to worry about. It is an attempt at gaslighting
you, a manufacturing of a cognitive dissonance.

This is, of course, the opposite of what you need to soberly investigate the
situation, collect evidence, and work out what appears most plausible to you.
Just when you would need to adopt a methodical stance, a sober and patient
composure, and employ your critical faculties, your reason and sanity, they put
you in panic mode and erode your confidence in your own abilities to work this
out.

You need to get out of this mindset. The fact that you have doubts is not a sign
that there's something wrong with you, but that there's something right with
you. Your brain is doing its work and duty. That's what you've got it for. You'd
better let it do it, and arrange for favorable conditions, i.e. minimize panic
and emphasize rational and methodical thought and inquiry. Think of a judge.
What would a judge do? You expect judges to be sober, calm and methodical. They
collect the available evidence, have people from different sides testify, and
work out what part of this is reliable, and what isn't. It isn't a guarantee for
arriving at the truth, but it comes close to the best method we have, as the
mere mortals we are.

And consider: If you can't expect your God to support you in such an honest and
diligent investigation, if you think that he would prefer you sticking to your
faith in panic, why are you worshiping him? What would be the virtue in this?
Would that be the loving, benevolent God you always believed in? Rather not, I'm
sure! So what do you have to fear?
