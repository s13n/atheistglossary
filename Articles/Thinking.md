# Thinking

I am very much a disciple of the Enlightenment, if that is even an appropriate
thing to say. I try to live the "Maxim of Enlightenment" that Kant has
formulated: "Have the courage to use your own understanding". See also my
article on Enlightenment. This maxim encourages, even demands, two things from
me. Firstly I need to develop my understanding, which means I need to learn.
Secondly I need to use my understanding, which means I need to think. To think
by myself, to be specific, not to devolve it to someone else. As a pun on
Descartes, I like to say that "I am, therefore I think".

The bible does not typically encourage this at all. Frequently it discourages it
explicitly, and demands obedience instead. One of the more explicit quotes is
from Proverbs 3:5-6 "Trust in the Lord with all your heart and lean not on your
own understanding; in all your ways submit to him, and he will direct your
paths." This goes directly against my own philosophy in a very basic and
fundamental way. I can't possibly reconcile this with what I hold dear, and it
is one of the most important reasons why I can't be a Christian, or an adherent
of any bible-based religion, for that matter.

I know that many believers do not take this quite as seriously, and of course I
have met many Christians who think for themselves, and do that very effectively.
As long as the thinking doesn't come into conflict with their faith, or with
what the bible says, there is no problem. But when it does conflict, the bible
essentially demands to stop thinking for yourself and blindly "Trust in the
Lord". I can't and I won't! I know that many Christians decry this as pride, but
it rather is principle. If the bible or God or priests or whoever can't make me
understand through explanation, they can't make me oblige, except through force.
I'm not going to yield to people commanding me out of laziness or assumption of
superiority, and I don't expect anybody else to obey me in this way.

This is not a convenient or even selfish stance. It places high demands on
myself. I need to try to understand where others are coming from, and what their
point is. I need to humble myself in the face of superior knowledge and
experience, and recognize when my own understanding is insufficient for coming
up with a good conclusion by myself. But at the same time I must recognize when
others are trying to punch above their weight, and pretend a level of knowledge
they don't have, or are simply wrong in their understanding. It means trying to
be neither overconfident nor underconfident. It is impossible to always get this
right, but that's no excuse for not trying.

I have little patience for Christians trying to tell me that God is so much
superior that I have no chance understanding him properly, so I must always
yield to his divine knowledge. And that God's will as revealed in the bible is
so deep and complex that I can't hope to fully understand it. Yet they treat me
as if they had it all figured out, and they knew exactly where I was wrong. They
are arrogant and at the same time try to come across as humble. That's
hypocrisy. If God should want something from me, I think I can expect him to
make me understand, and not just make me obey. If the bible can't properly
explain something to me, I don't accept the blame for that.

In my experience from reading the bible, God is not difficult to understand at
all. It only gets difficult when you try to reconcile him with your idealistic
notion of a perfect God. The God of the bible is in no way perfect, quite far
from it in fact, hence the notion of a perfect God is only going to mess up your
brain. If you take it from the bible as it is, taking into account that
different authors have different viewpoints, and filling in some context where
necessary, God is not mysterious at all. He wears his vices on his sleeve. If
anything, he is less complex than ordinary human beings. He appears limited,
handicapped, emotionally inept. Bent on compensating his own deficiencies with
excessive force.

If you allow yourself the freedom and the courage to use your own understanding
of the bible text as it is, reading it without the preconditioning received from
religious education and indoctrination, and without yielding to your own wishful
thinking and to an undue submissiveness, if you think for yourself what it says
and what it means, only then will you come to understand the bible the way it
presents itself, the way it speaks for itself. Otherwise you are in danger to
read something into the bible, rather than from it.

That may well be very enlightening.
