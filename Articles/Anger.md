# Anger

If you are a recently "deconverted" Ex-Christian, you will likely be rather
angry. You may admit it to yourself or not, but it is actually fairly difficult
to not be angry about having been fed such nonsense by people you have trusted
all along.

It is the anger of the betrayed. "Hell hath no fury like a woman scorned" is a
well known proverb that is supposed to be about rejected love, but the fury of
the betrayed is not specific to women. The religious betrayal is all the worse
for having started at a very early age, when you were totally dependent on the
sincerity and benevolence of your minders. To find precisely those at the
forefront of the betrayal drives a thorn deeply and painfully into your side.

Trying to suppress and hide this anger wouldn't work and hence isn't going to be
a good coping strategy. It would only make you look erratic and irritable, and
people would have difficulties figuring out what the problem is with you. It is
also likely to make the anger more explosive. But acting the anger out
indiscriminately is also going to cause problems. You've got to work with your
emotions and understand them. Understand what their roots and sources are. And
understand what you actually want. You might be able to channel the anger into
something useful and beneficial. Beneficial to you, at least, but maybe also in
a more general way.

Such angry people have written books, founded organizations, started Youtube
channels and podcasts, organized conferences and debates, thereby trying to
carry the problem back into society, to help people and find comrades. Lots of
beneficial activity has its origin and driving force in personal anger about
something that people find deeply wrong and offensive.

Maybe this is the evolutionary benefit of anger: To propel you into action, and
to make you direct your energy at changing an insufferable condition.

Having been betrayed, however, often evokes desires of revenge and retribution.
I don't need to say how destructive this can be. So you'd better ask yourself
what it is that drives you. Do you want to pay back to those people that you
feel have betrayed you? I'm not saying you shouldn't, it is a totally
understandable reaction. But you'd first have to be honest to yourself about
your motives. Only then can you channel them into something that actually helps.

Think about it: Who has betrayed you consciously? Who knew better and played
theater to you, and why? Who was trying to be genuine, but perhaps mistaken? It
is an unfortunate fact of life that people can be catastrophically wrong with
the best of intentions. So what were the intentions of those people who wronged
you? It is not only the consequences that matter. Intentions matter, too! Don't
punish someone who doesn't deserve it!

My own fault line here is, when people don't respect my own autonomous will and
decision. It is OK to have differing opinions, and who can be absolutely sure to
be right? If you think that I'm wasting my life, that I'm bound to go to hell,
that I live a purposeless life, that's fine with me. You're entitled to an
opinion like that. But I'm equally entitled to have my own opinions on these
questions, and when it comes to my own life, I am the one who calls the shots.
You have no right to ram your opinions down my throat, no right to decide about
my life and how I conduct it, and no right to blackmail me emotionally. If you
have a problem with my behavior towards you, you have a right to complain. If
you have a problem with the way I live my life, that's your problem, not mine.

And when it comes to raising children and educate them to be decent and
successful persons, the first priority should be to help them become such
autonomous people who can conduct their own life and develop their own opinions.
It is not about telling them what's right, but to let them find out what's
right. It is about education and not about indoctrination. About trust instead
of control. It is about liberating a mind, instead of incarcerating it in an
ideological cage full of fears. Children will not be successful in their life
when they think exactly like you. They will be successful when they think by
themselves.

When your parents and teachers have done that, they have done right, even when
they taught you stuff you found out to be wrong. They got the most important
lesson right: To make your honesty rise above your desires. To make you capable
of thinking for yourself, and cast off an opinion that turns out to be wrong,
however dear it may be.

And if they didn't, be glad you learned it anyway. Show them how that's better
than following some doctrine that was handed down by dubious authorities who
pretend to speak with divine authority. Be an example, not an avenger.
