# Accuracy

Some Christians try to gain a lot of credibility for Christianity from the fact
that the scriptures we have seem to have been preserved quite accurately. I
don't consider that convincing, and I have to say that there is a tendency to
use this argument in a fradulent way. Let me explain.

First some background: Until the printing press was invented in the 15th
century, books had to be copied manually, which wasn't just laborious and
expensive, it also was error prone. But books needed to be copied, not just to
spread them, but also to replace old, worn out copies. Only in the luckiest of
cases do books survive for thousands of years, usually because they have been
buried somewhere and were forgotten, until they were rediscovered much later.
The normal case is for us to have rather late copies, with all the accumulated
errors from many acts of copying.

Interestingly this is reminiscent of evolution, which also relies on copying,
with the occasional error creeping in.

Now, to get an accurate copy, you'd have to be very diligent when copying, and
you'd have to use as old an original as possible, so that the number of
intermediate stages is minimized. But you can only copy what you have. And once
you've lost the original, how can you determine the accuracy of your copy? In
principle, you first have to reconstruct the original before you can say how
accurate your copy is. But how do you reconstruct an original we don't have?

If you just have a single copy, that's practically impossible. But the more
copies you have, the more chances you have to figure out their trajectory of
transmission. You can analyze the differences to develop an idea when each of
the differences got introduced. Ideally you can then plot a family tree of
copies and infer common ancestors. To some degree that allows you to reconstruct
what earlier copies will have looked like. And that allows you to assess the
accuracy of your copy. This is painstaking and laborious, but in the case of the
bible lots of people have worked on it and published their results.

Now, it should be clear that all this can't give any indication of whether the
content is true. It only tells you how accurately it was transmitted. You can
accurately transmit a falsehood in just the same way as you can transmit a
truth. And there's where the fraud comes into it. In apologetic circles, the
accurate transmission is often used to argue for the veracity of the content.
This should be recognized as obviously wrong, but amazingly many people swallow
that.

Now that we have the printing press, the internet, and computers, it is very
easy to make accurate copies of books, even books that contain no truthful
statement at all! We have many millions of accurate copies of Harry Potter, but
the content is still fictional, a novel. It even contains references to existing
locations, for example King's Cross in London, and yet the story is invented
from front to back. Most people understand that, but why should this be any
different with ancient books and manual copying?

So what does it even matter for Christian apologists? If even an accurately
copied document can say wrong things, how does accuracy help their case of
defending Christianity? Well, accuracy at least means that if there is a mistake
in the text, it likely was there in the original already. With accurate copying,
the chances are smaller that the error crept in later. In fact this raises the
stakes for apologists, since if there should be a mistake in the content, they
wouldn't be able to blame the later scribes. It would look bad for the original!

For us laypeople, it is important to distinguish between truthfulness and
accuracy. They are not related. Having one is no indication of the other. If
somebody tries to suggest that to you, even implicitly, they're trying to
deceive you.
