# Foxholes

There are no atheists in foxholes, so the old saying goes. What it really means
is that when you are mortally scared, if the bullets buzz around your head, if
you acutely fear for your life, atheism has nothing to offer you, and you turn
to God, you pray to him for your life.

I don't know whether that's true, and fortunately I haven't gotten into such a
desperate situation that I would have experienced myself, although I did come
close once. I once nearly drowned when swimming in the sea. I got into currents
that obstructed my return to firm ground underneath my feet, and after an
exhausting struggle I ended up on a ragged rock, bruised and glad I made it.
Maybe I was just too busy surviving for being able to spare a thought of God.
Maybe if I had just been sitting there in a foxhole, with nothing to do but
waiting for the terminal bullet, maybe then I would have started to pray. I just
don't know. But the actual point is, what does that argument actually mean? What
is it intended to tell us?

Let me offer my take on it. I believe we all have an inbuilt will to live. It
has nothing to do with God, it is a result of our evolution. I think it is
evident that someone with a will to live would expend more effort to actually
survive compared to someone with no such will. This translates in an advantage
for the genes of the former to propagate, and with time those with a will to
live will more and more outnumber those without.

This is a subconscious thing, and I'd posit that it precedes humans by a great
deal. I think it is the same with many animals, even those who lack
consciousness. It isn't a conscious will, it is a much more basic, existential,
subconscious will. You could call it a primeval psychological force. It is the
driving force for the antelope trying to flee the lion, or the crows that
harrass and attack predators to protect their offspring.

Do not make the mistake of confusing this with an individual will to live. The
evolutionary forces act on the genes, not the individual. Altruistic behavior is
fully compatible with this, and not at all a counterargument.

In a war, commanders use this to their advantage. They know full well what
people are capable of when stripped bare of their cultural veneer, and are
thrown back to their subconscious forces. They want to bring out the animal
within man, and use it for their purposes. So if you are exposed to your own
naked fear of death, and stripped of any protection culture or family could
provide, you exhibit your most animalistic behavior.

That's not evidence for the existence of a God. That's evidence for the
existence of a primeval will to live. If there are no atheists in foxholes, and
if prayer would actually help, we shouldn't have many dead soldiers, right? But
we do. They may pray or not, they die in droves anyway. But before they die,
they give their absolute utmost to survive, often mobilizing forces they didn't
think they had.

That's not easy to explain with a God, but it is effortlessly explained with
evolution, including the propensity to appeal to supernatural forces when in
grave danger. Anyone who wants to create an argument for God from that must be
very cynical.

Note that the more extreme forms of Christianity, i.e. the fundamentalist
evangelicals, are only too ready to invoke your fear of death to scare you into
their faith. In a way they seem to know what is at the very bottom of your
psychology, and they want to get you by the subconscious, by what they can
confidently expect in every human. They try to use the foxhole effect, in a
similar way as the military commanders use it. They want to remove the veneer of
culture from your thinking and throw you back to the naked struggle for
survival. When you're a soldier, or when you're in a cult (is there much of a
difference?), a conscious mind with all the ifs and buts is only a hindrance.

This human reaction, this mechanism, had survival value, and perhaps it still
has. And given how deeply it is built into our system, we should definitely take
it seriously. There's no point in denying such a fundamental part of the human
condition. But at the same time it should be clear that it doesn't attest to any
supernatural truth. It is firmly rooted in our nature, even our biology! If
anything it attests to that! And given how dangerous it can be when abused, we
should value the tenets of our culture, which are aimed at mitigating it.
