# Rationality

Some religious circles are busy trying to convince themselves, and convince
everybody else, that their religion is rational. That there is nothing wrong
with it, and you can rest your mind in the confidence that it all makes sense,
even if you may not oversee the full extent of it.

You can see this at work when people defend their faith against people like me
by saying that it needs to be taken within its proper context, that the critique
is shallow and uninformed, that it fails to engage with the full depth of the
matter. All the while they don't usually offer any help as to what the proper
context would be, and how the critique would be resolved in this way. They're
effectively telling you that you're too dim to get it, and they save themselves
the futile attempt to bring you up to snuff.

This is arrogant, of course, but it is even worse, because I'm quite confident
that most of them can't actually follow up with any deeper explanation at all.
There never is anything substantial forthcoming. Their rebuttal is empty. They
appear to be telling themselves that surely, it all would make sense if one
thought about it more deeply, but they are not the ones able to do it. They're
not quite prepared to admit it, but they are happy to pass you on to someone
they think can deliver. Their favorite apologist. But whenever I follow their
recommendation, I often find something that is at least as vacuous, but even
more overconfident and pompous.

I have come to the conclusion that not only is this not rational, it is not
intended to be rational. The rationality is a veneer, but only one of several
conflicting veneers that can be put on depending on the situation. The
contradictions are not a bug, they are a feature. They absolve people from
having to think, and draw awkward conclusions. In other words, they serve to
allow people to be, and remain, irrational.

If you hold two conflicting opinions in your mind simultaneously, rationality
would demand that you resolve the conflict. If both opinions are dear to you, it
would lead to an awkward dilemma, you would have to sacrifice something. The
reward would be a more coherent worldview, but it comes at the cost of a loss.
You may well fear the net outcome would be negative. In which case you might
prefer carrying on with the contradiction unresolved, and if you manage to cloak
it in some appearance of "higher" rationality, it might not feel like a conflict
anymore. In fact, you could even fancy yourself having arrived at a "higher
plane" of comprehension! Where, obviously, not everyone would be able to follow.

For example, if you believe in a God that is simultaneously loving and vengeful,
omnipotent and undetectable, like the Christian God, you have an obvious
contradiction. But you can't let go of enough godly traits to resolve it, it
wouldn't be the Christian God anymore! Instead of resolving the conflict, you
embrace it. You learn how to use it against your critics. You baffle them by
presenting them something that obviously makes no sense at all, and tell them
that they are too stupid to understand it, and that if understood properly, more
deeply, and in the right context, it makes supreme sense!

There is no way, of course, to make it make sense, but that is the point! It is
not supposed to make sense! It is supposed to allow you to keep contradictory
beliefs in your mind simultaneously, and to blunt any doubt or criticism that
you may develop by yourself, or that anyone else may level at you. If you have
ever despaired when trying to point out a contradiction to anyone who is hell
bent on ignoring it, you know what I mean. It may be blazingly obvious to you,
yet they steadfastly fail to see it! Because they would not allow a thought to
enter their mind, that would make them see what they don't want to see.

The cost is a divorce from reality. Reality isn't contradictory like that. They
are nurturing a phantasm. Of course phantasms can contradict themselves and
other phantasms. This is why those alleged "higher truths" won't ever form a
coherent whole, giving rise to numerous schools of thought and denominations. If
those "higher truths" were actually true, none of this would ever arise, because
people would come to the same conclusions when actually engaging in rational
inquiry.

The way out is a ruthless devotion to reality. There is something ascetic about
that, because you have to deny yourself the comfort of a convenient, but false
belief. You have to muster the courage to resolve a contradiction by letting go
of what turns out to be wrong. You will eventually land in a better position, in
a more confident and relaxed state of mind, because the need to make the
contradiction disappear from view is gone. It will make you a more honest
person. But you have to make the investment first, to be able to collect the
harvest later. It is a wager worth taking, and the rational thing to do.
