# Aliens

I always found the prospect of alien life in the universe intriguing. Judging
from popular science, and the way it is being presented in mass media, finding
alien life is a rather high priority and a prime motivator for a lot of science.
On one end of the scale, people fantasize about sophisticated, superintelligent
aliens who are way beyond us humans in their abilities. On the other end some
scientists already freak out over findings of traces of water on other planets.

While water seems to be a universal prerequisite for life, at least to us, it
should be clear that there is a long way from water to life. And from life there
is a long way to intelligent life. I am underwhelmed by the evidence for alien
life so far, I have to say. I frown upon those scientists jumping the gun,
although I realize that they can land themselves in the news that way. Perhaps
it is their 15 minutes of fame that drives them. I'd say to them: Wake me up
when you've found a self-replicating metabolism. I trust that'll take a while.

The superhuman aliens on the other end seem to be some kind of God replacement.
The way they are imagined, discussed and depicted cries out anthropocentrism.
Regardless whether you think of the xenomorph that appeared in Alien, or the
grey alien that developed into a pervasive meme after the 1960s, the appearance
is clearly derived from humans. A torso with a spine, sometimes ending in a
tail, sometimes not, two legs, two arms, a head with a mouth, a pair of eyes,
perhaps even a nose, and a kind of skull that presumably contains a brain, which
is why its size is typically exaggerated. There's no reason an extraterrestrian
alien should look like that, except that it appeals to the image we humans have.

Even on earth we have fairly intelligent animals like the octopus, which look
completely different. Even their brain is completely different, in that it is
distributed rather than concentrated in one distinct place. Is there any reason
an intelligent alien can't look like that? Yet, who has ever reported to have
been abducted by octopus-like aliens? Or even aliens that are different to the
point of being unrecognizable?

To me this is overwhelming evidence that it is human imagination with no basis
in any fact. Aliens are much too humanoid to be credible. In this it resembles
Gods, which are also humanoid imaginations, and therefore completely
implausible.

The irony is that while both aliens and Gods are being presented as way beyond
human comprehension, and human abilities, they are simultaneously presented with
lots of human traits. Something doesn't compute here. The pinnacle of absurdity
is reached when you are supposed to develop a personal relationship with this
allegedly incomprehensible God. It is at least as nonsensical as people
imagining having personal relationships with an Alien xenomorph. Some even
fantasize having sex with him, including imagining him being endowed with a
humanoid penis.

For me, the conclusion is that as much as I don't believe in Gods, I don't
believe in aliens, either. In both cases their humanoid character is a dead
giveaway of their imaginary nature. And just like with the Gods, I can of course
not prove aliens don't exist. But I'm convinced they don't, because if they did,
they would most certainly would be so much different from what we imagine, it
wouldn't even make sense to think about them.
