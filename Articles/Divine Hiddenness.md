# Divine hiddenness

This is an annoying term. Note that it presupposes that God exists, but for some
reason hides from us. But I don't even subscribe to the presupposition! God
isn't hidden, he doesn't exist!

This is one of the many instances where the way the question is being formulated
smuggles the desired conclusion into it right from the start. The moment I even
accept the question in a debate, I already have an uphill task explaining that
there's something wrong with the terminology right from the get-go.

And all too often, the debate directly proceeds to try to find the problem with
the people. Surely, it must be their fault that God is hidden, right?

I have little patience for this suggestive and dishonest debate style. There is
no point even talking about divine hiddenness when you haven't yet agreed about
God's existence. So God's existence is the question, not his visibility. You
might just as well ask about the hiddenness of the Easter Bunny.

On top of that, I am rather sure that if God really existed, and cared about us
even one hundredth as much as Christians believe, there would be zero reason to
talk about his hiddenness. He would be all over the place and leave no doubt
even for atheists. With all the properties that are typically ascribed to him in
Christian doctrine, his hiddenness is utterly nonsensical.

Once more Christians want to have it all ways at once. God is infinitely
powerful, benevolent and omniscient, but at the same time extremely secretive
and reclusive and ambiguous, so that in order to reach him you need to really,
really, really want it and pray for it and be patient and humble and worthy, and
perhaps when you're lucky you will be granted a sign of his presence, that more
sober people will immediately recognize as a misapprehension of an ordinary and
natural occurrence, brought about by wishful thinking. Is that the kind of
impressive showing worthy of such a mighty God?

Even worse, Christians believe that God and/or Jesus will stage just such an
impressive, imposing and unmistakable show upon the second coming! So he could
do it if he only wanted! But that would be too simple for some unexplained and
implausible reason. First comes an extended hide-and-seek period to raise the
thrill, right? A 2000 year extended drum roll ahead of the real show that never
comes...

And, of course, if God doesn't show up despite your needy and craving
evocations, it is you who's to blame for that, right? Surely you didn't want it
really really really enough! Your heart might not have been pure enough! You
might not have been worth it! The simplest, most plausible and most reasonable
conclusion from all this is excluded in principle, even though it is blatantly
obvious!

The world isn't so twisted that the obvious can't be true. In fact, the obvious
usually IS true. Sometimes, appearance can be deceiving, but that's no reason to
think it always deceives! More often, I would argue, it is wishful thinking that
deceives. The belief that there should be a God is clearly driven by wishful
thinking, since people bend over backwards to excuse everything that could
suggest this isn't true. When people behave like that with respect to other
topics, you'd immediately recognize the problem. So why not here?
