# The Cross

I recently had quite a laugh when I saw Seth Andrews comparing the cross with
the guillotine. What would a religion be like that has the guillotine as its
primary religious symbol, instead of the cross?

You think this would be ridiculous? Well, why? Both are instruments for
executing people. In fact, we nowadays think of the guillotine as quite a
horrific tool, and we have long ceased using it for its intended purpose.
Whenever we think someone has to be righteously executed, we think it needs to
be done in a "humane" way, which progressed from the guillotine to hanging to
electrocuting to poisoning.

But ask yourself: How would you want to be executed if it had to be? If the end
result is the same, what makes one method superior to another? I dare say it is
the amount of suffering that it entails for you. And, frankly, I think that the
guillotine probably wins here. If your head gets chopped off with one clean and
sudden cut, surely the amount of suffering is bound to be minimal.

Indeed the guillotine was invented for just that purpose: To make executions
"humane". As such, it was a definite improvement over the previous practice. And
it is a massive improvement over crucifixion. Crucifixion was specifically
invented to inflict a maximum amount of suffering. It wasn't enough to just kill
someone, it was a demonstration of power to such an extent that the delinquent
was submitted to as much agony as possible, and was deprived of all dignity and
humanity. It came as close to erasing the entire individuum and his legacy, as
was imaginable.

But somehow, we usually view the guillotine as the most despicable of the
execution methods! Pause for a second to consider why! Is it because of the
blood that's flowing profusely? But surely, that's not to the disadvantage of
the delinquent, who gets killed no matter what! It is more a matter of taste for
the surviving witnesses! We are usually quite unfazed when we don't have to
watch it, as for example in a slaughterhouse where the animals experience a
similarly bloody killing.

Contrasting this, we seem to have no problem carrying the symbol of a much more
cruel way of killing around with us. Hanging from a necklace, for example. And
we are used to talk about "carrying one's cross". Which is meant to express the
readiness to endure the hardships of life as they occur. The actual meaning
however, is rather closer to digging one's own grave.

That's how deep the Christian reframing of the symbol goes: We lose all
connection to what it actually meant. If we had a guillotine hanging on a
necklace, it would be rather provocative. But the much more sinister symbol is
viewed as not just normal, but even virtuous!
