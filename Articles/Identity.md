# Identity

When you consider yourself a Christian or an Atheist, is that your identity?

Sure, people usually say that they *are* a Christian, or *are* an Atheist. But
what does this actually say? When I gradually grew up to realize that I don't
believe in God, after having been raised in a Catholic environment, have I
somehow become someone different? I would have thought I'm the same person,
regardless of what I believe or don't believe!

Shouldn't the attributes that describe who I *am* be restricted to those that
are unchanging? Such as my gender, or ethnicity, or genetic makeup?

I know people use this sort of language all the time, without thinking much
about it, but it rubs me up the wrong way. In my opinion, this dubious use of
language gives rise of rather large misunderstandings about what one's identity
is. People apparently think their belief, their job, their preferences, even
their fashion style is part of their identity!

I regard this as bullshit. If you can change it relatively easily, it can not be
part of your identity. Hence, even though I use this dubious language, too, in
order to follow the usual custom, I do not think that I *am* an atheist. I agree
to this usage of the word only when it is understood to mean that I don't
believe in a God.  If it is meant to express my identity, or a facet of my
identity, I disagree.

I feel this abuse of the word *identity* has gradually become worse, in that it
appears to compartmentalize and pigeon-hole people more and more, not just along
the lines of ethnicity, skin colour, gender and language, which come closest to
permanent personal attributes, but also along lines of creed, political opinion,
job, social activism and so on. So you can allegedly *be* a white male young
earth creationist evangelical Christian, an anti-abortionist, an authoritarian
and traditionalist taxi driver. In my opinion, none of this is part of your
identity, except the white male part, simply because you could simply change
your mind and thus change your pigeon hole.

If you think about it, it doesn't matter what you *are*. It matters what you
*do*! This used to be part of the American Spirit! No matter where you came from
or think you are, what mattered was your work and your actions. That's why such
a motley crew of immigrants from all over the world could form a successful and
productive society and country. What has become of that? Have people forgotten
all about it?

My own country used to prosecute and kill people for what they were, and bloody
wars have been fought between peoples and groups who thought they were somehow
superior to the others. Are we to make the same mistakes all over again?

Being a human being is all the identity you should really need. If you think you
need any more, you are up against either a restrictive aspect of society, or up
against a psychological need. That's not necessarily wrong, but it isn't
essential. We need an egalitarian society that doesn't need all those pigeon
holes, and makes it easy to move between them.
