# Salvation

For a European like me it seems distinctly odd that so many US-Americans oppose
public health insurance, and at the same time have a desire for divine
salvation. All the while the former has shown to be useful, while the latter
hasn't.

Perhaps the crucial difference is that salvation is a purely personal thing,
whereas public health care is rather like an insurance, where some pay more than
they benefit, and some benefit more than they pay. Paying more than they benefit
seems to be hard to bear for some people.

Perhaps I'm outing myself as a commie (even though I would never label me that),
but I find this a normal and desirable (or should I say, Christian) thing: Those
who are disadvantaged and unlucky deserve to benefit more than they pay, and
those who are advantaged and lucky should acknowledge it by paying more than
they benefit. It is a social justice thing. The one thing one needs to look
after is to make sure that the system isn't being rigged and abused by the wrong
people.

The concept of salvation, in contrast, seems expressly designed to be abused. It
starts with an invented problem, for which salvation is sold as the solution,
and the benefit only arrives after death. Installments, however, are to be paid
right away, in this life already.

If you object to calling the problem invented, please show me that it is real.
As far as I can tell, the problem isn't something that you observe in reality,
it is inferred from "The Fall" in Genesis, which I have addressed elsewhere, and
I think this inference is nonsense.

In fact it seems to me that for many centuries, Jews did not infer from Genesis
that they were needy of salvation as a result of an "original sin" committed
by Adam and Eve. This appears to be a thing that only "dawned" on Christians. So
either the Jews were too thick to get it, or the Christians made it up. You can
guess what I think.

Anyway, why would a benevolent God have any interest in arranging things this
way? I can immediately see, however, why your religious leaders would want you
to believe it! They receive your installments!

I mean, your belief is your outlook, but if you want to convince other people,
perhaps you shouldn't support such dubious promises that would even make an
insurance salesman blush.
