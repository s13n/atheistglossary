# Anthropocentrism

We can't help picturing ourselves in the middle. That's our psychological
perspective. But we're not. It is a mistake. This has given rise to numerous
disappointments, extending right to our entire worldview. Everything we
experience is tainted by our self-centered bias. Even though we sorta know that
the earth is round, we still talk and think in terms of "up" and "down" (also
figuratively, like up in heaven and down in hell). Up and down is wired deeply
into our subconscious worldview. But "up" and "down" is pointless in the bigger
scheme of things! As soon as we leave our local surroundings, the concept loses
its validity. And that's not the only concept that turns out to be
anthropocentric. Getting rid of our anthropocentric bias is one of the most
important problems in developing a good worldview that holds up in the face of
scientific discovery.

This bias is strong and pervasive. People began with the idea that they are at
the center of the universe, they are the pinnacle of creation, and its ultimate
purpose. And it seems that many people are still stuck in this illusion. But
then we learned that earth is a ball rather than a disk, which assaulted our
sense of up and down. Then we learned that earth is not the center of the
universe, that we actually revolve around the sun, and there are some more
planets circling it. Then we learned that the sun isn't the center, either.
Instead, it is a random sun in a random galaxy of which there are innumerable
other examples that may or may not have planets where conditions permit the
evolution of living organisms.

We also learned that we are nothing special in that we evolved in exactly the
same way as any other life form, and will likely go extinct at some point,
possibly very soon because of our own stupidity in dealing with the implications
of our intelligence (now, there's an irony!). And we learned that our cognitive
system and decision making isn't all rational, not even all conscious, and
certainly not all truthful.

This is all very humbling. It leads further and further away from the Christian
message, which tells us that we are at the center of God's attention. So if you
look at it, the Christian religion is an excercise in anthropocentrism through
and through. The believer pictures himself to be made in the image of God. You
could hardly make it clearer than that. It restores and defends human vanity
against science. It is the explicit undoing of all the ways in which science has
slighted humans, and the restoring of self-superiority. We imagine ourselves in
place of the creator. As the creator of the universe, we would want to make sure
that we emerge as the pinnacle, wouldn't we? So God as our mirror image is
supposed to have done exactly that!

In fact, as many thinkers have noted, the situation is the other way around: The
Christian God was made in the image of man. Xenophanes of ancient Greece is
famous for saying that if animals had gods, they would look like the animals. In
this manner, the entire religion is an expression of self-importance of
believers, a kind of narcissistic reassurance. What believers see as the truth
of God is only the truth of their psychological needs and preferences. The
Jewish religion, and by extension Christianity, is curdled from common human
narcissism and thus extremely anthropocentrist. Us being like God satisfies our
narcissism, and God being like us vindicates our aspirations.

You may object that it is the opposite, because it is self-degradation before a
higher force. It is an excercise of humbling oneself, a recognition of the
smallness and neediness of man. But this isn't a real contradiction. As long as
you think that your religion is better than another, your God greater than
another, your holy book more truthful than others, you are not humble, you are
boastful. You have no plausible reason to believe this, you are starting from
the same position as all religions, and if yours appears more reasonable and
credible to you, it is because you are used to it more. So I don't buy for a
second that your religion is about being humble. The only thing I do concede is
that your religion provides you with a cheap pretext for presenting yourself as
humble when you really are the opposite.

It suffices to watch evangelical church services to see this boastful non-humble
stance acted out in full. There we have the example of a Christianity so
overconfident in itself that every trace of humbleness is effectively
extirpated.

For me, personally, the obvious anthropocentrism of religious belief is the
strongest argument against religion and for my atheism. More than the theodicy
or God's hiddenness. Anthropocentrism would be compelling enough an argument for
me to become atheist, even if no other compelling argument was available.
