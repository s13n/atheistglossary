# Proving God

Trying to prove or disprove God is silly. I find it amazing that people still
try it. Shouldn't we have learned something from millennia of failed attempts?

The only thing that I consider moderately interesting here is to look at some of
the historic examples of such proofs, and see how they fail. That can be
educational. It can sharpen your mind regarding the myriad ways how reasoning
can go astray. Especially when you are needy.

Why don't we just accept that there's no proof either way, and focus on
plausibility and credibility instead? Because, if you think about it, for most
interesting questions of life, there is no conclusive proof, and you have to
decide according to plausibility. Some argue that you have to bet - see Pascal's
Wager - but I disagree. It is not about betting, it is about forming an opinion
from imperfect information and incomplete evidence.

This sounds a bit too rational, so I'd like to point out that much of this
opinion-forming happens subconsciously in practice. It is your bullshit detector
working behind the scenes. Before something reaches consciousness, it will
already have been filtered by a subconscious process. And chances are that
believers and unbelievers will differ in their subconscious filters, i.e. their
bullshit detector will produce different results.

This affects both sides, of course, so at the end, we all have beliefs and
opinions that aren't supported by hard evidence to a sufficient extent. But what
we can do about this, is to scrutinize our beliefs for inconsistencies, for
signs of wishful thinking, for circular reasoning, for errors of logic, or
conflicts with observable reality. If others point this out to us, we can
honestly consider the critique. Others can see faults in someone's thinking more
easily than oneself, especially when the ego is somehow involved. This is why we
engage in debates and conversations, right? To improve and progress!

I am a convinced atheist. I have had half a century to think about it and check
my reasoning. I was raised in a catholic home, so I sucked religious thinking in
with mother's milk, and had to unlearn some thinking habits in the process of
growing out of religion. I am pretty damn sure I have got it more right than
most theists I have come to know in my life, or have read books from. In fact,
one of the bigger factors for my becoming atheist was the embarrassingly poor
defense of Christian belief by some of the most eminent apologists. To see
myself, a rookie who reads a book by Hans Küng or C.S. Lewis or other eminent
believers, cringing at the obviously fallacious arguments offered by them,
arguments that are clearly motivated by a desire to believe, and not a desire to
find the truth about the matter, meant for me to be skeptical of any
authorities, and to be courageous in following my own reasoning.

And then to read books by eminent atheists, and thinking: Yes! He says it like
it is! Books by John L. Mackie, or Bertrand Russell, or Christopher Hitchens. I
really can't help noticing the difference in clarity of thought and lucidity of
argumentation, especially when they clearly don't follow their own wishful
thinking.

So don't try to prove God to me, you would only sound ignorant. Try to convince
me with plausible arguments. And preferably avoid those that have been debunked
at nauseam already.
