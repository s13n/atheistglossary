# Exodus

While it is clear that the Exodus story is a powerful story that's foundational
for the Jews, and by extension the other religions that derive from the Jewish
religion, it should be equally clear that it is a myth, and not historical
reality. I mean, you have a foreign God strike hard the Egyptians, and,
including their Pharao, they still have their doubts and are unconvinced? It
can't have been that impressive then, can it?

The myth, however, is quite remarkable, whether it is historically true or not.
And, arguably, after such a long time, the myth is more important than the
facts. The myth has created its own reality. Rather than trying to uncover what
really happened, which is very probably impossible after such a long time, it
might be more appropriate to evaluate the myth as such, with no implication
regarding its factual accuracy.

The myth shows a previously enslaved people breaking free under the reign of
somebody (Moses) who goes on to erect a theocracy with a single God at the top,
and himself as God's deputy. It shows a God that is the opposite of universal,
it is a specifically Jewish God that is in competition with the Gods of other
people. The existence of other Gods is accepted as self evident. For the Jews it
is a matter of loyalty and identity to believe in their own God, not a matter of
existence or otherwise of any other God. The whole idea of a covenant only makes
sense when the Jewish God isn't the only one.

Witness how Moses installs himself in between God and his people. At the time of
the older forefathers like Abraham, Genesis had the people interact direcly with
God. Moses won't tolerate that. He scares his people off the idea that they
could even have a direct look at God, let alone a conversation. He commands a
massacre when he finds his people attempting to bypass him and worhip God
directly (which he and the authors of the story depict as a false idol). Moses
is totally committed to making himself the sole arbiter between God and his
people, and thereby install himself als the unassailable leader. Moses is a
tyrant with a tight grip on power politics, who doesn't shy away from brutal
terror and cunning ruses to secure his position.

The position of the sole earthly deputy of a monotheistic God has a lot to offer
for an authoritarian leader. If it wouldn't exist, it would have to be invented.
In fact, it doesn't matter if the God exists, because the people don't have any
direct contact anyway. All they know about God comes from Moses' mouth. Or comes
from a ritual that Moses sets up and controls. All that God does, he does via
Moses. Moses can portray himself as the humble servant of the Lord, who is
merely a conduit for God's will, and at the same time control everything about
it, i.e. the theology, the rules, the rituals, the functions and duties of
everybody. Whether God exists or not makes a difference only for Moses, and for
nobody else.

The way this state of affairs is being presented in Exodus makes clear that the
authors saw this form of theocratic tyranny as the desirable state of affairs
for the Jewish people. Whenever the people pushed back, which I find utterly
understandable and sympathetic, the authors rebuke them. For them, pushing back
against Moses' unlimited authority is pushing back against God himself, and
deserves divine retribution.

That's not only convenient for Moses, it is convenient for the authors of
Exodus. Those authors, whoever it was (not Moses, anyway), were apparently
defending and grounding their own tyrannical aspirations. They skillfully
created a myth that justified their own power grab. If you go on and regard the
text itself as the divinely inspired word of God, you vindicate this ruse and
elevate it to indubitable authority. I'm pretty sure that there was no God
involved in writing Exodus, even though it is evidently the work of geniuses,
but there was a lot of Machiavellism involved (The Moses story could well have
been one of the most important inspirations for Machiavelli).

I'm not of course suggesting that the authors invented the whole story from
scratch. Rather the opposite, when you are a prospective political leader with
power aspirations and no inhibitions, you try to use the existing myths as a
quarry for building your own mythical edifice. You present yourself as the one
who honors and fulfills the traditions best. You "prove" that by carefully
selecting and arranging the preexisting mythological pieces to support your
case. You pretend to make Israel great again in a way it never was, by
presenting mythological history in a way that appears to lead to your desired
outcome. Your message is that God wanted it that way all along, and it is you
who qualifies to lead the way and make it all happen, thereby fulfilling your
people's destiny.

If you're falling for this, you are falling for a time tested strategy of power
usurpers that was used throughout millenia. The people who shaped the Moses
portrait in Exodus were amongst the all time masters of this propagandist trade.
