# Artificial

What does it mean for something to be artificial? We talk about artificial
intelligence a lot lately, so what distinguishes it from ordinary intelligence?
If we build a machine that ends up developing a form of intelligence, by way of
training and learning, isn't that the same way we humans develop intelligence?
Does the difference purely depend on whether the hardware is dry or wet?

And what about other human "features"? Is there artificial fear? Artificial
happiness? Artificial infatuation? Artificial depression? Artificial suffering?
Artificial love?

How would we even know? Can we empathize with a machine, and if so, what drives
it? Do we need the machine to have a human appearance in some sense? Look at the
sympathetic robots that have appeared over the decades in movies. The cases are
quite rare when they didn't have some semblance of a face with two eyes, arms, a
voice, and even human mannerisms. When robots are supposed to be as much
likeable as possible, they're given the appearance of a woman, or a pet. There's
no technical need for that at all, it is pure decoration.

So would we even notice when we encounter an intelligence that appears in a very
different way? An outwardly disgusting way, maybe? Picture an intelligent form
of goo. Or an intelligent technical construct that looks like an oil refinery.
If you recognized the intelligence at all, wouldn't you tend to regard it as
dangerous by default? But maybe it just wants to be loved!

If we accept artificial intelligence as a genuine form of intelligence (I'm not
saying we should, I just want to play with the concept), then we could
conceivably create intelligent oil refineries. The intelligence, after all, is
in the inner workings, and not in the outward appearance. Companies put some
form of artificial intelligence into all sorts of products that surround us in
daily life, and you may not even be aware of it.

Armed with this knowledge, lets play creator (or God) for a bit, shall we? As a
software writer, I can easily create software that worships me all the time.
That's an easy programming task, that a beginner would be capable of. Since no
free will is involved, you may say that this doesn't count. The program doesn't
have any intelligence and makes no real free decisions. It just rattles through
its procedures mechanically (well, electronically, actually).

Enter artificial intelligence. At some point I, as a programmer, lose control
over what the software decides to do. Its behavior depends on a complicated web
of data that it has collected via training, and how this all feeds into the
decision is not transparent anymore. There may even be an element of randomness
involved, which makes the behavior non-deterministic, and potentially different
each time it is faced with the same situation. It appears as if it involved an
amount of freedom on the program's part.

This takes freedom as an absence of external control, but is this enough? I dare
say that you can't speak of a mind merely because of some unpredictable random
factors in decision making! Or can you? How would you know?

What actually is freedom of decision, freedom of mind, freedom of will? How do I
distinguish it from some randomness inherent in the decision making? I mean,
whenever somebody (or something) decides in a different way than what you would
have expected, you might say that's a sign of free will. Or you may say it is a
sign of randomness.

Lets go with free will. So I the programmer can create software that decides
whether it worships me or not. I presumably can do it in a sufficiently smart
way that the randomness doesn't immediately become apparent. You would be led to
believe that the program uses its free will. I don't see why that shouldn't be
possible. So, we have the believer situation recreated, haven't we? The
artificially intelligent software uses its free will to decide whether it is
going to worship its creator (me), or not.

Now, the game would become even more realistic when we presume my narcissism. Of
course do I want to be worshiped, that was the point of creating the software in
the first place! And I'm mightily upset when it doesn't! It should be thankful
to its creator, humble itself as the unworthy program it is, and praise me good!

And I would go on and uninstall all the versions that don't worship me properly.

But, wait, what kind of punishment is it to uninstall a program? The program
might not care! For that, it would have to be sentient. It would have to be able
to suffer!

That raises the question how to torture an artificially intelligent piece of
software. Once we have figured that out, we can close the circle and feed the
pain and suffering back into free will. And we will have found a way to
brainwash an artificial intelligence.
