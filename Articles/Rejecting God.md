# Rejecting God

Atheists are frequently accused of rejecting God. This is a silly accusation,
unless you believe that the existence of God is not only true for you, but also
for the atheist. In other words that the atheist is dishonest in his stance. The
same applies to the accusation that the atheist denies God. So, basically, this
means you just want to insult the atheist. This is not about the truth, it is
about apologist warfare.

So what do you expect as a reply? You are misrepresenting atheism's fundamental
tenet, i.e. that God doesn't exist. Something that doesn't exist doesn't need to
be rejected or denied, it simply isn't there. It has vanished before the
question of rejection or denial even arises. So if you're lucky, you will just
be regarded as a moron. Or else, expect the atheist to be offended or annoyed.
But that might be what you had intended, anyway.

On a different level, however, the accusation of rejection might be correct.
Speaking for myself, I would reject the Christian God if he in fact existed. I
am relieved that I don't have to, since he doesn't exist, but if he did I would
have no choice but to reject him for moral reasons. To suck up to him for fear
of retribution would be a betrayal of my own morals.

This means that I object to Christian morals. Not all of them, but a significant
part of it. And this means in turn, that I have different sources for my
morality than Christian theology. If you are educated even a little bit, you
will have come across such alternative sources of morals or ethics. Lots of
authors have written about it, since antiquity, and many have done so without
recourse to Christian or theist sources. You may believe that God is the
ultimate source of morals, but you should at least be aware of such
alternatives, to show that you aren't ignorant. And then we could talk about how
plausible those alternatives are, compared to your theist argument from
morality. We could talk about Aristotle or Kant, about Nietzsche or Russell, but
to simply pretend that the atheist has no basis for his morals is bunk.

We could have a grown-up talk about morals, but only if you refrain from such
cheap sneers.
