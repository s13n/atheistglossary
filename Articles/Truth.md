# Truth

In John 18:38, Pilate famously asks the rethorical question "What is truth?" - a
very good question, indeed!

Now, I don't think this is a real historical conversation that John recorded.
How would he have known, anyway? He wasn't present to witness it, this much is
quite certain. John has Jesus and Pilate act out a drama he designed according
to his own theological concept. And, so it seems to me, he wanted to picture
Pilate as a kind of cynic or nihilist. He had him question the very concept of
truth.

But in my eyes this puts Pilate in a better light than Jesus, because he's
asking the right question. Jesus poses as the bearer of absolute and
unquestionable truth, and Pilate seems to ask: How do you know?

How do you know, indeed! What is the source of truth, and how can you be sure
you found it? Maybe Jesus can't be expected to ask this question, but everyone
else should.

Many Christians think they found the source of truth in Jesus or God or the
bible. But they're fooling themselves. They effectively made a decision to
*treat* it as the absolute truth, but they actually have as little idea as I
have whether it *is* the truth. The decision is arbitrary, and probably more
informed by ancestry than by deliberation, even when they try to bolster it with
apologetics.

For me, this doesn't hold water. It is a kind of wishful thinking. You wish that
there should be absolute truth (or even ultimate truth), and you project this
wish onto Jesus or God or the bible. And then you wipe your involvement from
your mind, and pretend that this absolute truth is just there. As a dogma, or
even as a definition.

Truth for me is something that can be searched and (if you're lucky) found. But
you will never be sure if it is really the truth. There is no absolute truth,
except in mathematics, where you can prove things to complete certainty. In my
world, you will have to get accustomed to uncertainty.

If that frightens you, ask yourself what you need certainty for. Wouldn't a high
likelihood be sufficient in most cases? If you are crossing a street, do you
need absolute certainty that no car is coming your way? Can you ever be totally
sure about that? I bet that if you think about it honestly, you'll concede that
in most practical situations in life, you don't have absolute certainty, but
it's OK because you get on rather easily, and in the few cases where your
expectations fail, you deal with it in some way without losing it completely.

So in daily life, it is usually about confidence, not about certainty. You don't
need to know, you need to intuit. You usually are prepared for your intuition to
fail every now and then, but you adjust as long as it isn't catastrophic.

If you see it from this angle, you realize that absolute truth is a luxury that
you can do without in practical life. Truth is a concept that needs some
qualification. I'm not trying to push you into relativism or nihilism, I want
you to question the role truth plays in your life.

I much rather like to look the other way: There may not be an absolute truth,
but there is an absolute untruth. The quest for truth really consists of
realizations of untruths. You approach truth by identifying things that are
wrong, in particular your own beliefs and assumptions that are not true. It is a
humbling but liberating exercise. You liberate yourself from your own errors,
and that is how you undo one shackle after the other, and back out of one dead
end after the other.

The ultimate judge on what is true and what isn't is nature itself. It is not
some God or holy scripture. It is the entirety of what we all are part of, the
environment we evolved in. This is why science is a better way to approach the
truth than religion, because it checks itself perpetually and recklessly against
reality, to weed out wrong concepts. Religion, in contrast, puts an unnatural
(and, as it turns out, wrong) concept at its base, and declares it to be
unquestionably true. This is a recipe for failure.

There may well be an ultimate truth, but I doubt we'll ever find it. We can
approach it, though, but only when we are humble and are prepared to change our
notion of truth whenever we encounter a conflict with nature.
