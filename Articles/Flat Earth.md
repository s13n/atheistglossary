# Flat Earth

I know that Christians don't believe in a flat earth, except a few diehards.
Don't misunderstand my intention here, I don't want to associate you with
flat-earthers. But if you are a Christian round-earther, you should pause for a
moment and ask yourself why. After all, the Old Testament makes it quite clear
that it regards the earth as flat!

I am of course aware how apologists try to solve this problem, often by
asserting that the relevant passages of the text are not to be taken literally.
But, of course, when you go down that rabbit hole, where do you stop? How do you
distinguish between the passages that are to be taken literally, and those that
aren't? Biblical literalists will have trouble with this.

The underlying problem is that given the limited knowledge of the bronze age, it
was quite reasonable to assume that the earth is flat. The notion of up and down
is so deeply ingrained in human thinking, you can't overcome this easily. You
naturally assume that the entire world is like this, i.e. that there's a global
up and a global down that's the same for everybody. Given this basic assumption,
everything else in the flat-earth world view falls into place almost
automatically. Hence it is no surprise at all that such a view appears in the
most ancient and most fundamental legends contained in the bible. It would
rather be a surprise if it didn't.

The notion of a round earth could only arise as a consequence of people
traveling farther, so that they come to witness the effects of a round earth.
Amongst those is the fact that when you travel south, the solar path across the
sky appears higher, and when you travel north it appears lower. For intelligent
and alert travellers across the mediterranean sea this effect is quite
noticeable and begs for an explanation. If you stay in whatever town you happen
to live in, you never become aware of it. It took the enquiring, modern mind of
the ancient Greeks to make sense of it, and so they eventually came up with the
idea that the earth is actually round, which simultaneously meant that the sun
must go around the earth.

So if the Greeks had figured it out, why didn't the Bible follow? Two reasons:

1. The Greeks came too late, and you don't change a sacred text because of new
  evidence. It would look as if God had changed his mind, and absolute truths
  aren't absolutely true anymore. Not good for religious authority!
1. What do ordinary people know about the Greeks? Religion is about the ordinary
  people, and they would have their serious problems with grasping the idea of a
  round earth, anyway. So the Bible authors and custodians decided to ignore the
  new insight.

Remember that it took a long time until the insight of the Greeks reached the
masses. Only in modern times, at the end of the middle ages, did this knowledge
leave the small learned circles and start to inform the ordinary people. You
could argue that the Bible custodians had it right: There was no need to change
their texts and teachings, since they controlled the imagination of ordinary
people anyway. This grasp has only slipped gradually since the renaissance and
enlightenment.

But now that it has, apologists have to assert that the Bible taught a round
earth all along. Not very convincing. Especially since many people have carried
on believing in a flat earth well into the last century, when finally technology
allowed space travel, and practically showed to everybody that the earth is
indeed round. Remember the first photograph of the earth taken from space!
Science may convince the intelligent, but the masses are won by images.

Accepting a round earth requires giving up the notion of a clear up and down. As
soon as you leave the surface of the earth, those concepts lose all meaning,
even though they are central to human thinking. Christians basically had to
relocate heaven and hell from a real place to an imagined place, since in the
modern universe, there is no physical location for them anymore. In this sense,
the round earth was, and still is, a threat to Christian religious belief.
