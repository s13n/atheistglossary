# Irreducible Complexity

This whole topic of complexity that is allegedly irreducible is embarrassingly
stupid. I think it illustrates how even simple realizations are blocked by a
dogmatic mindset that just can't see what's right in front of your eyes!

Now, everybody is probably familiar with the thought that something seems so
complex that you can't imagine how it could have come about, let alone by
chance. But that may just be a lack of imagination. A lot of things appear
miraculous as long as you don't know how they are made. But that doesn't mean
that it is impossible! Not knowing how mobile phones work doesn't make them
impossible, obviously!

So why should the complexity of, say, an eye be irreducible? In other, better
words, why should it be impossible for an eye to evolve in a long series of
small steps, with each step offering some survival advantage? It might be hard
to imagine due to the long timespan involved, but to find it difficult is quite
different from finding it impossible.

In fact, a difficulty is just the right stimulus for scientists! As soon as
someone finds it difficult to explain how the eye came about, there's bound to
be someone who devotes himself to figuring it out! That's what science is about,
right? And, unsurprisingly, people have figured it out. They have outlined
possible evolutionary pathways how an eye can evolve in small steps over a long
period of time. In fact, they have found out that eyes have evolved separately
in several instances, thereby making eye evolution somewhat commonplace.

It goes to show that it is nonsense to argue irreducibility by trying to take
away individual features of the eye, such as the lens, and then asserting that
the whole thing wouldn't work without it. That shows nothing at all. The eye
didn't evolve by simply adding features. You actually have to trace the path of
evolution, i.e. you have to try to find intermediate stages on this evolutionary
path. You'll find that things don't necessarily become ever more complex, there
are instances when features disappear that have become superfluous or even
burdensome.

Note that this is mirrored in the way humans "design" or construct artificial
things. It also happens in small steps, over a potentially long time. And it
often involves temporary structures that become superfluous when the artefact is
complete. Making a bridge or a dome requires some scaffolding and tooling, which
gets removed when the building is complete. If you are looking at the completed
structure without knowing how it was made, you may well consider it irreducible.
Once you know about the scaffolding and the tooling, you see that building the
structure in small steps from individual parts is quite possible.

I have expanded on this realization that human design is actually very similar
to evolution in the article on Intelligent Design. The bottomline is that it is
completely misguided to put evolution and design as opposing concepts. They're
not. In actual fact, design is a form of evolution. Whoever believes that design
happens in a big imaginative leap doesn't know what design is, and probably
never designed anything noteworthy. Design, like evolution, is iterative and
proceeds in steps, guided by feedback that helps selecting what works best.

So talking about irreducible complexity just shows your ignorance. It shows that
you hold impossible what really is only your lack of imagination, and it shows
that you don't understand how complexity comes about, regardless of whether it
is natural complexity, or artificial complexity.
