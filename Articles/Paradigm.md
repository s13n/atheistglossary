# Paradigm

Since Kuhn wrote his famous book "The structure of scientific revolutions" in
the 60's, no egghead elitist text can do without mentioning the word "paradigm"
about 20 times. The book itself, of course, uses it 20 times per page, or so.
Kuhn is kind enough to define it in the preface of his book like this:
"universally recognized scientific achievements that, for a time, provide model
problems and solutions to a community of practitioners", which looks strangely
inconspicuous for a term that has since taken the intellectual world by storm.

But what it really is used for, is to talk about ways of thinking about
scientific problems, or problems in general. In science, the biggest revolutions
come about through a change in the way of thinking. Often, the change in
thinking is triggered by growing evidence that the current way of thinking
doesn't quite cut the mustard. That the current way fails to provide an
appropriate explanation for a number of observations that beg for an
explanation.

It is quite normal that there are observations that lack a good explanation.
Usually those don't lead to a paradigm change, because most people trust that
the current paradigm will eventually yield a satisfying explanation, it just
hasn't been found yet. Somebody really talented and diligent will eventually
crack this nut. But every now and then people gradually come to realize that
something is wrong on a more fundamental level. Things just stubbornly refuse to
add up, and something more radical has to happen to find the solution.

One of the most consequential paradigm shifts of the past may well have been
triggered by the realization that "God did it" isn't actually an explanation.
You can say this even though you have no clue what you're talking about, and it
provides zero opportunity for making predictions and checking them against what
actually happens. In short, it does nothing to make you understand. In order to
understand the world, you have to look at the world, and ignore God. When people
did that, and applied their brain power to real world problems instead of
worrying about angels on pinheads, knowledge and understanding suddenly
exploded. We're still riding the pressure wave. I'm simplifying for exposition.

Another such paradigm shift was associated with the speed of light. When people
had mustered the skill to actually measure the speed of light, which was quite a
challenge at the time, they fully expected it to be relative to the matter it
went through. After all, a water wave also has a speed that is relative to the
water. If the water is still, the apparent wave speed is different from when the
water itself moves, for example a flowing river. But with light one problem is
that in a vacuum what is the matter? An elusive material called the
"luminiferous ether" was postulated to fill this function, even though nobody
could find it. But it turned out that it had no effect on the speed, either.
Clever experiments were devised that would have shown a speed difference if the
ether existed. There was none, and the scientists had to make sense of that.

After brewing for a while, the solution was that there was no ether, that light
was a wave that could go through vacuum with no carrier material at all (which
blew a few minds), and that it was actually the same as other types of wave such
as radio or X-rays, except for the wavelength. But the solution led immediately
to the problem how to measure the speed of the wave when you have no material it
goes through. The speed is relative to... what? A vacuum is nothing, and can
nothing have a speed? It brewed for another while, and along came Einstein with
an idea: The speed is always the same, it is a natural constant. Instead, time
and lengths vary. This blew a lot more minds. The relativity principles felt to
many like the rug was pulled from under their feet. Light speed is always the
same, but time is not... what?!?!!!

This is what such a paradigm shift is about: It blows some minds who are unable
to readjust their thinking. It feels completely unintuitive and wrong to them,
even crazy. Some people stick to the old ways until they die. A new generation,
though, grows up with the new way and finds it almost self evident. They see how
much it has shown its usefulness and prediction power, and they work with it.

I say that atheism and theism are two different paradigms, so it is to be
expected that people will find themselves unable to accept, or even properly
understand, the other way. Switching from one to the other doesn't just mean to
accept new evidence, it involves changing the thinking in a quite fundamental
way. It is not just that some aspect of life like the light speed is affected,
its effect is sweeping and changes the way we look at the world. I find it
understandable that people don't follow at the drop of a hat. Even in the best
case it will take years until you build up enough confidence that the new way is
the right way. And this usually only happens when you were aware of the problems
with the old way for a while already. People who are happy with the old way see
no reason to change. And in the case of atheism and theism, this applies in both
directions.

Getting people to switch therefore begins with trying to convince them that
their current viewpoint has a serious problem. That things don't add up. That
really, when looking at it diligently and honestly, it isn't tenable. It needs
to go. Only then you have a chance to create an acceptance for change.
