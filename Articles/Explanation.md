# What is an explanation?

Christians frequently offer God as an explanation. Allegedly, God is an
explanation for the origin of our universe, for the formation of life on earth
and of course our own species, for good and evil, for our moral intuition, and
so on. Not only is God offered as an explanation, he's offered as THE
explanation, the only one that is true and credible.

I disagree. For me, God isn't an explanation at all, not even a wrong one. I
have certain expectations of an explanation, and the God explanation doesn't
deliver. Sometimes I think that Christians secretly know this, because usually
they seem to think their job is done when they have shot down other
explanations, and their God explanation will then win by default.

But for me, it doesn't win by default. Even if every other explanation was
wrong, the God explanation would still be useless. Let me explain.

What distinguishes an explanation from an assertion? Is it the usage of the word
"because"? I don't think so. You haven't explained why we have our moral
intuition when you state: "Because God has written it on our heart." You have
just made an assertion. You haven't bothered to provide any evidence for it, you
haven't shown why I should take your assertion for a fact, in contrast to
numerous different assertions that can be made about the same question. You
haven't provided any way to check the veracity of your assertion. In short, you
haven't provided any insight.

This is what separates explanations from assertions: Insight. Explanations
attempt to make you understand, they don't just try to convey a factual
statement. An explanation doesn't try to indoctrinate, it tries to educate. It
tries to enable you to develop new questions, derive new insights, discover new
trains of thought. Good explanations don't try to plug holes, they open doors.

What door is opened by the so called explanation that God wrote the moral law on
our hearts? I'd say none at all. This statement provides no insight in the why,
when and how, it doesn't lead to new trains of thought that could further
elucidate the problem, it doesn't even detail what exactly it is that was
written on our hearts. It is, in fact, so useless that I suspect its real
purpose is not to answer the question, but to discourage people from asking more
such questions.

It doesn't frustrate me that Christian apologists come up with such useless
explanations. To be honest, I don't expect anything better from them. But it
frustrates me that so many people apparently are satisfied with such bullshit.
That they bring themselves to believe that this is not only an explanation, it
is the best one! It demonstrates to me that their aim isn't understanding, they
don't want to satisfy their curiosity, they are not on the lookout for
interesting new trains of thought, they don't pursue progress. They want to
bolster their belief and harden it against contest and doubt.

But what also frustrates me is atheists and skeptics engaging in discussions
with such apologists, and debating such "explanations" as if they were to be
taken seriously. It is of course useful and interesting to discuss the origin of
our moral instinct, and of our moral laws, to pick this one example. But it is
totally wrong to discuss it under the pretext that there are two opposing views,
both of which have their right of existence. Any discussion with apologists
needs to start with the atheist pointing out how the theist explanation doesn't
qualify as one. Otherwise you just give room to the usual theist tactics, namely
that they concentrate on shooting down the atheist position. This engages the
atheist in a position where, instead of attacking the theist bullshit, he finds
himself defending his own position, being driven further and further into the
details, while more and more of the audience tune out. At the end, the audience
may well remember all sorts of objections against the atheist position, but none
against the theist position, and the theist position wins by default.

No, the one message that is most important to get across, is that the theist
position has nothing useful to offer. That their explanations don't even meet
the most basic expectations one usually has of an explanation. On the skeptic
and atheist side, we are well aware that we don't have a full explanation for
everything. The theist side tries to look better by pretending they have all
those explanations on offer, and they are really simple. You need to pull the
plug on that.
