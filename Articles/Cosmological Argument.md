# The Cosmological Argument

One of the most popular arguments for God's existence in apologist circles seems
to be the cosmological argument, i.e. "God is the only viable explanation for
the Universe." (I'm trying to paraphrase it as briefly as possible)

I note that many treatises have been written over many centuries, without a
universally accepted solution in sight. So the matter doesn't seem as easy as
apologists believe. In fact, I don't even understand why this argument is so
popular. Let me explain.

For me, the origin of the universe is simply one of the many problems we haven't
solved yet. My own life is unaffected by this question, so I am in no hurry to
come up with an answer, not even a provisional one. I can wait. I know
scientists are working on it, and I find mightily interesting what they have
found out so far. But it looks like there's quite a lot missing still, and the
resolution may well not arise during my lifetime. That's disappointing, but no
big deal.

As an argument for God, I find it rather pathetic. Its biggest fault is that it
doesn't actually explain anything when you assert that God created the universe
(see my entry on "Explanation"). Apologists seem to think that the mere fact
that science hasn't yet produced a full answer makes their God explanation win
by default. But they only replace the partial scientific answer with a
non-answer. And provide no conclusive supporting evidence, to boot!

In a way they're lucky that science isn't done yet. If it was, their argument
would be completely superfluous. So they try to exploit the fact that scientists
work to find a *useful* answer that actually helps *understanding* things, and
they don't pretend to know before they have found it. The religious always
pretend to know, and their answer to every question is God, which has the
advantage that you can blurt it out mechanically, even when you have no effing
clue what you are talking about.

So ask yourself: What do you need the answer for? In which way is your life
affected by knowing or not knowing how our universe came about? Does it change
how you feed yourself, how you find partners, how you earn your living, how you
stay healthy? Has it got any bearing on your actual daily life? If not, why
would you not simply stay on the sideline as an interested spectator, and
suspend judgment until the experts have cracked it?

Closing note: Just to be sure, I am not arguing against you taking interest in
the question. If you are interested enough, by all means become an expert and
help the other experts solve the problem! Maybe it will be you who can
contribute an important clue! But please don't try to tell me that this is a
question every layman needs an answer for.
