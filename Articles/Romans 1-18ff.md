# Romans 1:18ff

One of the most popular bible verses Christian apologists like to cite at the
unbeliever is from Paul's letter to the Romans, namely verse 1:18, and up to
1:20 at least. Paul accuses people of wilfully denying God even though they
clearly see his divine nature, and he threatens them with God's wrath.

The irony, of course, is that citing bible verses at someone who doesn't regard
the bible as authoritative in any way, is guaranteed to fail. Quite obviously,
many apologists are unable to put themselves in the position of an unbeliever,
even when it is entirely hypothetical. I suspect that they regard it as
blasphemy to even hypothetically presume a non-authoritative bible. I would
regard this as brainwashing. To forbid oneself a hypothetical thought means
shackling ones mind and abandoning free thinking, and with it freedom
altogether.

If you look at Paul's verses with a free thinking mind that isn't committed to
take it for the indubitable word of God, you see immediately that this is just
Paul embarking on a rant. I find it embarrassing, and it leaves me stone cold.

Paul appears to be frustrated of people who don't see things as he sees them.
What is plain to him isn't plain to everybody, and it is not their fault. His
preachings might just be unconvincing. They're certainly unconvincing to me!
That's no excuse for wishing down God's wrath on everyone.

The background, of course, is that Paul, and with him many believers, see God's
work in everything, and hence believe that everybody else ought to see it in the
same way. So if they don't, they must be lying. Or, as he formulates it, they
are suppressing the truth.

This is actually quite dangerous. In this manner, you can accuse anybody who
happens to disagree with you of lying deliberately. Very quickly this can morph
into an attempt at character assassination, and indeed as Paul is thundering on,
you can see this in action. In a totalitarian system, it could mean the death
sentence.

For me, this bible quote shows that Paul had a grave deficit of character, in
that he was prepared to unfairly and summarily condemn people who merely
disagree with him. How much this is a Christian trait is not for me to judge,
but my view on it is certainly very dim.
