# Modesty

If you are or have been a Christian woman, I won't need to explain to you how
religion demands modesty from women in particular. Of course it isn't exclusive
to women, men are not excluded from modesty demands, but in practice it very
much is directed at women. I think there is a valid and desirable core to
modesty, but it isn't what it is nowadays commonly taken to mean.

As an atheist, don't expect me to subscribe to the notion that modesty somehow
pleases God. I wouldn't understand that argument anyhow. If God made you, with
all of your properties, why should you not strut your God-given stuff? Wouldn't
that be some kind of celebration of God's greatness? Why would you cover that
in sackcloth in order to not elicit any attraction?

Even worse is the notion that you, as a woman, are somehow responsible for the
reaction of men in your surroundings. Everybody, including men, is responsible
for their own behavior, and should be capable of acting responsibly. A
transgression of a man is his fault, even if you as a woman should present
yourself stark naked. It might sound strange if you were raised in the US, but
in Europe it is not uncommon to have mixed nude beaches, mixed sauna or similar
examples of naked people meeting. None are invitations for men to grope, ogle or
otherwise molest women. No need for a woman to accept the blame when it happens.

Honestly, I regard it as offensive when people seem to believe that men are
somehow incapable by nature to behave themselves. I'm a man, and I'didn't find
it particularly difficult to learn how to behave. This seems especially
difficult to grok for men who come from a patriarchal culture that makes women
guilty by default for the behavior of men, while at the same time putting women
under the guardianship and supervision of men.

Which is paradoxical, right? If men have such difficulties holding back and
controlling themselves, why aren't they under the guardianship of women? You
can't simultaneously make women responsible and in need of supervision! All
advantage to men, and all disadvantage to women, is that how modesty works?

So, to reiterate, don't tell me that modesty is there to protect women! It
protects men. From having to behave.

So we've covered the bad interpretation of modesty, but what is the good one?
Imagine a community meeting where people meet from very different walks of life.
Imagine some people coming with expensive garment and jewelry that others simply
can't afford. If you're poor, you may very easily become intimidated or feel
slighted and inferior. When you are meeting people with the intent to have an
eye-level conversation, and engage in an egalitarian way, then modesty can go a
long way towards creating a productive atmosphere.

That's the original motivation behind a school uniform, btw., in that it makes
the pupils outwardly equal and remove some elements of social hierarchy. It
conveys the message to children that their worth isn't measured in how expensive
their clothes are.

In such situations modesty has the function of toning down egoistic attention
seeking and self-parading for the sake of a more harmonic interaction with
people. I imagine this was important in early Christian communities, where
people of very different wealth were participating in the same group. That's how
I interpret verses like 1 Tim 2:9-10, as applying to the situation in their
Christian community.

But there are also other situations. In some situations, I would want and
appreciate people strutting their stuff and being immodest. What would a fashion
show be without exuberance and exhibitionism? Is that harmful? I don't see why!
There needs to be an opportunity to show and revel in what you have got, and it
can feel pretty damn good, too! What's wrong with being adventurous, playful,
bold, or even over the top when the occasion is right? Trying out things and see
how it goes is half the fun!

Modesty, from this angle, isn't just about outward appearance. It isn't about
skirt length and hair cover. It is about toning down your ego to make more room
for others, to make them feel more comfortable, more valued. But not to give
them a license for misbehaving! And it is not a stance for life, but for a
situation, for an environment.

And every now and then, women need to show immodesty to disabuse men of their
patriarchal delusions.
