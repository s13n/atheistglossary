# Job

The book of Job is one of the best-known old testament books, and deservedly so,
because of its special topic, i.e. the tale of woe of Job, whose faith was
cruelly tried by God, and went through almost inhuman hardship, only to be
finally materially rewarded for all this endurance.

This is a remarkable story that has inspired and divided generations of
believers (and unbelievers). On the most superficial level, its message is that
you should endure hardship and apparent unfairness, because God is just,
ultimately, and will eventually reward your steadfast loyalty.

But is that really just? Is this what fulfills your expectation of justice?

It doesn't fulfill mine! I don't subscribe to the notion of justice this
biblical story presents. It also presents a wrong notion of virtue.

Job is described as a righteous and very rich man (compare that to the opinion
Jesus has of rich people). He becomes the object of a kind of bet between God
and Satan about whether Job's righteousness would survive great hardship. I find
quite telling in this description, that a distinction is made between God and
Satan at all. The text is simultaneously careful to attribute to Satan all the
hardship Job has to endure, and makes clear God's superiority over Satan.
Whatever Satan does to Job has God's explicit permission. Satan can't act out
his own free will.

This is a rather lame attempt to remove the responsibility for evil from God and
attribute it to Satan. From an ethical standpoint God is at least as much
responsible for what happens to Job as Satan, and the story would be ethically
equivalent if Satan had been omitted entirely, and God himself would inflict the
evil on Job directly.

But what really rubs me up the wrong way is the morale of the story as it is
presented. The overarching lesson the text tries to convey, is that the crucial
virtue is loyalty to your God, and it is this loyalty that gets rewarded. You
are supposed to be slavishly obedient, and refrain from criticizing or even
questioning your God, whatever happens to you. God can do to you whatever he
likes, and can never be held responsible. God is not your partner you can have a
discussion with. God is not even a judge you can appeal to, or before whom you
can defend yourself. Job would expressly wish to have a court case opened where
he can defend himself, and present his side of the story to the judge. But he
doesn't get that. The story tells us that we can't have that.

This is in some contrast with older biblical stories where a protagonist had a
discussion with God, and in some cases even managed to get concessions from him.
See Abraham as an example. As time progresses, and the stories in the bible
unfold, this is less and less possible. God moves further and further away from
man, and becomes not just progressively inaccessible, but also progressively
more unassailable and beyond any responsibility. People are left merely to obey
and be loyal, whatever happens. This is increasingly tyrannical.

Of course, a just system would not have the judge and the accused be the same
person. It is obvious that this would not produce justice. The same is true when
the judge is also the plaintiff. So in a totalitarian system the top guy is
exempt from all justice, since any judge would necessarily be of a lower rank,
and therefore in no position to even pass, let alone enforce a verdict. The
mafia boss is beyond reproach, whatever he does, because there's no judge to
whom he has to answer, and whose verdict he is forced to respect.

But just like I despise a mafia system, I despise this notion of justice, which
really is a notion of slavish obedience. What the story of Job suggests as a
virtue, is a moral failure to me. The demur that God represents perfect justice,
and therefore doesn't need to be accused, is patently ridiculous when we are
faced with a story that hinges on his being deliberately unjust! I acknowledge
that it is useless to rail against an unavoidable fate, as unjust as it may
seem, but it is appropriate to accuse the responsible, when there is one, no
matter how powerful he happens to be. Justice should not yield to hierarchy, on
the contrary it becomes more important when a hierarchy is involved! Justice is
supposed to compensate for a power difference, instead of reinforcing it,
otherwise it would just be another tool for the benefit of the powerful, and
hence it would serve to increase the injustice.

What the story of Job therefore rather unashamedly shows to me, is that a
pyramid system with a single figure at the top can not be just, even when that
figure is a God. Its morale is that your only option is unconditional loyalty
and obedience, which for me adds insult to injury.
