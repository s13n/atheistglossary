# Theology

The Christian religion is simple enough to teach it to children at a very early
age. But if you want to criticize it, you apparently need at least a PhD, if not
a professorship in theology. Ordinary people supposedly are just too illiterate
and ignorant to engage in a meaningful critique of this religion.

In Wikipedia, I read that theology "is the systematic study of the nature of the
divine and, more broadly, of religious belief". Thankfully, they avoid the word
"science", as it would be inappropriate. And while I fully understand how one
can study the nature of religious belief, I am at a loss how one studies the
nature of the divine. I always thought that the divine is supernatural, so how
can it have a nature? Isn't that an oxymoron?

Anyway, I regularly come across people who defend Christianity by insinuating
that the critic doesn't know enough about the subject in the first place. The
gist is that once you are really knowledgeable about Christianty, you invariably
find it to be true. And they point to eminent theologians to prove their point.

In practice, I find that the more you know about Christianity, the more
convinced you become that it is bullshit. Indeed, many atheists know a lot more
about Christianity than most believers. But that's hard to admit for a believer.
The idea that a non-believer could have consciously discarded their belief not
because they are ignorant, but because of their intimate knowledge of
Christianity, spooks them. See the entry on apostasy.

If you assume that theologians are intelligent, you may wonder why they still
believe in Christianity. That's a good question, which is only partly addressed
by Upton Sinclair's famous remark that "it is difficult to get a man to
understand something, when his salary depends on his not understanding it". It
is true that the salary of many theologians depends on their belief, and indeed
there seem to be a significant number of preachers who don't believe in private.
Daniel Dennett did a study on them that unearthes some of their motives.

But there are also numerous theologians who continue believing despite their
knowledge. To me they form the more interesting case. I find it fascinating how
an outsider like me often can spot fallacies in the position of a theologian
rather easily, when the theologian himself appears to be convinced he has a
sound argument. It frequently is rather obvious how the position of a theologian
is informed by his belief, despite his insistence that he looks at the matter
objectively. That's just plain bias, which immediately pulls the plug on any
argument that claims truth.

This all makes theology a fruitless and pointless endeavor to me. I have a lot
of respect for textual criticism, for religious history, comparative religion,
and similar fields that really seem to be concerned with reality, but the rest
is basically a waste of time in my opinion.

When I read such monumental works as Hans Küng's "Does God exist?", I cringe.
This example of an apologetic book is obviously a labor of love, and the author
wrestles with the topic in a thorough and learned way, but at the end he may
have convinced himself, but not me. It seems to me that he not so much answers
the question convincingly, he buries it underneath a mountain of suggestive
language that betrays his own wishful thinking. Poor guy, such a lot of work,
and all for nothing!

Have a look at this textual miniature mountain that is Küng's description of
God: "the absolute-relative, thisworldly-otherworldly, transcendent-immanent,
all-encompassing-all-permeating, most-real Reality". Are you impressed?
Intimidated? Flabbergasted? Amused? Embarrassed? To me, this is facepalm-grade
bullshit.

Küng has been regarded as a heretic catholic throughout his professional career,
so I would naturally have some sympathy with him, but that a man whose
intelligence quite likely exceeded mine, could have engaged in such obvious
nonsense, and this with as straight a face as imaginable, is the kind of stuff
that shows the danger of religion to me.
