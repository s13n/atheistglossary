# Atheist vs. Agnostic

I'm calling myself an atheist. So some smartass is bound to remind me that as
long as I haven't got conclusive proof, I would have to call myself agnostic, in
recognition that I don't truly know for sure what I claim.

There was a period of time in my early twenties where I found this argument
convincing, and I consequently called myself agnostic. It appeared to me that
this was basically the humble and honest thing to do. But I gradually realized
that this is beside the point. With a stance like this, we should all be
agnostics. So are we all the same then? Of course not!

The result of this objection is only to level every disagreement and obfuscate
the actual topic. Because it is about what we are convinced of, not what we can
prove beyond any doubt. Richard Dawkins said that on a continuum of 100% sure of
atheism and 100% sure of theism, he is somewhere around 98% sure of atheism.
Does this mean he's an agnostic, just because he is conscious that he hasn't got
a full proof?

No, obviously, Dawkins is an atheist like me, full stop. Saying so doesn't imply
having a proof, it expresses a conviction. Christians usually have no
inhibitions calling themselves Christians, even when they know they have no
conclusive proof. So they have no license to demand this from atheists. If they
feel entitled to stand by their conviction, atheists are, too.

So, in a sense, we can be atheists and agnostics at the same time, without any
contradiction. To make this even more complicated, an atheist can, and often
does, differentiate between the general possibility of a deity of whatever
description, and of the existence of a specific God of a specific religion.

I would concur there. I would say that I think the Christian God is such
implausible nonsense that it is inconceivable that he exists, but that the
possibility is much higher that some sort of divine being exists which is quite
different from the Christian God. Still rather negligible in my opinion, but on
a very different level than the Christian God. So I'm an atheist in both senses,
but more so regarding the Christian God.

And the Christian apologist, by this reasoning, is a theist regarding the
Christian God, but an atheist regarding any other. It still means he's a theist
overall, but it is not cheeky or unfair to point out that theists are typically
atheists regarding any other God except their own. It means that they would have
to come up not just with plausible arguments for their own god, but also with
plausible arguments against all the other ones.

So the bottomline really is that this name calling is silly. In the end it is
the argument that counts, and how convincing it is. Either you have good
arguments, or you haven't, regardless of what title you give to a position.
