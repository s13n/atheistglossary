# Parousia

Jesus' failure to reappear as promised is arguably Christianity's greatest
embarrassment. What scholars call parousia is Jesus' unexplained "delay" in
fulfilling his promise that appears in all three synoptic gospels, that he'll
return before the present generation expires. This is generally understood to
mean that he would return within half a century or so, because otherwise all his
contemporaries would be dead. Evidently, that hasn't happened, and Christianity
had to find excuses, explanations and reinterpretations of scripture to account
for that.

Now, to put things into perspective: The duration of the "delay" so far is
longer than most of the time span covered in the old testament. The time from
Abraham - taking the legend as fact - to Jesus was shorter! Even if you take
Jesus' promise seriously - indeed, *especially* when you take it seriously - you
have to deal with the extraordinary length of this delay! And I have to say that
I find most attempts at explaining away the problem downright ridiculous.

Even more ridiculous is the many deluded believers who have been convinced
throughout Christian history that the second coming must be imminent, owing to
the world being so bad at the time. Yet Jesus steadfastly refused to come, the
world refused to end, and things went better instead. There could be a lesson in
that, a lesson that many believers refuse to accept.

If you aren't bound to Christian dogma, the explanation is of course
self-evident. Jesus was an apocalyptic preacher, and just like any other
apocalyptic preacher of any time, he was wrong. The world doesn't do them the
favor of ending prematurely. That's just not how things work. If people want
humanity to end, they have to arrange for that themselves, as they can't rely on
divine support in this respect.

Except, of course, that bringing humanity to an end will not bring back Jesus,
either. People who think they can twist his arm are deluding themselves, again!

The "delay" has further ramifications. Before Jesus, there was a more or less
steady stream of new scriptures appearing on the scene. The notion that the
books of Moses go back to the man himself is childish, in reality the old
testament books all are much younger, but there still is a timeline that spans
at least half a millennium, during which there was active production of
scriptures. The new testament was written in its entirety in little more than
half a century. And then it stops. No new scriptures, for 1900 years now. The
book is closed.

Interestingly, Mohammed opened it again 500 years later, to add another chapter,
but then made the same mistake of declaring it the final word. As a result, his
creed, Islam, is similarly outdated and petrified.

And it shows. All the content of those scriptures are by now so far removed from
our own situation, our thinking, our culture, our values, our knowledge, that it
should be interesting for its historical content and its impact on cultural
development, but not as anything even remotely authoritative for our present
life. It almost universally looks like a big step backwards, from the
perspective of today. If people think Jesus and his teachings are "modern", they
haven't read ancient Greek texts, who have been more modern hundreds of years
before. And they don't seem to have taken notice of actual modern texts, e.g. of
the enlightenment and of humanism.

There presumably would have been a chance of integrating this into the Christian
creed, if scripture would have expanded continually since then. We've had enough
people we could call "prophet". But that hasn't happened, because Jesus was
regarded as the final word, and when his second coming is imminent, why bother
with more scriptures? Hence I find that the notion of the second coming has
severely debilitated the Christian religion, and by now it is so much beyond
repair, that abolishing it only seems logical.
