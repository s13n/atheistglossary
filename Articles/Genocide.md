# Genocide

I am worried. I see people increasingly accept, justify, and even commit
genocide today! Wars have broken out that have a distinct genocidal aspect to
them. People get sent into battle with the express aim of eliminating the other
side completely. And the justification for that sounds eerily familiar. It
certainly should sound familiar to those who have read the Old Testament!

Recently, the eminent biblical scholar and public intellectual (in Christian
circles, anyway) William Lane Craig held a series of debates and lectures on the
topic of the Canaanite conquest as described in the book of Joshua, after having
been commanded by God in the book of Deuteronomy. Craig of course sets out to
defend the God in whom he believes, from the accusation of commanding the
Israelites to commit genocide. How he does it, the defenses he brings to the
table, are amongst the most awful and shocking examples of apologetics that I
have seen in my life. They would have been perfect for the Nazis murdering the
Jews, and the whole thing is a prime example how a very intelligent mind can be
fucked up beyond all repair by religion.

The problem is that he has a large base of followers who regard him as an
authority, so what he says carries a lot of weight for them, and very likely
fucks up their brains, too. And this isn't just talk, this stuff gets enacted,
in our present times, on various battlefields. Of course the problem isn't just
with one man, and I only use him as a prominent example for a wider phenomenon
that already costs, and will continue to cost a lot of lives.

The biblical background is of course the conquest of Canaan by the Israelites
after their exodus from Egypt under Moses. The Israelites need a new place for
them to stay, their "promised land", but it isn't vacant, so they need to
conquer it. So they do what accompanies every genocide, they vilify the people
living there, accuse them of moral depravities of all sorts, so that any human
empathy with them is stunted, and head for their destruction. Their God commands
them to show no mercy and even kill women, children and livestock to make sure
that the Canaanites are destroyed comprehensively. In fact, the commands are so
extreme that they surpass in cruelty a lot of other genocides that have happened
throughout history. Yet Craig rises to the challenge of justifying it.

It is a pretty complete list of moral abdication and total failure of
conscience: The Canaanites deserved it, if the children had been spared they'd
retaliate some day, the land was promised to the Israelites, the Canaanites had
400 years before the Israelites came, it was a precondition for Jesus'
salvation, God commanded it so it must be good, as God's nature is such that he
would never command anything bad, and our God-derived morality basically means
to obey God in whatever he says. The killed children were actually lucky because
they get into paradise earlier that way. The real victims are the Israelites
because they have to fulfill such a ghastly duty which could weigh heavily on
their minds. And it wasn't a real genocide anyway, because there were still some
Canaanites left afterwards!

I kid you not, he defended all that in completely earnest honesty, without
apparently any realization of the atrocity. He believes this is moral, where it
is the destruction of morals. Craig effectively abolishes morals by reducing it
to obedience. Do what you're told, rather than what you think is right! If
you're told to kill, do it diligently and with abandon, and don't let any
scruples get in your way, because God knows better! That is essentially the
message out of all this!

Imagine telling this to Nazis about killing Jews, or the Hutus in Rwanda about
killing Tutsis, or Israelites killing Palestinians, or ISIS killing Yasidis, or
Russians killing Ukrainians, or (insert your own example here). Isn't there
always a depraved, godless group you'd like wiped out? Whose posessions you'd
like to have? Whose influence you fear? Well, have a go, the bible gives you the
green light! I trust you will find an explanation how God commands you to do
what you want done anyway!

If you look at how apologists like Craig, based on the bible, manage to explain
that killing Canaanite children is OK, because the Canaanites killed their
children in rituals, then you should see how low the bar for such explanations
lies. It doesn't need to make sense even superficially, because people will
accept it anyway, when they need the explanation to excuse them for doing what
they want to do. People are capable of moral thinking, but they are also capable
of abolishing it at the first stumbling block, when they need a justification
for something they want. What better excuse could there be than an eminent
scholar whose profession it is to think deeply and learnedly about this stuff?

That's what I'm worried about: People getting mindfucked in droves into
accepting, justifying and participating in the next genocide, believing they're
doing God's will. And feeling justified by the teachings of deranged scholars
who might not even realize what a monstrous moral failure they have become.
Morals are not about obedience, they are about assuming responsibility, and that
includes knowing when to refuse to obey. Past genocides have made this lesson
abundantly clear!
