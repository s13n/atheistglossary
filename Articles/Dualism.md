# Dualism

The classical dualism is the notion that there are two realms of reality: The
material world, and the world of ideas. Also called the mind-body dualism. This
is in opposition to materialism, which states that there's no real difference
between the two, and mind is only an emergent feature of matter. Of course I
simplify greatly here, otherwise I would be writing a book instead of a brief
article.

I am a materialist in a way, in that I think that mind is an emergent feature of
matter. In my view there is no mind-body problem, i.e. the interaction of mind
and matter is no problem, as they occupy the same realm, since it is all based
on physical substance. This is also why our mind is seated in the brain, because
it is an emergent feature of a complex information processing system such as our
brain. This means there is no way of separating the mind from the brain. Without
the brain, there is no mind. And no soul, either.

However, I am also a kind of dualist in that I think that ideas, or more
generally information, is something that needs to be looked at separately.
Information needs matter as a carrier, but it can move from one carrier to
another without changing its meaning, so it does make sense to treat it as
separate from the carrier. The carrier may be an electromagnetic wave, so when I
say that matter acts as a carrier, I want this to be taken in a rather loose
sense. In this sense, pretty much anything physical can serve as a carrier, even
waves, fields and forces.

Even though information can move from carrier to carrier, and in this sense
transcends the carriers, it depends on a carrier for its existence. No carrier
means no information. Therefore there is an interaction and an interdependence.
Information can arise from matter, and matter can be influenced by information.
Thus, there is no matter-information problem. Even though it makes sense to
treat information on its own, it still remains a feature of the physical world.

I think it is fairly easy to follow my explanation when you are a computer
scientist or are otherwise knowledgeable about information science. Treating
software as separate from hardware feels natural to you, but you simultaneously
are conscious that software needs a memory to store it in, and a processor to
execute it. In this sense, software needs physical hardware to exist, but it
still is an entity all of its own, which can be reasoned about and manipulated
without regards to its physical representation. In fact it is a normal feature
of a computer that information flows from one carrier to another, changing its
physical representation from electrical charge to electrical current to magnetic
fields and back again, without users even being aware of it.

I assume that artificial information processing systems will eventually reach
the capability level of a human mind. This means that I believe that the human
brain is an information processing system that is in no way fundamentally
different from other information processing systems. Ideas, decisions, feelings,
opinions etc. are an emergent property of a sufficiently complex information
processing system, and thus are in principle within reach to be implemented in
artificial systems, too. We are not there yet, and it is unclear how long it
will take until we are, but that doesn't change my opinion.

In a way I believe that the classical dualism is outdated. It ought to be
replaced by a dualism that is based on information. But since information
depends on a physical carrier, it is questionable whether the moniker "dualism"
is even appropriate. The central insight is that besides matter as such, there
is also the topic of how matter is structured and organised, and how that
permits information to be stored, moved and processed. I'm confident that this
can ultimately explain life, too.

In a similar vein, there is no distinction between the natural and the
supernatural. There's no supernatural, everything is natural, you just may not
realize it. The supernatural is merely the unknown and poorly understood
natural. Once we understand it, it ceases to appear supernatural. See the entry
on Miracles.
