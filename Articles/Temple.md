# Temple

Does God need a home? If so, what for?

Lots of religions build temples for their Gods, so clearly it is widespread to
answer with a yes, and in a sense this mirrors and confirms the anthropocentric
view of God that I comment on elsewhere. But of course Gods are variously
imagined to be everywhere, or in heaven, or in people, so having a home for them
seems somewhat redundant. And in a way it is, since temples have been destroyed,
but the religion persisted. Witness the temple in Jerusalem, which was built and
destroyed twice, while the Jewish religion persists.

So if the temple is redundant, why bother? Building and maintaining a temple is
expensive, you wouldn't burden yourself with that for no reason!

The fact that temples are so widespread is a dead giveaway that they do have an
important function, and it isn't difficult to figure out what it is. It is not
so much the seat of God, but of his earthly deputies. They need a place from
where they can reign over their flock, exert their authority over the proper
interpretation of the scriptures, demonstrate their power and unassailability,
and collect everybody to effectively communicate with them. It is about
representation and control. A temple is very similar to a palace.

In fact, in a theocracy, where there is no distinction between the government
and the priesthood, the temple IS a palace. They're one and the same.

The beginning of Christianity casts an interesting light on that. As you may
know, the Jewish temple in Jerusalem was destroyed in the year 70 during the war
between the Romans and the Jews. That was less than 40 years after the
crucifixion, and before the gospels were written. The gospel writers, along with
early Christianity, faced the problem that the temple wasn't there anymore. So
they taught that Jesus was the temple, and Jesus was everywhere people gathered
in his name. That is quite convenient for a young sect whose followers were
somewhat scattered and had to operate clandestinely. Furthermore, when your
religion doesn't yet have any worldly power, you can't afford a temple anyway.

Centuries later, when Christianity rose to prominence and could command a
corresponding budget, temples reappeared. It didn't matter that the gospels had
basically declared them superfluous, they reappeared because of the important
function described above. The power hierarchy within the church needed a proper
physical representation.

The Jews themselves also form an interesting example. They are much more focused
on the one single temple that can only be located on the temple mount in
Jerusalem, but when it is destroyed, religion still carries on. God has no home?
Not a real problem, religion can cope! You could say that the temple is somehow
simultaneously very important, and superfluous.

But it does of course make a difference whether there is one temple, many
temples or none at all. It makes a difference to the power structure within the
church leadership. Temples mark the power centers, and it is no accident that
the biggest and most pompous temples are erected where the power is biggest. In
Christianity with its splits and factions, the biggest temples are in Rome,
where the catholic church has its power center, in Constantinople (today's
Istanbul) where the eastern church had its power center after the schism in
1054, in Moscow where the Russian orthodox church still has its power center
after splitting off from Constantinople in 1448, and so on. In general, if the
church is organized in a top-down, pyramidal way, it will build a temple at the
location of the top leadership. This should be enough evidence that it has
nothing to do with God, but with the worldly leadership and its outward
symbolism.

It is this symbolism that will also make the temples so prone to destruction.
Whenever another power, either another religion or a worldly power, tries to
take over a people, they are intent on destroying their temples. The israelites
have done it in various stories of the old testament, and of course they have
suffered it when they were on the losing side. The muslim Al Aksa Mosque was
built on top of the ruins of the second Jewish temple on Jerusalem's temple
mount, so it is a quite typical example of this principle. When a temple is a
symbol of power, and the power changes, the temple must go. And what better
symbol could you have, than erecting a new temple on the ruins of the old?

People normally get this, so it seems a bit odd that they still think that
temples are about God, or Jesus. They are about those who govern the faith, and
use it to wield power over people. They are symbols of a religion having turned
into a system of dominion, a top-down reign. Whenever you visit one and admire
the splendor and pomp, particularly in Catholic or Orthodox churches, spend a
minute to think about what the purpose is of all that, and what it has to do
with God, Jesus, faith, and love.
