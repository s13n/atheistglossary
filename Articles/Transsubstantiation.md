# Transsubstantiation

If you're not a catholic, you may not even know what this is. It is a
specifically catholic and orthodox concept, which stipulates that, as an effect
of a ceremony during mass, the communion bread and wine turn
("transsubstantiate") into the body and blood of Jesus. They don't *symbolize*
the body and the blood, they *become* the body and the blood.

It is a proper act of magic! You need to have the license to perform it, i.e.
you have to be an ordained priest. If you think Hogwarts, you're not that far
off!

Luther of course threw this notion out in the 16th century, and therefore this
topic remains one of the most prominent disagreements between protestants on one
side, and catholics and orthodox on the other. But his main motive probably
wasn't a dislike of magic, but a question of priestly power. The license to
perform magic is a source of worldly power that priests have over their flock.

Luther was only too conscious of that. He wanted to wrestle power from their
hands, from the hands of the catholic church, which is much more about politics
than about theology. The reformation succeeded because German princes had the
same political aim, so that a coalition between protestants and princes formed.
Luther's argument was that the Christian doesn't need such a professional
magician for his salvation. It is a matter for the direct relationship between
the believer and his God. The catholic priest was the middleman that Luther
tried to cut out.

Politics aside, as important this may have been in the protestant revolution,
you may ask why the catholic and orthodox churches stick to this patently
ridiculous concept even in our times. I note that there is a movement in the
catholic church, perhaps somehow associated with the late pope Benedict, to
present the catholic creed as a rational creed, i.e. one that you are justified
to believe on rational grounds. How you can even start to do this over the
background of transsubstantiation is totally beyond me.

But I suspect that there is an insiduous sense in it. People sometimes say that
we live in a postfactual world, where it is a personal matter what is true and
what isn't. Where everybody can, and perhaps even should, have their personal
truth. We also witness how this notion factors into power politics, how it can
be used as an instrument to gain and keep power. If you can convince people that
transsubstantiation is *rational*, there's nothing you can't convince them of.
What defense against any sort of bullshit does a mind have, once it accepts
transsubstantiation as a fact?

The catholic church has understood a very long time ago, that when you control
people's perception of reality, you control their reality. The catholic church
has lived in a postfactual world for many centuries now. It may well be the
oldest postfactual organisation that still persists. If you want to understand
postfactualism, studying the catholic church wouldn't be the worst idea you
could have.

No wonder the catholic church won't let go of such a mind-hacking tool that has
served them for centuries. It rather is noteworthy how slow they were in
adopting the powerful new tools, that we have developed for postfactual
reality-creation. The church has gotten too old and inert to grok it. But
politicians on the authoritarian end of the spectrum certainly noticed, as did
modern megachurches. A mighty power struggle is on across the world, and it
partly builds on the tools the catholic church had developed a very long time
ago.

Things like transsubstantiation, which you may look upon with a mild smile,
dismissing it as a bit silly, but perfectly legitimate for an old and cranky but
dignified church, may serve as the thin end of a wedge they drive into your
mind. A wedge that opens a crack wide enough to fill in any amount of other
bullshit. See it as a tool to mess with your mind. By the time you have accepted
it, it can be used to set up a cognitive dissonance that makes you vulnerable to
irrational trains of thought. To make you malleable enough to help them to
domination. It is not your truth you'll end up with, it is theirs.

If you are like most catholics I grew up with, you don't take the
transsubstantiation seriously. You don't even try to integrate it into your
concept of truth. That's the healthy way to deal with it. The attempt to
integrate it into your concept of reason will derail your mind. I find people
scary who take this seriously. Keep your idea of truth untainted by such
bullshit! There's no truth to it, not even a hidden one.
