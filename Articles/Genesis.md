# Genesis

The story at the very beginning of the bible, in the book of Genesis, where God
creates everything, has been ridiculed a lot in atheist circles for being so
naive and inaccurate. He created day and night before creating the sun and the
moon. How stupid! Didn't he realize that the light comes from the sun? And when it speaks of the firmament and the waters, it means the authors had a flat earth in mind, with a dome above it that had the starts attached to it!

I can understand that criticism, because of those fundamentalist Christians that
take the biblical creation story literally. They even take literally that it
took six days to create everything! I totally agree that this literal reading is
nonsense. But if it isn't to be taken literally, you need to think what the
various metaphors stand for. Assume that everything is a metaphor. What does it
mean then?

Taking a step or two back, and instead of being riveted to the details, trying
to absorb the gist, I notice that God not so much creates the cosmos, he orders
it. He separates light from dark, and water from earth, and the water underneath
from the water above. I am aware that he creates the heavens and the earth in
the first sentence, but I'm not convinced that this means creation from nothing.
It could also be a separation of heaven from earth, hence another act of
ordering.

The image I have in mind from this, is that at the beginning, there wasn't
nothing, there was chaos. All matter was already there, but it was unstructured.
And the thing that God created was order and structure. God didn't create plants
either, he caused or made or allowed the earth to bring them forth.

From this perspective, the creation story isn't so much a story of making
things, but of enabling and ordering things. It reflects a developmental
viewpoint, where God represents a force that drives differentiation and
evolution, where God stands for the development of an increasingly complex and
differentiated world.

Also, think about what creating things would have meant for stone-age people.
They made simple things, which didn't consist of a myriad of parts that combined
to implement a complicated function. Their daily experience was with simple
tools, bows, stone walls, sticks etc. that didn't represent an intricate
mechanism. So if you are thinking about creating a watch, or even a spacecraft,
you are on the wrong track. This sort of complexity was way beyond the mental
capacity of the people back then.

The biblical creation story therefore symbolizes the development of order in the
universe, where the universe was perceived as very small compared to what we
know today. Most of the universe was filled with the earth, specifically the
part of the earth known to the stone age people, what we now call the middle
east. And the rest of the universe was arranged around it, in rather close
proximity.

The development of order, of course, was the development of order in the minds
of humans. It represents the increasing awareness and comprehension of the world
in the minds of humanity. God, in this viewpoint, stands for the collective mind
of humanity, where with "collective mind" I mean culture. Where the bible has
God create order in the universe, I translate that human culture was making
progressively more sense of the world. People collectively increased their
understanding and their control over the world.

The authors of the oldest books in the bible didn't have a notion of culture.
The word doesn't appear in the bible. Whenever they were talking about what we
would call a culture, they impersonated it as a God. A culture wasn't thinkable
without an associated God. There couldn't be a moral and intellectual framework
that stood on its own. There had to be a God at its foundation. So God is a
metaphor for a shared lookout on reality, a symbol for the order in reality.

The fact that we can nowadays contemplate a culture separately from a God is a
later innovation. Hence it is misguided to think of God as a being. I think it
is better seen as a principle, which has been impersonated as a being, because
people had no other mental concept to deal with it. The creator God of Genesis
is thus an expression of an ordering principle, and not of a creative being.

I know how speculative this is, and I'm fully prepared to see it challenged or
opposed. But give it a moment of consideration, whether there could be some
grain of truth in it.
