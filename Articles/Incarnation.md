# Incarnation

Have you ever wondered what the point is in a God incarnating himself as a human
being? Isn't that a strange concept? Why would an omniscient and almighty God
even want to, or need to, do that? Is it to make a certain experience that, as
a God, he couldn't make? Or is is a piece of theater to show off something?

In the old testament, a God that has too many human traits of the worst kind --
jealousy, pride, cruelty, capriciousness, narcissism, vengefulness -- tries to
distance himself from humans, and forbids them to look at him, and even to make
images of him. The old testament makes the point that he is **not** human, that
he occupies a wholly different level, and that humans have to be loyal and
obedient. When he is called "father", it has very little to do with our idea of
a father and the relationship with him. He is the father of a nation, a rather
abstract and distant concept.

In the new testament, he is depicted as much closer to humans, as a father you
can have a personal relationship with. A buddy in heaven, especially since
identifying him with Jesus. And, indeed, as if to underscore it even further, he
incarnates himself in human form as Jesus. But at the same time God is depicted
as much more perfect, with much less of a personality problem as the God of the
old testament, which sets him apart from humans.

In both cases it appears to be a play with closeness and distance. They are
projections of our own human problems with closeness and distance. God is an
empty canvas onto which people project their battle with their own humanity. By
looking at God, you look at your own aspirations.

People who are angry are fascinated with God's wrath. People who are vengeful
emphasize God's "justice". They look to the old testament God. They see their own
feelings represented and vindicated by God. God is imagined to act out what they
would have liked to do, but are powerless to act out. Angry people worship
an angry God. A God who is distant and scarily powerful.

People who are craving for love and compassion imagine a kind, approachable,
loving God. A buddy they can share their cravings and their tribulations with,
who is at hand as a listener and emotional support. Someone you can pray to and
hope for the best. Someone who can look into your heart and see the good in you.
A father of the caring kind, not the punishing kind.

Look at how people see God, what they see in God, and you see what occupies
their own mind. God's perceived character is a reflection of their own
character, of their wishes and needs.

Therefore, God has all traits. A blank canvas onto which everybody can paint
with his own colors will be a rainbow, a patchwork of the entire spectrum.

Seen from this angle, it appears evident that after a long old testament period
of a distant, emotionally deficient God, who serves best as the projection of
angry people, the pendulum would eventually swing the other way, and produce an
image of a sympathetic and kind father God. The Jews had reason to be angry,
having been conquered and subdued several times in succession. So they imagined
a vengeful and powerful God who would turn the table eventually, if only they
pleased him enough. When their temple was destroyed the second time, the time
was right to try something different.

What could be a more striking demonstration of such a kind and approachable God
than having him incarnate as a human, and have the human suffer the same as
everyone suffers? This is distance reduction in its most extreme form, isn't it?
Except, of course, that it was promptly retracted. A fully human God, that is
too much for many. So God became fully human, but only a bit. He died, but
resurrected. He suffered, kind of, but not really. His blood was spilled, but he
didn't really need it anyway. This is the typical Christian equivocation that
wants to have it all ways at once.

Since then we can imagine the meek and mild Jesus as well as the vengeful and
mighty God, and they're supposed to be the same. All our inner cravings and
needs are somehow represented. Previously that needed several Gods with their
distinct traits, but the incarnation of Jesus, and the trinity, bring it
together in one God.
