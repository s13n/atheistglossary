# The Passion

For Christians, the cross symbolizes the defeat of death, but I think this must
go down in history as one of the most hilarious and mind-bending inventions in
the hotly contested field of religious nuttery! The crazier it is, the more
credible it seems to become in the realm of religion. Indeed, some Christians
seem to take pride in the fact that their belief is nonsensical.

Of course, what really happened in the first century is to a large extent lost
in the mist of time. The gospels are not very credible and partially contradict
each other, and this opens up room for speculation. I suspect many people have
their own favorite speculation about what really happened at the time of Jesus'
crucifixion. We're unlikely to ever be able to confirm or contradict them, so
all of them need to be taken with a shovel of salt, but that doesn't reduce the
fun of speculation. Let me share my favorite speculation with you:

For me, Jesus was a hillbilly from rural Galilee, whose intelligence and
charisma set him apart from his peers, so that he slipped into the role of a
preacher and leader of a group of rebels. This, by itself, wouldn't have made
him that special, since there were several of this type at the time, and the
unpopular Roman occupation gave rise to a widespread desire to throw off the
shackles, throw out the Romans, and erect a Jewish kingdom. This blended with
religious and apocalyptic conceptions to a degree that makes it impossible to
separate. This hillbilly enjoyed increasing success in his rural environment,
which at some point made him feel ready to go to Jerusalem, the big city.

Now, it is a bit unclear what he intended to do there. I believe he somehow
thought that things generally would come to a climax at the passover festival,
where there would be a very large crowd in Jerusalem. He had his followers armed
at the eve of passover, while they were camping outside Jerusalem in
anticipation of the showdown. But it turns out he underestimated the alertness
of the authorities in Jerusalem, who weren't willing to risk an uprising, and
viewed any armed group with utmost suspicion. They monitored the situation
closely, and decided to take out the leader of the group with a well exercised
surgical operation that would cause minimal upset. Whatever Jesus had in mind
was preempted, he was killed on the cross, and for the authorities in Jerusalem,
the matter was settled.

In short, Jesus' abilities were overtaxed at the point where he left his rural
environment and decided to conquer the city. He wasn't the prototypical street
warrier and guerilla, he was more concerned with the realm of religious and
social ideas, with a strong bent towards apocalyptic imagery, and not with the
dirty business of fighting. You may call it naive, but he may have thought that
God would somehow arrange a victory in the context of the passover festival,
perhaps a spontaneous uprising of the large crowd there, which would not require
much warfare, yet make the Romans flee from the glory of God. But he had no clue
how organized, effective and unemotional the Romans and the Jewish authorities
would be. If he hadn't been killed, and an uprising really had taken place, it
would have meant major bloodshed. The Romans were prepared for such an event and
wouldn't have given in easily.

This would have abruptly ended the matter, and we probably wouldn't have heard
about Jesus and the entire episode today, if it hadn't been for some people who
picked up on Jesus to form their own theology out of the fragments of Jesus'
teachings. They probably thought that Jesus was too smart to fail so miserably,
hence he must have intended this outcome. They reinterpreted Jesus accordingly,
and came up with what we know today as the Christian doctrine. Paul stands out,
of course, but a group of Jesus' family and followers seem to have remained in
Jerusalem to carry on his legacy. They disagreed with each other, but the Jewish
war tilted the balance decisively in favor of Paul's party. That's another
story, however.

It follows that I don't believe Jesus was buried in a tomb, let alone
resurrected from there. I think those are later legends.

Again, this is nothing more than a speculation that I happen to find plausible.
Many of you probably won't. That's OK with me.
