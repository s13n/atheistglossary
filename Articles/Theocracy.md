# Theocracy

Have you ever wondered why countries, where allegedly God himself rules,
invariably turn out so awful? Indeed, I am bewildered how people can still crave
for theocracies, with all the bad experiences we have made with it, throughout
history up to the present day!

Now, you may interject that in a theocracy, it isn't actually God who rules, it
is people. The point is that those people are appointed, or legitimized, or
inspired by God. They are intermediaries supposedly acting on God's behalf. But
that is precisely the problem with it! How do you know they are appointed by
God? How do you know they actually act on his behalf, and not their own? How do
you know they actually receive any communication from God, let alone
inspiration? How do you know they're not just pretending? That they don't play
you for a sucker?

There are several theocracies currently on earth. Most people would agree that
Iran, Afghanistan or Saudi Arabia are theocracies. Most people I know of would
not want to live there. In fact people often want to leave those countries if
given a chance! Why? Because they are tyrannies! Because corruption is rife
there, and people have no say there. A ruling elite pretends to represent God's
will, but in reality they pursue their own interest in terms of wealth and
power.

You can hardly escape the realization that theocracies are popular with
autocrats and tyrants, and unpopular with ordinary citizens. This calls for an
explanation by theists.

Some theists will not resist the temptation to assert that the fallen nature of
humans makes the unpopularity of the theocracy their own fault, and hence the
theocracy itself their well deserved earthly punishment, foreshadowing the
eternal damnation that awaits them. I would counter that it shows the theocracy
to be a complete train wreck for theists. The very godly rule they aspire to
turns out to be among the worst tyrannies in practice. There is something very
fundamentally wrong with the concept.

Of course, this is because God doesn't exist. Many of the rulers who act as his
earthly deputies, I am quite sure, know that. They are intelligent enough to
realize how they can harness the belief of the people, and their cravings and
fears, and they are ruthless enough to pretend and play the part to get the
people's support. They likely are narcissists, whose talents include knowing
exactly how to manipulate people, what to say and do to make them agree and do
what the leaders wants. In a way they aren't God's deputies, in their minds they
are the Gods, who have learned that pretending to be the humble servant of God
gives them the godly powers they like to have for themselves.

And, of course, no "real" God ever intervenes. Sometimes such tyrants fall from
the sky in the plane or helicopter they sit in. Sometimes they get deposed by an
angry mob. If nothing else, they eventually die from a natural cause.
Regardless, this often triggers a somewhat messy and dangerous process of
succession, until another tyrant has installed himself successfully. You may of
course want to see the hand of God in this, but in actuality there's no sign of
that. It is your projection, you prefer to see it that way.

It makes much more sense to see it as a power play between human rivals, using
believers as useful idiots who can be manipulated to support what in reality
serves to oppress them. But it rarely suffices to manipulate them, as there will
always be many who see through the fraud, recognize the corruption, and strive
to rid themselves of the oppressor. Hence a theocracy will always need violence
and fear to maintain the rule. It will always include spying on people, and keep
them under "spiritual" supervision, to ensure that they can be forced back into
line early enough before trouble emerges.

Theocracies therefore include a thought police, an inquisition, a system of
imposing ideological purity, threatening serious consequences for leaving the
prescribed rails laid by the prevailing doctrine. And often this doctrine is
openly nonsensical, contradictory, unbelievable and unjust, because it isn't
about truth, but because it serves as a litmus test for loyalty. If you show
yourself to be ready and willing to twist your mind into a pretzel to
accommodate the clearly wrong doctrine, you can be assumed to be loyal.

If you should maintain logic and reason instead, you show that there is
something you value more than loyalty, and that is dangerous for a tyrant. Those
people need to be disposed of early enough. If you want to survive in such a
system, you need to be prepared to swear your oath to nonsense, to show that you
are a mindless and thoughtless idiot who's prepared to do anything and
everything without a conscience getting in the way. That you are happy to follow
God's command whatever it is, which is really the tyrant's command. You may even
know that it is the tyrant's command, and not God's, but you will be in no
position to openly make that distinction, since nobody can be tolerated to drive
such a wedge between God and his self declared humble servant, the tyrant.

That's theocracy, and has always been. A fraud, a tyranny, dependent on useful
idiots for its establishment, but capable of maintaining its rule by fear and
deception alone.
