# Hitler

I'm a German, so I've got to have an opinion on Hitler, right? Well, duh,
everyone has got an opinion on Hitler! The strongest opinions are held by those
who know least of him, it seems. That's not meant to be a defense, neither for
Hitler himself, nor for those who know so little.

I'm too young to have a personal involvement here, and even my deceased father
was only 10 years old when Hitler killed himself before he could be captured by
the Soviet troops that moved into Berlin in the last weeks of the war. But I
have learned something about it in school, and I have read quite a bit about it
on my own. I have had conversations with grandparents and other witnesses of the
time. I have an opinion, and I believe it is a somewhat "informed" one.

People love vilifying Hitler, because it gives them the high ground from where
they can pretend that all evil was concentrated in this single person, and by
exorcising him, they can wipe evil from the face of the earth. If other people
were guilty, they were corrupted by him. It was him who killed the Jews and
waged war against the world. No wonder he did all that, since he was an atheist!
How would he have known any better?

If you recline a bit in your armchair, and think about it for a minute, you
realize it isn't very likely that this is the whole story, is it? It is just too
damn convenient to absolve oneself of any fault by thrusting all on one man! And
pardon me for adding this: There's a bit too much Jesus in this image, too!
Casting all the sins of the world onto one man, where have I seen this before?

You don't singlehandedly kill millions of people and wage war against half the
world, surrounded by people who all want to stop you from doing it! You do it
because they all help you doing it! Hitler didn't actually kill anyone
personally, except himself. He led a system in which the actual killing was done
by a great many supporters, who acted out of a conviction that they were doing
the right thing, or at least the inevitable thing.

And the great majority of those supporters were Christians. At the time, the
German population was over 95% Christian, about half of them Catholic and the
other half Protestant. Christians were Nazis, operated concentration camps,
served in the army and in the SA and SS. So putting everything down to atheism
would be a grave misapprehension of the facts.

The question whether Hitler was an atheist is a moot point. It doesn't seem he
was, since he used God in his argumentation frequently, but how genuine that was
is a matter of debate. At any rate he was filled with a conviction that he, and
Germany as a whole, was on a divine mission, and that doesn't sit well with
atheism. But the much more important question is why he had such a lot of
support amongst a predominantly religious population! If religion really makes
you a better person, then Third Reich Germany gives you some explaining to do.

So the very first and important thing to do is to jettison this personified
notion of evil, this urge to rid oneself of it by casting one's own demons onto
Hitler and chase him over the cliff, to use a biblical metaphor. The question is
how much Hitler do you have in you, do we all have in us? Hitler is dead, but we
are very much alive! Don't think about how abnormal Hitler was, but how normal
he was!

If you have a hard look at our contemporary times, some 80 years after Hitler,
you realize that nothing has changed. Charismatic figures have no problem
capturing people's preconceptions and fears, turning them into a sense of duty
and urgency, and to find some minority they can use as scapegoats. And religious
people have no problems finding the justification in their holy scriptures.

In Germany of the 1930s, Christians had no problem finding the Jews suspicious
and dangerous. They had killed Jesus, right? Open antisemitism traced back to at
least Luther, and thus was a common stance particularly in Protestantism, and
many believers were filled with a sense of duty to finally solve the problem
once and for all, even if it was a ghastly and dirty duty.

The sense that there is a nefarious group of people that is responsible for much
evil in the world, that they want to corrupt the system, hollow it out from the
inside, work in the dark, steal your rights and your property, enslave you
within your own country, lead your children astray -- does that sound familiar?
The idea that drastic measures need to be taken, that you can't afford to play
by the rules anymore, that it needs to get ugly before it can get better, that
the righteous must take matters in their own hands, and use force to "cleanse"
the world from those scoundrels, where have you heard that lately?

Those are recipes for mass murder. Once you think that due process can't be
afforded anymore, and you can assign guilt without needing to prove it in each
and every case, you are ready for committing atrocities. You have jettisoned the
one thing that could stand between you and the abyss: A sense of fairness, and a
realization that your opponent is just as human as you.
