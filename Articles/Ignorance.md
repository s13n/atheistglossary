# Ignorance

If you have been embroiled in debates with "true believers" as an atheist or
skeptic, you will have encountered this. Your interlocutor presents you with the
most trivial and stupid arguments imaginable, for example that "evolutionists"
believe that people come about randomly, or the universe pops up from nothing.
As if they want to say: "See how ridiculously stupid your position really is!"

They are parading their ignorance as if they thought they had it all figured
out, and a more detailed and learned discussion wasn't necessary, owing to the
obvious idiocy of the argument you are advocating. It seems the less they know
about something, the more boastful they are in summarily dismissing your point.

That's the inherent arrogance of the Dunning-Kruger effect, it seems. They are
stupid enough to not realize just how stupid they are. And, indeed it is quite
stupid and arrogant to dismiss evolution in such a simple way. It effectively
calls millions of scientists and other educated people utterly silly and
clueless. You would think that if they had a few working brain cells, it would
occur to them how implausible it is, that for the last 150 years the vast
majority of specialists subscribe to a notion, that can be dismissed as absurd
so simply with the most superficial knowledge. They ought to pause for a moment
to realize that it can't possibly be that simple!

But wait....!

What does it look like from the other side? The true believer debating an
"atheist", being confronted with the most trivial and stupid atheist arguments
imaginable, for example that universe doesn't need a creator.

You can see where I'm heading, right? When is someone ignorant, i.e. how do you
diagnose ignorance? Is it just as ignorant for the atheist to say that the
universe doesn't need a creator, as it is ignorant of the believer to say that
it needs one? Aren't both tacitly appealing to basic ideas they take as
self-evidently true? At the end, the question is: What is a good argument, and
what isn't?

Taking ignorance to mean that someone doesn't know something, it is trivially
clear that both believer and atheist will be ignorant in some respect. You can't
know everything, right? While the atheist may know quite a bit about science,
and the believer may know a lot about theology, it is not clear to what extent
this is relevant to the debate they're having. What is the necessary knowledge
background for a non-ignorant debate of some topic? When debating the origin of
the universe, for example, is it self-evident that theology is irrelevant, and
physics is essential, as the atheist would probably assert? Isn't this already
predetermining the answer? What if theology *does* have an important
contribution to make here?

When debating such a topic, both sides will naturally want to pull the argument
towards their respective field of expertise, where they are comfortable, and the
opponent is not, hoping that they can demonstrate the ignorance of the opponent.

If you want to be fair, you have to be aware of your own preconceptions, or the
tacit foundation on which your conviction rests. The stuff that you take for
granted, and not worthy of being mentioned explicitly. For example that the
origin of the universe is a question for physics to answer. I include myself in
this, i.e. this is what I assume. I don't expect disciplines like theology to
contribute anything noteworthy to the question of the origin of the universe.
But I am aware of this. I can accept when it is being questioned, and when that
happens, I think I can offer some justification for why I assume this.

So how do we detect ignorance? When someone is unaware of his preconceptions.
Make sure you know yours. For example, when discussing the origin of the
universe, can you say why you think this is a question for physics (or for
theology, if you're on the other size)? Why do you believe that your side can
make substantial contributions to the topic, and the opponent's side can't?

You might realize that you have to go back to the rather fundamental question of
how we can know anything, how knowledge is attained, how truth is distinguished
from falsehood, and what can count as evidence.
