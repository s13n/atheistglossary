# Morals

Believers like to assert that there are absolute, objective morals, and they use
this as evidence for God. But an assertion isn't evidence, and they can't come
up with a compelling argument for the existence of objective morals, either.

On one hand, they tend to use the example that humans universally believe that
killing children for fun is bad. I'm not entirely convinced that this is
universally true. You'd wish so, but how do we know? Plus, there's this
suspicious mentioning of fun. As if killing children for other reasons wouldn't
be that bad.

Now, we know that much of Christianity has a problem with fun in general. Sex
for example isn't for fun, either, it is for procreation. The saying goes: "If
you're having fun, you're doing it wrong". But the reason for including the fun
aspect in the moral argument against killing children is a different one: In the
bible, there are multiple stories where children are killed with God's obvious
approval. So, one is led to believe, it is the fun that's immoral, not the
killing.

Whichever atrocities you can think of, you will find upon closer inspection of
the bible, that under some circumstances they seem allowed, and justified.
Murder, rape, enslavement, betrayal, theft, you name it, are all OK when the
right perpetrator is doing it to the right victim. Biblical morals are quite
obviously not absolute and objective, they are situational.

On the other hand, believers argue that without objective morals, there would be
no morals at all, because it would all be subjective, and everybody could pick
and choose the morals that suit his own fancy. This argument strikes me as
utterly disingenuous. It manages to miss the blatantly obvious fact that there
is something called "society", which we live in, and which imposes its rules on
us. Conversely, why should morals be God's business at all? Morals are about how
we treat each other in society, right? God is neither affected nor responsible.
It is *our* job to take care of *our* affairs, and that includes laying down the
rules and guidelines for everybody to follow.

Moral questions are in constant and vocal debate in society. In democratic
societies, where elected representatives of the people debate and eventually
pass laws that bind all citizens, where there is public discourse over those
prospective laws, it must count as total loss of reality to miss this in
discussions about morals. Our sense of morals is influenced by such discussions,
and the laws that emerge are to a certain extent informed by whatever consensus
or compromise results from the discourse.

This is also why different societies come up with different laws, and different
morals. And they change over time, too. They even change within religion.
Absolute morals are wishful thinking, at best!

This is very different from subjective morals. You don't pick and choose morals
as an individual. You could only do this as a hermit, and then you don't need
morals at all, because you have no social interaction. Morals evolved because
they are needed when people live together. The bigger a society gets, the more
elaborate morals become. Societies are more successful with good morals than
with bad morals. Even though it isn't genetic, it still is subject to evolution.

There is one way how you can pick and choose your morals: You can move and join
a society that has your favorite morals. Or at least morals that are closer to
yours. If they accept you there. Many people try that. If you don't do that, you
will have to adhere to the rules of your society, at least to the extent that
makes your life practically liveable. You can break a rule, if you accept the
backlash. That can sometimes be the right thing to do, btw.

And, of course, in a democratic society, you can participate in the public
discourse on morals and values, and thereby perhaps convince enough people to
make a difference in the lawmaking of your country. That's how morals change,
admittedly at a rather glacial speed. But even glaciers melt, as we are only too
aware. In a dictatorship or kingdom or theocracy, you would have no say at all.

Evangelicals in the USA engage in this with a strategic plan, yet they
completely deny the fact that they're doing it. They try to win over the public
to their own subjective moral standard, using all fair and unfair means to
influence the debate, and at the same time pretend that their morals are
objective. In my mind, that's immoral.
