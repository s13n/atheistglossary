# Intelligent Design

Many people believe that the universe must have been designed by an intelligent
being, because it appears to be intelligently designed. They have a god in mind,
of course, but for reasons rooted in political tactics they often avoid to admit
it. The argument is little more than an appeal to intuition. This will become
apparent when we look at what the words actually mean that are used here.

How would you recognize intelligent design?

More than 200 years ago, William Paley used the watchmaker analogy to explain
this. This is unsurprising, as in his time the English had the lead in watch
making, and watch making symbolized technological advance. Today, we probably
would use a smartphone as the example, which, incidentally, can also act as a
watch.

Paley's argument hinges upon the recognition that a watch he finds on the ground
is an artificial object that was consciously made for a purpose, in contrast to
a stone that could very well have been put there by some unconscious forces of
nature.

But what makes him able to distinguish this? I dare postulate that, had Paley
been raised in an isolated amazonian tribe without any contact to civilization
as we know it, he would have been unable to make this distinction. He would have
recognized that the watch wasn't anything he had seen before, in contrast to the
stone, which would have been familiar to him. But since he would have had no
idea of metallurgy, of mechanics, of mechanical timekeeping in particular, he
would have no idea what to make of the watch.

To recognize intelligent design, you need to know what intelligent design is,
and this means you need to have examples you can compare with. If the appearance
in question is too far from the examples you know, you can't tell anymore.

If you look at a watch as an example of something that was intelligently
designed, you need to say what you regard as the intelligence needed to make it,
and what you regard as the design process. So if you think that the universe was
designed in a similar way as a watch, you should be able to point out the
similarities.

As far as I'm concerned, I hardly see any. On closer look it is the differences
rather than the similarities that get emphasized.

For example, what is intelligent in the way the universe is put together? I find
the universe rather chaotic and utterly puzzling. If this is the result of
intelligent action, I fail to understand the intelligence in it, and even less
do I understand the purpose, should there be any. As far as I am concerned, it
could be the result of unconscious, automatic and somewhat random processes,
because I am unable to find a compelling reason why it shouldn't. People arguing
from the fine tuning of the cosmological constants, or from the emergence of
intelligent life on earth, appear to fool themselves.

Neither do I recognize design. Intelligent design advocates seem to have a poor
understanding of how design works. I am doing design as part of my own work, and
I can attest that it is a highly iterative process that lives on trying things
and produce failures and revising plans and try again with a refined exemplar,
until the result finds approval and satisfaction. I have never ever seen a
designer speak something into existence just like that. I have seen people have
ideas, but that isn't design. The creator-god that Christians have in mind is
most definitively not a designer. None of the descriptions I came across mention
anything resembling iterative design.

If you posit intelligent design for the universe, you should be able to describe
the iterative process, along with some reasoning on the decisions taken, the
alternatives tried, the mistakes made, the lessons learned and the compromises
made. If you can't do that, you should refrain from calling it design, because
you have no idea what the process was like. In fact, you know nothing, you just
believe it must have been supernatural, because anything else is beyond your
imagination.
