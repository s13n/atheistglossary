# Trinity

The Christian idea of the trinity certainly stands out amongst religions as a
hilarious invention, and it is not surprising that it baffles, or amuses, or
even offends believers of other religions.

How, one is compelled to wonder, can such an implausible concept come about in
the first place? Is it a result of drug abuse?

I think the answer is fairly benign. It is a result of the fact that we ended up
with several different gospels that each purport to tell the same story, but
differ fundamentally in their portrayal of Jesus.

The question is, of course, "who is Jesus?" Is he a prophet? Is he a human being
who was exalted by God? Is he the son of God? Is he the (jewish) messiah? Is he
God? Has he always been God, from the beginning of time?

If you regard all four gospels as equally valid, and there probably wasn't any
compelling reason to prefer one over another, you of course face the problem of
reconciling those different portrayals. Many Christians don't seem to have any
difficulty with that, but I find that remarkable, because when you look at the
texts, they differ so markedly that reconciling the portrayals of Jesus seems
impossible. I view the trinity as a slashing of this Gordian knot.

The trinity is as bold as it is ingenious. It boldly throws logic and reason out
of the window and declares that all of those contradictory descriptions of Jesus
are equally true. The first reaction to that would of course be what I wrote
above: Amused, baffled, perhaps offended. I imagine something like: "Huh? Are
you nuts? What are you smoking?"

But the genius in it is that once you have accepted those contradictions as part
of a deeper, mysterious "truth", you are primed to accept any nonsense that gets
thrown at you. The trinity breaks open your critical defense line, so that once
you've accepted it, you are available for all sorts of contradictory "truths" in
Christianity that you'd not normally accept. And Christianity is full of
contradictions. God is Father and Son, Human and God, dead and alive, king and
servant, and so on, all at the same time. It seems that once people's critical
thinking has been blunted with the trinity, there's no holding back, and
doctrine can wallow in contradictions. They start to presume some deep and
mysterious "truth" in such contradictions, hence they believe the contradiction
is only superficious.

This isn't limited to religion. We see in our present world how the same trick
works in politics and general society. Make people accept a fundamental
contradiction, and they have lost their defense against all sorts of nonsense.
From there you can construct a parallel reality, or "alternative facts", that
condense into a hermetic system of beliefs that is totally other-worldly, and
practically unassailable.

The initial hurdle is best overcome with a combination of constant repetition,
and a bait that is the object of strong desire. In Christianity, the bait is
salvation. Coupling this with a perpetual repetition of the trinity will
eventually make it appear totally normal, and people don't ever come around
noticing that there's something weird about it.

I was the same when I was a child. The trinity had been spoon-fed to me from an
early age, and it appeared totally normal to me, until a developing critical
mind finally got around to scrutinizing this. I needed learning about Islam in
school, and their rejection of the trinity, to trigger my own thought process.
And the more I thought about it, the weirder it appeared to me. But to work
through it and eventually discard it still took years.

When they invented the trinity, the early Christians stumbled upon a powerful
mind hack. You have to give them credit for that. Coupled with early
indoctrination, this is hard to get out of your system, and I guess the later
the harder. But in spite of all this, if you look at it soberly, it was, is and
remains plain nonsense.
