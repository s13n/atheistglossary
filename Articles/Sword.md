# Sword

Have you ever wondered what Jesus and his group of disciples actually had in
mind for the passover celebrations?

We all know the story, don't we? The group spent the night before passover
outside Jerusalem in the garden of Gethsemane. Jesus was sleepless because he
wrestled with the prospects of what was to come, but most of his disciples just
fell asleep. And then they were surprised by a squad that picked out Jesus from
their midst and arrested him. Some disciples tried to mount some sort of
resistance, and Peter is said to have cut off a soldier's ear when he attacked
him with his sword. But Jesus had him put away the sword and give up. Jesus was
carried away and the remaining disciples fled, or in any way seem to have
escaped arrest somehow. There are many more details of course, but you probably
need no reminder.

It took a long time until it hit me: They had swords! The disciples were armed!
What was that good for? Did they do it behind Jesus' back? No, it turns out
Jesus approved it! Jesus found it a good idea to carry swords during the night
leading up to passover!

Now, I don't think that the story is historically accurate. But if it was
composed to convey a certain message, even worse, because what was the message
that the swords were supposed to tell? Meek and mild Jesus as a warrior?

To add a bit of background: Do not even think for a second that it was normal
for people to carry swords at this time. The times were famously rebellious, and
the Romans as well as the Jewish authorities were very careful to avoid
bloodshed during a mass event like the passover celebrations, where Jerusalem
was packed with people! Imagine a group of swordsmen causing panic there! The
authorities did everything to keep a lid on that. No visitor to Jerusalem would
have been allowed to carry a weapon!

Christians would probably say that the swords were carried in order to bring
Jesus' arrest about, so they would have served as a provocation that made sure
that Jesus' mission wasn't foiled by authorities simply ignoring him. But that
strikes me as a post-hoc rationalization. I think the disciples, with Jesus at
the helm, had a plan, and Jesus' arrest foiled it. The interesting question is,
what could this plan have been? A plan, obviously, that included the use of
weapons!

Now, we are in wild speculation territory here, so I can fully understand if you
are not prepared to follow me here. But it can be fun to speculate. So let me
give it a go!

Suppose Jesus wasn't quite the meek and mild version as he was portrayed later.
Jews at the time were craving for a messiah who would deliver them from Roman
oppression, and they were fully prepared for it to be a violent and bloody
affair. Maybe Jesus was catering to the same expectation! Maybe he was a proper
rebel, a Che Guevara type, and not a pacifist at all! This doesn't need to go at
the expense of his theological role, indeed there was no such thing as a divide
between the secular and the religious at the time. Being a religious teacher and
a guerilla leader was quite compatible.

Remember that Jesus came from rural Galilee, and quite possibly wasn't used to
large numbers of people in one place, and had no experience with the amount of
policing and safeguarding that took place in Jerusalem around passover. He was a
hillbilly who didn't know what he was facing there. He had charisma, but no
experience. He and his group thought that they had God's support, and had the
courage and the charismatic leader/messiah to make it all happen. So they
prepared to throw themselves into a battle that they expected to take place at
passover, and perhaps they had wind of other groups who would attempt the same,
but they had no idea that their group would get decapitated before it even
started.

The operation of the Roman guards was conducted with surgical precision. They
apparently wanted to avoid a big upheaval, and unnecessary bloodshed. They must
have studied the group well enough to be able to tell whom they needed to take
out in order to incapacitate the entire group. They plucked out the leader, and
the others didn't know what to do anymore. The Romans didn't anticipate,
ironically, that the disciples, in their shock and frustration, would proceed to
imagine a new religion into existence, by reinterpreting their messiah into a
pacifist passover lamb that had conquered the world after all. A religion that
would, a couple of centuries later, conquer the Roman empire.
