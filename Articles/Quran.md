# The Quran

Muslims assert that the Quran is the final revelation of God. That's one of
Islam's dogmas. For me that's the best aspect of the Quran, the prospect that
this whole revelation thing is well and truly over. Frankly, who needs any more
of this nonsense? It's not even funny!

If this should be the pinnacle of godly revelations, I posit that it paints God
in a very poor light. The Bible already offers a less than flattering
description of God, but the Quran makes it worse.

The Quran pretends to be dictated by God. Dictated word by word, not just
inspired! Most Christians do not believe anything like this about the Bible.
You'd think that when you read God's prose directly, it would adhere to a higher
standard in every respect, but that doesn't match what you read in the Quran!

There are of course a few passages where God is obviously not the speaker, but
those don't bother me much. I take issue with the often petty and repetitive
nature of the narrative, which occasionally contradicts itself, so that islamic
scholars think that a later surah can correct an earlier one (as if God needed
to correct himself). This is compounded by the odd decision of early muslims to
order the surahs according to size rather than chronologically. And sometimes,
the content is just damn convenient for Mohammed, almost as if God wanted to do
him a favor.

Considering that this is supposed to be the final word of God, almost like a
last will, I would expect a text that is focused on the important, with a
perspective on the long run, on the stuff that keeps its validity over time. I
would expect clear, unambiguous language, and a structuring of the text that is
practical and topical. I would expect that it portrays God in a way that
harmonizes with his description as gracious and merciful (nearly every surah
begins with this assertion), but the text conveys a different image of a grumpy,
tyrannical God who is unable to rein in his unruly people, and time and again
resorts to threatening them.

No, sorry, the idea that this could have been written or dictated by an actual
God is so implausible, it crosses into the ridiculous. Like the books of the
Bible, it is clearly a human work, and very much reflects the perspective and
interest of Muhammad at his time. I really don't see how I should be compelled
to look up to such a God.

And I haven't even started about the war mongering to spread the religion, the
misogyny and subordination of women, the hypocritical stance towards wine and
pig meat, and much more.

Even the islamic critique of Christianity as not really monotheistic is
hypocritical. Muhammad is at least as highly revered as Mary in the Catholic
church, including the divine attributes like bodily ascension into heaven,
sinlessness and all. Some might say this stops just short of deification, but I
don't care about such fine points. The overall stance is to elevate Muhammad
above ordinary humans, and if you do that, and if you additionally believe in
angels, spirits, djinns and such creatures, you should be rather careful when
accusing others of polytheism.

So, in summary, my bullshit detector is at least as much in the bright red as it
is with Christianity. Holy books that were dictated verbatim by some God are
simply nonsense, as should be obvious to everyone who actually reads them.
Religious people should really think twice before asserting that. They should
consider if they really are doing their favorite God a favor that way. The cases
I know of, in Mormonism, Islam, Christianity and Judaism, of divinely inspired
or even dictated books, are a disgrace and embarrassment for the God who
allegedly is their ultimate source.

Sounds arrogant? I know. And I don't care. You can't tell me that I have to read
a mediocre book and then refrain from criticising it because God. If you want me
to be in awe, show me something worthy.
