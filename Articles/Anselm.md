# Anselm's ontological argument

You may have come across Anselm's argument in some form, even when you don't
know what ontology is. Anselm of Canterbury lived a long time ago, in the 11th
century, and was quite proud of his argument for God's existence, although it
lost traction in younger times. But every now and then it gets warmed up by
someone. Its essence is the assertion that something is greater when it exists
than when it doesn't, and we call the greatest conceivable being "God", so he
must exist.

Now, I have written about existence in another entry and won't repeat this here.
But Anselm's argument is hilarious in other ways, which has given rise to jokes.
My own joke is that the argument proves the existence of Christianity:

The greatest conceivable idiocy must exist, because an existing idiocy is
greater than a nonexisting one. We call this "Christianity".

On a less jocular note, I wonder what makes Anselm so confident that existence
is greater than nonexistence. Isn't this wishful thinking already?

You could just as well make the opposite claim. I assume that logically
contradictory things can't exist. There is no square triangle, for example. But
is it conceivable? Would it be great if it was? In this case, probably not.

Now consider a God. A God that is conceived to be omni-everything (benevolent,
loving, powerful, etc.) is logically contradictory, as the theodicy has shown.
Yet such a God would arguably be greater than one that sticks to the laws of
logic. You could regard it as a lack of power when God has to obey laws of
logic. But the simplest escape here, I think, is to drop the notion that
existence is great. A fictitious omni-everything God would then be greater than
a God that is shackled by logic. To put it in another way, a God could be
greater and much more powerful when he's not restricted by the constraints of
existence in reality.

Speaking generally, isn't it the case that the greatest things are fictitious?

The idea of greatest conceivability is also dubious. Couldn't it be that there
is always something even greater than the thing we're holding in our mind right
now? Maybe it is like numbers, where there is always a number that's greater
than the one you're thinking about. Maybe greatness extends to infinity! What
would that mean for God? That God is infinitely great? But then some would argue
that actual infinites can't exist.

Sounds potty, right? But the whole thing is potty, whichever way you look at it.
It is an exercise in fiddling with poorly defined vocabulary and dubious leaps
of logic.

You can make a sport out of it. Some more recent philosophers have expanded the
argument into a form that is much harder to follow and understand, so that its
flaws are much harder to identify. The competition seems to be between those who
hide the flaws, and those who ferret them out.

I don't think this is relevant for us mere mortals. When a God needs such
amounts of intellectual masturbation to make himself credible, there must be
something wrong on a rather more fundamental level.
