# Monotheism

Officially, Christianity is a monotheistic religion. There is only one God. But
at the same time there's Jesus, the trinity, the holy family, the devil, angels
and spirits. There is an entire zoo of creatures that populate a continuum from
humans to Gods, and despite all claims to the contrary, this blurs the
distinction considerably. It is no surprise that the trinity is opposed by the
Muslims and the Jews as not really monotheistic. They're right: It is a confused
monotheism at best, with an unexplainable mystery at its core.

The Jewish monotheism is dubious, too. While they reject a trinity, they also
have a zoo of "intermediate" creatures similar to Christianity, and, perhaps
even more importantly, their scripture describes over a large part the
competition between the Israelite God and competing Gods. The existence of other
Gods is taken for granted, in the sense that every people having their own God
or Gods is being shown as the natural and self-evident state of affairs.
Throughout a long period of Jewish history, they have been henotheistic and not
monotheistic. Their worship of a single God was motivated by loyalty demanded by
a covenant, and not by the denial of the existence of other Gods.

This leads to the peculiar situation that according to the bible, The Jewish God
is the only God, and the creator of the Universe, but he then chooses one fairly
small tribe in the near east as his chosen people and goes on to combat other
people along with their Gods. One invariably wonders where they're coming from!

Maybe you object that those other Gods are not real, and people are in error
when they believe in a nonexisting God. But that's not what the bible says. The
bible portrays the other Gods as weaker than the Israelite God, but they are not
denounced as nonexisting! Anyway, believing in a nonexisting God wouldn't
justify the wrath that is being displayed in the bible so frequently. This wrath
is obviously motivated by jealousy, which implies existence. Indeed, the ten
commandments explicitly portray God as jealous!

So I find it apt to say that while Christianity is theoretically monothistic, it
is polytheistic historically, and in some sense also practically. It is one of
the many internal contradictions and inconsistencies on which Christianity
thrives.

Why are there still so many traces of polytheism in an allegedly monotheistic
religion? Why has nobody cleaned it up?

Well, arguably, Jesus tried to do that. But his followers promptly messed it up
again, by declaring him to be God. I am pretty certain that Jesus never called
himself God. He would have regarded that as strongly blasphemous. This goes to
show that people don't actually want a clean monotheism. They want to be able to
deify whom they worship. They certainly don't want their favorite prophet to be
a mere mortal like everybody else. This is clearest in Islam, where Mohammed is
at the same time regarded as a human being and an almost God-like figure with
the ability to perform miracles.

The catholic church has cultivated this almost to perfection. It took this
desire of the believers on board, realizing that it wouldn't be able to stop it
anyway, and proceeded to integrate it and control it. Instead of letting people
create their own prophets who would invariably go on and threaten the authority
of the church, the church came up with a process to absorb such prophets into
the fold. At the same time, the church tried to control the people's
imagination. If you can get the people to see miracles that somehow adopt the
christian imagery, you can use it to bolster your religious monopoly. When
people flock around a faith healer, it is important for the church to make sure
it is a christian faith healer.

In the end, the catholic church is extremely flexible regarding the tenets of
their members' beliefs, as long as they're not in rebellion against the
authority of the church. If the people want saints, given them saints! If they
want miracles, give them miracles! If they want demons, give them demons! Adopt
them with great ceremony and thus subordinate them under the large umbrella of
the church. Whether they make sense is beside the point. None of this makes
sense, anyway! Exorcists or faith healers or lay preachers aren't a threat to
the church. Apostates and heretics are.

From this perspective, the question of monotheism is irrelevant. Who cares what
Christianity actually is? It is more important what it can be! If you need the
intellectual clarity of monotheism, it is monotheistic. If you need to integrate
the variety of popular superstition, it is not. Christianity is used to having
it both ways at once.
