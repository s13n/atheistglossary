# Sex

It seems to me that many Christians are so obsessed with sex, that they suspect
everybody else to be obsessed with sex. Some even believe that people become
atheists in order to be able to engage in sinful sexual activities. This reveals
a lot about the thinking of Christians, but nothing at all about the thinking of
atheists.

Now, it is true that sex plays a rather large role in public discourse in a
secular society. It is used pervasively in advertising, for example. In a way,
it seems that everybody is indeed obsessed with sex, not just Christians. It
would be more healthy, I often think, if everybody would dial it down a notch or
two. Because actually, sex is normal. The whole hullabaloo can make you overlook
this simple fact.

Instead of showing a relaxed attitude, people freak out even on minor occasions,
such as an exposed nipple of a female breast. For somebody who didn't grow up in
the USA, this appears distinctly bonkers. What's the harm, one inevitably asks.
What sense does it make to protect children from this sight, when only a few
years earlier, they have had their mother's breast in full close-range view, and
even sucked on the nipple that now supposedly harms them to just look at?

A bit earlier even, they (most likely) passed through the vagina which under no
circumstances they are allowed to look at now. The same body parts that are most
directly involved in our very existence are those who are under the most strict
taboo! How does this make any sense at all? And don't try to tell me that those
are the most "private" parts! In discussion, they are very public, indeed!

A relaxed attitude would take the basic facts of life, like birth, sexual
reproduction, and the naked body, as totally normal and in no need of
obfuscation. I believe that if it were treated as normal, there wouldn't be
nearly as much confusion, abuse, ignorance, repression and obsession in
relationships. Sexual attraction is not just a fact of life, it is a direct
consequence of sexual reproduction, and they evolved simultaneously. We
literally wouldn't be there without.

The attempt to control sexual attraction by cloaking the female body is both
misguided and unfair. It is misguided because the attraction doesn't go away
just like that, and can only become more irrational and more driven by
illusions. And it is unfair because it makes a one-sided responsibility of what
in reality afflicts everybody in a similar way. In some religions this has
assumed a totally ridiculous form: Women are made responsible for whatever men
do to them because of their sexual fantasies. The men are effectively absolved
from any responsibility for their own conduct, and declared the victim when they
really are the perpetrator. Very convenient for men, totally unfair for women.
This is patriarchy in its most blatant form.

But, one could suspect, this patriarchy has evolved as a successful model for a
society. It wouldn't exist if it didn't work! Controlling sex through taboos
might have survival value. Or, to be precise, might have had survival value in
the past.

This is certainly plausible. If controlling sex in this way leads to less chance
of contracting a sexually transmitted disease, it would lead to a survival
advantage. The same applies if it leads to fewer fights and skirmishes within a
tribe. And if it leads to more and longer term commitment between parents and
offspring, it might increase the survival chances of the offspring.

While this might all be true, it doesn't mean that this form of sexual taboo is
the only, or the best, way of achieving this goal. Evolution doesn't necessarily
come up with the best solution, it comes up with a sufficiently good, working
solution, given the circumstances. Those circumstances, however, have changed
radically in the last few centuries. For example, we have a rather effective
health system now. We have condoms and contraceptives. We have social security
and child care. In short, we have come up with much better ways of improving the
survival chances, so that we don't need those crude taboos anymore. We could put
the fun back into sex, and - even better - the normalcy and relaxedness.
