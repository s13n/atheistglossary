# Existence and Causation

You may think this is all rather straightforward. Things exist or they don't,
and when they begin to exist, there must be a cause for that. Common sense
stuff, really.

But you would be fooling yourself, as this stuff gets more difficult the more
you dig into it. You eventually reach a point where your head spins, and you
cease to think you know anything. That's not your failure, because this stuff
genuinely is difficult and intricate. I can't do it justice here, but I can try
to shatter your unwarranted confidence.

What does it mean to exist? The things around you, which clearly exist, all are
made of matter that existed previously. The computer I'm typing this on is made
of plastic and metals that all were there in some form before my computer was
manufactured. So when exactly did my computer begin to exist? When the last
screw was tightened? When the operating system was installed? And what was the
cause for its existence?

I once replaced my computer's disk. Does this mean that it is still the same
computer? Or has my computer ceased to exist when I took out the old disk, and a
new computer was created when I mounted the new disk?

And when would my computer cease to exist? When it becomes nonfunctional in some
way? Or do I need to shred it in order for it to cease to exist?

You see that even for rather mundane things, the question of existence quickly
becomes ambiguous and unclear. Existing things are made of components that also
exist at the same time, and this continues recursively down to the last detail.
You can exchange components, and thereby alter a thing's composition, without
causing it to cease to exist. In the extreme, you could exchange every component
of something one by one, so that ultimately nothing remains that was in the
original thing, and it would still exist. Living organisms are like that. They
have a metabolism that leads to a gradual exchange of pretty much all atoms that
make up the organism, and yet the organism as a whole persists.

So every actual thing needs some "feature" where you can attach the meaning of
existence. In other words, you need some kind of "essence" or "identity" of a
thing. As long as this essence is there, the thing can be said to exist. It
wouldn't be anything material, since we have seen that you can exchange the
material without affecting the existence of the thing. It must rather be the
particular arrangement or structure of the material that represents this
essence. The thing comes into existence at the point in time when this
arrangement is established, and its existence ceases when the arrangement is
dissolved.

But this means that for each thing, you have to answer the question of what its
essence is. This is anything but obvious, even for rather common things.
Consider a hurricane. In contrast to computers, hurricanes form spontaneously.
Nobody assembles them purposefully from parts. What are the spatial and temporal
limits of a hurricane, i.e. where and when does it exist? And what is its cause?

You find that a hurricane has some athmospheric preconditions which are
necessary for its formation, but it has no identifiable cause. If the
preconditions are there, a minuscule and random event can develop into a
hurricane through gradual accretion and expansion, and such random events occur
all the time in incredibly high numbers. There's simply no way to identify the
actual event that started the hurricane, so one could postulate a hypothetical
cause, but this would be a mere assertion without any way to verify it. For all
practical purposes, the hurricane has no cause. It arises randomly.

It doesn't even have clear boundaries, in either space or time. There's no exact
time when it has started or ended, and no exact spacial boundaries it is
confined by. Air continuously enters and exits the active zone of a hurricane,
so it isn't made of clearly identifiable material, either. But the hurricane as
a whole clearly exists - as a configuration of matter, a pattern, so to say. So
what would you say is its essence, on which its existence hinges?

Note that I haven't even touched upon nonmaterial things, and it is already
rather difficult. You could ask what it means for an electrical field to exist.
Or what it means for a feeling to exist. That'll be your homework.

I don't have a resolution for all this, but I encourage you to have a hard look
at the arguments for and against God that depend upon a notion of existence and
causation. Ask yourself what the words mean that are used in these arguments,
and how they apply to the problem they purport to solve. You may well find that
the arguments decompose in front of your eyes.
