# Movement

What's a "Movement"? Lots of people talk about the "Atheist Movement" or the
"Secularist Movement" or the "Woke Movement" or similar. Now, I'm an atheist. Am
I therefore part of the "Atheist Movement"?

I don't think so. I have reservations regarding this "Movement" way of thinking.
Maybe a bit of differentiation is in order. To associate individual people with
a movement means to associate them with a common goal, and somehow suppose that
they are interested in actively promoting that goal. What would that goal be,
and how come you are supposing that I'm interested in promoting it?

My perception is that the goal of the "Atheist Movement" is assumed to be to
combat religion. To proselytize people out of their religion. To "deconvert"
them. Some atheists may have that goal, but I don't. I don't believe your
religion is bad for *you*, and therefore try to get you out. Only *you* can make
that judgment. I believe that your religion might be bad for *me*, and therefore
I try to rein it in.

My goal is not to stop people being religious, but to stop them being religious
bigots. I don't try to convince people that I'm right, but that I may have a
good point worth pondering. My hope is that if people view me as someone who
seriously considers things, and takes available arguments in all directions into
account, they will respect my position and refrain from trying to dominate me
and deprive me of my freedoms. My concern isn't what people believe, but what
they do.

Now, obviously, what people believe has a bearing on what they do. So I can't
ignore entirely what people think, even though I'd prefer if I could ignore it.
Thoughts are free. This is one of my most important maxims. But, sadly, I know
that by itself, this would be naive. People who genuinely believe that atheists
are evil, will at some point interfere with my own freedom in a way that is
unbearable to me. I will have to try to preempt them. I know that when I
tolerate an intolerant religion, I will eventually lose my own freedom.

So when I say I'm trying to stop people being religious bigots, I really defend
my own freedom. That's my goal, and is what makes me "move".

Now, if you have an important goal that many people share, it makes sense to
band together in order to combine your forces. At this point, the goal becomes
political. Seen from this angle, a "movement" is a group of people banding
together to promote a political goal. There's nothing wrong with that,
conversely it may be the only way to amass enough clout to effect change on a
societal level. But there's a snag:

People project their own expectations and goals onto the movement they
participate in.

For example, some people in the "Atheist Movement" have tacitly or openly
assumed that the goal isn't just atheism, but also feminism, an egalitarian
society, pacifism, social justice, and more. They have assumed that atheism
amounts to a certain respectful and nonviolent way of engaging with each other.
They have assumed that fellow atheists are generally nice people. And they have
become frustrated when experiencing that it isn't so.

There's no rule that when you don't believe in a God, you also can't be an
asshole. Many things just are separate, as much as you want them to come in
unison. Atheist bigots exist just like religious bigots exist. Hence you have to
decide whether it is belief you want to combat, or bigotry. If it should be the
latter, you may find that you have quite a few potential allies amongst the
believers. Don't make them your enemies needlessly.

I've never been with the "New Atheist Movement", although I found them
interesting and stimulating for a while. Part of their motivation and reason for
existence was the threat of religious bigots, especially following the 2001
attacks, where it became crystal clear what the potential is of religious
fundamentalism. And by analogy, what Christian fundamentalism can result in. In
that I was fully on board. But I felt that the view of the common goal got
diluted, and an increasing number of people integrated topics into the agenda
that had little to do with atheism. And consequently the "community" started to
descend into internal bickering.
