# Resurrection

Most Christians would regard the resurrection of Jesus after his death on the
cross as the centerpiece of christian belief. If you don't believe this, you're
not a Christian.

You probably know by now that I don't believe it, but why do you? What is it
that convinces you? A story of an empty tomb in a book that promotes a religion,
rather than trying to relate only the facts? Seriously?

The bible itself shows that it didn't trust the empty tomb alone. Or why would
there be the story in Acts that Jesus walked amongst the disciples on earth for
40 more days, before ceremoniously rising into heaven? Clearly, something needed
explaining or demonstrating here, in addition to the empty tomb as such!

The gospel of Matthew adds more support to this, when he discusses the
allegation that Jesus' body was stolen from the tomb, framing it as a calculated
lie of the priests. Clearly, there are numerous non-miraculous ways how an empty
tomb could have resulted. We don't know for sure what happened, but there's
absolutely no reason to presume that a miracle was involved.

If you take all this together, it actually reinforces the impression that this
is all manufactured. At the very least, you can't use the empty tomb as evidence
for your faith, when you need that faith in the first place in order to presume
it was miraculous.

Consider: Jesus died the death of a rebel, a secessionist from Rome, even though
he allegedly told his disciples to expect a new kingdom with him at the top.
Clearly, without further explanations and demonstrations, nobody would have
believed this. At best, he looked like someone deluded. Especially after the
destruction of Jerusalem a few decades after his death, when most of the new
testament books were written, people needed to be convinced that his case was
different from all those self-declared messiases that led the Jewish uprising
against Rome which ended with a total and brutal defeat. A straight-minded guy
at the time would have ticked off Jesus as just the same as all those others.

So the elephant question in the room was: How do I know Jesus wasn't just
another failed rebel? And so the scribes made him resurrect and thereafter roam
the streets to demonstrate it. It is telling that some people actually went
further and speculated that Jesus remained on earth and went to India, or
France, or wherever. Indeed, it is hard to explain why he should have stayed for
40 days and then rise to heaven. What would have been the point in that? If he
had needed to explain his resurrection, why for just 40 days? Why would he need
to demonstrate it in person to a few of his disciples, but nobody else?

For example, instead of rising to heaven on the 40th day, he could have done
that immediately at the end of his crucifixion. He might even have had more
witnesses that way! Instead, he appeared to a few people in a fairly private
setting. Scholars widely believe, that the first gospel of Mark didn't
originally have this story at all, of Jesus remaining on earth after his
resurrection. That this passage at the end was added later.

A sober interpretation of this leads to the conclusion that it was made up. The
later the text, the more the story was elaborated. Clearly, this was a reaction
to the doubts that people had, and a sign of the development of theological
doctrine. But at the same time, it departs more and more from the facts. At the
end, it is all story and no facts.
