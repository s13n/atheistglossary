# Gut

Do you make important decicions with your mind or with your gut? Things like
getting a new job, file for divorce, buy a house, this level of stuff. Decisions
that tend to have long term impact, and which usually take a while to consider
and work on.

A related question would be if rational decision making would demand that you
give your mind the preference over your gut. That perhaps you should even let
your mind override your gut.

You will perhaps be surprised that I wouldn't answer with a clear yes. I believe
that important questions require you to answer with both mind and gut. A good
decision has both in agreement. The gut stands for a subconscious layer of your
mind, and I trust you understood that it is a metaphor right from the start. So
really we talk about having agreement between a conscious view, and a
subconscious view on a certain problem you're wrestling with. To put it
differently, you need to both believe the decision is right, and feel it is
right. If you let one overrule the other, chances are that you will come to
regret the decision, or to not adopt it and carry it through with enough
confidence.

There are several instances of decisions where it took many months for me to
come to a point where I felt that my reason and my feeling were in agreement,
and in all of those cases the decision I arrived at held up, and I didn't regret
it. Sometimes the gut leads and the mind follows, sometimes it is the other way
around. It doesn't matter, as long as you have the time to let it sort itself
out before you need to decide. I know that it isn't always like that, but if you
feel you're not ready to decide, that should not be dismissed lightly. Even when
you can't put off the decision any longer, you may still be able to find a way
to make a provisional arrangement that makes the decision you have to commit to
less permanent, in other words you may be able to make an interim decision that
buys you some space for the actual decision.

I don't see this in any relation with "spirituality" or even with a "soul" or a
divine influence on myself. I happen to think that the subconscious in people
needs to be taken seriously, and the ratio by itself isn't always "right". There
is a balance that needs to be struck between both sides of your self, the
conscious and the subconscious. Focusing on the rational mind may obstruct parts
of yourself that would be able to make sense in a different way. Often, those
parts contribute something that is difficult to articulate and explain. A
decision may feel wrong without you being able to explain why.

I used to think that in such a case you ought to be able to figure out the
reason, and come up with a plausible rationalization which can be explained to
others. I'm not so optimistic anymore, because I have seen people trying to
rationalize it in a way that wasn't genuine. I wanted an explanation and they
made one up for me, but it wasn't helpful. Some would say that the heart has its
reasons, and often they can't be explained. The heart is a metaphor, of course,
but the core of what it has to say is genuine. Yet, I do think it pays to try to
figure out what the heart wants to say, i.e. to understand what the problem is
that gives you a bad feeling.

When I was a teenager, the decision what to do with this religious belief
gradually raised its ugly head. The good thing that I instinctively got right
was to let it grow on me. There was no immediate need to decide anything, and
even though I had my doubts, I still played along, fulfilling the expectations
of my family and environment. I went through the confirmation ritual where I
basically had to commit myself to Christianity, without actually being
sufficiently convinced. I felt I made promises I wasn't ready to make, but I
also felt it wasn't my fault. And I didn't think it carried that much weight,
because I wasn't intending to become a bad person anyway. I wasn't following the
faith tenets in the literal sense, but I followed it to the extent where it
mattered in practice.

That's more or less what my stance was. It bought me the time and relaxedness to
develop my position more throroughly. I'm sure it provided me with more peace of
mind afterwards, and helped me avoid the more impetuous separation from the
faith that people tend to go through when they deconstruct later. And when I
finally made the small step of deregistering from the Catholic church (which was
an official act in Germany) in my twenties, I was at ease with the decision and
haven't regretted it ever since. My gut was in line with my mind.
