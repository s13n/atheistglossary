# Historicity of Jesus

If you are a Christian, you will invariably believe that Jesus really existed,
i.e. he appeared on earth physically as a human being - or at least he appeared
to be one. In other words, you believe he's historical.

Some atheists believe this, too, but others believe that he's a myth. He never
appeared on earth as a person, and all biblical accounts are mythological, with
no historical factual grounding.

I tend to assume a historical core in those accounts, so I technically believe
in the historicity of Jesus. But I have to qualify this here. Firstly, I don't
know. I have a hunch, but I am far from confident in this respect. As far as I'm
concerned, I see reasonable arguments on both sides. And I am comfortable with
this insecurity here, since I don't need a definitive answer. Nothing important
depends on it.

As interesting as history is, and for me it is especially cultural history that
I find very interesting, I realize that I will never learn how it really was.
Cultural history is about the question: How have things ended up the way they
are? Many of the customs we have, the beliefs we hold, the rules we follow, have
deep roots in the past, and I'm interested in finding out about them. At the
same time I am only too aware that it will always remain down to interpretation.
That we only have sparse information about the past, particularly the distant
past, and that there are many different ways how the jigsaw puzzle can be
assembled. I consequently can't say with confidence how things were, and how
they developed. I can only approach it, and work out what I find plausible. And
I have to be prepared to change my mind when significant new data comes in.

So I find it plausible that a person named Jesus lived in Palestine some 2000
years ago, who served as the nucleus of a legend that has been spun around him
posthumously. However, the historical Jesus, I suspect, has very little to do
with the legend, other than serving as the starting point, so that all we have
now is the legend, and the person has dissolved in the mist of time. At any
rate, I very much doubt that we can learn anything substantial at all about the
historical person when reading the bible. I consider the search for the real
Jesus futile.

But I would argue that this doesn't matter. The only thing that matters about
Jesus is the legend. Whether the historical person existed or not, changes
nothing for the legend. And it changes nothing in cultural history. It is the
legend that had, and still has, a profound impact on culture, and a purely
fictional Jesus wouldn't change this at all. Indeed, our culture owes much to
various human inventions. Fiction is a powerful driver of culture. Just as the
cultural influence of Homer's Iliad and Odyssey doesn't depend on whether
Achilles, Odysseus or Helena actually existed, or were mere inventions of an
antique author, the cultural influence of the Bible doesn't depend on the
historicity of Jesus, or any other of the protagonists.

The story, or stories, are very interesting to me. Not the least because such a
lot in our culture still derives from them. You don't understand our culture,
our time, our society, if you haven't looked into the Bible. But you need to
take the stories for what they are: Legends.

If you asked me whether Jesus really existed, the most likely answer you would
get from me is: Who cares?
