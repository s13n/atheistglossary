# Conspiracy

As a child, I was taught the story of how Judas had betrayed Jesus with a kiss.
How the Jews were after Jesus' life because he, and his teachings, were
dangerous for the Jewish authorities, the high priests in the temple. Judas, so
I was taught, revealed the disciples' whereabouts to them, in a conspiracy
against Jesus.

Much later, I came to realize that this story made very little sense. It is one
of those conspiracy theories that we are plagued with to the current day. People
believe them because they feed into their convictions, their wishful thinking,
their world view. And people come up with them to fill in the blanks when they
don't know. Once you believe the conspiracy theory, facts don't matter anymore.

Do you believe the Judas story? Have you ever had second thoughts about it?
Doubted its details, its logic? Consider: You are member of a tight-knit group
of disciples of some kind of prophet or rebel. Somebody offers you money if you
disclose the whereabouts of your group to the very authorities that you want to
get rid of. And in order to earn your money, you lead them to the group, in
person, and kiss the leader in front of everybody, so that he can be arrested.

I find that extremely brazen! Everybody from the group will see you as a traitor
from there on! Would you think you could get away with that? You would have to
be as stupid as a mule to not realize what danger you get yourself into! I mean,
the diciples *are* frequently depicted as a bit stupid in the gospels, but this
knocks the bottom out of the barrel!

There are more reasons to doubt the story. There's the notion that Jesus'
teachings would have been dangerous for the Jewish authorities, as he wanted to
erect a new kingdom, and that would have resulted in their loss of power. I
think that's a massive exaggeration. Nobody would have regarded a group of a few
dozen disciples as a threat to their power. I guess it barely appeared on their
radar. The much more direct threat was a prospect of violent unrest during the
passover festivities, when Jerusalem was full of people. For that, it must have
been more important for the authorities, that the disciples had swords, than
what Jesus had to teach. With a weapon, you can cause a lot more upheaval than
with a sermon, at least momentarily.

The surgical way in which the authorities conducted their operation attests to
that. If you want to neutralize a militant group, it is enough to remove their
leader, and they will not know what to do. If you want to get rid of an
ideology, you need to go after all those who spread it. The way the authorities
reacted shows that they weren't worried about the long-term effect the group
could have had. They tried to remove an immediate threat, and did so
effectively.

I doubt they even needed somebody to betray the group. If they knew enough to
approach Judas, they would have known enough to keep an eye on the group and
know where they are. I don't think they would have needed Judas to identify
Jesus, either. Quite possibly Judas got accused for no fault of his own.

That's what conspiracy theories often do. The bedrock of every conspiracy theory
is the notion that everything happens for a reason, and this reason is often
hidden from you. People who accept that things may well happen for no particular
reason at all, perhaps even at random, are much less susceptible to a conspiracy
theory. But if you need a reason, you invent one. Seemingly innocuous details
all of a sudden acquire an inflated significance. Judas may have kissed Jesus
for a completely harmless reason, but the coincidence gets reinterpreted as
evidence. Once it has started, a conspiracy theory mushrooms, and ever more
details feed into it that wouldn't normally have any significance.

This is driven by a pressing need to explain something shocking, the sudden
execution of Jesus. The most direct explanation is too embarrassing, and thus
unacceptable, so other explanations were needed that reinforce what the
disciples had believed anyhow. It is the beliefs that people hold dear, which
feed into a conspiracy theory. It is easier to reinterpret part of reality with
the aid of a few selectively picked factoids, than to scrutinize and perhaps
surrender some of one's own beliefs. And the more such factoids you pile up,
each of them usually completely innocuous, perhaps even random, the more they
appear to reinforce each other. It becomes more and more compelling, even though
it is the selection of factoids in the first place that determined the result.
You don't notice anymore how this is just a reflection of your own bias. At the
end you don't even realize when facts are made up entirely.

What can help here? We actually do know how to investigate things properly, and
we could just follow the method meticulously. This is what a court is expected
to do. But even more important than the method are two other things:

First, you must separate the things you know from those you don't know, and
especially admit to not knowing.

Second, you must be aware of your own biases. You must have the integrity and
courage to search for truth where you don't want it to be.
