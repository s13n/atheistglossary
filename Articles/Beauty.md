# Beauty

You may know by now that I'm an engineer of profession, and quite possibly also
the engineer type of person overall, whatever that may mean. I heard people say
well, what do you know of beauty? Beyond the arrogance and condescension, there
may actually be a valuable core to this, which is worth pondering.

So, what is beauty, actually, and to what extent are the ideas and views about
beauty different between people in general, and in our context here between
believers and atheists in particular?

I do observe in my social interactions that other people are often more directly
visually orientated than I am. For them, beauty almost invariably is associated
with visuals. They may understand the metaphor of a beautiful thought, but only
really as a metaphor, not as meaning that something nonvisual can have its own,
proper, type of beauty.

This is not a moot question. Would you say that blind people can have a sense of
beauty? I'm talking about those who became blind before they had a chance to
develop a visual sense of beauty, so that if you'd grant them a sense of beauty
at all, it would have to be an entirely nonvisual one, a non-metaphorical one.

I'm not blind, but I also have a sense of beauty that is nonvisual, and I would
posit that it isn't just metaphorical. I am convinced that nonvisual things can
genuinely be beautiful. A sound can be beautiful. A thought can be beautiful. A
mathematical proof can be beautiful. An engineering solution can be beautiful.

I don't state this to diminish visual beauty, but rather to expand it, and also
to abstract away from it, to make it easier to recognize that it is a more
general thing, not directly dependent on a particular human sense like vision.
Indeed, you sometimes hear that there is a "sense of beauty", as if it were a
separate sense, and not a way of interpreting what one of the ordinary senses
tell you.

I tend to think that if you view beauty as a separate sense, it is more directly
related to human intelligence and the human ability to think abstractly. If
beauty is merely what appears attractive to you, then animals have a sense of
beauty, too. The beauty of the male peacock attracts the female peacock, and we
humans regard it as beautiful by some sort of coincindence. The attraction of a
male boar for the female depends a lot more on pheromones coming from their
salivation, which we humans will likely not regard as beautiful at all, but for
the sow it arguably represents a form of beauty.

It follows that the more directly the concept of beauty is tied to a particular
sense, the more it is going to be a biological function. This means that, for me
at least, this idea of beauty is not at all pointing to the supernatural. It
rather confirms biological evolution in some way. If anything needs explanation,
it is the more abstract idea of beauty that we humans are sometimes capable of.
But this is just one instance of the general ability of humans of abstract
thought. There is nothing special to beauty here. The question is how can we
explain the human ability to think in abstract terms, and the sense of beauty
then mostly explains itself.

If your concept of beauty doesn't have this abstract property, it is no
different from the sense of beauty of the sow that finds the stench of the male
boar's saliva attractive. It is a biological function related to hormones, which
evolved along with the rest of you.
