# Perfection

God is supposed to be perfect, right? But why? What purpose does this alleged
perfection have? Could an imperfect God not have achieved the same? Wouldn't
that be more plausible, even?

Note that it is in no way inherent in the concept of God to be perfect. The
Greek gods are anything but perfect. They have the same whims and moods as
humans, and they make mistakes, just on a much bigger scale than humans are able
to. The same, incidentally, applies to the old testament God. If he would have
been perfect, there would have been no need for a flood, for example. And need I
point to the changing moods of God? Even meek and mild Jesus occasionally throws
a tantrum. To call that perfect makes a mockery of the word.

So what does perfection even mean against such a background? How come that a
perfect God ends up creating such an imperfect world, and such imperfect beings?
Beings, for whom he had to come up with elaborate trickery and magic to save
them from annihilation. To me, that looks like a monstrous botch, a flawed
construction that gets patched up with a ludicrous stopgap that is being sold as
the ultimate solution, the "good news"! If it was still under warranty, I'd give
it back!

I would find it much more plausible for our universe to be created by one of the
Greek Gods, probably while drunk. That would match the result much better. In
fact, what would a perfect universe even look like? I struggle with that
question much like I struggle with imagining paradise. All I can come up with
would be mind numbingly boring, and where's the perfection in that?

To be blunt, I have abolished perfection from my mind. I have no use for it. It
doesn't exist, never has, and never will.

You may object that this robs me of something to aim for, and condemns me to
mediocrity. That's not true. I can still aim for the better. I have not lost my
capacity to judge, I merely acknowledge that it is limited, and not universal.
Someone else may have a different idea of what is better.

The big advantage is that the goal to improve things is reachable. Aiming for
perfection is always going to fail. Aiming to improve things means that you have
a chance to reward yourself, while aiming for perfection always ends in
frustration. Being humble pays.

So how did this obviously imperfect Christian God become perfect? I guess it was
invented to fend off criticism. To put God on an unassailable pedestal. God
isn't *actually* perfect, he's *notionally* perfect. Calling him imperfect isn't
false, it is disloyal and pretentious. Calling God perfect isn't trying to make
a factually true statement, but to establish an insurmountable hierarchy that
makes it illicit to say, or even think, otherwise. In some monarchies and
dictatorships it is similarly illicit to criticise the ruler.

So when Christians call God perfect, it doesn't really say anything factual
about God, it tells you something about their belief system. It shows that they
dare not think about a fault that God might have, or might have committed. They
see it as blasphemous to doubt God's perfection. They fear the wrath of God
descending upon them: "Me, imperfect? I'll show you who's imperfect here, you
puny earthling!"

So, ironically, they call God perfect, because they fear his imperfection.
