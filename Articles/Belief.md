# Belief

A popular quote misattributed to Chesterton reads: "When people stop believing
in God, they don't believe in nothing — they believe in anything." What appears
witty and evident to some, doesn't hold much water in my opinion. It ought to be
checked against reality. Two questions present themselves:

1. Do atheists really believe in anything?
2. Do theists equally believe in anything?

I acknowledge that atheists don't believe in nothing. They are often unfairly
accused of this by ignoramuses, but I doubt that one can live life believing in
nothing at all. For example, I believe that the bank will keep my money in my
account, that the sun will rise again next morning, that my car will start when
I need it, and that people I value very much will not suddenly turn against me
out of nothing. To note but a very few things I believe in, deservedly or not.
But I don't believe in God, and neither do I believe in anything.

And I note that theists apparently believe in a bewildering multitude of
nonsensical things, so that from my perspective it is the theists that are more
prone to believing in anything. This is of course helped by holy scriptures that
are ambiguous and arcane, by believers that neither read nor understand them,
and by churches that thrive on that.

To me, it rather looks like religions don't change the situation much at all.
Some people are more gullible than others, and both religious and nonreligious
actors try to take advantage of that.

For example, take the recent Covid pandemic. Both religious and nonreligious
actors tried to convince people that vaccines are a bad thing, which is the
opposite of what reason and knowledge would suggest. Some preachers and churches
made people believe that they are protected by Jesus' blood, even though nothing
of the sort appears in the bible. Other nonreligious actors made people believe
that vaccines are part of a conspiracy to control people and rob them of their
freedom. It is moot which of them is worse: They're all bonkers.

Clearly, religion does not prevent people from believing in anything, as often
enough they themselves make people believe in anything! Churches and cults do
the same, regardless whether a God is involved or not.

Of course, there are big differences between churches regarding what they teach
and make people believe, and the same applies to nonreligious actors. It would
be wrong to dismiss all of them summarily. But how do you tell them apart? Who
can be trusted? Who are you justified in believing, and why?

Spoiler: It has nothing to do with God.

You will have to apply critical thinking, and that is something you need to
learn and practice. You need to ask yourself: What are they actually saying?
What do they claim to know? How do they know it? Is it coherent, does it make
sense? What do others say and how do they know? How vulnerable am I to believe
what I want to be true, rather than what is true, i.e. can I handle the truth?

To use a notorious example, remember Dr. Fauci vs. President Trump during the
Covid crisis, when some day Trump suggested using disinfectant to cure it?
Fortunately, hardly anyone tried it, so most people appear to have had enough
sense to answer the above questions right in this case. But it is worth
reiterating them:

What was Trump actually saying? Well, he followed up on someone else reporting
that sun exposure and cleaning agents can kill the virus on outside surfaces, by
suggesting to try this inside the body. He appeared to think that what works
outside would have a good chance of working in the body, too, not realizing that
it would be positively harmful, i.e. that what would kill the virus in the body
would also kill or damage the cells of the body, and thereby make things a lot
worse for the person.

What did he claim to know? Nothing really, but his self-description as a "stable
genius" was likely to convince people that he was on to something.

How would he know it? Well, he doesn't. He's not an expert in this, and would
have had to draw from expert knowledge instead of making stuff up by himself.

Was it coherent, does it make sense? No. Expecting substances to work inside the
body in the same way as outside is beyond naive. You certainly don't need to
know much to recognize that it is nonsense. But in a country where it is
necessary to warn people that a cup of coffee might be hot, you never know how
many people would still take it seriously.

What did others say? Well, they almost unanimously warned against doing so,
amongst them people who can rightly be considered to be experts, like Dr. Fauci.

How vulnerable were you to wishful thinking? That'll be your homework.

It is not always this easy, but you should make it a habit trying to think
methodically about such things, rather than letting you get swayed. In church as
well as outside.
