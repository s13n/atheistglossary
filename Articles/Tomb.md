# Tomb

Why is the empty tomb so important for Christians? You would have thought that
the actual important point is that Jesus was resurrected! What does this have to
do with an empty tomb? Is it not the reappearance of Jesus, that matters?

Think about it: If you met someone who you assumed was dead, and you spoke to
him in person, would you need an empty tomb to convince you that he's alive?

The bible has Jesus spend 40 days on earth after his resurrection, before he
finally ascends to heaven. During this time he has multiple encounters with
various people who presumably could attest to that. If that isn't enough to
convince people, what does an empty tomb add? People haven't witnessed the
actual resurrection, which would have been a rather different level of evidence!
Only seeing an empty tomb strikes me as superfluous evidence, evidence that
doesn't add anything to the encounters Jesus allegedly had with all those
people.

If the tomb had not been empty, and Jesus still had met all those people, that
would have been interesting! But with the story as it is, the empty tomb is just
what you would have expected anyway.

But the gospel writers apparently put great weight in the empty tomb, and manage
to tell a story that differs between each of them in ways that are very hard to
reconcile. Each one tells a different story of what exactly happened at the
tomb. I can't help feeling that it would have been more credible if they hadn't
told anything about an empty tomb. As it is, the whole thing smells.

As evidence for a resurrection, an empty tomb is pretty feeble. You had someone
in a tomb, and a while later it is empty. What happened? Resurrection isn't the
first thing I would think of. Someone removed the corpse, is what I would think
of it.

The stone that was rolled in front of the entrance presumably is intended to
show that this is implausible, but why should someone who is able to roll the
stone in front of the entrance not also be capable of rolling it away again?
There's nothing in the story that would make a removal of the corpse by someone
an impossibility, or even an implausibility. In no way does a resurrection
appear as the most plausible explanation.

It seems the gospel writers gradually "improved" the story over time, in order
to make it more plausible. Mark as the first gospel writer has the stone removed
already when the women arrived. Matthew as the next felt that this wasn't
convincing enough, and had the stone rolled away by an angel while the women
were looking. Luke returns to the version where the stone was removed already,
but he adds a second man in shiny clothing telling them what happened,
apparently figuring that two witnesses is better than one.  John, finally, has
two disciples come to the tomb and investigate, and only after they're gone do
the two angels appear to Mary Magdalene who was standing outside the tomb,
weeping.

If you had four witnesses before a court tell those stories, they would be
regarded as quite unreliable. There are clear signs that every one of them adds
details to the story in order to make it more convincing, and in so doing
creates contradictions that end up making it less convincing. Nothing of what
they describe makes an actual resurrection plausible. The common content is
basically just an empty tomb. So what? is what I'm compelled to ask!

It should be sufficient to see Jesus walking about and talking to people to
establish that he's alive. And if he was dead before, he must have been
resurrected. But for some reason, the gospel writers didn't put that much trust
in their own story, and felt that they had to double down. And, oddly enough,
people didn't readily recognize Jesus when he was walking amongst them, even
though they must have seen him alive only a few days before. That isn't very
convincing, either. I can only say: I don't buy it.
