# Free Will

Christopher Hitchens answered the question whether we have free will like this:
"Of course we have free will, we have no choice!". I disagree. People do seem to
have the choice, and many choose to have no free will. I chose to have one.

In a way this ironical quip encapsulates the absurdity of the term. Why do I
think it is absurd? Well, our free will is being used against us. When in court
as an accused, for example for a case of theft, we can't simply defend ourselves
with the claim that we aren't responsible for our acts, because we had no free
will. We could claim that we were under the influence of some drug that
debilitated our capacity for rational thought, but then we probably would be
responsible for taking the drug in the first place. So, clearly, we aren't free
in our application of our own free will. We are obliged to use it in a certain
way that suits society. Failing to do this can land us in jail. Many Christians
will add that it can also land us in hell.

While I would definitely say that we have a will, it is neither entirely free,
nor is it entirely under our own control. Our will is somewhat free. It is free
within limits. Free will isn't a yes/no question, it is one of degree.

This blends in with the question of freedom itself. In my mind, the question of
free will is pretty much the same as the question of freedom. If you have no
freedom, your free will is pointless, a liability. If you have no free will, any
freedom is useless, as you wouldn't know what to do with it. Having complete
freedom without any restrictions whatsoever is just as scary as having no
freedom at all.

In practice, we're always testing our limits, the limits of our freedom. We try
things and see how it goes. We can try things just in our head, or in reality.
The former allows us to work out if something is a good idea, before we act on
it and create a fact that we can't undo, which is what the latter does.

For me, the question of free will isn't very interesting. The interesting
question is how we use the freedom we have, and our mental capacity, to decide
which of our whims and wills we should act upon. Whims and wills occur to all of
us. Only when they rise to the level of consciousness we can decide on them.

Even a dog has a free will. A dog is free to decide when to scratch itself, or
when to bark. On what grounds does the dog decide? If you have a trained dog,
you will probably agree that it doesn't seem to act on a whim alone. The dog
does seem to weigh its whim against its training, and seems capable to suppress
the whim when the training induces it. Sometimes the impulse outweighs the
conditioning from training, and the dog barks.

My point, or rather my impression, is that it is not fundamentally different
with us humans, only much more complex and nuanced. Like the dog, we have our
impulses and whims, and we also have our training and our inhibitions, and what
we do usually results from those forces balancing against each other. In simple
terms, it is a kind of control process that processes inputs in a way that
assigns weights to the inputs in order to come to some output.

What sets us apart from the dog is that we can become conscious of this
balancing process, i.e. we can think about it. But we still balance inputs
against each other. When I say "we", I mean adults. We don't apply the same rule
to small children. This is a strong indicator that it has to do with training.

So when we talk about free will, about responsibility, about maturity even, we
talk about whether we have learned sufficiently from our training to get this
balancing right, in the sense that it is roughly compatible with what society
expects or finds acceptable. If you depart too far from this, don't be surprised
to have your maturity questioned. There is no free will without the context of a
society that sets the conditions for it.

When you, as an adult, exercise your free will in a way that seriously violates
a societal rule, society will complain that you acted against your
responsibility, and hence didn't use your freedom in the right way. Similarly,
Christians argue that using your free will in the wrong way earns you
potentially eternal punishment. In both cases, the argument requires that you
had the freedom in the first place, or else your violation of the rules wouldn't
be your fault. But at the same time the looming punishment at least partly
defeats the freedom that was the precondition. Particularly in the case of
eternal punishment -- that really makes a complete mockery of any freedom.

It follows that free will is probably the wrong way to think about this whole
topic. You better focus on training, conscience and responsibility.
