# Protestantism

I grew up as a Catholic, but before I turned atheist in my teens, I had a phase
where I looked at Protestantism as the better version of Christianity. This was
actually a mild and meek form of Lutheran Protestantism, not the evangelical or
fundamentalist variety. The Catholic creed included some tenets that I found
utterly silly (transsubstantiation, anyone?), and the protestants appeared
reasonable in comparison.

It didn't last long, though. I couldn't help noticing the large number of
different protestant sects, of which there was a fair number operating in my
neck of the woods. All of them seemed utterly convinced of the veracity of their
own version of the Christian religion. If I had understood one thing about
religion at this time, it was that there was no way of telling who was right.
All of the protestants pretended to follow scripture alone, and this appeared at
least somewhat reasonable compared to following a dogma decreed by a pope who
was acting on the presumption of inerrancy.

But even though they all had more or less the same scriptures, the protestants
still disagreed about rather fundamental issues. Even worse, they still believed
in obviously silly things, even after having abandoned the most silly Catholic
teachings. So the question was, on what grounds can you throw out one thing, but
keep another? It appeared rather arbitrary to me, and I wasn't at an age where I
knew much about sophisticated theological arguments. The more I encountered, the
weirder it seemed to me.

This soon became rather irrelevant when I turned to atheism, but for quite some
time I regarded Protestantism as the saner version of Christianity. The mild and
meek version of it, anyway. But over time, I sort of reversed. Now, I actually
prefer Catholicism again, for a somewhat ironic reason. In my experience,
Catholics were more likely to show a relaxed attitude regarding the dogma. I
guess, if your dogmatic tenets are so openly silly, you either learn to take it
with a shovel of salt, or you become a total weirdo. In other words, when both
sides are fundamentally wrong, the side that's more silly is also going to be
the one that is less dogmatic. Witness the many protestants who manage to
believe in earnest that their creed is actually objectively the better one.

Of course, if you occasionally encounter a Catholic who's totally committed to
the veracity of Catholicism, it appears still more absurd than an encounter with
a committed fundamentalist protestant, if that's even possible.

How refreshing then, when you meet Christians who have understood that they
can't have any certainty about which tenets are right or wrong. They aren't
going to aggressively proselytize you, and they are religious with a twinkle in
their eye.

They would probably best be called Cultural Christians. That's not the same as
Progressive Christians, but both are typically fairly undogmatic, which is a
welcome reminder that not all Christians are of the evangelical or
fundamentalist type. Of course, I'm unconvinced by either, but at least the
Cultural Christians aren't as insufferable as the evangelicals.
