# Obeying God

I'm not the kind of person who is easily convinced that he should obey. I want
to understand why. I don't want to be told, I want to be convinced. I'm not
particularly unruly, but somewhat stubborn. I think I have the right to an
explanation of why I should comply with some rule.

This doesn't mean that I reject all authority. I just expect authority to be
justified, to be earned. When I accept something on authority, I do so
provisionally, in recognition that I'm not able right now to check this myself.
I reserve the right to revisit the topic, if it appears important enough to me,
and work things out for myself, and thus perhaps arrive at a disagreement with
the authority.

This even applies to laws. I'm perfectly aware that I'm not in a position to
change the law to suit my fancy, and I wouldn't want to anyway. But I am in a
position to make up my mind about any particular law, and criticise it. I can
obey such a law because I have to, because of my general opinion that laws ought
to be obeyed for the benefit of a functioning society, even when I consider the
particular law to be wrong. In some cases, however, I consciously disobey a law
that I find wrong, as an act of resistance, being conscious that I might be
fined.

I thankfully live in a democratic society, where lawmakers are elected, so there
is some influence I can exert on this process, albeit a small one. I can speak
to the people who make the laws. I can try to convince them of my opinions and
objections. And they can try to convince me that their law is appropriate. Laws
can, and must, be debated.

Bismarck is quoted with stating that "laws are like sausages, it is better not
to see them being made." I disagree. It isn't good to ignore how sausages are
made. Such willful ignorance begets abominations. On the contrary, because it is
such a ghastly process, it needs to be performed in the open, where there is
scrutiny! Making laws is very much the same. If they are made in the closet by a
select few, or even a single ruler, and then presented to a public that hasn't
got any say, and is invariably compelled to obey them, then you will have
abominable laws. I don't see on what ethical basis you can be compelled to obey
such laws. For me, absolutist monarchy or dictatorship is therefore
fundamentally unacceptable.

You may of course object that God is different, since God is by definition
benevolent and omniscient, hence his laws will be impeccable, in ways that I
can't even hope to understand. This means that I have to obey because I haven't
a clue, and can't have a clue. I'm like a toddler, and God is like a parent. I
should follow his commands for the same reason that a toddler should obey a
parental command to not cross the street, for reasons that are incomprehensible
for the toddler, but obvious for the adult.

I take issue with this. We do grow up. We're not perpetual toddlers in need of
supervision. We have our faults, but to dismiss the possibility that we can grow
up, be responsible and collectively make our own rules, is an utterly defeatist
position. It reduces humans to the level of dogs, and thereby denies humanity
its most fundamental distinctive property. It also is at odds with the notion
that we are made "in the image of God" (But see my article on that). Either we
are capable of responsible consideration and collective rule making, and then we
are perfectly able and entitled to scrutinize and criticize every rule, holy or
not, that we are supposed to obey, or we are not, and then we are closer to dogs
than we are to divinity. Pick your preference, mine is clear.

When I scrutinize the biblical rules and laws with this mindset, I can't help
noting that they are spectacularly inappropriate for something that claims
divine wisdom. You do not need anything approaching divinity to be able to see
this. If this is all a benevolent and omniscient God could come up with, I
prefer to do without, thank you very much!
