# Paul

I find Paul's character and role in the bible totally strange. Haven't you
wondered about that, too? His writings are the earliest documents about Jesus,
decades before the gospels, yet he has very little to say about Jesus himself.
He never met him, except in a vision, and he obviously disagreed quite
fundamentally with the disciples in Jerusalem who knew Jesus personally. Why
does such a figure have such an amount of influence on Christian doctrine?
Wouldn't you expect that those people who actually knew Jesus should have the
highest authority?

Paul didn't seem to have much interest in Jesus the person. Which puzzles me,
since he should have had a lot of opportunities to speak to Jesus'
contemporaries, not merely the disciples, but also other witnesses who must
still have been around when he was on his mission. If I had been in his
situation, I would have used my time in Jerusalem to collect as much information
about Jesus as I can. Maybe he has, but it doesn't seem worth mentioning much.
How odd!

His writings are letters to Christian communities he founded in places quite far
from Jerusalem. I imagine that the Christians there would have been keen to
learn about Jesus, his life, his teachings and his environment! From where else
would they have gotten such information? My conclusion is that for Paul, the
person didn't matter. For him, Jesus was much more a concept. He didn't write
about people, but about ideas. His ideas!

Little wonder that he seems to have been at odds with the disciples in
Jerusalem. I figure that when you had known Jesus in person and listened to his
words, you would be alienated by someone who attempted to preach the gospel
while disinterested in Jesus as the person they used to know, and in what he had
to say. Tellingly, even the sermon on the mount, arguably Jesus' most famous
teaching, is absent from Paul's texts, just as if Paul had no idea it happened!

Maybe the dislike was mutual, since Paul doesn't give most of the disciples or
apostles or acquaintances of Jesus any significant mentioning. He ignores most
of them, and with them a lot of their narrative. For example, the empty tomb is
missing entirely, including all the personnel associated with it, like the women
and Joseph of Arimathea. Given how important it was for him that Jesus actually
rose from the dead, this is very strange! Why would he omit such evidence?
Somehow I feel that he wanted to cut down his reliance on testimony from Jesus'
direct witnesses as much as possible.

Was he jealous of them? They had something he didn't have: Direct experience of
Jesus. Note that I don't count the Damascus vision as a direct experience.
Anyway, the gist that transpires to me is that Paul didn't want to have much to
do with the Jesus gang in Jerusalem. He distanced himself physically, he
preached to different people (not Jews, but Gentiles), he developed his own
theology and he fostered and nursed his own parishes. And when he went back to
Jerusalem, his interaction with the parish there was clearly fraught with
tension.

The council of Jerusalem that is described in the book of Acts took place some
20 years after the crucifixion. I'm not sure, but it looks as if it was called
by the group in Jerusalem. They were unhappy about Paul teaching stuff that had
nothing to do with Jesus' teachings, such as dispensing Gentiles from
circumcision when they wanted to join the creed. It seems they summoned Paul to
give him a dressing down. Paul, of course, wouldn't tell the account in that
way. For him, it was enough when he could tell his followers far away from
Jerusalem, that the guys in Jerusalem were fine with him, and they had worked
everything out. Even when they hadn't, really. Who would know?

So Paul did his own thing, ably and stubbornly, by ignoring the group in
Jerusalem and their teachings to a large extent, which was easy since they
didn't appear to produce any noteworthy scriptures of their own, relying on
meeting in person instead. This appears to have given him a couple of decades of
head start, and when the Jerusalem group was dispersed as a result of the Jewish
war and the destruction of Jerusalem and the temple, his written legacy gave his
followers the lead. In essence he created his own creed using boulders from the
Christian quarry.

And today, we are faced with the outcome, which is an amalgamation of at least
two different creeds. And somehow most people believe that it really is one
coherent whole. I don't know how people can maintain that with a straight face.
