# Atheist Glossary

This project is an amalgam of a gloss and a glossary, in that each entry aims to
explain and discuss a certain word or topic associated with atheism in a concise
way. It is intentionally subjective, from an atheist perspective, and sometimes
a bit emotional.

Each entry should take around 3 to 5 minutes to read or recite, to avoid the
tl;dr effect. No effort is made to offer an exhaustive or complete treatment, or
provide evidence. It is all meant as food for thought and further discussion and
research, and I'm not claiming to be authoritative, truthful or even fair.

I'm sometimes addressing a fictitious evangelical Christian, mainly for the sake
of style. My imagined main audience, however, is the aspiring atheist, who finds
himself dealing with a predominantly Christian environment that's putting him
into the defensive position. I want to give examples how he can argue his
position, and indeed find his own position in the first place. In this sense I
am a counterapologetic, but not in the formal way. I merely present a personal
viewpoint that draws from my own experience of a lifetime. Needless to say,
people are free to disagree.

I am a German of around 60 years of age, who was raised in a Catholic home, and
grew out of religion in his teens. I can therefore look back on >40 years of
atheism, during which I have always remained interested in Christianity, mainly
in aspects of cultural history. For some time towards the end of the last
century, I was concerned very little with religion, but over the last 20 or 25
years interest grew back, because of an increasing fundamentalism and a rise in
religiously motivated violence. I now see fundamentalist religion as one of the
most important threats to societies all over the world, and a real danger for
enlightenment and progress. I therefore feel compelled to contribute what little
I can contribute to the defense of my own values against those backwards forces.

This concerns the USA in particular, where those backwards forces have taken the
country to the brink of a civil war. I am located across the pond in Europe, but
I am fully aware that we are going to be affected here no matter what, and I
feel for the many secularists in the USA who witness society around them
gradually going mad.

I believe that, given my background, I can contribute a more relaxed and less
partisan position to the discourse in the USA. I understand that in the current
heated climate there, people tend towards more categorical and extreme
positions, to more vitriolic language, to more display of anger and hate, and to
less interest in the position of the other side. But I doubt that we will have
progress when everyone is just sitting in the trenches on his side, lobbing the
grenades over to the other side.

Having said that, I'm not trying to compromise in search of an agreement. I just
want to show a settled atheist viewpoint that isn't hurting from the wounds of
a recent deconversion.
